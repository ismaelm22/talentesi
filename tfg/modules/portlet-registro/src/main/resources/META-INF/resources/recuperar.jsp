<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%><%@
taglib
	uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%><%@
taglib
	uri="http://liferay.com/tld/theme" prefix="liferay-theme"%><%@
taglib
	uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>



<portlet:renderURL var='login'>
	<portlet:param name ='jspPage' value="/login.jsp" />
</portlet:renderURL>

<div class="site-signup col-lg-6">
	<h1>Recuperar contraseña</h1>

	<p>Por favor, ingrese su mail para enviar una notificación para
		modificar su contraseña.</p>

	<div class="row">
		<div class="col-lg-12">
			<form id="request-password-reset-form"
				action="${login}" method="post" role="form">
				<div
					class="form-group field-passwordresetrequestform-email required">
					<label class="control-label" >Email</label>
					<input type="text" id="passwordresetrequestform-email"
						class="form-control" name="PasswordResetRequestForm[email]">

				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Enviar</button>
				</div>
			</form>
		</div>
	</div>
</div>
