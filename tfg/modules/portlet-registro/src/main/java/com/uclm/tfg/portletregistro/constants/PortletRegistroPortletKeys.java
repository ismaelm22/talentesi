package com.uclm.tfg.portletregistro.constants;

/**
 * @author ismaelmedinarodriguez
 */
public class PortletRegistroPortletKeys {

	public static final String PortletRegistro = "PortletRegistro";
	private static final String _LOGEADO = "LIFERAY_SHARED_LOT_LOGEADO";
}