package com.uclm.tfg.portletregistro.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.uclm.tfg.portletregistro.constants.PortletRegistroPortletKeys;

import portlet.commons.constants.SessionVariablesPortletKeys;
import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Profesor;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.display-name=portlet-registro Portlet", "javax.portlet.init-param.view-template=/login.jsp",
		"javax.portlet.name=" + PortletRegistroPortletKeys.PortletRegistro,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class PortletRegistroPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		renderRequest.setAttribute("themeDisplay", themeDisplay);
		SessionVariablesPortletKeys.limpiarSessionVariables(renderRequest);
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "login")
	public void login(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		String email = ParamUtil.getString(uploadPortletRequest, "email");
		String pwd = ParamUtil.getString(uploadPortletRequest, "password");

		List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);
		List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
		List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);

		boolean loginCorrecto = false;

		if (!alumno.isEmpty()) {
			if (alumno.get(0).getPwd().equals(pwd)) {
				loginCorrecto = true;
				SessionVariablesPortletKeys.setRol(actionRequest, "alumno");
				SessionVariablesPortletKeys.setEmail(actionRequest, email);
			}

		} else if (!profesor.isEmpty()) {
			if (profesor.get(0).getPwd().equals(pwd)) {
				loginCorrecto = true;
				SessionVariablesPortletKeys.setRol(actionRequest, "profesor");
				SessionVariablesPortletKeys.setEmail(actionRequest, email);
			}

		} else if (!empresa.isEmpty()) {
			if (empresa.get(0).getPwd().equals(pwd)) {
				loginCorrecto = true;
				SessionVariablesPortletKeys.setRol(actionRequest, "empresa");
				SessionVariablesPortletKeys.setEmail(actionRequest, email);
			}
		}

		if (loginCorrecto) {
			SessionVariablesPortletKeys.setLogeado(actionRequest, Boolean.TRUE);
			String urlhome = "http://localhost:8080/web/talent-esi/home";
			((ActionResponse) actionResponse).sendRedirect(urlhome);
		} else {
			actionRequest.setAttribute("error", true);
		}

	}

	@ProcessAction(name = "registrar")
	public void registrar(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		try {
			CaptchaUtil.check(actionRequest);
			System.out.println("CAPTCHA validated successfully");
		} catch (CaptchaException e) {

			SessionErrors.add(actionRequest, "errorMessage");
		}

		final ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String rol = ParamUtil.getString(uploadPortletRequest, "rol");

		SessionVariablesPortletKeys.setRol(actionRequest, rol);
		SessionVariablesPortletKeys.setLogeado(actionRequest, true);
		SessionVariablesPortletKeys.setEditarPerfil(actionRequest, true);

		String email = ParamUtil.getString(uploadPortletRequest, "email");
		String pwd = ParamUtil.getString(uploadPortletRequest, "pwd");
		SessionVariablesPortletKeys.setEmail(actionRequest, email);

		if (rol.equals("alumno")) {
			AlumnoLocalServiceUtil.addAlumno(email, pwd, "", "", "", null, 0, td.getCompanyId());
		} else if (rol.equals("profesor")) {
			ProfesorLocalServiceUtil.addProfesor(email, pwd, "", "", "", "", td.getCompanyId());
		} else if (rol.equals("empresa")) {
			EmpresaLocalServiceUtil.addEmpresa(email, pwd, "", "", "", "", td.getCompanyId());
		}

		String urlhome = "http://localhost:8080/web/talent-esi/profile";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		CaptchaUtil.serveImage(resourceRequest, resourceResponse);
		super.serveResource(resourceRequest, resourceResponse);
	}

}