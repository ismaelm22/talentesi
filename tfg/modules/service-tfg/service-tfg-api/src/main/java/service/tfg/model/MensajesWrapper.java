/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Mensajes}.
 * </p>
 *
 * @author Ismael Medina
 * @see Mensajes
 * @generated
 */
@ProviderType
public class MensajesWrapper implements Mensajes, ModelWrapper<Mensajes> {
	public MensajesWrapper(Mensajes mensajes) {
		_mensajes = mensajes;
	}

	@Override
	public Class<?> getModelClass() {
		return Mensajes.class;
	}

	@Override
	public String getModelClassName() {
		return Mensajes.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("mensajeId", getMensajeId());
		attributes.put("companyId", getCompanyId());
		attributes.put("remitente", getRemitente());
		attributes.put("destinatario", getDestinatario());
		attributes.put("mensaje", getMensaje());
		attributes.put("fecha", getFecha());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long mensajeId = (Long)attributes.get("mensajeId");

		if (mensajeId != null) {
			setMensajeId(mensajeId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String remitente = (String)attributes.get("remitente");

		if (remitente != null) {
			setRemitente(remitente);
		}

		String destinatario = (String)attributes.get("destinatario");

		if (destinatario != null) {
			setDestinatario(destinatario);
		}

		String mensaje = (String)attributes.get("mensaje");

		if (mensaje != null) {
			setMensaje(mensaje);
		}

		Date fecha = (Date)attributes.get("fecha");

		if (fecha != null) {
			setFecha(fecha);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _mensajes.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _mensajes.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _mensajes.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _mensajes.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<service.tfg.model.Mensajes> toCacheModel() {
		return _mensajes.toCacheModel();
	}

	@Override
	public int compareTo(service.tfg.model.Mensajes mensajes) {
		return _mensajes.compareTo(mensajes);
	}

	@Override
	public int hashCode() {
		return _mensajes.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _mensajes.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new MensajesWrapper((Mensajes)_mensajes.clone());
	}

	/**
	* Returns the destinatario of this mensajes.
	*
	* @return the destinatario of this mensajes
	*/
	@Override
	public java.lang.String getDestinatario() {
		return _mensajes.getDestinatario();
	}

	/**
	* Returns the mensaje of this mensajes.
	*
	* @return the mensaje of this mensajes
	*/
	@Override
	public java.lang.String getMensaje() {
		return _mensajes.getMensaje();
	}

	/**
	* Returns the remitente of this mensajes.
	*
	* @return the remitente of this mensajes
	*/
	@Override
	public java.lang.String getRemitente() {
		return _mensajes.getRemitente();
	}

	/**
	* Returns the uuid of this mensajes.
	*
	* @return the uuid of this mensajes
	*/
	@Override
	public java.lang.String getUuid() {
		return _mensajes.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _mensajes.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _mensajes.toXmlString();
	}

	/**
	* Returns the fecha of this mensajes.
	*
	* @return the fecha of this mensajes
	*/
	@Override
	public Date getFecha() {
		return _mensajes.getFecha();
	}

	/**
	* Returns the company ID of this mensajes.
	*
	* @return the company ID of this mensajes
	*/
	@Override
	public long getCompanyId() {
		return _mensajes.getCompanyId();
	}

	/**
	* Returns the mensaje ID of this mensajes.
	*
	* @return the mensaje ID of this mensajes
	*/
	@Override
	public long getMensajeId() {
		return _mensajes.getMensajeId();
	}

	/**
	* Returns the primary key of this mensajes.
	*
	* @return the primary key of this mensajes
	*/
	@Override
	public long getPrimaryKey() {
		return _mensajes.getPrimaryKey();
	}

	@Override
	public service.tfg.model.Mensajes toEscapedModel() {
		return new MensajesWrapper(_mensajes.toEscapedModel());
	}

	@Override
	public service.tfg.model.Mensajes toUnescapedModel() {
		return new MensajesWrapper(_mensajes.toUnescapedModel());
	}

	@Override
	public void persist() {
		_mensajes.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_mensajes.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this mensajes.
	*
	* @param companyId the company ID of this mensajes
	*/
	@Override
	public void setCompanyId(long companyId) {
		_mensajes.setCompanyId(companyId);
	}

	/**
	* Sets the destinatario of this mensajes.
	*
	* @param destinatario the destinatario of this mensajes
	*/
	@Override
	public void setDestinatario(java.lang.String destinatario) {
		_mensajes.setDestinatario(destinatario);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_mensajes.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_mensajes.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_mensajes.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the fecha of this mensajes.
	*
	* @param fecha the fecha of this mensajes
	*/
	@Override
	public void setFecha(Date fecha) {
		_mensajes.setFecha(fecha);
	}

	/**
	* Sets the mensaje of this mensajes.
	*
	* @param mensaje the mensaje of this mensajes
	*/
	@Override
	public void setMensaje(java.lang.String mensaje) {
		_mensajes.setMensaje(mensaje);
	}

	/**
	* Sets the mensaje ID of this mensajes.
	*
	* @param mensajeId the mensaje ID of this mensajes
	*/
	@Override
	public void setMensajeId(long mensajeId) {
		_mensajes.setMensajeId(mensajeId);
	}

	@Override
	public void setNew(boolean n) {
		_mensajes.setNew(n);
	}

	/**
	* Sets the primary key of this mensajes.
	*
	* @param primaryKey the primary key of this mensajes
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_mensajes.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_mensajes.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the remitente of this mensajes.
	*
	* @param remitente the remitente of this mensajes
	*/
	@Override
	public void setRemitente(java.lang.String remitente) {
		_mensajes.setRemitente(remitente);
	}

	/**
	* Sets the uuid of this mensajes.
	*
	* @param uuid the uuid of this mensajes
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_mensajes.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MensajesWrapper)) {
			return false;
		}

		MensajesWrapper mensajesWrapper = (MensajesWrapper)obj;

		if (Objects.equals(_mensajes, mensajesWrapper._mensajes)) {
			return true;
		}

		return false;
	}

	@Override
	public Mensajes getWrappedModel() {
		return _mensajes;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _mensajes.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _mensajes.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_mensajes.resetOriginalValues();
	}

	private final Mensajes _mensajes;
}