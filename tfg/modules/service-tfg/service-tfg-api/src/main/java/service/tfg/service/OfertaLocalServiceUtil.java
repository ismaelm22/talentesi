/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Oferta. This utility wraps
 * {@link service.tfg.service.impl.OfertaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Ismael Medina
 * @see OfertaLocalService
 * @see service.tfg.service.base.OfertaLocalServiceBaseImpl
 * @see service.tfg.service.impl.OfertaLocalServiceImpl
 * @generated
 */
@ProviderType
public class OfertaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link service.tfg.service.impl.OfertaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean hasAlumnoOferta(long alumnoId, long ofertaId) {
		return getService().hasAlumnoOferta(alumnoId, ofertaId);
	}

	public static boolean hasAlumnoOfertas(long alumnoId) {
		return getService().hasAlumnoOfertas(alumnoId);
	}

	public static boolean hasEmpresaOferta(long empresaId, long ofertaId) {
		return getService().hasEmpresaOferta(empresaId, ofertaId);
	}

	public static boolean hasEmpresaOfertas(long empresaId) {
		return getService().hasEmpresaOfertas(empresaId);
	}

	public static boolean hasProfesorOferta(long profesorId, long ofertaId) {
		return getService().hasProfesorOferta(profesorId, ofertaId);
	}

	public static boolean hasProfesorOfertas(long profesorId) {
		return getService().hasProfesorOfertas(profesorId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int getAlumnoOfertasCount(long alumnoId) {
		return getService().getAlumnoOfertasCount(alumnoId);
	}

	public static int getEmpresaOfertasCount(long empresaId) {
		return getService().getEmpresaOfertasCount(empresaId);
	}

	/**
	* Returns the number of ofertas.
	*
	* @return the number of ofertas
	*/
	public static int getOfertasCount() {
		return getService().getOfertasCount();
	}

	public static int getProfesorOfertasCount(long profesorId) {
		return getService().getProfesorOfertasCount(profesorId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Oferta> findByOfertaId(
		long ofertaId) {
		return getService().findByOfertaId(ofertaId);
	}

	public static java.util.List<service.tfg.model.Oferta> findByTitulo(
		java.lang.String titulo) {
		return getService().findByTitulo(titulo);
	}

	public static java.util.List<service.tfg.model.Oferta> findByareaType(
		java.lang.String area) {
		return getService().findByareaType(area);
	}

	public static java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId) {
		return getService().getAlumnoOfertas(alumnoId);
	}

	public static java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId, int start, int end) {
		return getService().getAlumnoOfertas(alumnoId, start, end);
	}

	public static java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return getService()
				   .getAlumnoOfertas(alumnoId, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId) {
		return getService().getEmpresaOfertas(empresaId);
	}

	public static java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId, int start, int end) {
		return getService().getEmpresaOfertas(empresaId, start, end);
	}

	public static java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return getService()
				   .getEmpresaOfertas(empresaId, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of ofertas
	*/
	public static java.util.List<service.tfg.model.Oferta> getOfertas(
		int start, int end) {
		return getService().getOfertas(start, end);
	}

	public static java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId) {
		return getService().getProfesorOfertas(profesorId);
	}

	public static java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId, int start, int end) {
		return getService().getProfesorOfertas(profesorId, start, end);
	}

	public static java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return getService()
				   .getProfesorOfertas(profesorId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the alumnoIds of the alumnos associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the alumnoIds of alumnos associated with the oferta
	*/
	public static long[] getAlumnoPrimaryKeys(long ofertaId) {
		return getService().getAlumnoPrimaryKeys(ofertaId);
	}

	/**
	* Returns the empresaIds of the empresas associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the empresaIds of empresas associated with the oferta
	*/
	public static long[] getEmpresaPrimaryKeys(long ofertaId) {
		return getService().getEmpresaPrimaryKeys(ofertaId);
	}

	/**
	* Returns the profesorIds of the profesors associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the profesorIds of profesors associated with the oferta
	*/
	public static long[] getProfesorPrimaryKeys(long ofertaId) {
		return getService().getProfesorPrimaryKeys(ofertaId);
	}

	/**
	* Adds the oferta to the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was added
	*/
	public static service.tfg.model.Oferta addOferta(
		service.tfg.model.Oferta oferta) {
		return getService().addOferta(oferta);
	}

	/**
	* Creates a new oferta with the primary key. Does not add the oferta to the database.
	*
	* @param ofertaId the primary key for the new oferta
	* @return the new oferta
	*/
	public static service.tfg.model.Oferta createOferta(long ofertaId) {
		return getService().createOferta(ofertaId);
	}

	/**
	* Deletes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta that was removed
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	public static service.tfg.model.Oferta deleteOferta(long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteOferta(ofertaId);
	}

	/**
	* Deletes the oferta from the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was removed
	*/
	public static service.tfg.model.Oferta deleteOferta(
		service.tfg.model.Oferta oferta) {
		return getService().deleteOferta(oferta);
	}

	public static service.tfg.model.Oferta fetchOferta(long ofertaId) {
		return getService().fetchOferta(ofertaId);
	}

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static service.tfg.model.Oferta fetchOfertaByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchOfertaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the oferta with the primary key.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	public static service.tfg.model.Oferta getOferta(long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getOferta(ofertaId);
	}

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta
	* @throws PortalException if a matching oferta could not be found
	*/
	public static service.tfg.model.Oferta getOfertaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getOfertaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the oferta in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was updated
	*/
	public static service.tfg.model.Oferta updateOferta(
		service.tfg.model.Oferta oferta) {
		return getService().updateOferta(oferta);
	}

	public static void addAlumnoOferta(long alumnoId, long ofertaId) {
		getService().addAlumnoOferta(alumnoId, ofertaId);
	}

	public static void addAlumnoOferta(long alumnoId,
		service.tfg.model.Oferta oferta) {
		getService().addAlumnoOferta(alumnoId, oferta);
	}

	public static void addAlumnoOfertas(long alumnoId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().addAlumnoOfertas(alumnoId, ofertas);
	}

	public static void addAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		getService().addAlumnoOfertas(alumnoId, ofertaIds);
	}

	public static void addEmpresaOferta(long empresaId, long ofertaId) {
		getService().addEmpresaOferta(empresaId, ofertaId);
	}

	public static void addEmpresaOferta(long empresaId,
		service.tfg.model.Oferta oferta) {
		getService().addEmpresaOferta(empresaId, oferta);
	}

	public static void addEmpresaOfertas(long empresaId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().addEmpresaOfertas(empresaId, ofertas);
	}

	public static void addEmpresaOfertas(long empresaId, long[] ofertaIds) {
		getService().addEmpresaOfertas(empresaId, ofertaIds);
	}

	public static void addOferta(long companyId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion,
		java.util.Collection<service.tfg.model.Profesor> profesores,
		java.util.Collection<service.tfg.model.Empresa> empresas) {
		getService()
			.addOferta(companyId, titulo, descripcion, area, competencias,
			duracion, profesores, empresas);
	}

	public static void addProfesorOferta(long profesorId, long ofertaId) {
		getService().addProfesorOferta(profesorId, ofertaId);
	}

	public static void addProfesorOferta(long profesorId,
		service.tfg.model.Oferta oferta) {
		getService().addProfesorOferta(profesorId, oferta);
	}

	public static void addProfesorOfertas(long profesorId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().addProfesorOfertas(profesorId, ofertas);
	}

	public static void addProfesorOfertas(long profesorId, long[] ofertaIds) {
		getService().addProfesorOfertas(profesorId, ofertaIds);
	}

	public static void clearAlumnoOfertas(long alumnoId) {
		getService().clearAlumnoOfertas(alumnoId);
	}

	public static void clearEmpresaOfertas(long empresaId) {
		getService().clearEmpresaOfertas(empresaId);
	}

	public static void clearProfesorOfertas(long profesorId) {
		getService().clearProfesorOfertas(profesorId);
	}

	public static void deleteAlumnoOferta(long alumnoId, long ofertaId) {
		getService().deleteAlumnoOferta(alumnoId, ofertaId);
	}

	public static void deleteAlumnoOferta(long alumnoId,
		service.tfg.model.Oferta oferta) {
		getService().deleteAlumnoOferta(alumnoId, oferta);
	}

	public static void deleteAlumnoOfertas(long alumnoId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().deleteAlumnoOfertas(alumnoId, ofertas);
	}

	public static void deleteAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		getService().deleteAlumnoOfertas(alumnoId, ofertaIds);
	}

	public static void deleteEmpresaOferta(long empresaId, long ofertaId) {
		getService().deleteEmpresaOferta(empresaId, ofertaId);
	}

	public static void deleteEmpresaOferta(long empresaId,
		service.tfg.model.Oferta oferta) {
		getService().deleteEmpresaOferta(empresaId, oferta);
	}

	public static void deleteEmpresaOfertas(long empresaId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().deleteEmpresaOfertas(empresaId, ofertas);
	}

	public static void deleteEmpresaOfertas(long empresaId, long[] ofertaIds) {
		getService().deleteEmpresaOfertas(empresaId, ofertaIds);
	}

	public static void deleteProfesorOferta(long profesorId, long ofertaId) {
		getService().deleteProfesorOferta(profesorId, ofertaId);
	}

	public static void deleteProfesorOferta(long profesorId,
		service.tfg.model.Oferta oferta) {
		getService().deleteProfesorOferta(profesorId, oferta);
	}

	public static void deleteProfesorOfertas(long profesorId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		getService().deleteProfesorOfertas(profesorId, ofertas);
	}

	public static void deleteProfesorOfertas(long profesorId, long[] ofertaIds) {
		getService().deleteProfesorOfertas(profesorId, ofertaIds);
	}

	public static void setAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		getService().setAlumnoOfertas(alumnoId, ofertaIds);
	}

	public static void setEmpresaOfertas(long empresaId, long[] ofertaIds) {
		getService().setEmpresaOfertas(empresaId, ofertaIds);
	}

	public static void setProfesorOfertas(long profesorId, long[] ofertaIds) {
		getService().setProfesorOfertas(profesorId, ofertaIds);
	}

	public static void updateAlumnosolcitudes(long ofertaId,
		java.util.Collection<service.tfg.model.Alumno> alumnos)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateAlumnosolcitudes(ofertaId, alumnos);
	}

	public static void updateEmresasOferta(long ofertaId,
		java.util.Collection<service.tfg.model.Empresa> empresas)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateEmresasOferta(ofertaId, empresas);
	}

	public static void updateOferta(long ofertaId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService()
			.updateOferta(ofertaId, titulo, descripcion, area, competencias,
			duracion);
	}

	public static void updateProfesoresOferta(long ofertaId,
		java.util.Collection<service.tfg.model.Profesor> profesor)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateProfesoresOferta(ofertaId, profesor);
	}

	public static OfertaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<OfertaLocalService, OfertaLocalService> _serviceTracker =
		ServiceTrackerFactory.open(OfertaLocalService.class);
}