/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import service.tfg.model.Oferta;

import java.util.List;

/**
 * The persistence utility for the oferta service. This utility wraps {@link service.tfg.service.persistence.impl.OfertaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see OfertaPersistence
 * @see service.tfg.service.persistence.impl.OfertaPersistenceImpl
 * @generated
 */
@ProviderType
public class OfertaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Oferta oferta) {
		getPersistence().clearCache(oferta);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Oferta> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Oferta> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Oferta> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Oferta update(Oferta oferta) {
		return getPersistence().update(oferta);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Oferta update(Oferta oferta, ServiceContext serviceContext) {
		return getPersistence().update(oferta, serviceContext);
	}

	/**
	* Returns all the ofertas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching ofertas
	*/
	public static List<Oferta> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public static List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByUuid_First(java.lang.String uuid,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta[] findByUuid_PrevAndNext(long ofertaId,
		java.lang.String uuid, OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByUuid_PrevAndNext(ofertaId, uuid, orderByComparator);
	}

	/**
	* Removes all the ofertas where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of ofertas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching ofertas
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching ofertas
	*/
	public static List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public static List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta[] findByUuid_C_PrevAndNext(long ofertaId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(ofertaId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the ofertas where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of ofertas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching ofertas
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the ofertas where area = &#63;.
	*
	* @param area the area
	* @return the matching ofertas
	*/
	public static List<Oferta> findByareaType(java.lang.String area) {
		return getPersistence().findByareaType(area);
	}

	/**
	* Returns a range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public static List<Oferta> findByareaType(java.lang.String area, int start,
		int end) {
		return getPersistence().findByareaType(area, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByareaType(java.lang.String area, int start,
		int end, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .findByareaType(area, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByareaType(java.lang.String area, int start,
		int end, OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByareaType(area, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByareaType_First(java.lang.String area,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByareaType_First(area, orderByComparator);
	}

	/**
	* Returns the first oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByareaType_First(java.lang.String area,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByareaType_First(area, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByareaType_Last(java.lang.String area,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByareaType_Last(area, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByareaType_Last(java.lang.String area,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByareaType_Last(area, orderByComparator);
	}

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where area = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta[] findByareaType_PrevAndNext(long ofertaId,
		java.lang.String area, OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByareaType_PrevAndNext(ofertaId, area, orderByComparator);
	}

	/**
	* Removes all the ofertas where area = &#63; from the database.
	*
	* @param area the area
	*/
	public static void removeByareaType(java.lang.String area) {
		getPersistence().removeByareaType(area);
	}

	/**
	* Returns the number of ofertas where area = &#63;.
	*
	* @param area the area
	* @return the number of matching ofertas
	*/
	public static int countByareaType(java.lang.String area) {
		return getPersistence().countByareaType(area);
	}

	/**
	* Returns all the ofertas where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the matching ofertas
	*/
	public static List<Oferta> findByOfertaId(long ofertaId) {
		return getPersistence().findByOfertaId(ofertaId);
	}

	/**
	* Returns a range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public static List<Oferta> findByOfertaId(long ofertaId, int start, int end) {
		return getPersistence().findByOfertaId(ofertaId, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByOfertaId(long ofertaId, int start,
		int end, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .findByOfertaId(ofertaId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByOfertaId(long ofertaId, int start,
		int end, OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByOfertaId(ofertaId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByOfertaId_First(long ofertaId,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByOfertaId_First(ofertaId, orderByComparator);
	}

	/**
	* Returns the first oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByOfertaId_First(long ofertaId,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .fetchByOfertaId_First(ofertaId, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByOfertaId_Last(long ofertaId,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByOfertaId_Last(ofertaId, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByOfertaId_Last(long ofertaId,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByOfertaId_Last(ofertaId, orderByComparator);
	}

	/**
	* Removes all the ofertas where ofertaId = &#63; from the database.
	*
	* @param ofertaId the oferta ID
	*/
	public static void removeByOfertaId(long ofertaId) {
		getPersistence().removeByOfertaId(ofertaId);
	}

	/**
	* Returns the number of ofertas where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the number of matching ofertas
	*/
	public static int countByOfertaId(long ofertaId) {
		return getPersistence().countByOfertaId(ofertaId);
	}

	/**
	* Returns all the ofertas where titulo = &#63;.
	*
	* @param titulo the titulo
	* @return the matching ofertas
	*/
	public static List<Oferta> findByTitulo(java.lang.String titulo) {
		return getPersistence().findByTitulo(titulo);
	}

	/**
	* Returns a range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public static List<Oferta> findByTitulo(java.lang.String titulo, int start,
		int end) {
		return getPersistence().findByTitulo(titulo, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByTitulo(java.lang.String titulo, int start,
		int end, OrderByComparator<Oferta> orderByComparator) {
		return getPersistence()
				   .findByTitulo(titulo, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public static List<Oferta> findByTitulo(java.lang.String titulo, int start,
		int end, OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByTitulo(titulo, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByTitulo_First(java.lang.String titulo,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByTitulo_First(titulo, orderByComparator);
	}

	/**
	* Returns the first oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByTitulo_First(java.lang.String titulo,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByTitulo_First(titulo, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public static Oferta findByTitulo_Last(java.lang.String titulo,
		OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByTitulo_Last(titulo, orderByComparator);
	}

	/**
	* Returns the last oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public static Oferta fetchByTitulo_Last(java.lang.String titulo,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().fetchByTitulo_Last(titulo, orderByComparator);
	}

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where titulo = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta[] findByTitulo_PrevAndNext(long ofertaId,
		java.lang.String titulo, OrderByComparator<Oferta> orderByComparator)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence()
				   .findByTitulo_PrevAndNext(ofertaId, titulo, orderByComparator);
	}

	/**
	* Removes all the ofertas where titulo = &#63; from the database.
	*
	* @param titulo the titulo
	*/
	public static void removeByTitulo(java.lang.String titulo) {
		getPersistence().removeByTitulo(titulo);
	}

	/**
	* Returns the number of ofertas where titulo = &#63;.
	*
	* @param titulo the titulo
	* @return the number of matching ofertas
	*/
	public static int countByTitulo(java.lang.String titulo) {
		return getPersistence().countByTitulo(titulo);
	}

	/**
	* Caches the oferta in the entity cache if it is enabled.
	*
	* @param oferta the oferta
	*/
	public static void cacheResult(Oferta oferta) {
		getPersistence().cacheResult(oferta);
	}

	/**
	* Caches the ofertas in the entity cache if it is enabled.
	*
	* @param ofertas the ofertas
	*/
	public static void cacheResult(List<Oferta> ofertas) {
		getPersistence().cacheResult(ofertas);
	}

	/**
	* Creates a new oferta with the primary key. Does not add the oferta to the database.
	*
	* @param ofertaId the primary key for the new oferta
	* @return the new oferta
	*/
	public static Oferta create(long ofertaId) {
		return getPersistence().create(ofertaId);
	}

	/**
	* Removes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta that was removed
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta remove(long ofertaId)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().remove(ofertaId);
	}

	public static Oferta updateImpl(Oferta oferta) {
		return getPersistence().updateImpl(oferta);
	}

	/**
	* Returns the oferta with the primary key or throws a {@link NoSuchOfertaException} if it could not be found.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public static Oferta findByPrimaryKey(long ofertaId)
		throws service.tfg.exception.NoSuchOfertaException {
		return getPersistence().findByPrimaryKey(ofertaId);
	}

	/**
	* Returns the oferta with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta, or <code>null</code> if a oferta with the primary key could not be found
	*/
	public static Oferta fetchByPrimaryKey(long ofertaId) {
		return getPersistence().fetchByPrimaryKey(ofertaId);
	}

	public static java.util.Map<java.io.Serializable, Oferta> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the ofertas.
	*
	* @return the ofertas
	*/
	public static List<Oferta> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of ofertas
	*/
	public static List<Oferta> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas
	*/
	public static List<Oferta> findAll(int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ofertas
	*/
	public static List<Oferta> findAll(int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the ofertas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ofertas.
	*
	* @return the number of ofertas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	* Returns the primaryKeys of alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of alumnos associated with the oferta
	*/
	public static long[] getAlumnoPrimaryKeys(long pk) {
		return getPersistence().getAlumnoPrimaryKeys(pk);
	}

	/**
	* Returns all the alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the alumnos associated with the oferta
	*/
	public static List<service.tfg.model.Alumno> getAlumnos(long pk) {
		return getPersistence().getAlumnos(pk);
	}

	/**
	* Returns a range of all the alumnos associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of alumnos associated with the oferta
	*/
	public static List<service.tfg.model.Alumno> getAlumnos(long pk, int start,
		int end) {
		return getPersistence().getAlumnos(pk, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of alumnos associated with the oferta
	*/
	public static List<service.tfg.model.Alumno> getAlumnos(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Alumno> orderByComparator) {
		return getPersistence().getAlumnos(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of alumnos associated with the oferta
	*/
	public static int getAlumnosSize(long pk) {
		return getPersistence().getAlumnosSize(pk);
	}

	/**
	* Returns <code>true</code> if the alumno is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	* @return <code>true</code> if the alumno is associated with the oferta; <code>false</code> otherwise
	*/
	public static boolean containsAlumno(long pk, long alumnoPK) {
		return getPersistence().containsAlumno(pk, alumnoPK);
	}

	/**
	* Returns <code>true</code> if the oferta has any alumnos associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with alumnos
	* @return <code>true</code> if the oferta has any alumnos associated with it; <code>false</code> otherwise
	*/
	public static boolean containsAlumnos(long pk) {
		return getPersistence().containsAlumnos(pk);
	}

	/**
	* Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	*/
	public static void addAlumno(long pk, long alumnoPK) {
		getPersistence().addAlumno(pk, alumnoPK);
	}

	/**
	* Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumno the alumno
	*/
	public static void addAlumno(long pk, service.tfg.model.Alumno alumno) {
		getPersistence().addAlumno(pk, alumno);
	}

	/**
	* Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos
	*/
	public static void addAlumnos(long pk, long[] alumnoPKs) {
		getPersistence().addAlumnos(pk, alumnoPKs);
	}

	/**
	* Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos
	*/
	public static void addAlumnos(long pk,
		List<service.tfg.model.Alumno> alumnos) {
		getPersistence().addAlumnos(pk, alumnos);
	}

	/**
	* Clears all associations between the oferta and its alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated alumnos from
	*/
	public static void clearAlumnos(long pk) {
		getPersistence().clearAlumnos(pk);
	}

	/**
	* Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	*/
	public static void removeAlumno(long pk, long alumnoPK) {
		getPersistence().removeAlumno(pk, alumnoPK);
	}

	/**
	* Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumno the alumno
	*/
	public static void removeAlumno(long pk, service.tfg.model.Alumno alumno) {
		getPersistence().removeAlumno(pk, alumno);
	}

	/**
	* Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos
	*/
	public static void removeAlumnos(long pk, long[] alumnoPKs) {
		getPersistence().removeAlumnos(pk, alumnoPKs);
	}

	/**
	* Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos
	*/
	public static void removeAlumnos(long pk,
		List<service.tfg.model.Alumno> alumnos) {
		getPersistence().removeAlumnos(pk, alumnos);
	}

	/**
	* Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos to be associated with the oferta
	*/
	public static void setAlumnos(long pk, long[] alumnoPKs) {
		getPersistence().setAlumnos(pk, alumnoPKs);
	}

	/**
	* Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos to be associated with the oferta
	*/
	public static void setAlumnos(long pk,
		List<service.tfg.model.Alumno> alumnos) {
		getPersistence().setAlumnos(pk, alumnos);
	}

	/**
	* Returns the primaryKeys of empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of empresas associated with the oferta
	*/
	public static long[] getEmpresaPrimaryKeys(long pk) {
		return getPersistence().getEmpresaPrimaryKeys(pk);
	}

	/**
	* Returns all the empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the empresas associated with the oferta
	*/
	public static List<service.tfg.model.Empresa> getEmpresas(long pk) {
		return getPersistence().getEmpresas(pk);
	}

	/**
	* Returns a range of all the empresas associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of empresas associated with the oferta
	*/
	public static List<service.tfg.model.Empresa> getEmpresas(long pk,
		int start, int end) {
		return getPersistence().getEmpresas(pk, start, end);
	}

	/**
	* Returns an ordered range of all the empresas associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of empresas associated with the oferta
	*/
	public static List<service.tfg.model.Empresa> getEmpresas(long pk,
		int start, int end,
		OrderByComparator<service.tfg.model.Empresa> orderByComparator) {
		return getPersistence().getEmpresas(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of empresas associated with the oferta
	*/
	public static int getEmpresasSize(long pk) {
		return getPersistence().getEmpresasSize(pk);
	}

	/**
	* Returns <code>true</code> if the empresa is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	* @return <code>true</code> if the empresa is associated with the oferta; <code>false</code> otherwise
	*/
	public static boolean containsEmpresa(long pk, long empresaPK) {
		return getPersistence().containsEmpresa(pk, empresaPK);
	}

	/**
	* Returns <code>true</code> if the oferta has any empresas associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with empresas
	* @return <code>true</code> if the oferta has any empresas associated with it; <code>false</code> otherwise
	*/
	public static boolean containsEmpresas(long pk) {
		return getPersistence().containsEmpresas(pk);
	}

	/**
	* Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	*/
	public static void addEmpresa(long pk, long empresaPK) {
		getPersistence().addEmpresa(pk, empresaPK);
	}

	/**
	* Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresa the empresa
	*/
	public static void addEmpresa(long pk, service.tfg.model.Empresa empresa) {
		getPersistence().addEmpresa(pk, empresa);
	}

	/**
	* Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas
	*/
	public static void addEmpresas(long pk, long[] empresaPKs) {
		getPersistence().addEmpresas(pk, empresaPKs);
	}

	/**
	* Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas
	*/
	public static void addEmpresas(long pk,
		List<service.tfg.model.Empresa> empresas) {
		getPersistence().addEmpresas(pk, empresas);
	}

	/**
	* Clears all associations between the oferta and its empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated empresas from
	*/
	public static void clearEmpresas(long pk) {
		getPersistence().clearEmpresas(pk);
	}

	/**
	* Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	*/
	public static void removeEmpresa(long pk, long empresaPK) {
		getPersistence().removeEmpresa(pk, empresaPK);
	}

	/**
	* Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresa the empresa
	*/
	public static void removeEmpresa(long pk, service.tfg.model.Empresa empresa) {
		getPersistence().removeEmpresa(pk, empresa);
	}

	/**
	* Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas
	*/
	public static void removeEmpresas(long pk, long[] empresaPKs) {
		getPersistence().removeEmpresas(pk, empresaPKs);
	}

	/**
	* Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas
	*/
	public static void removeEmpresas(long pk,
		List<service.tfg.model.Empresa> empresas) {
		getPersistence().removeEmpresas(pk, empresas);
	}

	/**
	* Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas to be associated with the oferta
	*/
	public static void setEmpresas(long pk, long[] empresaPKs) {
		getPersistence().setEmpresas(pk, empresaPKs);
	}

	/**
	* Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas to be associated with the oferta
	*/
	public static void setEmpresas(long pk,
		List<service.tfg.model.Empresa> empresas) {
		getPersistence().setEmpresas(pk, empresas);
	}

	/**
	* Returns the primaryKeys of profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of profesors associated with the oferta
	*/
	public static long[] getProfesorPrimaryKeys(long pk) {
		return getPersistence().getProfesorPrimaryKeys(pk);
	}

	/**
	* Returns all the profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the profesors associated with the oferta
	*/
	public static List<service.tfg.model.Profesor> getProfesors(long pk) {
		return getPersistence().getProfesors(pk);
	}

	/**
	* Returns a range of all the profesors associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of profesors associated with the oferta
	*/
	public static List<service.tfg.model.Profesor> getProfesors(long pk,
		int start, int end) {
		return getPersistence().getProfesors(pk, start, end);
	}

	/**
	* Returns an ordered range of all the profesors associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profesors associated with the oferta
	*/
	public static List<service.tfg.model.Profesor> getProfesors(long pk,
		int start, int end,
		OrderByComparator<service.tfg.model.Profesor> orderByComparator) {
		return getPersistence().getProfesors(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of profesors associated with the oferta
	*/
	public static int getProfesorsSize(long pk) {
		return getPersistence().getProfesorsSize(pk);
	}

	/**
	* Returns <code>true</code> if the profesor is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	* @return <code>true</code> if the profesor is associated with the oferta; <code>false</code> otherwise
	*/
	public static boolean containsProfesor(long pk, long profesorPK) {
		return getPersistence().containsProfesor(pk, profesorPK);
	}

	/**
	* Returns <code>true</code> if the oferta has any profesors associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with profesors
	* @return <code>true</code> if the oferta has any profesors associated with it; <code>false</code> otherwise
	*/
	public static boolean containsProfesors(long pk) {
		return getPersistence().containsProfesors(pk);
	}

	/**
	* Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	*/
	public static void addProfesor(long pk, long profesorPK) {
		getPersistence().addProfesor(pk, profesorPK);
	}

	/**
	* Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesor the profesor
	*/
	public static void addProfesor(long pk, service.tfg.model.Profesor profesor) {
		getPersistence().addProfesor(pk, profesor);
	}

	/**
	* Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors
	*/
	public static void addProfesors(long pk, long[] profesorPKs) {
		getPersistence().addProfesors(pk, profesorPKs);
	}

	/**
	* Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors
	*/
	public static void addProfesors(long pk,
		List<service.tfg.model.Profesor> profesors) {
		getPersistence().addProfesors(pk, profesors);
	}

	/**
	* Clears all associations between the oferta and its profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated profesors from
	*/
	public static void clearProfesors(long pk) {
		getPersistence().clearProfesors(pk);
	}

	/**
	* Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	*/
	public static void removeProfesor(long pk, long profesorPK) {
		getPersistence().removeProfesor(pk, profesorPK);
	}

	/**
	* Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesor the profesor
	*/
	public static void removeProfesor(long pk,
		service.tfg.model.Profesor profesor) {
		getPersistence().removeProfesor(pk, profesor);
	}

	/**
	* Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors
	*/
	public static void removeProfesors(long pk, long[] profesorPKs) {
		getPersistence().removeProfesors(pk, profesorPKs);
	}

	/**
	* Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors
	*/
	public static void removeProfesors(long pk,
		List<service.tfg.model.Profesor> profesors) {
		getPersistence().removeProfesors(pk, profesors);
	}

	/**
	* Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors to be associated with the oferta
	*/
	public static void setProfesors(long pk, long[] profesorPKs) {
		getPersistence().setProfesors(pk, profesorPKs);
	}

	/**
	* Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors to be associated with the oferta
	*/
	public static void setProfesors(long pk,
		List<service.tfg.model.Profesor> profesors) {
		getPersistence().setProfesors(pk, profesors);
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static OfertaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<OfertaPersistence, OfertaPersistence> _serviceTracker =
		ServiceTrackerFactory.open(OfertaPersistence.class);
}