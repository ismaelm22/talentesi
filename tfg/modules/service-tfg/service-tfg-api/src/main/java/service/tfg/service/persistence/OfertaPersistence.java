/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import service.tfg.exception.NoSuchOfertaException;

import service.tfg.model.Oferta;

/**
 * The persistence interface for the oferta service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see service.tfg.service.persistence.impl.OfertaPersistenceImpl
 * @see OfertaUtil
 * @generated
 */
@ProviderType
public interface OfertaPersistence extends BasePersistence<Oferta> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OfertaUtil} to access the oferta persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ofertas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching ofertas
	*/
	public java.util.List<Oferta> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the first oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the last oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the last oferta in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta[] findByUuid_PrevAndNext(long ofertaId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Removes all the ofertas where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of ofertas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching ofertas
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching ofertas
	*/
	public java.util.List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta[] findByUuid_C_PrevAndNext(long ofertaId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Removes all the ofertas where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of ofertas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching ofertas
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the ofertas where area = &#63;.
	*
	* @param area the area
	* @return the matching ofertas
	*/
	public java.util.List<Oferta> findByareaType(java.lang.String area);

	/**
	* Returns a range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public java.util.List<Oferta> findByareaType(java.lang.String area,
		int start, int end);

	/**
	* Returns an ordered range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByareaType(java.lang.String area,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas where area = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param area the area
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByareaType(java.lang.String area,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByareaType_First(java.lang.String area,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the first oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByareaType_First(java.lang.String area,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the last oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByareaType_Last(java.lang.String area,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the last oferta in the ordered set where area = &#63;.
	*
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByareaType_Last(java.lang.String area,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where area = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param area the area
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta[] findByareaType_PrevAndNext(long ofertaId,
		java.lang.String area,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Removes all the ofertas where area = &#63; from the database.
	*
	* @param area the area
	*/
	public void removeByareaType(java.lang.String area);

	/**
	* Returns the number of ofertas where area = &#63;.
	*
	* @param area the area
	* @return the number of matching ofertas
	*/
	public int countByareaType(java.lang.String area);

	/**
	* Returns all the ofertas where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the matching ofertas
	*/
	public java.util.List<Oferta> findByOfertaId(long ofertaId);

	/**
	* Returns a range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public java.util.List<Oferta> findByOfertaId(long ofertaId, int start,
		int end);

	/**
	* Returns an ordered range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByOfertaId(long ofertaId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByOfertaId(long ofertaId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByOfertaId_First(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the first oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByOfertaId_First(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the last oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByOfertaId_Last(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the last oferta in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByOfertaId_Last(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Removes all the ofertas where ofertaId = &#63; from the database.
	*
	* @param ofertaId the oferta ID
	*/
	public void removeByOfertaId(long ofertaId);

	/**
	* Returns the number of ofertas where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the number of matching ofertas
	*/
	public int countByOfertaId(long ofertaId);

	/**
	* Returns all the ofertas where titulo = &#63;.
	*
	* @param titulo the titulo
	* @return the matching ofertas
	*/
	public java.util.List<Oferta> findByTitulo(java.lang.String titulo);

	/**
	* Returns a range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of matching ofertas
	*/
	public java.util.List<Oferta> findByTitulo(java.lang.String titulo,
		int start, int end);

	/**
	* Returns an ordered range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByTitulo(java.lang.String titulo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas where titulo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param titulo the titulo
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ofertas
	*/
	public java.util.List<Oferta> findByTitulo(java.lang.String titulo,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByTitulo_First(java.lang.String titulo,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the first oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByTitulo_First(java.lang.String titulo,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the last oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta
	* @throws NoSuchOfertaException if a matching oferta could not be found
	*/
	public Oferta findByTitulo_Last(java.lang.String titulo,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Returns the last oferta in the ordered set where titulo = &#63;.
	*
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	public Oferta fetchByTitulo_Last(java.lang.String titulo,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the ofertas before and after the current oferta in the ordered set where titulo = &#63;.
	*
	* @param ofertaId the primary key of the current oferta
	* @param titulo the titulo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta[] findByTitulo_PrevAndNext(long ofertaId,
		java.lang.String titulo,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException;

	/**
	* Removes all the ofertas where titulo = &#63; from the database.
	*
	* @param titulo the titulo
	*/
	public void removeByTitulo(java.lang.String titulo);

	/**
	* Returns the number of ofertas where titulo = &#63;.
	*
	* @param titulo the titulo
	* @return the number of matching ofertas
	*/
	public int countByTitulo(java.lang.String titulo);

	/**
	* Caches the oferta in the entity cache if it is enabled.
	*
	* @param oferta the oferta
	*/
	public void cacheResult(Oferta oferta);

	/**
	* Caches the ofertas in the entity cache if it is enabled.
	*
	* @param ofertas the ofertas
	*/
	public void cacheResult(java.util.List<Oferta> ofertas);

	/**
	* Creates a new oferta with the primary key. Does not add the oferta to the database.
	*
	* @param ofertaId the primary key for the new oferta
	* @return the new oferta
	*/
	public Oferta create(long ofertaId);

	/**
	* Removes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta that was removed
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta remove(long ofertaId) throws NoSuchOfertaException;

	public Oferta updateImpl(Oferta oferta);

	/**
	* Returns the oferta with the primary key or throws a {@link NoSuchOfertaException} if it could not be found.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta
	* @throws NoSuchOfertaException if a oferta with the primary key could not be found
	*/
	public Oferta findByPrimaryKey(long ofertaId) throws NoSuchOfertaException;

	/**
	* Returns the oferta with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta, or <code>null</code> if a oferta with the primary key could not be found
	*/
	public Oferta fetchByPrimaryKey(long ofertaId);

	@Override
	public java.util.Map<java.io.Serializable, Oferta> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the ofertas.
	*
	* @return the ofertas
	*/
	public java.util.List<Oferta> findAll();

	/**
	* Returns a range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of ofertas
	*/
	public java.util.List<Oferta> findAll(int start, int end);

	/**
	* Returns an ordered range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas
	*/
	public java.util.List<Oferta> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns an ordered range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ofertas
	*/
	public java.util.List<Oferta> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the ofertas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of ofertas.
	*
	* @return the number of ofertas
	*/
	public int countAll();

	/**
	* Returns the primaryKeys of alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of alumnos associated with the oferta
	*/
	public long[] getAlumnoPrimaryKeys(long pk);

	/**
	* Returns all the alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the alumnos associated with the oferta
	*/
	public java.util.List<service.tfg.model.Alumno> getAlumnos(long pk);

	/**
	* Returns a range of all the alumnos associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of alumnos associated with the oferta
	*/
	public java.util.List<service.tfg.model.Alumno> getAlumnos(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the alumnos associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of alumnos associated with the oferta
	*/
	public java.util.List<service.tfg.model.Alumno> getAlumnos(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Alumno> orderByComparator);

	/**
	* Returns the number of alumnos associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of alumnos associated with the oferta
	*/
	public int getAlumnosSize(long pk);

	/**
	* Returns <code>true</code> if the alumno is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	* @return <code>true</code> if the alumno is associated with the oferta; <code>false</code> otherwise
	*/
	public boolean containsAlumno(long pk, long alumnoPK);

	/**
	* Returns <code>true</code> if the oferta has any alumnos associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with alumnos
	* @return <code>true</code> if the oferta has any alumnos associated with it; <code>false</code> otherwise
	*/
	public boolean containsAlumnos(long pk);

	/**
	* Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	*/
	public void addAlumno(long pk, long alumnoPK);

	/**
	* Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumno the alumno
	*/
	public void addAlumno(long pk, service.tfg.model.Alumno alumno);

	/**
	* Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos
	*/
	public void addAlumnos(long pk, long[] alumnoPKs);

	/**
	* Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos
	*/
	public void addAlumnos(long pk,
		java.util.List<service.tfg.model.Alumno> alumnos);

	/**
	* Clears all associations between the oferta and its alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated alumnos from
	*/
	public void clearAlumnos(long pk);

	/**
	* Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPK the primary key of the alumno
	*/
	public void removeAlumno(long pk, long alumnoPK);

	/**
	* Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumno the alumno
	*/
	public void removeAlumno(long pk, service.tfg.model.Alumno alumno);

	/**
	* Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos
	*/
	public void removeAlumnos(long pk, long[] alumnoPKs);

	/**
	* Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos
	*/
	public void removeAlumnos(long pk,
		java.util.List<service.tfg.model.Alumno> alumnos);

	/**
	* Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnoPKs the primary keys of the alumnos to be associated with the oferta
	*/
	public void setAlumnos(long pk, long[] alumnoPKs);

	/**
	* Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param alumnos the alumnos to be associated with the oferta
	*/
	public void setAlumnos(long pk,
		java.util.List<service.tfg.model.Alumno> alumnos);

	/**
	* Returns the primaryKeys of empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of empresas associated with the oferta
	*/
	public long[] getEmpresaPrimaryKeys(long pk);

	/**
	* Returns all the empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the empresas associated with the oferta
	*/
	public java.util.List<service.tfg.model.Empresa> getEmpresas(long pk);

	/**
	* Returns a range of all the empresas associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of empresas associated with the oferta
	*/
	public java.util.List<service.tfg.model.Empresa> getEmpresas(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the empresas associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of empresas associated with the oferta
	*/
	public java.util.List<service.tfg.model.Empresa> getEmpresas(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Empresa> orderByComparator);

	/**
	* Returns the number of empresas associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of empresas associated with the oferta
	*/
	public int getEmpresasSize(long pk);

	/**
	* Returns <code>true</code> if the empresa is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	* @return <code>true</code> if the empresa is associated with the oferta; <code>false</code> otherwise
	*/
	public boolean containsEmpresa(long pk, long empresaPK);

	/**
	* Returns <code>true</code> if the oferta has any empresas associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with empresas
	* @return <code>true</code> if the oferta has any empresas associated with it; <code>false</code> otherwise
	*/
	public boolean containsEmpresas(long pk);

	/**
	* Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	*/
	public void addEmpresa(long pk, long empresaPK);

	/**
	* Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresa the empresa
	*/
	public void addEmpresa(long pk, service.tfg.model.Empresa empresa);

	/**
	* Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas
	*/
	public void addEmpresas(long pk, long[] empresaPKs);

	/**
	* Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas
	*/
	public void addEmpresas(long pk,
		java.util.List<service.tfg.model.Empresa> empresas);

	/**
	* Clears all associations between the oferta and its empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated empresas from
	*/
	public void clearEmpresas(long pk);

	/**
	* Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPK the primary key of the empresa
	*/
	public void removeEmpresa(long pk, long empresaPK);

	/**
	* Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresa the empresa
	*/
	public void removeEmpresa(long pk, service.tfg.model.Empresa empresa);

	/**
	* Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas
	*/
	public void removeEmpresas(long pk, long[] empresaPKs);

	/**
	* Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas
	*/
	public void removeEmpresas(long pk,
		java.util.List<service.tfg.model.Empresa> empresas);

	/**
	* Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresaPKs the primary keys of the empresas to be associated with the oferta
	*/
	public void setEmpresas(long pk, long[] empresaPKs);

	/**
	* Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param empresas the empresas to be associated with the oferta
	*/
	public void setEmpresas(long pk,
		java.util.List<service.tfg.model.Empresa> empresas);

	/**
	* Returns the primaryKeys of profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return long[] of the primaryKeys of profesors associated with the oferta
	*/
	public long[] getProfesorPrimaryKeys(long pk);

	/**
	* Returns all the profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the profesors associated with the oferta
	*/
	public java.util.List<service.tfg.model.Profesor> getProfesors(long pk);

	/**
	* Returns a range of all the profesors associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of profesors associated with the oferta
	*/
	public java.util.List<service.tfg.model.Profesor> getProfesors(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the profesors associated with the oferta.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the oferta
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profesors associated with the oferta
	*/
	public java.util.List<service.tfg.model.Profesor> getProfesors(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Profesor> orderByComparator);

	/**
	* Returns the number of profesors associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @return the number of profesors associated with the oferta
	*/
	public int getProfesorsSize(long pk);

	/**
	* Returns <code>true</code> if the profesor is associated with the oferta.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	* @return <code>true</code> if the profesor is associated with the oferta; <code>false</code> otherwise
	*/
	public boolean containsProfesor(long pk, long profesorPK);

	/**
	* Returns <code>true</code> if the oferta has any profesors associated with it.
	*
	* @param pk the primary key of the oferta to check for associations with profesors
	* @return <code>true</code> if the oferta has any profesors associated with it; <code>false</code> otherwise
	*/
	public boolean containsProfesors(long pk);

	/**
	* Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	*/
	public void addProfesor(long pk, long profesorPK);

	/**
	* Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesor the profesor
	*/
	public void addProfesor(long pk, service.tfg.model.Profesor profesor);

	/**
	* Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors
	*/
	public void addProfesors(long pk, long[] profesorPKs);

	/**
	* Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors
	*/
	public void addProfesors(long pk,
		java.util.List<service.tfg.model.Profesor> profesors);

	/**
	* Clears all associations between the oferta and its profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta to clear the associated profesors from
	*/
	public void clearProfesors(long pk);

	/**
	* Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPK the primary key of the profesor
	*/
	public void removeProfesor(long pk, long profesorPK);

	/**
	* Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesor the profesor
	*/
	public void removeProfesor(long pk, service.tfg.model.Profesor profesor);

	/**
	* Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors
	*/
	public void removeProfesors(long pk, long[] profesorPKs);

	/**
	* Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors
	*/
	public void removeProfesors(long pk,
		java.util.List<service.tfg.model.Profesor> profesors);

	/**
	* Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesorPKs the primary keys of the profesors to be associated with the oferta
	*/
	public void setProfesors(long pk, long[] profesorPKs);

	/**
	* Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the oferta
	* @param profesors the profesors to be associated with the oferta
	*/
	public void setProfesors(long pk,
		java.util.List<service.tfg.model.Profesor> profesors);

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}