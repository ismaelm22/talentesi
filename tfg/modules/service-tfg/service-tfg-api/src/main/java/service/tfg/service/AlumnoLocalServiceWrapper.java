/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AlumnoLocalService}.
 *
 * @author Ismael Medina
 * @see AlumnoLocalService
 * @generated
 */
@ProviderType
public class AlumnoLocalServiceWrapper implements AlumnoLocalService,
	ServiceWrapper<AlumnoLocalService> {
	public AlumnoLocalServiceWrapper(AlumnoLocalService alumnoLocalService) {
		_alumnoLocalService = alumnoLocalService;
	}

	@Override
	public boolean hasOfertaAlumno(long ofertaId, long alumnoId) {
		return _alumnoLocalService.hasOfertaAlumno(ofertaId, alumnoId);
	}

	@Override
	public boolean hasOfertaAlumnos(long ofertaId) {
		return _alumnoLocalService.hasOfertaAlumnos(ofertaId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _alumnoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _alumnoLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _alumnoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _alumnoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _alumnoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of alumnos.
	*
	* @return the number of alumnos
	*/
	@Override
	public int getAlumnosCount() {
		return _alumnoLocalService.getAlumnosCount();
	}

	@Override
	public int getOfertaAlumnosCount(long ofertaId) {
		return _alumnoLocalService.getOfertaAlumnosCount(ofertaId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _alumnoLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _alumnoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _alumnoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _alumnoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<service.tfg.model.Alumno> findByEmailId(
		java.lang.String email) {
		return _alumnoLocalService.findByEmailId(email);
	}

	/**
	* Returns a range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of alumnos
	*/
	@Override
	public java.util.List<service.tfg.model.Alumno> getAlumnos(int start,
		int end) {
		return _alumnoLocalService.getAlumnos(start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId) {
		return _alumnoLocalService.getOfertaAlumnos(ofertaId);
	}

	@Override
	public java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId, int start, int end) {
		return _alumnoLocalService.getOfertaAlumnos(ofertaId, start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Alumno> orderByComparator) {
		return _alumnoLocalService.getOfertaAlumnos(ofertaId, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _alumnoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _alumnoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the ofertaIds of the ofertas associated with the alumno.
	*
	* @param alumnoId the alumnoId of the alumno
	* @return long[] the ofertaIds of ofertas associated with the alumno
	*/
	@Override
	public long[] getOfertaPrimaryKeys(long alumnoId) {
		return _alumnoLocalService.getOfertaPrimaryKeys(alumnoId);
	}

	/**
	* Adds the alumno to the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was added
	*/
	@Override
	public service.tfg.model.Alumno addAlumno(service.tfg.model.Alumno alumno) {
		return _alumnoLocalService.addAlumno(alumno);
	}

	/**
	* Creates a new alumno with the primary key. Does not add the alumno to the database.
	*
	* @param alumnoId the primary key for the new alumno
	* @return the new alumno
	*/
	@Override
	public service.tfg.model.Alumno createAlumno(long alumnoId) {
		return _alumnoLocalService.createAlumno(alumnoId);
	}

	/**
	* Deletes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno that was removed
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Alumno deleteAlumno(long alumnoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _alumnoLocalService.deleteAlumno(alumnoId);
	}

	/**
	* Deletes the alumno from the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was removed
	*/
	@Override
	public service.tfg.model.Alumno deleteAlumno(
		service.tfg.model.Alumno alumno) {
		return _alumnoLocalService.deleteAlumno(alumno);
	}

	@Override
	public service.tfg.model.Alumno fetchAlumno(long alumnoId) {
		return _alumnoLocalService.fetchAlumno(alumnoId);
	}

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	@Override
	public service.tfg.model.Alumno fetchAlumnoByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _alumnoLocalService.fetchAlumnoByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the alumno with the primary key.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Alumno getAlumno(long alumnoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _alumnoLocalService.getAlumno(alumnoId);
	}

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno
	* @throws PortalException if a matching alumno could not be found
	*/
	@Override
	public service.tfg.model.Alumno getAlumnoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _alumnoLocalService.getAlumnoByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the alumno in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was updated
	*/
	@Override
	public service.tfg.model.Alumno updateAlumno(
		service.tfg.model.Alumno alumno) {
		return _alumnoLocalService.updateAlumno(alumno);
	}

	@Override
	public service.tfg.model.AlumnoCurriculumBlobModel getCurriculumBlobModel(
		java.io.Serializable primaryKey) {
		return _alumnoLocalService.getCurriculumBlobModel(primaryKey);
	}

	@Override
	public void addAlumno(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String apellidos,
		java.lang.String competencias, java.sql.Blob curriculum, long ofertaId,
		long companyId) {
		_alumnoLocalService.addAlumno(email, pwd, nombre, apellidos,
			competencias, curriculum, ofertaId, companyId);
	}

	@Override
	public void addOfertaAlumno(long ofertaId, long alumnoId) {
		_alumnoLocalService.addOfertaAlumno(ofertaId, alumnoId);
	}

	@Override
	public void addOfertaAlumno(long ofertaId, service.tfg.model.Alumno alumno) {
		_alumnoLocalService.addOfertaAlumno(ofertaId, alumno);
	}

	@Override
	public void addOfertaAlumnos(long ofertaId,
		java.util.List<service.tfg.model.Alumno> alumnos) {
		_alumnoLocalService.addOfertaAlumnos(ofertaId, alumnos);
	}

	@Override
	public void addOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		_alumnoLocalService.addOfertaAlumnos(ofertaId, alumnoIds);
	}

	@Override
	public void clearOfertaAlumnos(long ofertaId) {
		_alumnoLocalService.clearOfertaAlumnos(ofertaId);
	}

	@Override
	public void deleteOfertaAlumno(long ofertaId, long alumnoId) {
		_alumnoLocalService.deleteOfertaAlumno(ofertaId, alumnoId);
	}

	@Override
	public void deleteOfertaAlumno(long ofertaId,
		service.tfg.model.Alumno alumno) {
		_alumnoLocalService.deleteOfertaAlumno(ofertaId, alumno);
	}

	@Override
	public void deleteOfertaAlumnos(long ofertaId,
		java.util.List<service.tfg.model.Alumno> alumnos) {
		_alumnoLocalService.deleteOfertaAlumnos(ofertaId, alumnos);
	}

	@Override
	public void deleteOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		_alumnoLocalService.deleteOfertaAlumnos(ofertaId, alumnoIds);
	}

	@Override
	public void setOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		_alumnoLocalService.setOfertaAlumnos(ofertaId, alumnoIds);
	}

	@Override
	public void updateAlumno(long alumnoId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String competencias,
		java.sql.Blob curriculum)
		throws com.liferay.portal.kernel.exception.PortalException {
		_alumnoLocalService.updateAlumno(alumnoId, nombre, apellidos,
			competencias, curriculum);
	}

	@Override
	public void updateAlumnoOferta(long alumnoId, long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		_alumnoLocalService.updateAlumnoOferta(alumnoId, ofertaId);
	}

	@Override
	public AlumnoLocalService getWrappedService() {
		return _alumnoLocalService;
	}

	@Override
	public void setWrappedService(AlumnoLocalService alumnoLocalService) {
		_alumnoLocalService = alumnoLocalService;
	}

	private AlumnoLocalService _alumnoLocalService;
}