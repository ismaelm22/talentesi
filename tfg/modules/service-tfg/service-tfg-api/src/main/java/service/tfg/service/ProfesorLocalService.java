/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import service.tfg.model.Profesor;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Profesor. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Ismael Medina
 * @see ProfesorLocalServiceUtil
 * @see service.tfg.service.base.ProfesorLocalServiceBaseImpl
 * @see service.tfg.service.impl.ProfesorLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ProfesorLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProfesorLocalServiceUtil} to access the profesor local service. Add custom service methods to {@link service.tfg.service.impl.ProfesorLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaProfesor(long ofertaId, long profesorId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaProfesors(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getOfertaProfesorsCount(long ofertaId);

	/**
	* Returns the number of profesors.
	*
	* @return the number of profesors
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProfesorsCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	public List<Profesor> findByEmailId(java.lang.String email);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Profesor> getOfertaProfesors(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Profesor> getOfertaProfesors(long ofertaId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Profesor> getOfertaProfesors(long ofertaId, int start, int end,
		OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns a range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of profesors
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Profesor> getProfesors(int start, int end);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Returns the ofertaIds of the ofertas associated with the profesor.
	*
	* @param profesorId the profesorId of the profesor
	* @return long[] the ofertaIds of ofertas associated with the profesor
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getOfertaPrimaryKeys(long profesorId);

	/**
	* Adds the profesor to the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Profesor addProfesor(Profesor profesor);

	/**
	* Creates a new profesor with the primary key. Does not add the profesor to the database.
	*
	* @param profesorId the primary key for the new profesor
	* @return the new profesor
	*/
	public Profesor createProfesor(long profesorId);

	/**
	* Deletes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor that was removed
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Profesor deleteProfesor(long profesorId) throws PortalException;

	/**
	* Deletes the profesor from the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Profesor deleteProfesor(Profesor profesor);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Profesor fetchProfesor(long profesorId);

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Profesor fetchProfesorByUuidAndCompanyId(java.lang.String uuid,
		long companyId);

	/**
	* Returns the profesor with the primary key.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Profesor getProfesor(long profesorId) throws PortalException;

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor
	* @throws PortalException if a matching profesor could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Profesor getProfesorByUuidAndCompanyId(java.lang.String uuid,
		long companyId) throws PortalException;

	/**
	* Updates the profesor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Profesor updateProfesor(Profesor profesor);

	public void addOfertaProfesor(long ofertaId, long profesorId);

	public void addOfertaProfesor(long ofertaId, Profesor profesor);

	public void addOfertaProfesors(long ofertaId, List<Profesor> profesors);

	public void addOfertaProfesors(long ofertaId, long[] profesorIds);

	public void addProfesor(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String apellidos,
		java.lang.String area, java.lang.String despacho, long companyId);

	public void clearOfertaProfesors(long ofertaId);

	public void deleteOfertaProfesor(long ofertaId, long profesorId);

	public void deleteOfertaProfesor(long ofertaId, Profesor profesor);

	public void deleteOfertaProfesors(long ofertaId, List<Profesor> profesors);

	public void deleteOfertaProfesors(long ofertaId, long[] profesorIds);

	public void setOfertaProfesors(long ofertaId, long[] profesorIds);

	public void updateProfesor(long profesorId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String area,
		java.lang.String despacho) throws PortalException;
}