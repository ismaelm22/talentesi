/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.sql.Blob;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Alumno}.
 * </p>
 *
 * @author Ismael Medina
 * @see Alumno
 * @generated
 */
@ProviderType
public class AlumnoWrapper implements Alumno, ModelWrapper<Alumno> {
	public AlumnoWrapper(Alumno alumno) {
		_alumno = alumno;
	}

	@Override
	public Class<?> getModelClass() {
		return Alumno.class;
	}

	@Override
	public String getModelClassName() {
		return Alumno.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("alumnoId", getAlumnoId());
		attributes.put("companyId", getCompanyId());
		attributes.put("email", getEmail());
		attributes.put("pwd", getPwd());
		attributes.put("nombre", getNombre());
		attributes.put("apellidos", getApellidos());
		attributes.put("competencias", getCompetencias());
		attributes.put("ofertaId", getOfertaId());
		attributes.put("curriculum", getCurriculum());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long alumnoId = (Long)attributes.get("alumnoId");

		if (alumnoId != null) {
			setAlumnoId(alumnoId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String pwd = (String)attributes.get("pwd");

		if (pwd != null) {
			setPwd(pwd);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidos = (String)attributes.get("apellidos");

		if (apellidos != null) {
			setApellidos(apellidos);
		}

		String competencias = (String)attributes.get("competencias");

		if (competencias != null) {
			setCompetencias(competencias);
		}

		Long ofertaId = (Long)attributes.get("ofertaId");

		if (ofertaId != null) {
			setOfertaId(ofertaId);
		}

		Blob curriculum = (Blob)attributes.get("curriculum");

		if (curriculum != null) {
			setCurriculum(curriculum);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _alumno.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _alumno.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _alumno.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _alumno.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<service.tfg.model.Alumno> toCacheModel() {
		return _alumno.toCacheModel();
	}

	@Override
	public int compareTo(service.tfg.model.Alumno alumno) {
		return _alumno.compareTo(alumno);
	}

	@Override
	public int hashCode() {
		return _alumno.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _alumno.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new AlumnoWrapper((Alumno)_alumno.clone());
	}

	/**
	* Returns the apellidos of this alumno.
	*
	* @return the apellidos of this alumno
	*/
	@Override
	public java.lang.String getApellidos() {
		return _alumno.getApellidos();
	}

	/**
	* Returns the competencias of this alumno.
	*
	* @return the competencias of this alumno
	*/
	@Override
	public java.lang.String getCompetencias() {
		return _alumno.getCompetencias();
	}

	/**
	* Returns the email of this alumno.
	*
	* @return the email of this alumno
	*/
	@Override
	public java.lang.String getEmail() {
		return _alumno.getEmail();
	}

	/**
	* Returns the nombre of this alumno.
	*
	* @return the nombre of this alumno
	*/
	@Override
	public java.lang.String getNombre() {
		return _alumno.getNombre();
	}

	/**
	* Returns the pwd of this alumno.
	*
	* @return the pwd of this alumno
	*/
	@Override
	public java.lang.String getPwd() {
		return _alumno.getPwd();
	}

	/**
	* Returns the uuid of this alumno.
	*
	* @return the uuid of this alumno
	*/
	@Override
	public java.lang.String getUuid() {
		return _alumno.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _alumno.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _alumno.toXmlString();
	}

	/**
	* Returns the curriculum of this alumno.
	*
	* @return the curriculum of this alumno
	*/
	@Override
	public Blob getCurriculum() {
		return _alumno.getCurriculum();
	}

	/**
	* Returns the alumno ID of this alumno.
	*
	* @return the alumno ID of this alumno
	*/
	@Override
	public long getAlumnoId() {
		return _alumno.getAlumnoId();
	}

	/**
	* Returns the company ID of this alumno.
	*
	* @return the company ID of this alumno
	*/
	@Override
	public long getCompanyId() {
		return _alumno.getCompanyId();
	}

	/**
	* Returns the oferta ID of this alumno.
	*
	* @return the oferta ID of this alumno
	*/
	@Override
	public long getOfertaId() {
		return _alumno.getOfertaId();
	}

	/**
	* Returns the primary key of this alumno.
	*
	* @return the primary key of this alumno
	*/
	@Override
	public long getPrimaryKey() {
		return _alumno.getPrimaryKey();
	}

	@Override
	public service.tfg.model.Alumno toEscapedModel() {
		return new AlumnoWrapper(_alumno.toEscapedModel());
	}

	@Override
	public service.tfg.model.Alumno toUnescapedModel() {
		return new AlumnoWrapper(_alumno.toUnescapedModel());
	}

	@Override
	public void persist() {
		_alumno.persist();
	}

	/**
	* Sets the alumno ID of this alumno.
	*
	* @param alumnoId the alumno ID of this alumno
	*/
	@Override
	public void setAlumnoId(long alumnoId) {
		_alumno.setAlumnoId(alumnoId);
	}

	/**
	* Sets the apellidos of this alumno.
	*
	* @param apellidos the apellidos of this alumno
	*/
	@Override
	public void setApellidos(java.lang.String apellidos) {
		_alumno.setApellidos(apellidos);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_alumno.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this alumno.
	*
	* @param companyId the company ID of this alumno
	*/
	@Override
	public void setCompanyId(long companyId) {
		_alumno.setCompanyId(companyId);
	}

	/**
	* Sets the competencias of this alumno.
	*
	* @param competencias the competencias of this alumno
	*/
	@Override
	public void setCompetencias(java.lang.String competencias) {
		_alumno.setCompetencias(competencias);
	}

	/**
	* Sets the curriculum of this alumno.
	*
	* @param curriculum the curriculum of this alumno
	*/
	@Override
	public void setCurriculum(Blob curriculum) {
		_alumno.setCurriculum(curriculum);
	}

	/**
	* Sets the email of this alumno.
	*
	* @param email the email of this alumno
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_alumno.setEmail(email);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_alumno.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_alumno.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_alumno.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_alumno.setNew(n);
	}

	/**
	* Sets the nombre of this alumno.
	*
	* @param nombre the nombre of this alumno
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_alumno.setNombre(nombre);
	}

	/**
	* Sets the oferta ID of this alumno.
	*
	* @param ofertaId the oferta ID of this alumno
	*/
	@Override
	public void setOfertaId(long ofertaId) {
		_alumno.setOfertaId(ofertaId);
	}

	/**
	* Sets the primary key of this alumno.
	*
	* @param primaryKey the primary key of this alumno
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_alumno.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_alumno.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the pwd of this alumno.
	*
	* @param pwd the pwd of this alumno
	*/
	@Override
	public void setPwd(java.lang.String pwd) {
		_alumno.setPwd(pwd);
	}

	/**
	* Sets the uuid of this alumno.
	*
	* @param uuid the uuid of this alumno
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_alumno.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AlumnoWrapper)) {
			return false;
		}

		AlumnoWrapper alumnoWrapper = (AlumnoWrapper)obj;

		if (Objects.equals(_alumno, alumnoWrapper._alumno)) {
			return true;
		}

		return false;
	}

	@Override
	public Alumno getWrappedModel() {
		return _alumno;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _alumno.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _alumno.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_alumno.resetOriginalValues();
	}

	private final Alumno _alumno;
}