/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Empresa}.
 * </p>
 *
 * @author Ismael Medina
 * @see Empresa
 * @generated
 */
@ProviderType
public class EmpresaWrapper implements Empresa, ModelWrapper<Empresa> {
	public EmpresaWrapper(Empresa empresa) {
		_empresa = empresa;
	}

	@Override
	public Class<?> getModelClass() {
		return Empresa.class;
	}

	@Override
	public String getModelClassName() {
		return Empresa.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("empresaId", getEmpresaId());
		attributes.put("companyId", getCompanyId());
		attributes.put("email", getEmail());
		attributes.put("pwd", getPwd());
		attributes.put("nombre", getNombre());
		attributes.put("cif", getCif());
		attributes.put("direccion", getDireccion());
		attributes.put("telefono", getTelefono());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long empresaId = (Long)attributes.get("empresaId");

		if (empresaId != null) {
			setEmpresaId(empresaId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String pwd = (String)attributes.get("pwd");

		if (pwd != null) {
			setPwd(pwd);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String cif = (String)attributes.get("cif");

		if (cif != null) {
			setCif(cif);
		}

		String direccion = (String)attributes.get("direccion");

		if (direccion != null) {
			setDireccion(direccion);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _empresa.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _empresa.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _empresa.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _empresa.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<service.tfg.model.Empresa> toCacheModel() {
		return _empresa.toCacheModel();
	}

	@Override
	public int compareTo(service.tfg.model.Empresa empresa) {
		return _empresa.compareTo(empresa);
	}

	@Override
	public int hashCode() {
		return _empresa.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _empresa.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new EmpresaWrapper((Empresa)_empresa.clone());
	}

	/**
	* Returns the cif of this empresa.
	*
	* @return the cif of this empresa
	*/
	@Override
	public java.lang.String getCif() {
		return _empresa.getCif();
	}

	/**
	* Returns the direccion of this empresa.
	*
	* @return the direccion of this empresa
	*/
	@Override
	public java.lang.String getDireccion() {
		return _empresa.getDireccion();
	}

	/**
	* Returns the email of this empresa.
	*
	* @return the email of this empresa
	*/
	@Override
	public java.lang.String getEmail() {
		return _empresa.getEmail();
	}

	/**
	* Returns the nombre of this empresa.
	*
	* @return the nombre of this empresa
	*/
	@Override
	public java.lang.String getNombre() {
		return _empresa.getNombre();
	}

	/**
	* Returns the pwd of this empresa.
	*
	* @return the pwd of this empresa
	*/
	@Override
	public java.lang.String getPwd() {
		return _empresa.getPwd();
	}

	/**
	* Returns the telefono of this empresa.
	*
	* @return the telefono of this empresa
	*/
	@Override
	public java.lang.String getTelefono() {
		return _empresa.getTelefono();
	}

	/**
	* Returns the uuid of this empresa.
	*
	* @return the uuid of this empresa
	*/
	@Override
	public java.lang.String getUuid() {
		return _empresa.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _empresa.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _empresa.toXmlString();
	}

	/**
	* Returns the company ID of this empresa.
	*
	* @return the company ID of this empresa
	*/
	@Override
	public long getCompanyId() {
		return _empresa.getCompanyId();
	}

	/**
	* Returns the empresa ID of this empresa.
	*
	* @return the empresa ID of this empresa
	*/
	@Override
	public long getEmpresaId() {
		return _empresa.getEmpresaId();
	}

	/**
	* Returns the primary key of this empresa.
	*
	* @return the primary key of this empresa
	*/
	@Override
	public long getPrimaryKey() {
		return _empresa.getPrimaryKey();
	}

	@Override
	public service.tfg.model.Empresa toEscapedModel() {
		return new EmpresaWrapper(_empresa.toEscapedModel());
	}

	@Override
	public service.tfg.model.Empresa toUnescapedModel() {
		return new EmpresaWrapper(_empresa.toUnescapedModel());
	}

	@Override
	public void persist() {
		_empresa.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_empresa.setCachedModel(cachedModel);
	}

	/**
	* Sets the cif of this empresa.
	*
	* @param cif the cif of this empresa
	*/
	@Override
	public void setCif(java.lang.String cif) {
		_empresa.setCif(cif);
	}

	/**
	* Sets the company ID of this empresa.
	*
	* @param companyId the company ID of this empresa
	*/
	@Override
	public void setCompanyId(long companyId) {
		_empresa.setCompanyId(companyId);
	}

	/**
	* Sets the direccion of this empresa.
	*
	* @param direccion the direccion of this empresa
	*/
	@Override
	public void setDireccion(java.lang.String direccion) {
		_empresa.setDireccion(direccion);
	}

	/**
	* Sets the email of this empresa.
	*
	* @param email the email of this empresa
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_empresa.setEmail(email);
	}

	/**
	* Sets the empresa ID of this empresa.
	*
	* @param empresaId the empresa ID of this empresa
	*/
	@Override
	public void setEmpresaId(long empresaId) {
		_empresa.setEmpresaId(empresaId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_empresa.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_empresa.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_empresa.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_empresa.setNew(n);
	}

	/**
	* Sets the nombre of this empresa.
	*
	* @param nombre the nombre of this empresa
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_empresa.setNombre(nombre);
	}

	/**
	* Sets the primary key of this empresa.
	*
	* @param primaryKey the primary key of this empresa
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_empresa.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_empresa.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the pwd of this empresa.
	*
	* @param pwd the pwd of this empresa
	*/
	@Override
	public void setPwd(java.lang.String pwd) {
		_empresa.setPwd(pwd);
	}

	/**
	* Sets the telefono of this empresa.
	*
	* @param telefono the telefono of this empresa
	*/
	@Override
	public void setTelefono(java.lang.String telefono) {
		_empresa.setTelefono(telefono);
	}

	/**
	* Sets the uuid of this empresa.
	*
	* @param uuid the uuid of this empresa
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_empresa.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmpresaWrapper)) {
			return false;
		}

		EmpresaWrapper empresaWrapper = (EmpresaWrapper)obj;

		if (Objects.equals(_empresa, empresaWrapper._empresa)) {
			return true;
		}

		return false;
	}

	@Override
	public Empresa getWrappedModel() {
		return _empresa;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _empresa.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _empresa.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_empresa.resetOriginalValues();
	}

	private final Empresa _empresa;
}