/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Oferta service. Represents a row in the &quot;BBDD_Oferta&quot; database table, with each column mapped to a property of this class.
 *
 * @author Ismael Medina
 * @see OfertaModel
 * @see service.tfg.model.impl.OfertaImpl
 * @see service.tfg.model.impl.OfertaModelImpl
 * @generated
 */
@ImplementationClassName("service.tfg.model.impl.OfertaImpl")
@ProviderType
public interface Oferta extends OfertaModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link service.tfg.model.impl.OfertaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Oferta, Long> OFERTA_ID_ACCESSOR = new Accessor<Oferta, Long>() {
			@Override
			public Long get(Oferta oferta) {
				return oferta.getOfertaId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Oferta> getTypeClass() {
				return Oferta.class;
			}
		};
}