/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import service.tfg.exception.NoSuchEmpresaException;

import service.tfg.model.Empresa;

/**
 * The persistence interface for the empresa service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see service.tfg.service.persistence.impl.EmpresaPersistenceImpl
 * @see EmpresaUtil
 * @generated
 */
@ProviderType
public interface EmpresaPersistence extends BasePersistence<Empresa> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmpresaUtil} to access the empresa persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the empresas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching empresas
	*/
	public java.util.List<Empresa> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns an ordered range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the first empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the last empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the last empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the empresas before and after the current empresa in the ordered set where uuid = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa[] findByUuid_PrevAndNext(long empresaId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Removes all the empresas where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of empresas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching empresas
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching empresas
	*/
	public java.util.List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns an ordered range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the first empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the last empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the last empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the empresas before and after the current empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa[] findByUuid_C_PrevAndNext(long empresaId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Removes all the empresas where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of empresas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching empresas
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the empresas where email = &#63;.
	*
	* @param email the email
	* @return the matching empresas
	*/
	public java.util.List<Empresa> findByEmailId(java.lang.String email);

	/**
	* Returns a range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public java.util.List<Empresa> findByEmailId(java.lang.String email,
		int start, int end);

	/**
	* Returns an ordered range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns an ordered range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the first empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the last empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the last empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the empresas before and after the current empresa in the ordered set where email = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa[] findByEmailId_PrevAndNext(long empresaId,
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Removes all the empresas where email = &#63; from the database.
	*
	* @param email the email
	*/
	public void removeByEmailId(java.lang.String email);

	/**
	* Returns the number of empresas where email = &#63;.
	*
	* @param email the email
	* @return the number of matching empresas
	*/
	public int countByEmailId(java.lang.String email);

	/**
	* Returns all the empresas where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching empresas
	*/
	public java.util.List<Empresa> findBynombreEmpresa(java.lang.String nombre);

	/**
	* Returns a range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public java.util.List<Empresa> findBynombreEmpresa(
		java.lang.String nombre, int start, int end);

	/**
	* Returns an ordered range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findBynombreEmpresa(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns an ordered range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public java.util.List<Empresa> findBynombreEmpresa(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findBynombreEmpresa_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the first empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchBynombreEmpresa_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the last empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public Empresa findBynombreEmpresa_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Returns the last empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public Empresa fetchBynombreEmpresa_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the empresas before and after the current empresa in the ordered set where nombre = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa[] findBynombreEmpresa_PrevAndNext(long empresaId,
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator)
		throws NoSuchEmpresaException;

	/**
	* Removes all the empresas where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombreEmpresa(java.lang.String nombre);

	/**
	* Returns the number of empresas where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching empresas
	*/
	public int countBynombreEmpresa(java.lang.String nombre);

	/**
	* Caches the empresa in the entity cache if it is enabled.
	*
	* @param empresa the empresa
	*/
	public void cacheResult(Empresa empresa);

	/**
	* Caches the empresas in the entity cache if it is enabled.
	*
	* @param empresas the empresas
	*/
	public void cacheResult(java.util.List<Empresa> empresas);

	/**
	* Creates a new empresa with the primary key. Does not add the empresa to the database.
	*
	* @param empresaId the primary key for the new empresa
	* @return the new empresa
	*/
	public Empresa create(long empresaId);

	/**
	* Removes the empresa with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa that was removed
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa remove(long empresaId) throws NoSuchEmpresaException;

	public Empresa updateImpl(Empresa empresa);

	/**
	* Returns the empresa with the primary key or throws a {@link NoSuchEmpresaException} if it could not be found.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public Empresa findByPrimaryKey(long empresaId)
		throws NoSuchEmpresaException;

	/**
	* Returns the empresa with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa, or <code>null</code> if a empresa with the primary key could not be found
	*/
	public Empresa fetchByPrimaryKey(long empresaId);

	@Override
	public java.util.Map<java.io.Serializable, Empresa> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the empresas.
	*
	* @return the empresas
	*/
	public java.util.List<Empresa> findAll();

	/**
	* Returns a range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of empresas
	*/
	public java.util.List<Empresa> findAll(int start, int end);

	/**
	* Returns an ordered range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of empresas
	*/
	public java.util.List<Empresa> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns an ordered range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of empresas
	*/
	public java.util.List<Empresa> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the empresas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of empresas.
	*
	* @return the number of empresas
	*/
	public int countAll();

	/**
	* Returns the primaryKeys of ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return long[] of the primaryKeys of ofertas associated with the empresa
	*/
	public long[] getOfertaPrimaryKeys(long pk);

	/**
	* Returns all the ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return the ofertas associated with the empresa
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk);

	/**
	* Returns a range of all the ofertas associated with the empresa.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the empresa
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of ofertas associated with the empresa
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the ofertas associated with the empresa.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the empresa
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas associated with the empresa
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator);

	/**
	* Returns the number of ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return the number of ofertas associated with the empresa
	*/
	public int getOfertasSize(long pk);

	/**
	* Returns <code>true</code> if the oferta is associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	* @return <code>true</code> if the oferta is associated with the empresa; <code>false</code> otherwise
	*/
	public boolean containsOferta(long pk, long ofertaPK);

	/**
	* Returns <code>true</code> if the empresa has any ofertas associated with it.
	*
	* @param pk the primary key of the empresa to check for associations with ofertas
	* @return <code>true</code> if the empresa has any ofertas associated with it; <code>false</code> otherwise
	*/
	public boolean containsOfertas(long pk);

	/**
	* Adds an association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	*/
	public void addOferta(long pk, long ofertaPK);

	/**
	* Adds an association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param oferta the oferta
	*/
	public void addOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Adds an association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void addOfertas(long pk, long[] ofertaPKs);

	/**
	* Adds an association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas
	*/
	public void addOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Clears all associations between the empresa and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa to clear the associated ofertas from
	*/
	public void clearOfertas(long pk);

	/**
	* Removes the association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	*/
	public void removeOferta(long pk, long ofertaPK);

	/**
	* Removes the association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param oferta the oferta
	*/
	public void removeOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Removes the association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void removeOfertas(long pk, long[] ofertaPKs);

	/**
	* Removes the association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas
	*/
	public void removeOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Sets the ofertas associated with the empresa, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas to be associated with the empresa
	*/
	public void setOfertas(long pk, long[] ofertaPKs);

	/**
	* Sets the ofertas associated with the empresa, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas to be associated with the empresa
	*/
	public void setOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}