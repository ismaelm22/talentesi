/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import service.tfg.exception.NoSuchMensajesException;

import service.tfg.model.Mensajes;

/**
 * The persistence interface for the mensajes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see service.tfg.service.persistence.impl.MensajesPersistenceImpl
 * @see MensajesUtil
 * @generated
 */
@ProviderType
public interface MensajesPersistence extends BasePersistence<Mensajes> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MensajesUtil} to access the mensajes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the mensajeses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes[] findByUuid_PrevAndNext(long mensajeId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Removes all the mensajeses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of mensajeses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching mensajeses
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes[] findByUuid_C_PrevAndNext(long mensajeId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Removes all the mensajeses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching mensajeses
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the mensajeses where remitente = &#63;.
	*
	* @param remitente the remitente
	* @return the matching mensajeses
	*/
	public java.util.List<Mensajes> findByremitenteId(
		java.lang.String remitente);

	/**
	* Returns a range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByremitenteId(
		java.lang.String remitente, int start, int end);

	/**
	* Returns an ordered range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByremitenteId(
		java.lang.String remitente, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns an ordered range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findByremitenteId(
		java.lang.String remitente, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByremitenteId_First(java.lang.String remitente,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the first mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByremitenteId_First(java.lang.String remitente,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the last mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findByremitenteId_Last(java.lang.String remitente,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the last mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchByremitenteId_Last(java.lang.String remitente,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where remitente = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes[] findByremitenteId_PrevAndNext(long mensajeId,
		java.lang.String remitente,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Removes all the mensajeses where remitente = &#63; from the database.
	*
	* @param remitente the remitente
	*/
	public void removeByremitenteId(java.lang.String remitente);

	/**
	* Returns the number of mensajeses where remitente = &#63;.
	*
	* @param remitente the remitente
	* @return the number of matching mensajeses
	*/
	public int countByremitenteId(java.lang.String remitente);

	/**
	* Returns all the mensajeses where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @return the matching mensajeses
	*/
	public java.util.List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario);

	/**
	* Returns a range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public java.util.List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end);

	/**
	* Returns an ordered range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns an ordered range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public java.util.List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findBydestinatarioId_First(java.lang.String destinatario,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the first mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchBydestinatarioId_First(java.lang.String destinatario,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the last mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public Mensajes findBydestinatarioId_Last(java.lang.String destinatario,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Returns the last mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public Mensajes fetchBydestinatarioId_Last(java.lang.String destinatario,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where destinatario = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes[] findBydestinatarioId_PrevAndNext(long mensajeId,
		java.lang.String destinatario,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException;

	/**
	* Removes all the mensajeses where destinatario = &#63; from the database.
	*
	* @param destinatario the destinatario
	*/
	public void removeBydestinatarioId(java.lang.String destinatario);

	/**
	* Returns the number of mensajeses where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @return the number of matching mensajeses
	*/
	public int countBydestinatarioId(java.lang.String destinatario);

	/**
	* Caches the mensajes in the entity cache if it is enabled.
	*
	* @param mensajes the mensajes
	*/
	public void cacheResult(Mensajes mensajes);

	/**
	* Caches the mensajeses in the entity cache if it is enabled.
	*
	* @param mensajeses the mensajeses
	*/
	public void cacheResult(java.util.List<Mensajes> mensajeses);

	/**
	* Creates a new mensajes with the primary key. Does not add the mensajes to the database.
	*
	* @param mensajeId the primary key for the new mensajes
	* @return the new mensajes
	*/
	public Mensajes create(long mensajeId);

	/**
	* Removes the mensajes with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes that was removed
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes remove(long mensajeId) throws NoSuchMensajesException;

	public Mensajes updateImpl(Mensajes mensajes);

	/**
	* Returns the mensajes with the primary key or throws a {@link NoSuchMensajesException} if it could not be found.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public Mensajes findByPrimaryKey(long mensajeId)
		throws NoSuchMensajesException;

	/**
	* Returns the mensajes with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes, or <code>null</code> if a mensajes with the primary key could not be found
	*/
	public Mensajes fetchByPrimaryKey(long mensajeId);

	@Override
	public java.util.Map<java.io.Serializable, Mensajes> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the mensajeses.
	*
	* @return the mensajeses
	*/
	public java.util.List<Mensajes> findAll();

	/**
	* Returns a range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of mensajeses
	*/
	public java.util.List<Mensajes> findAll(int start, int end);

	/**
	* Returns an ordered range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of mensajeses
	*/
	public java.util.List<Mensajes> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator);

	/**
	* Returns an ordered range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of mensajeses
	*/
	public java.util.List<Mensajes> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the mensajeses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of mensajeses.
	*
	* @return the number of mensajeses
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}