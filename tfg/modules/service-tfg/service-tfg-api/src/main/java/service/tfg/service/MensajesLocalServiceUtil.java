/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Mensajes. This utility wraps
 * {@link service.tfg.service.impl.MensajesLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Ismael Medina
 * @see MensajesLocalService
 * @see service.tfg.service.base.MensajesLocalServiceBaseImpl
 * @see service.tfg.service.impl.MensajesLocalServiceImpl
 * @generated
 */
@ProviderType
public class MensajesLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link service.tfg.service.impl.MensajesLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of mensajeses.
	*
	* @return the number of mensajeses
	*/
	public static int getMensajesesCount() {
		return getService().getMensajesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Mensajes> findBydestinatarioId(
		java.lang.String destinatario) {
		return getService().findBydestinatarioId(destinatario);
	}

	public static java.util.List<service.tfg.model.Mensajes> findByremitenteId(
		java.lang.String remitente) {
		return getService().findByremitenteId(remitente);
	}

	/**
	* Returns a range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of mensajeses
	*/
	public static java.util.List<service.tfg.model.Mensajes> getMensajeses(
		int start, int end) {
		return getService().getMensajeses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the mensajes to the database. Also notifies the appropriate model listeners.
	*
	* @param mensajes the mensajes
	* @return the mensajes that was added
	*/
	public static service.tfg.model.Mensajes addMensajes(
		service.tfg.model.Mensajes mensajes) {
		return getService().addMensajes(mensajes);
	}

	/**
	* Creates a new mensajes with the primary key. Does not add the mensajes to the database.
	*
	* @param mensajeId the primary key for the new mensajes
	* @return the new mensajes
	*/
	public static service.tfg.model.Mensajes createMensajes(long mensajeId) {
		return getService().createMensajes(mensajeId);
	}

	/**
	* Deletes the mensajes with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes that was removed
	* @throws PortalException if a mensajes with the primary key could not be found
	*/
	public static service.tfg.model.Mensajes deleteMensajes(long mensajeId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteMensajes(mensajeId);
	}

	/**
	* Deletes the mensajes from the database. Also notifies the appropriate model listeners.
	*
	* @param mensajes the mensajes
	* @return the mensajes that was removed
	*/
	public static service.tfg.model.Mensajes deleteMensajes(
		service.tfg.model.Mensajes mensajes) {
		return getService().deleteMensajes(mensajes);
	}

	public static service.tfg.model.Mensajes fetchMensajes(long mensajeId) {
		return getService().fetchMensajes(mensajeId);
	}

	/**
	* Returns the mensajes with the matching UUID and company.
	*
	* @param uuid the mensajes's UUID
	* @param companyId the primary key of the company
	* @return the matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static service.tfg.model.Mensajes fetchMensajesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchMensajesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the mensajes with the primary key.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes
	* @throws PortalException if a mensajes with the primary key could not be found
	*/
	public static service.tfg.model.Mensajes getMensajes(long mensajeId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getMensajes(mensajeId);
	}

	/**
	* Returns the mensajes with the matching UUID and company.
	*
	* @param uuid the mensajes's UUID
	* @param companyId the primary key of the company
	* @return the matching mensajes
	* @throws PortalException if a matching mensajes could not be found
	*/
	public static service.tfg.model.Mensajes getMensajesByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getMensajesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the mensajes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param mensajes the mensajes
	* @return the mensajes that was updated
	*/
	public static service.tfg.model.Mensajes updateMensajes(
		service.tfg.model.Mensajes mensajes) {
		return getService().updateMensajes(mensajes);
	}

	public static void addMensae(java.lang.String remitente,
		java.lang.String destinatario, java.lang.String mensaje,
		java.time.LocalDate fecha, long companyId) {
		getService()
			.addMensae(remitente, destinatario, mensaje, fecha, companyId);
	}

	public static MensajesLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MensajesLocalService, MensajesLocalService> _serviceTracker =
		ServiceTrackerFactory.open(MensajesLocalService.class);
}