/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import service.tfg.model.Alumno;

import java.util.List;

/**
 * The persistence utility for the alumno service. This utility wraps {@link service.tfg.service.persistence.impl.AlumnoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see AlumnoPersistence
 * @see service.tfg.service.persistence.impl.AlumnoPersistenceImpl
 * @generated
 */
@ProviderType
public class AlumnoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Alumno alumno) {
		getPersistence().clearCache(alumno);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Alumno> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Alumno> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Alumno> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Alumno update(Alumno alumno) {
		return getPersistence().update(alumno);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Alumno update(Alumno alumno, ServiceContext serviceContext) {
		return getPersistence().update(alumno, serviceContext);
	}

	/**
	* Returns all the alumnos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching alumnos
	*/
	public static List<Alumno> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public static List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByUuid_First(java.lang.String uuid,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno[] findByUuid_PrevAndNext(long alumnoId,
		java.lang.String uuid, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(alumnoId, uuid, orderByComparator);
	}

	/**
	* Removes all the alumnos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of alumnos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching alumnos
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching alumnos
	*/
	public static List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public static List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno[] findByUuid_C_PrevAndNext(long alumnoId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(alumnoId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the alumnos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of alumnos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching alumnos
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the alumnos where email = &#63;.
	*
	* @param email the email
	* @return the matching alumnos
	*/
	public static List<Alumno> findByEmailId(java.lang.String email) {
		return getPersistence().findByEmailId(email);
	}

	/**
	* Returns a range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public static List<Alumno> findByEmailId(java.lang.String email, int start,
		int end) {
		return getPersistence().findByEmailId(email, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByEmailId(java.lang.String email, int start,
		int end, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .findByEmailId(email, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByEmailId(java.lang.String email, int start,
		int end, OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByEmailId(email, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByEmailId_First(java.lang.String email,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().findByEmailId_First(email, orderByComparator);
	}

	/**
	* Returns the first alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByEmailId_First(java.lang.String email,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().fetchByEmailId_First(email, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByEmailId_Last(java.lang.String email,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().findByEmailId_Last(email, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByEmailId_Last(java.lang.String email,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().fetchByEmailId_Last(email, orderByComparator);
	}

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where email = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno[] findByEmailId_PrevAndNext(long alumnoId,
		java.lang.String email, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByEmailId_PrevAndNext(alumnoId, email, orderByComparator);
	}

	/**
	* Removes all the alumnos where email = &#63; from the database.
	*
	* @param email the email
	*/
	public static void removeByEmailId(java.lang.String email) {
		getPersistence().removeByEmailId(email);
	}

	/**
	* Returns the number of alumnos where email = &#63;.
	*
	* @param email the email
	* @return the number of matching alumnos
	*/
	public static int countByEmailId(java.lang.String email) {
		return getPersistence().countByEmailId(email);
	}

	/**
	* Returns all the alumnos where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the matching alumnos
	*/
	public static List<Alumno> findByofertaAsignada(long ofertaId) {
		return getPersistence().findByofertaAsignada(ofertaId);
	}

	/**
	* Returns a range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public static List<Alumno> findByofertaAsignada(long ofertaId, int start,
		int end) {
		return getPersistence().findByofertaAsignada(ofertaId, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByofertaAsignada(long ofertaId, int start,
		int end, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .findByofertaAsignada(ofertaId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findByofertaAsignada(long ofertaId, int start,
		int end, OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByofertaAsignada(ofertaId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByofertaAsignada_First(long ofertaId,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByofertaAsignada_First(ofertaId, orderByComparator);
	}

	/**
	* Returns the first alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByofertaAsignada_First(long ofertaId,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchByofertaAsignada_First(ofertaId, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findByofertaAsignada_Last(long ofertaId,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByofertaAsignada_Last(ofertaId, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchByofertaAsignada_Last(long ofertaId,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchByofertaAsignada_Last(ofertaId, orderByComparator);
	}

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where ofertaId = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno[] findByofertaAsignada_PrevAndNext(long alumnoId,
		long ofertaId, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findByofertaAsignada_PrevAndNext(alumnoId, ofertaId,
			orderByComparator);
	}

	/**
	* Removes all the alumnos where ofertaId = &#63; from the database.
	*
	* @param ofertaId the oferta ID
	*/
	public static void removeByofertaAsignada(long ofertaId) {
		getPersistence().removeByofertaAsignada(ofertaId);
	}

	/**
	* Returns the number of alumnos where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the number of matching alumnos
	*/
	public static int countByofertaAsignada(long ofertaId) {
		return getPersistence().countByofertaAsignada(ofertaId);
	}

	/**
	* Returns all the alumnos where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching alumnos
	*/
	public static List<Alumno> findBynombreAlumno(java.lang.String nombre) {
		return getPersistence().findBynombreAlumno(nombre);
	}

	/**
	* Returns a range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public static List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end) {
		return getPersistence().findBynombreAlumno(nombre, start, end);
	}

	/**
	* Returns an ordered range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end, OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .findBynombreAlumno(nombre, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public static List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end, OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBynombreAlumno(nombre, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findBynombreAlumno_First(java.lang.String nombre,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findBynombreAlumno_First(nombre, orderByComparator);
	}

	/**
	* Returns the first alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchBynombreAlumno_First(java.lang.String nombre,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchBynombreAlumno_First(nombre, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public static Alumno findBynombreAlumno_Last(java.lang.String nombre,
		OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findBynombreAlumno_Last(nombre, orderByComparator);
	}

	/**
	* Returns the last alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static Alumno fetchBynombreAlumno_Last(java.lang.String nombre,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence()
				   .fetchBynombreAlumno_Last(nombre, orderByComparator);
	}

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where nombre = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno[] findBynombreAlumno_PrevAndNext(long alumnoId,
		java.lang.String nombre, OrderByComparator<Alumno> orderByComparator)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence()
				   .findBynombreAlumno_PrevAndNext(alumnoId, nombre,
			orderByComparator);
	}

	/**
	* Removes all the alumnos where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public static void removeBynombreAlumno(java.lang.String nombre) {
		getPersistence().removeBynombreAlumno(nombre);
	}

	/**
	* Returns the number of alumnos where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching alumnos
	*/
	public static int countBynombreAlumno(java.lang.String nombre) {
		return getPersistence().countBynombreAlumno(nombre);
	}

	/**
	* Caches the alumno in the entity cache if it is enabled.
	*
	* @param alumno the alumno
	*/
	public static void cacheResult(Alumno alumno) {
		getPersistence().cacheResult(alumno);
	}

	/**
	* Caches the alumnos in the entity cache if it is enabled.
	*
	* @param alumnos the alumnos
	*/
	public static void cacheResult(List<Alumno> alumnos) {
		getPersistence().cacheResult(alumnos);
	}

	/**
	* Creates a new alumno with the primary key. Does not add the alumno to the database.
	*
	* @param alumnoId the primary key for the new alumno
	* @return the new alumno
	*/
	public static Alumno create(long alumnoId) {
		return getPersistence().create(alumnoId);
	}

	/**
	* Removes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno that was removed
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno remove(long alumnoId)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().remove(alumnoId);
	}

	public static Alumno updateImpl(Alumno alumno) {
		return getPersistence().updateImpl(alumno);
	}

	/**
	* Returns the alumno with the primary key or throws a {@link NoSuchAlumnoException} if it could not be found.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public static Alumno findByPrimaryKey(long alumnoId)
		throws service.tfg.exception.NoSuchAlumnoException {
		return getPersistence().findByPrimaryKey(alumnoId);
	}

	/**
	* Returns the alumno with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno, or <code>null</code> if a alumno with the primary key could not be found
	*/
	public static Alumno fetchByPrimaryKey(long alumnoId) {
		return getPersistence().fetchByPrimaryKey(alumnoId);
	}

	public static java.util.Map<java.io.Serializable, Alumno> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the alumnos.
	*
	* @return the alumnos
	*/
	public static List<Alumno> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of alumnos
	*/
	public static List<Alumno> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of alumnos
	*/
	public static List<Alumno> findAll(int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of alumnos
	*/
	public static List<Alumno> findAll(int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the alumnos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of alumnos.
	*
	* @return the number of alumnos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	* Returns the primaryKeys of ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return long[] of the primaryKeys of ofertas associated with the alumno
	*/
	public static long[] getOfertaPrimaryKeys(long pk) {
		return getPersistence().getOfertaPrimaryKeys(pk);
	}

	/**
	* Returns all the ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return the ofertas associated with the alumno
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk) {
		return getPersistence().getOfertas(pk);
	}

	/**
	* Returns a range of all the ofertas associated with the alumno.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the alumno
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of ofertas associated with the alumno
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end) {
		return getPersistence().getOfertas(pk, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas associated with the alumno.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the alumno
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas associated with the alumno
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return getPersistence().getOfertas(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return the number of ofertas associated with the alumno
	*/
	public static int getOfertasSize(long pk) {
		return getPersistence().getOfertasSize(pk);
	}

	/**
	* Returns <code>true</code> if the oferta is associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	* @return <code>true</code> if the oferta is associated with the alumno; <code>false</code> otherwise
	*/
	public static boolean containsOferta(long pk, long ofertaPK) {
		return getPersistence().containsOferta(pk, ofertaPK);
	}

	/**
	* Returns <code>true</code> if the alumno has any ofertas associated with it.
	*
	* @param pk the primary key of the alumno to check for associations with ofertas
	* @return <code>true</code> if the alumno has any ofertas associated with it; <code>false</code> otherwise
	*/
	public static boolean containsOfertas(long pk) {
		return getPersistence().containsOfertas(pk);
	}

	/**
	* Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	*/
	public static void addOferta(long pk, long ofertaPK) {
		getPersistence().addOferta(pk, ofertaPK);
	}

	/**
	* Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param oferta the oferta
	*/
	public static void addOferta(long pk, service.tfg.model.Oferta oferta) {
		getPersistence().addOferta(pk, oferta);
	}

	/**
	* Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public static void addOfertas(long pk, long[] ofertaPKs) {
		getPersistence().addOfertas(pk, ofertaPKs);
	}

	/**
	* Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas
	*/
	public static void addOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().addOfertas(pk, ofertas);
	}

	/**
	* Clears all associations between the alumno and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno to clear the associated ofertas from
	*/
	public static void clearOfertas(long pk) {
		getPersistence().clearOfertas(pk);
	}

	/**
	* Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	*/
	public static void removeOferta(long pk, long ofertaPK) {
		getPersistence().removeOferta(pk, ofertaPK);
	}

	/**
	* Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param oferta the oferta
	*/
	public static void removeOferta(long pk, service.tfg.model.Oferta oferta) {
		getPersistence().removeOferta(pk, oferta);
	}

	/**
	* Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public static void removeOfertas(long pk, long[] ofertaPKs) {
		getPersistence().removeOfertas(pk, ofertaPKs);
	}

	/**
	* Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas
	*/
	public static void removeOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().removeOfertas(pk, ofertas);
	}

	/**
	* Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas to be associated with the alumno
	*/
	public static void setOfertas(long pk, long[] ofertaPKs) {
		getPersistence().setOfertas(pk, ofertaPKs);
	}

	/**
	* Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas to be associated with the alumno
	*/
	public static void setOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().setOfertas(pk, ofertas);
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static AlumnoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AlumnoPersistence, AlumnoPersistence> _serviceTracker =
		ServiceTrackerFactory.open(AlumnoPersistence.class);
}