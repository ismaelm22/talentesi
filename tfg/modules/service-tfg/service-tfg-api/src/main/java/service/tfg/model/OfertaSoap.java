/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class OfertaSoap implements Serializable {
	public static OfertaSoap toSoapModel(Oferta model) {
		OfertaSoap soapModel = new OfertaSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setOfertaId(model.getOfertaId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setTitulo(model.getTitulo());
		soapModel.setDescripcion(model.getDescripcion());
		soapModel.setArea(model.getArea());
		soapModel.setCompetencias(model.getCompetencias());
		soapModel.setDuracion(model.getDuracion());

		return soapModel;
	}

	public static OfertaSoap[] toSoapModels(Oferta[] models) {
		OfertaSoap[] soapModels = new OfertaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static OfertaSoap[][] toSoapModels(Oferta[][] models) {
		OfertaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new OfertaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new OfertaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static OfertaSoap[] toSoapModels(List<Oferta> models) {
		List<OfertaSoap> soapModels = new ArrayList<OfertaSoap>(models.size());

		for (Oferta model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new OfertaSoap[soapModels.size()]);
	}

	public OfertaSoap() {
	}

	public long getPrimaryKey() {
		return _ofertaId;
	}

	public void setPrimaryKey(long pk) {
		setOfertaId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getOfertaId() {
		return _ofertaId;
	}

	public void setOfertaId(long ofertaId) {
		_ofertaId = ofertaId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getTitulo() {
		return _titulo;
	}

	public void setTitulo(String titulo) {
		_titulo = titulo;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	public String getArea() {
		return _area;
	}

	public void setArea(String area) {
		_area = area;
	}

	public String getCompetencias() {
		return _competencias;
	}

	public void setCompetencias(String competencias) {
		_competencias = competencias;
	}

	public String getDuracion() {
		return _duracion;
	}

	public void setDuracion(String duracion) {
		_duracion = duracion;
	}

	private String _uuid;
	private long _ofertaId;
	private long _companyId;
	private String _titulo;
	private String _descripcion;
	private String _area;
	private String _competencias;
	private String _duracion;
}