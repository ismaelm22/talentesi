/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Oferta;
import service.tfg.model.Profesor;

import java.io.Serializable;

import java.util.Collection;
import java.util.List;

/**
 * Provides the local service interface for Oferta. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Ismael Medina
 * @see OfertaLocalServiceUtil
 * @see service.tfg.service.base.OfertaLocalServiceBaseImpl
 * @see service.tfg.service.impl.OfertaLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface OfertaLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OfertaLocalServiceUtil} to access the oferta local service. Add custom service methods to {@link service.tfg.service.impl.OfertaLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasAlumnoOferta(long alumnoId, long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasAlumnoOfertas(long alumnoId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEmpresaOferta(long empresaId, long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasEmpresaOfertas(long empresaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasProfesorOferta(long profesorId, long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasProfesorOfertas(long profesorId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAlumnoOfertasCount(long alumnoId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEmpresaOfertasCount(long empresaId);

	/**
	* Returns the number of ofertas.
	*
	* @return the number of ofertas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getOfertasCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProfesorOfertasCount(long profesorId);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	public List<Oferta> findByOfertaId(long ofertaId);

	public List<Oferta> findByTitulo(java.lang.String titulo);

	public List<Oferta> findByareaType(java.lang.String area);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getAlumnoOfertas(long alumnoId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getAlumnoOfertas(long alumnoId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getAlumnoOfertas(long alumnoId, int start, int end,
		OrderByComparator<Oferta> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getEmpresaOfertas(long empresaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getEmpresaOfertas(long empresaId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getEmpresaOfertas(long empresaId, int start, int end,
		OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns a range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of ofertas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getOfertas(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getProfesorOfertas(long profesorId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getProfesorOfertas(long profesorId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Oferta> getProfesorOfertas(long profesorId, int start, int end,
		OrderByComparator<Oferta> orderByComparator);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Returns the alumnoIds of the alumnos associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the alumnoIds of alumnos associated with the oferta
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getAlumnoPrimaryKeys(long ofertaId);

	/**
	* Returns the empresaIds of the empresas associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the empresaIds of empresas associated with the oferta
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getEmpresaPrimaryKeys(long ofertaId);

	/**
	* Returns the profesorIds of the profesors associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the profesorIds of profesors associated with the oferta
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getProfesorPrimaryKeys(long ofertaId);

	/**
	* Adds the oferta to the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Oferta addOferta(Oferta oferta);

	/**
	* Creates a new oferta with the primary key. Does not add the oferta to the database.
	*
	* @param ofertaId the primary key for the new oferta
	* @return the new oferta
	*/
	public Oferta createOferta(long ofertaId);

	/**
	* Deletes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta that was removed
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Oferta deleteOferta(long ofertaId) throws PortalException;

	/**
	* Deletes the oferta from the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Oferta deleteOferta(Oferta oferta);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Oferta fetchOferta(long ofertaId);

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Oferta fetchOfertaByUuidAndCompanyId(java.lang.String uuid,
		long companyId);

	/**
	* Returns the oferta with the primary key.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Oferta getOferta(long ofertaId) throws PortalException;

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta
	* @throws PortalException if a matching oferta could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Oferta getOfertaByUuidAndCompanyId(java.lang.String uuid,
		long companyId) throws PortalException;

	/**
	* Updates the oferta in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Oferta updateOferta(Oferta oferta);

	public void addAlumnoOferta(long alumnoId, long ofertaId);

	public void addAlumnoOferta(long alumnoId, Oferta oferta);

	public void addAlumnoOfertas(long alumnoId, List<Oferta> ofertas);

	public void addAlumnoOfertas(long alumnoId, long[] ofertaIds);

	public void addEmpresaOferta(long empresaId, long ofertaId);

	public void addEmpresaOferta(long empresaId, Oferta oferta);

	public void addEmpresaOfertas(long empresaId, List<Oferta> ofertas);

	public void addEmpresaOfertas(long empresaId, long[] ofertaIds);

	public void addOferta(long companyId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion,
		Collection<Profesor> profesores, Collection<Empresa> empresas);

	public void addProfesorOferta(long profesorId, long ofertaId);

	public void addProfesorOferta(long profesorId, Oferta oferta);

	public void addProfesorOfertas(long profesorId, List<Oferta> ofertas);

	public void addProfesorOfertas(long profesorId, long[] ofertaIds);

	public void clearAlumnoOfertas(long alumnoId);

	public void clearEmpresaOfertas(long empresaId);

	public void clearProfesorOfertas(long profesorId);

	public void deleteAlumnoOferta(long alumnoId, long ofertaId);

	public void deleteAlumnoOferta(long alumnoId, Oferta oferta);

	public void deleteAlumnoOfertas(long alumnoId, List<Oferta> ofertas);

	public void deleteAlumnoOfertas(long alumnoId, long[] ofertaIds);

	public void deleteEmpresaOferta(long empresaId, long ofertaId);

	public void deleteEmpresaOferta(long empresaId, Oferta oferta);

	public void deleteEmpresaOfertas(long empresaId, List<Oferta> ofertas);

	public void deleteEmpresaOfertas(long empresaId, long[] ofertaIds);

	public void deleteProfesorOferta(long profesorId, long ofertaId);

	public void deleteProfesorOferta(long profesorId, Oferta oferta);

	public void deleteProfesorOfertas(long profesorId, List<Oferta> ofertas);

	public void deleteProfesorOfertas(long profesorId, long[] ofertaIds);

	public void setAlumnoOfertas(long alumnoId, long[] ofertaIds);

	public void setEmpresaOfertas(long empresaId, long[] ofertaIds);

	public void setProfesorOfertas(long profesorId, long[] ofertaIds);

	public void updateAlumnosolcitudes(long ofertaId, Collection<Alumno> alumnos)
		throws PortalException;

	public void updateEmresasOferta(long ofertaId, Collection<Empresa> empresas)
		throws PortalException;

	public void updateOferta(long ofertaId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion)
		throws PortalException;

	public void updateProfesoresOferta(long ofertaId,
		Collection<Profesor> profesor) throws PortalException;
}