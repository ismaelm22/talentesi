/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class EmpresaSoap implements Serializable {
	public static EmpresaSoap toSoapModel(Empresa model) {
		EmpresaSoap soapModel = new EmpresaSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setEmpresaId(model.getEmpresaId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmail(model.getEmail());
		soapModel.setPwd(model.getPwd());
		soapModel.setNombre(model.getNombre());
		soapModel.setCif(model.getCif());
		soapModel.setDireccion(model.getDireccion());
		soapModel.setTelefono(model.getTelefono());

		return soapModel;
	}

	public static EmpresaSoap[] toSoapModels(Empresa[] models) {
		EmpresaSoap[] soapModels = new EmpresaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmpresaSoap[][] toSoapModels(Empresa[][] models) {
		EmpresaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmpresaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmpresaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmpresaSoap[] toSoapModels(List<Empresa> models) {
		List<EmpresaSoap> soapModels = new ArrayList<EmpresaSoap>(models.size());

		for (Empresa model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmpresaSoap[soapModels.size()]);
	}

	public EmpresaSoap() {
	}

	public long getPrimaryKey() {
		return _empresaId;
	}

	public void setPrimaryKey(long pk) {
		setEmpresaId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getEmpresaId() {
		return _empresaId;
	}

	public void setEmpresaId(long empresaId) {
		_empresaId = empresaId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getPwd() {
		return _pwd;
	}

	public void setPwd(String pwd) {
		_pwd = pwd;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getCif() {
		return _cif;
	}

	public void setCif(String cif) {
		_cif = cif;
	}

	public String getDireccion() {
		return _direccion;
	}

	public void setDireccion(String direccion) {
		_direccion = direccion;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	private String _uuid;
	private long _empresaId;
	private long _companyId;
	private String _email;
	private String _pwd;
	private String _nombre;
	private String _cif;
	private String _direccion;
	private String _telefono;
}