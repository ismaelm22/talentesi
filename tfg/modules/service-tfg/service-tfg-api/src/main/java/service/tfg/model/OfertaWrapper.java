/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Oferta}.
 * </p>
 *
 * @author Ismael Medina
 * @see Oferta
 * @generated
 */
@ProviderType
public class OfertaWrapper implements Oferta, ModelWrapper<Oferta> {
	public OfertaWrapper(Oferta oferta) {
		_oferta = oferta;
	}

	@Override
	public Class<?> getModelClass() {
		return Oferta.class;
	}

	@Override
	public String getModelClassName() {
		return Oferta.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("ofertaId", getOfertaId());
		attributes.put("companyId", getCompanyId());
		attributes.put("titulo", getTitulo());
		attributes.put("descripcion", getDescripcion());
		attributes.put("area", getArea());
		attributes.put("competencias", getCompetencias());
		attributes.put("duracion", getDuracion());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long ofertaId = (Long)attributes.get("ofertaId");

		if (ofertaId != null) {
			setOfertaId(ofertaId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String titulo = (String)attributes.get("titulo");

		if (titulo != null) {
			setTitulo(titulo);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}

		String area = (String)attributes.get("area");

		if (area != null) {
			setArea(area);
		}

		String competencias = (String)attributes.get("competencias");

		if (competencias != null) {
			setCompetencias(competencias);
		}

		String duracion = (String)attributes.get("duracion");

		if (duracion != null) {
			setDuracion(duracion);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _oferta.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _oferta.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _oferta.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _oferta.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<service.tfg.model.Oferta> toCacheModel() {
		return _oferta.toCacheModel();
	}

	@Override
	public int compareTo(service.tfg.model.Oferta oferta) {
		return _oferta.compareTo(oferta);
	}

	@Override
	public int hashCode() {
		return _oferta.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _oferta.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new OfertaWrapper((Oferta)_oferta.clone());
	}

	/**
	* Returns the area of this oferta.
	*
	* @return the area of this oferta
	*/
	@Override
	public java.lang.String getArea() {
		return _oferta.getArea();
	}

	/**
	* Returns the competencias of this oferta.
	*
	* @return the competencias of this oferta
	*/
	@Override
	public java.lang.String getCompetencias() {
		return _oferta.getCompetencias();
	}

	/**
	* Returns the descripcion of this oferta.
	*
	* @return the descripcion of this oferta
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _oferta.getDescripcion();
	}

	/**
	* Returns the duracion of this oferta.
	*
	* @return the duracion of this oferta
	*/
	@Override
	public java.lang.String getDuracion() {
		return _oferta.getDuracion();
	}

	/**
	* Returns the titulo of this oferta.
	*
	* @return the titulo of this oferta
	*/
	@Override
	public java.lang.String getTitulo() {
		return _oferta.getTitulo();
	}

	/**
	* Returns the uuid of this oferta.
	*
	* @return the uuid of this oferta
	*/
	@Override
	public java.lang.String getUuid() {
		return _oferta.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _oferta.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _oferta.toXmlString();
	}

	/**
	* Returns the company ID of this oferta.
	*
	* @return the company ID of this oferta
	*/
	@Override
	public long getCompanyId() {
		return _oferta.getCompanyId();
	}

	/**
	* Returns the oferta ID of this oferta.
	*
	* @return the oferta ID of this oferta
	*/
	@Override
	public long getOfertaId() {
		return _oferta.getOfertaId();
	}

	/**
	* Returns the primary key of this oferta.
	*
	* @return the primary key of this oferta
	*/
	@Override
	public long getPrimaryKey() {
		return _oferta.getPrimaryKey();
	}

	@Override
	public service.tfg.model.Oferta toEscapedModel() {
		return new OfertaWrapper(_oferta.toEscapedModel());
	}

	@Override
	public service.tfg.model.Oferta toUnescapedModel() {
		return new OfertaWrapper(_oferta.toUnescapedModel());
	}

	@Override
	public void persist() {
		_oferta.persist();
	}

	/**
	* Sets the area of this oferta.
	*
	* @param area the area of this oferta
	*/
	@Override
	public void setArea(java.lang.String area) {
		_oferta.setArea(area);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_oferta.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this oferta.
	*
	* @param companyId the company ID of this oferta
	*/
	@Override
	public void setCompanyId(long companyId) {
		_oferta.setCompanyId(companyId);
	}

	/**
	* Sets the competencias of this oferta.
	*
	* @param competencias the competencias of this oferta
	*/
	@Override
	public void setCompetencias(java.lang.String competencias) {
		_oferta.setCompetencias(competencias);
	}

	/**
	* Sets the descripcion of this oferta.
	*
	* @param descripcion the descripcion of this oferta
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_oferta.setDescripcion(descripcion);
	}

	/**
	* Sets the duracion of this oferta.
	*
	* @param duracion the duracion of this oferta
	*/
	@Override
	public void setDuracion(java.lang.String duracion) {
		_oferta.setDuracion(duracion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_oferta.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_oferta.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_oferta.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_oferta.setNew(n);
	}

	/**
	* Sets the oferta ID of this oferta.
	*
	* @param ofertaId the oferta ID of this oferta
	*/
	@Override
	public void setOfertaId(long ofertaId) {
		_oferta.setOfertaId(ofertaId);
	}

	/**
	* Sets the primary key of this oferta.
	*
	* @param primaryKey the primary key of this oferta
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_oferta.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_oferta.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the titulo of this oferta.
	*
	* @param titulo the titulo of this oferta
	*/
	@Override
	public void setTitulo(java.lang.String titulo) {
		_oferta.setTitulo(titulo);
	}

	/**
	* Sets the uuid of this oferta.
	*
	* @param uuid the uuid of this oferta
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_oferta.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OfertaWrapper)) {
			return false;
		}

		OfertaWrapper ofertaWrapper = (OfertaWrapper)obj;

		if (Objects.equals(_oferta, ofertaWrapper._oferta)) {
			return true;
		}

		return false;
	}

	@Override
	public Oferta getWrappedModel() {
		return _oferta;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _oferta.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _oferta.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_oferta.resetOriginalValues();
	}

	private final Oferta _oferta;
}