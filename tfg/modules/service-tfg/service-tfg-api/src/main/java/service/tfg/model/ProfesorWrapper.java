/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Profesor}.
 * </p>
 *
 * @author Ismael Medina
 * @see Profesor
 * @generated
 */
@ProviderType
public class ProfesorWrapper implements Profesor, ModelWrapper<Profesor> {
	public ProfesorWrapper(Profesor profesor) {
		_profesor = profesor;
	}

	@Override
	public Class<?> getModelClass() {
		return Profesor.class;
	}

	@Override
	public String getModelClassName() {
		return Profesor.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("profesorId", getProfesorId());
		attributes.put("companyId", getCompanyId());
		attributes.put("email", getEmail());
		attributes.put("pwd", getPwd());
		attributes.put("nombre", getNombre());
		attributes.put("apellidos", getApellidos());
		attributes.put("area", getArea());
		attributes.put("despacho", getDespacho());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long profesorId = (Long)attributes.get("profesorId");

		if (profesorId != null) {
			setProfesorId(profesorId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String pwd = (String)attributes.get("pwd");

		if (pwd != null) {
			setPwd(pwd);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String apellidos = (String)attributes.get("apellidos");

		if (apellidos != null) {
			setApellidos(apellidos);
		}

		String area = (String)attributes.get("area");

		if (area != null) {
			setArea(area);
		}

		String despacho = (String)attributes.get("despacho");

		if (despacho != null) {
			setDespacho(despacho);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _profesor.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _profesor.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _profesor.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _profesor.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<service.tfg.model.Profesor> toCacheModel() {
		return _profesor.toCacheModel();
	}

	@Override
	public int compareTo(service.tfg.model.Profesor profesor) {
		return _profesor.compareTo(profesor);
	}

	@Override
	public int hashCode() {
		return _profesor.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _profesor.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProfesorWrapper((Profesor)_profesor.clone());
	}

	/**
	* Returns the apellidos of this profesor.
	*
	* @return the apellidos of this profesor
	*/
	@Override
	public java.lang.String getApellidos() {
		return _profesor.getApellidos();
	}

	/**
	* Returns the area of this profesor.
	*
	* @return the area of this profesor
	*/
	@Override
	public java.lang.String getArea() {
		return _profesor.getArea();
	}

	/**
	* Returns the despacho of this profesor.
	*
	* @return the despacho of this profesor
	*/
	@Override
	public java.lang.String getDespacho() {
		return _profesor.getDespacho();
	}

	/**
	* Returns the email of this profesor.
	*
	* @return the email of this profesor
	*/
	@Override
	public java.lang.String getEmail() {
		return _profesor.getEmail();
	}

	/**
	* Returns the nombre of this profesor.
	*
	* @return the nombre of this profesor
	*/
	@Override
	public java.lang.String getNombre() {
		return _profesor.getNombre();
	}

	/**
	* Returns the pwd of this profesor.
	*
	* @return the pwd of this profesor
	*/
	@Override
	public java.lang.String getPwd() {
		return _profesor.getPwd();
	}

	/**
	* Returns the uuid of this profesor.
	*
	* @return the uuid of this profesor
	*/
	@Override
	public java.lang.String getUuid() {
		return _profesor.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _profesor.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _profesor.toXmlString();
	}

	/**
	* Returns the company ID of this profesor.
	*
	* @return the company ID of this profesor
	*/
	@Override
	public long getCompanyId() {
		return _profesor.getCompanyId();
	}

	/**
	* Returns the primary key of this profesor.
	*
	* @return the primary key of this profesor
	*/
	@Override
	public long getPrimaryKey() {
		return _profesor.getPrimaryKey();
	}

	/**
	* Returns the profesor ID of this profesor.
	*
	* @return the profesor ID of this profesor
	*/
	@Override
	public long getProfesorId() {
		return _profesor.getProfesorId();
	}

	@Override
	public service.tfg.model.Profesor toEscapedModel() {
		return new ProfesorWrapper(_profesor.toEscapedModel());
	}

	@Override
	public service.tfg.model.Profesor toUnescapedModel() {
		return new ProfesorWrapper(_profesor.toUnescapedModel());
	}

	@Override
	public void persist() {
		_profesor.persist();
	}

	/**
	* Sets the apellidos of this profesor.
	*
	* @param apellidos the apellidos of this profesor
	*/
	@Override
	public void setApellidos(java.lang.String apellidos) {
		_profesor.setApellidos(apellidos);
	}

	/**
	* Sets the area of this profesor.
	*
	* @param area the area of this profesor
	*/
	@Override
	public void setArea(java.lang.String area) {
		_profesor.setArea(area);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_profesor.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this profesor.
	*
	* @param companyId the company ID of this profesor
	*/
	@Override
	public void setCompanyId(long companyId) {
		_profesor.setCompanyId(companyId);
	}

	/**
	* Sets the despacho of this profesor.
	*
	* @param despacho the despacho of this profesor
	*/
	@Override
	public void setDespacho(java.lang.String despacho) {
		_profesor.setDespacho(despacho);
	}

	/**
	* Sets the email of this profesor.
	*
	* @param email the email of this profesor
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_profesor.setEmail(email);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_profesor.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_profesor.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_profesor.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_profesor.setNew(n);
	}

	/**
	* Sets the nombre of this profesor.
	*
	* @param nombre the nombre of this profesor
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_profesor.setNombre(nombre);
	}

	/**
	* Sets the primary key of this profesor.
	*
	* @param primaryKey the primary key of this profesor
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_profesor.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_profesor.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the profesor ID of this profesor.
	*
	* @param profesorId the profesor ID of this profesor
	*/
	@Override
	public void setProfesorId(long profesorId) {
		_profesor.setProfesorId(profesorId);
	}

	/**
	* Sets the pwd of this profesor.
	*
	* @param pwd the pwd of this profesor
	*/
	@Override
	public void setPwd(java.lang.String pwd) {
		_profesor.setPwd(pwd);
	}

	/**
	* Sets the uuid of this profesor.
	*
	* @param uuid the uuid of this profesor
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_profesor.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProfesorWrapper)) {
			return false;
		}

		ProfesorWrapper profesorWrapper = (ProfesorWrapper)obj;

		if (Objects.equals(_profesor, profesorWrapper._profesor)) {
			return true;
		}

		return false;
	}

	@Override
	public Profesor getWrappedModel() {
		return _profesor;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _profesor.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _profesor.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_profesor.resetOriginalValues();
	}

	private final Profesor _profesor;
}