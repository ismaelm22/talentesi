/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import service.tfg.exception.NoSuchAlumnoException;

import service.tfg.model.Alumno;

/**
 * The persistence interface for the alumno service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see service.tfg.service.persistence.impl.AlumnoPersistenceImpl
 * @see AlumnoUtil
 * @generated
 */
@ProviderType
public interface AlumnoPersistence extends BasePersistence<Alumno> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AlumnoUtil} to access the alumno persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the alumnos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching alumnos
	*/
	public java.util.List<Alumno> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the first alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the last alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the last alumno in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno[] findByUuid_PrevAndNext(long alumnoId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Removes all the alumnos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of alumnos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching alumnos
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching alumnos
	*/
	public java.util.List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno[] findByUuid_C_PrevAndNext(long alumnoId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Removes all the alumnos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of alumnos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching alumnos
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the alumnos where email = &#63;.
	*
	* @param email the email
	* @return the matching alumnos
	*/
	public java.util.List<Alumno> findByEmailId(java.lang.String email);

	/**
	* Returns a range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public java.util.List<Alumno> findByEmailId(java.lang.String email,
		int start, int end);

	/**
	* Returns an ordered range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the first alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the last alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the last alumno in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where email = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno[] findByEmailId_PrevAndNext(long alumnoId,
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Removes all the alumnos where email = &#63; from the database.
	*
	* @param email the email
	*/
	public void removeByEmailId(java.lang.String email);

	/**
	* Returns the number of alumnos where email = &#63;.
	*
	* @param email the email
	* @return the number of matching alumnos
	*/
	public int countByEmailId(java.lang.String email);

	/**
	* Returns all the alumnos where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the matching alumnos
	*/
	public java.util.List<Alumno> findByofertaAsignada(long ofertaId);

	/**
	* Returns a range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public java.util.List<Alumno> findByofertaAsignada(long ofertaId,
		int start, int end);

	/**
	* Returns an ordered range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByofertaAsignada(long ofertaId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos where ofertaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ofertaId the oferta ID
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findByofertaAsignada(long ofertaId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByofertaAsignada_First(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the first alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByofertaAsignada_First(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the last alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findByofertaAsignada_Last(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the last alumno in the ordered set where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchByofertaAsignada_Last(long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where ofertaId = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param ofertaId the oferta ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno[] findByofertaAsignada_PrevAndNext(long alumnoId,
		long ofertaId,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Removes all the alumnos where ofertaId = &#63; from the database.
	*
	* @param ofertaId the oferta ID
	*/
	public void removeByofertaAsignada(long ofertaId);

	/**
	* Returns the number of alumnos where ofertaId = &#63;.
	*
	* @param ofertaId the oferta ID
	* @return the number of matching alumnos
	*/
	public int countByofertaAsignada(long ofertaId);

	/**
	* Returns all the alumnos where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching alumnos
	*/
	public java.util.List<Alumno> findBynombreAlumno(java.lang.String nombre);

	/**
	* Returns a range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of matching alumnos
	*/
	public java.util.List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end);

	/**
	* Returns an ordered range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching alumnos
	*/
	public java.util.List<Alumno> findBynombreAlumno(java.lang.String nombre,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findBynombreAlumno_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the first alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchBynombreAlumno_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the last alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno
	* @throws NoSuchAlumnoException if a matching alumno could not be found
	*/
	public Alumno findBynombreAlumno_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Returns the last alumno in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public Alumno fetchBynombreAlumno_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the alumnos before and after the current alumno in the ordered set where nombre = &#63;.
	*
	* @param alumnoId the primary key of the current alumno
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno[] findBynombreAlumno_PrevAndNext(long alumnoId,
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException;

	/**
	* Removes all the alumnos where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombreAlumno(java.lang.String nombre);

	/**
	* Returns the number of alumnos where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching alumnos
	*/
	public int countBynombreAlumno(java.lang.String nombre);

	/**
	* Caches the alumno in the entity cache if it is enabled.
	*
	* @param alumno the alumno
	*/
	public void cacheResult(Alumno alumno);

	/**
	* Caches the alumnos in the entity cache if it is enabled.
	*
	* @param alumnos the alumnos
	*/
	public void cacheResult(java.util.List<Alumno> alumnos);

	/**
	* Creates a new alumno with the primary key. Does not add the alumno to the database.
	*
	* @param alumnoId the primary key for the new alumno
	* @return the new alumno
	*/
	public Alumno create(long alumnoId);

	/**
	* Removes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno that was removed
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno remove(long alumnoId) throws NoSuchAlumnoException;

	public Alumno updateImpl(Alumno alumno);

	/**
	* Returns the alumno with the primary key or throws a {@link NoSuchAlumnoException} if it could not be found.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno
	* @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	*/
	public Alumno findByPrimaryKey(long alumnoId) throws NoSuchAlumnoException;

	/**
	* Returns the alumno with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno, or <code>null</code> if a alumno with the primary key could not be found
	*/
	public Alumno fetchByPrimaryKey(long alumnoId);

	@Override
	public java.util.Map<java.io.Serializable, Alumno> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the alumnos.
	*
	* @return the alumnos
	*/
	public java.util.List<Alumno> findAll();

	/**
	* Returns a range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of alumnos
	*/
	public java.util.List<Alumno> findAll(int start, int end);

	/**
	* Returns an ordered range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of alumnos
	*/
	public java.util.List<Alumno> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns an ordered range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of alumnos
	*/
	public java.util.List<Alumno> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the alumnos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of alumnos.
	*
	* @return the number of alumnos
	*/
	public int countAll();

	/**
	* Returns the primaryKeys of ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return long[] of the primaryKeys of ofertas associated with the alumno
	*/
	public long[] getOfertaPrimaryKeys(long pk);

	/**
	* Returns all the ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return the ofertas associated with the alumno
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk);

	/**
	* Returns a range of all the ofertas associated with the alumno.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the alumno
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of ofertas associated with the alumno
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the ofertas associated with the alumno.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the alumno
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas associated with the alumno
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator);

	/**
	* Returns the number of ofertas associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @return the number of ofertas associated with the alumno
	*/
	public int getOfertasSize(long pk);

	/**
	* Returns <code>true</code> if the oferta is associated with the alumno.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	* @return <code>true</code> if the oferta is associated with the alumno; <code>false</code> otherwise
	*/
	public boolean containsOferta(long pk, long ofertaPK);

	/**
	* Returns <code>true</code> if the alumno has any ofertas associated with it.
	*
	* @param pk the primary key of the alumno to check for associations with ofertas
	* @return <code>true</code> if the alumno has any ofertas associated with it; <code>false</code> otherwise
	*/
	public boolean containsOfertas(long pk);

	/**
	* Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	*/
	public void addOferta(long pk, long ofertaPK);

	/**
	* Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param oferta the oferta
	*/
	public void addOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void addOfertas(long pk, long[] ofertaPKs);

	/**
	* Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas
	*/
	public void addOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Clears all associations between the alumno and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno to clear the associated ofertas from
	*/
	public void clearOfertas(long pk);

	/**
	* Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPK the primary key of the oferta
	*/
	public void removeOferta(long pk, long ofertaPK);

	/**
	* Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param oferta the oferta
	*/
	public void removeOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void removeOfertas(long pk, long[] ofertaPKs);

	/**
	* Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas
	*/
	public void removeOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertaPKs the primary keys of the ofertas to be associated with the alumno
	*/
	public void setOfertas(long pk, long[] ofertaPKs);

	/**
	* Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the alumno
	* @param ofertas the ofertas to be associated with the alumno
	*/
	public void setOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}