/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.sql.Blob;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class AlumnoSoap implements Serializable {
	public static AlumnoSoap toSoapModel(Alumno model) {
		AlumnoSoap soapModel = new AlumnoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAlumnoId(model.getAlumnoId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmail(model.getEmail());
		soapModel.setPwd(model.getPwd());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidos(model.getApellidos());
		soapModel.setCompetencias(model.getCompetencias());
		soapModel.setOfertaId(model.getOfertaId());
		soapModel.setCurriculum(model.getCurriculum());

		return soapModel;
	}

	public static AlumnoSoap[] toSoapModels(Alumno[] models) {
		AlumnoSoap[] soapModels = new AlumnoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AlumnoSoap[][] toSoapModels(Alumno[][] models) {
		AlumnoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AlumnoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AlumnoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AlumnoSoap[] toSoapModels(List<Alumno> models) {
		List<AlumnoSoap> soapModels = new ArrayList<AlumnoSoap>(models.size());

		for (Alumno model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AlumnoSoap[soapModels.size()]);
	}

	public AlumnoSoap() {
	}

	public long getPrimaryKey() {
		return _alumnoId;
	}

	public void setPrimaryKey(long pk) {
		setAlumnoId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAlumnoId() {
		return _alumnoId;
	}

	public void setAlumnoId(long alumnoId) {
		_alumnoId = alumnoId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getPwd() {
		return _pwd;
	}

	public void setPwd(String pwd) {
		_pwd = pwd;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidos() {
		return _apellidos;
	}

	public void setApellidos(String apellidos) {
		_apellidos = apellidos;
	}

	public String getCompetencias() {
		return _competencias;
	}

	public void setCompetencias(String competencias) {
		_competencias = competencias;
	}

	public long getOfertaId() {
		return _ofertaId;
	}

	public void setOfertaId(long ofertaId) {
		_ofertaId = ofertaId;
	}

	public Blob getCurriculum() {
		return _curriculum;
	}

	public void setCurriculum(Blob curriculum) {
		_curriculum = curriculum;
	}

	private String _uuid;
	private long _alumnoId;
	private long _companyId;
	private String _email;
	private String _pwd;
	private String _nombre;
	private String _apellidos;
	private String _competencias;
	private long _ofertaId;
	private Blob _curriculum;
}