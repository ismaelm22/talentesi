/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Alumno service. Represents a row in the &quot;BBDD_Alumno&quot; database table, with each column mapped to a property of this class.
 *
 * @author Ismael Medina
 * @see AlumnoModel
 * @see service.tfg.model.impl.AlumnoImpl
 * @see service.tfg.model.impl.AlumnoModelImpl
 * @generated
 */
@ImplementationClassName("service.tfg.model.impl.AlumnoImpl")
@ProviderType
public interface Alumno extends AlumnoModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link service.tfg.model.impl.AlumnoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Alumno, Long> ALUMNO_ID_ACCESSOR = new Accessor<Alumno, Long>() {
			@Override
			public Long get(Alumno alumno) {
				return alumno.getAlumnoId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Alumno> getTypeClass() {
				return Alumno.class;
			}
		};
}