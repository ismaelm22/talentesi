/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the curriculum column in Alumno.
 *
 * @author Ismael Medina
 * @see Alumno
 * @generated
 */
@ProviderType
public class AlumnoCurriculumBlobModel {
	public AlumnoCurriculumBlobModel() {
	}

	public AlumnoCurriculumBlobModel(long alumnoId) {
		_alumnoId = alumnoId;
	}

	public AlumnoCurriculumBlobModel(long alumnoId, Blob curriculumBlob) {
		_alumnoId = alumnoId;
		_curriculumBlob = curriculumBlob;
	}

	public long getAlumnoId() {
		return _alumnoId;
	}

	public void setAlumnoId(long alumnoId) {
		_alumnoId = alumnoId;
	}

	public Blob getCurriculumBlob() {
		return _curriculumBlob;
	}

	public void setCurriculumBlob(Blob curriculumBlob) {
		_curriculumBlob = curriculumBlob;
	}

	private long _alumnoId;
	private Blob _curriculumBlob;
}