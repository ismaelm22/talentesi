/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class ProfesorSoap implements Serializable {
	public static ProfesorSoap toSoapModel(Profesor model) {
		ProfesorSoap soapModel = new ProfesorSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setProfesorId(model.getProfesorId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setEmail(model.getEmail());
		soapModel.setPwd(model.getPwd());
		soapModel.setNombre(model.getNombre());
		soapModel.setApellidos(model.getApellidos());
		soapModel.setArea(model.getArea());
		soapModel.setDespacho(model.getDespacho());

		return soapModel;
	}

	public static ProfesorSoap[] toSoapModels(Profesor[] models) {
		ProfesorSoap[] soapModels = new ProfesorSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProfesorSoap[][] toSoapModels(Profesor[][] models) {
		ProfesorSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProfesorSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProfesorSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProfesorSoap[] toSoapModels(List<Profesor> models) {
		List<ProfesorSoap> soapModels = new ArrayList<ProfesorSoap>(models.size());

		for (Profesor model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProfesorSoap[soapModels.size()]);
	}

	public ProfesorSoap() {
	}

	public long getPrimaryKey() {
		return _profesorId;
	}

	public void setPrimaryKey(long pk) {
		setProfesorId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getProfesorId() {
		return _profesorId;
	}

	public void setProfesorId(long profesorId) {
		_profesorId = profesorId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getPwd() {
		return _pwd;
	}

	public void setPwd(String pwd) {
		_pwd = pwd;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getApellidos() {
		return _apellidos;
	}

	public void setApellidos(String apellidos) {
		_apellidos = apellidos;
	}

	public String getArea() {
		return _area;
	}

	public void setArea(String area) {
		_area = area;
	}

	public String getDespacho() {
		return _despacho;
	}

	public void setDespacho(String despacho) {
		_despacho = despacho;
	}

	private String _uuid;
	private long _profesorId;
	private long _companyId;
	private String _email;
	private String _pwd;
	private String _nombre;
	private String _apellidos;
	private String _area;
	private String _despacho;
}