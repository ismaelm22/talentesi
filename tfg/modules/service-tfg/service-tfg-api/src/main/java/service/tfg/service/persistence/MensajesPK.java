/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class MensajesPK implements Comparable<MensajesPK>, Serializable {
	public long mensajeId;
	public String remitente;
	public String destinatario;

	public MensajesPK() {
	}

	public MensajesPK(long mensajeId, String remitente, String destinatario) {
		this.mensajeId = mensajeId;
		this.remitente = remitente;
		this.destinatario = destinatario;
	}

	public long getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(long mensajeId) {
		this.mensajeId = mensajeId;
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	@Override
	public int compareTo(MensajesPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (mensajeId < pk.mensajeId) {
			value = -1;
		}
		else if (mensajeId > pk.mensajeId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = remitente.compareTo(pk.remitente);

		if (value != 0) {
			return value;
		}

		value = destinatario.compareTo(pk.destinatario);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MensajesPK)) {
			return false;
		}

		MensajesPK pk = (MensajesPK)obj;

		if ((mensajeId == pk.mensajeId) && (remitente.equals(pk.remitente)) &&
				(destinatario.equals(pk.destinatario))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, mensajeId);
		hashCode = HashUtil.hash(hashCode, remitente);
		hashCode = HashUtil.hash(hashCode, destinatario);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("mensajeId");
		sb.append(StringPool.EQUAL);
		sb.append(mensajeId);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("remitente");
		sb.append(StringPool.EQUAL);
		sb.append(remitente);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("destinatario");
		sb.append(StringPool.EQUAL);
		sb.append(destinatario);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}