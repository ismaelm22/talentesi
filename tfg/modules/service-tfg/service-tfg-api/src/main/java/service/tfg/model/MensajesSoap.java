/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Ismael Medina
 * @generated
 */
@ProviderType
public class MensajesSoap implements Serializable {
	public static MensajesSoap toSoapModel(Mensajes model) {
		MensajesSoap soapModel = new MensajesSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setMensajeId(model.getMensajeId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setRemitente(model.getRemitente());
		soapModel.setDestinatario(model.getDestinatario());
		soapModel.setMensaje(model.getMensaje());
		soapModel.setFecha(model.getFecha());

		return soapModel;
	}

	public static MensajesSoap[] toSoapModels(Mensajes[] models) {
		MensajesSoap[] soapModels = new MensajesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MensajesSoap[][] toSoapModels(Mensajes[][] models) {
		MensajesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MensajesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MensajesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MensajesSoap[] toSoapModels(List<Mensajes> models) {
		List<MensajesSoap> soapModels = new ArrayList<MensajesSoap>(models.size());

		for (Mensajes model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MensajesSoap[soapModels.size()]);
	}

	public MensajesSoap() {
	}

	public long getPrimaryKey() {
		return _mensajeId;
	}

	public void setPrimaryKey(long pk) {
		setMensajeId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getMensajeId() {
		return _mensajeId;
	}

	public void setMensajeId(long mensajeId) {
		_mensajeId = mensajeId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getRemitente() {
		return _remitente;
	}

	public void setRemitente(String remitente) {
		_remitente = remitente;
	}

	public String getDestinatario() {
		return _destinatario;
	}

	public void setDestinatario(String destinatario) {
		_destinatario = destinatario;
	}

	public String getMensaje() {
		return _mensaje;
	}

	public void setMensaje(String mensaje) {
		_mensaje = mensaje;
	}

	public Date getFecha() {
		return _fecha;
	}

	public void setFecha(Date fecha) {
		_fecha = fecha;
	}

	private String _uuid;
	private long _mensajeId;
	private long _companyId;
	private String _remitente;
	private String _destinatario;
	private String _mensaje;
	private Date _fecha;
}