/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import service.tfg.model.Empresa;

import java.util.List;

/**
 * The persistence utility for the empresa service. This utility wraps {@link service.tfg.service.persistence.impl.EmpresaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see EmpresaPersistence
 * @see service.tfg.service.persistence.impl.EmpresaPersistenceImpl
 * @generated
 */
@ProviderType
public class EmpresaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Empresa empresa) {
		getPersistence().clearCache(empresa);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Empresa> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Empresa> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Empresa> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Empresa update(Empresa empresa) {
		return getPersistence().update(empresa);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Empresa update(Empresa empresa, ServiceContext serviceContext) {
		return getPersistence().update(empresa, serviceContext);
	}

	/**
	* Returns all the empresas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching empresas
	*/
	public static List<Empresa> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public static List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the empresas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByUuid_First(java.lang.String uuid,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the empresas before and after the current empresa in the ordered set where uuid = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa[] findByUuid_PrevAndNext(long empresaId,
		java.lang.String uuid, OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findByUuid_PrevAndNext(empresaId, uuid, orderByComparator);
	}

	/**
	* Removes all the empresas where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of empresas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching empresas
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching empresas
	*/
	public static List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public static List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the empresas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Empresa> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the empresas before and after the current empresa in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa[] findByUuid_C_PrevAndNext(long empresaId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(empresaId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the empresas where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of empresas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching empresas
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the empresas where email = &#63;.
	*
	* @param email the email
	* @return the matching empresas
	*/
	public static List<Empresa> findByEmailId(java.lang.String email) {
		return getPersistence().findByEmailId(email);
	}

	/**
	* Returns a range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public static List<Empresa> findByEmailId(java.lang.String email,
		int start, int end) {
		return getPersistence().findByEmailId(email, start, end);
	}

	/**
	* Returns an ordered range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByEmailId(java.lang.String email,
		int start, int end, OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .findByEmailId(email, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the empresas where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findByEmailId(java.lang.String email,
		int start, int end, OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByEmailId(email, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByEmailId_First(java.lang.String email,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().findByEmailId_First(email, orderByComparator);
	}

	/**
	* Returns the first empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByEmailId_First(java.lang.String email,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().fetchByEmailId_First(email, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findByEmailId_Last(java.lang.String email,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().findByEmailId_Last(email, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchByEmailId_Last(java.lang.String email,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().fetchByEmailId_Last(email, orderByComparator);
	}

	/**
	* Returns the empresas before and after the current empresa in the ordered set where email = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa[] findByEmailId_PrevAndNext(long empresaId,
		java.lang.String email, OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findByEmailId_PrevAndNext(empresaId, email,
			orderByComparator);
	}

	/**
	* Removes all the empresas where email = &#63; from the database.
	*
	* @param email the email
	*/
	public static void removeByEmailId(java.lang.String email) {
		getPersistence().removeByEmailId(email);
	}

	/**
	* Returns the number of empresas where email = &#63;.
	*
	* @param email the email
	* @return the number of matching empresas
	*/
	public static int countByEmailId(java.lang.String email) {
		return getPersistence().countByEmailId(email);
	}

	/**
	* Returns all the empresas where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching empresas
	*/
	public static List<Empresa> findBynombreEmpresa(java.lang.String nombre) {
		return getPersistence().findBynombreEmpresa(nombre);
	}

	/**
	* Returns a range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of matching empresas
	*/
	public static List<Empresa> findBynombreEmpresa(java.lang.String nombre,
		int start, int end) {
		return getPersistence().findBynombreEmpresa(nombre, start, end);
	}

	/**
	* Returns an ordered range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findBynombreEmpresa(java.lang.String nombre,
		int start, int end, OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .findBynombreEmpresa(nombre, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the empresas where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching empresas
	*/
	public static List<Empresa> findBynombreEmpresa(java.lang.String nombre,
		int start, int end, OrderByComparator<Empresa> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBynombreEmpresa(nombre, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findBynombreEmpresa_First(java.lang.String nombre,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findBynombreEmpresa_First(nombre, orderByComparator);
	}

	/**
	* Returns the first empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchBynombreEmpresa_First(java.lang.String nombre,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .fetchBynombreEmpresa_First(nombre, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa
	* @throws NoSuchEmpresaException if a matching empresa could not be found
	*/
	public static Empresa findBynombreEmpresa_Last(java.lang.String nombre,
		OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findBynombreEmpresa_Last(nombre, orderByComparator);
	}

	/**
	* Returns the last empresa in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static Empresa fetchBynombreEmpresa_Last(java.lang.String nombre,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence()
				   .fetchBynombreEmpresa_Last(nombre, orderByComparator);
	}

	/**
	* Returns the empresas before and after the current empresa in the ordered set where nombre = &#63;.
	*
	* @param empresaId the primary key of the current empresa
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa[] findBynombreEmpresa_PrevAndNext(long empresaId,
		java.lang.String nombre, OrderByComparator<Empresa> orderByComparator)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence()
				   .findBynombreEmpresa_PrevAndNext(empresaId, nombre,
			orderByComparator);
	}

	/**
	* Removes all the empresas where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public static void removeBynombreEmpresa(java.lang.String nombre) {
		getPersistence().removeBynombreEmpresa(nombre);
	}

	/**
	* Returns the number of empresas where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching empresas
	*/
	public static int countBynombreEmpresa(java.lang.String nombre) {
		return getPersistence().countBynombreEmpresa(nombre);
	}

	/**
	* Caches the empresa in the entity cache if it is enabled.
	*
	* @param empresa the empresa
	*/
	public static void cacheResult(Empresa empresa) {
		getPersistence().cacheResult(empresa);
	}

	/**
	* Caches the empresas in the entity cache if it is enabled.
	*
	* @param empresas the empresas
	*/
	public static void cacheResult(List<Empresa> empresas) {
		getPersistence().cacheResult(empresas);
	}

	/**
	* Creates a new empresa with the primary key. Does not add the empresa to the database.
	*
	* @param empresaId the primary key for the new empresa
	* @return the new empresa
	*/
	public static Empresa create(long empresaId) {
		return getPersistence().create(empresaId);
	}

	/**
	* Removes the empresa with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa that was removed
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa remove(long empresaId)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().remove(empresaId);
	}

	public static Empresa updateImpl(Empresa empresa) {
		return getPersistence().updateImpl(empresa);
	}

	/**
	* Returns the empresa with the primary key or throws a {@link NoSuchEmpresaException} if it could not be found.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa
	* @throws NoSuchEmpresaException if a empresa with the primary key could not be found
	*/
	public static Empresa findByPrimaryKey(long empresaId)
		throws service.tfg.exception.NoSuchEmpresaException {
		return getPersistence().findByPrimaryKey(empresaId);
	}

	/**
	* Returns the empresa with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa, or <code>null</code> if a empresa with the primary key could not be found
	*/
	public static Empresa fetchByPrimaryKey(long empresaId) {
		return getPersistence().fetchByPrimaryKey(empresaId);
	}

	public static java.util.Map<java.io.Serializable, Empresa> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the empresas.
	*
	* @return the empresas
	*/
	public static List<Empresa> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of empresas
	*/
	public static List<Empresa> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of empresas
	*/
	public static List<Empresa> findAll(int start, int end,
		OrderByComparator<Empresa> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of empresas
	*/
	public static List<Empresa> findAll(int start, int end,
		OrderByComparator<Empresa> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the empresas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of empresas.
	*
	* @return the number of empresas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	* Returns the primaryKeys of ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return long[] of the primaryKeys of ofertas associated with the empresa
	*/
	public static long[] getOfertaPrimaryKeys(long pk) {
		return getPersistence().getOfertaPrimaryKeys(pk);
	}

	/**
	* Returns all the ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return the ofertas associated with the empresa
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk) {
		return getPersistence().getOfertas(pk);
	}

	/**
	* Returns a range of all the ofertas associated with the empresa.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the empresa
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of ofertas associated with the empresa
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end) {
		return getPersistence().getOfertas(pk, start, end);
	}

	/**
	* Returns an ordered range of all the ofertas associated with the empresa.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the empresa
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas associated with the empresa
	*/
	public static List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return getPersistence().getOfertas(pk, start, end, orderByComparator);
	}

	/**
	* Returns the number of ofertas associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @return the number of ofertas associated with the empresa
	*/
	public static int getOfertasSize(long pk) {
		return getPersistence().getOfertasSize(pk);
	}

	/**
	* Returns <code>true</code> if the oferta is associated with the empresa.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	* @return <code>true</code> if the oferta is associated with the empresa; <code>false</code> otherwise
	*/
	public static boolean containsOferta(long pk, long ofertaPK) {
		return getPersistence().containsOferta(pk, ofertaPK);
	}

	/**
	* Returns <code>true</code> if the empresa has any ofertas associated with it.
	*
	* @param pk the primary key of the empresa to check for associations with ofertas
	* @return <code>true</code> if the empresa has any ofertas associated with it; <code>false</code> otherwise
	*/
	public static boolean containsOfertas(long pk) {
		return getPersistence().containsOfertas(pk);
	}

	/**
	* Adds an association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	*/
	public static void addOferta(long pk, long ofertaPK) {
		getPersistence().addOferta(pk, ofertaPK);
	}

	/**
	* Adds an association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param oferta the oferta
	*/
	public static void addOferta(long pk, service.tfg.model.Oferta oferta) {
		getPersistence().addOferta(pk, oferta);
	}

	/**
	* Adds an association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public static void addOfertas(long pk, long[] ofertaPKs) {
		getPersistence().addOfertas(pk, ofertaPKs);
	}

	/**
	* Adds an association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas
	*/
	public static void addOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().addOfertas(pk, ofertas);
	}

	/**
	* Clears all associations between the empresa and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa to clear the associated ofertas from
	*/
	public static void clearOfertas(long pk) {
		getPersistence().clearOfertas(pk);
	}

	/**
	* Removes the association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPK the primary key of the oferta
	*/
	public static void removeOferta(long pk, long ofertaPK) {
		getPersistence().removeOferta(pk, ofertaPK);
	}

	/**
	* Removes the association between the empresa and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param oferta the oferta
	*/
	public static void removeOferta(long pk, service.tfg.model.Oferta oferta) {
		getPersistence().removeOferta(pk, oferta);
	}

	/**
	* Removes the association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public static void removeOfertas(long pk, long[] ofertaPKs) {
		getPersistence().removeOfertas(pk, ofertaPKs);
	}

	/**
	* Removes the association between the empresa and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas
	*/
	public static void removeOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().removeOfertas(pk, ofertas);
	}

	/**
	* Sets the ofertas associated with the empresa, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertaPKs the primary keys of the ofertas to be associated with the empresa
	*/
	public static void setOfertas(long pk, long[] ofertaPKs) {
		getPersistence().setOfertas(pk, ofertaPKs);
	}

	/**
	* Sets the ofertas associated with the empresa, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the empresa
	* @param ofertas the ofertas to be associated with the empresa
	*/
	public static void setOfertas(long pk,
		List<service.tfg.model.Oferta> ofertas) {
		getPersistence().setOfertas(pk, ofertas);
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static EmpresaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EmpresaPersistence, EmpresaPersistence> _serviceTracker =
		ServiceTrackerFactory.open(EmpresaPersistence.class);
}