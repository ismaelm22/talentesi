/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProfesorLocalService}.
 *
 * @author Ismael Medina
 * @see ProfesorLocalService
 * @generated
 */
@ProviderType
public class ProfesorLocalServiceWrapper implements ProfesorLocalService,
	ServiceWrapper<ProfesorLocalService> {
	public ProfesorLocalServiceWrapper(
		ProfesorLocalService profesorLocalService) {
		_profesorLocalService = profesorLocalService;
	}

	@Override
	public boolean hasOfertaProfesor(long ofertaId, long profesorId) {
		return _profesorLocalService.hasOfertaProfesor(ofertaId, profesorId);
	}

	@Override
	public boolean hasOfertaProfesors(long ofertaId) {
		return _profesorLocalService.hasOfertaProfesors(ofertaId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _profesorLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _profesorLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _profesorLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _profesorLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _profesorLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int getOfertaProfesorsCount(long ofertaId) {
		return _profesorLocalService.getOfertaProfesorsCount(ofertaId);
	}

	/**
	* Returns the number of profesors.
	*
	* @return the number of profesors
	*/
	@Override
	public int getProfesorsCount() {
		return _profesorLocalService.getProfesorsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _profesorLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _profesorLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _profesorLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _profesorLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<service.tfg.model.Profesor> findByEmailId(
		java.lang.String email) {
		return _profesorLocalService.findByEmailId(email);
	}

	@Override
	public java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId) {
		return _profesorLocalService.getOfertaProfesors(ofertaId);
	}

	@Override
	public java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId, int start, int end) {
		return _profesorLocalService.getOfertaProfesors(ofertaId, start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Profesor> orderByComparator) {
		return _profesorLocalService.getOfertaProfesors(ofertaId, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of profesors
	*/
	@Override
	public java.util.List<service.tfg.model.Profesor> getProfesors(int start,
		int end) {
		return _profesorLocalService.getProfesors(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _profesorLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _profesorLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the ofertaIds of the ofertas associated with the profesor.
	*
	* @param profesorId the profesorId of the profesor
	* @return long[] the ofertaIds of ofertas associated with the profesor
	*/
	@Override
	public long[] getOfertaPrimaryKeys(long profesorId) {
		return _profesorLocalService.getOfertaPrimaryKeys(profesorId);
	}

	/**
	* Adds the profesor to the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was added
	*/
	@Override
	public service.tfg.model.Profesor addProfesor(
		service.tfg.model.Profesor profesor) {
		return _profesorLocalService.addProfesor(profesor);
	}

	/**
	* Creates a new profesor with the primary key. Does not add the profesor to the database.
	*
	* @param profesorId the primary key for the new profesor
	* @return the new profesor
	*/
	@Override
	public service.tfg.model.Profesor createProfesor(long profesorId) {
		return _profesorLocalService.createProfesor(profesorId);
	}

	/**
	* Deletes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor that was removed
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Profesor deleteProfesor(long profesorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _profesorLocalService.deleteProfesor(profesorId);
	}

	/**
	* Deletes the profesor from the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was removed
	*/
	@Override
	public service.tfg.model.Profesor deleteProfesor(
		service.tfg.model.Profesor profesor) {
		return _profesorLocalService.deleteProfesor(profesor);
	}

	@Override
	public service.tfg.model.Profesor fetchProfesor(long profesorId) {
		return _profesorLocalService.fetchProfesor(profesorId);
	}

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	@Override
	public service.tfg.model.Profesor fetchProfesorByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _profesorLocalService.fetchProfesorByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the profesor with the primary key.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Profesor getProfesor(long profesorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _profesorLocalService.getProfesor(profesorId);
	}

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor
	* @throws PortalException if a matching profesor could not be found
	*/
	@Override
	public service.tfg.model.Profesor getProfesorByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _profesorLocalService.getProfesorByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Updates the profesor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was updated
	*/
	@Override
	public service.tfg.model.Profesor updateProfesor(
		service.tfg.model.Profesor profesor) {
		return _profesorLocalService.updateProfesor(profesor);
	}

	@Override
	public void addOfertaProfesor(long ofertaId, long profesorId) {
		_profesorLocalService.addOfertaProfesor(ofertaId, profesorId);
	}

	@Override
	public void addOfertaProfesor(long ofertaId,
		service.tfg.model.Profesor profesor) {
		_profesorLocalService.addOfertaProfesor(ofertaId, profesor);
	}

	@Override
	public void addOfertaProfesors(long ofertaId,
		java.util.List<service.tfg.model.Profesor> profesors) {
		_profesorLocalService.addOfertaProfesors(ofertaId, profesors);
	}

	@Override
	public void addOfertaProfesors(long ofertaId, long[] profesorIds) {
		_profesorLocalService.addOfertaProfesors(ofertaId, profesorIds);
	}

	@Override
	public void addProfesor(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String apellidos,
		java.lang.String area, java.lang.String despacho, long companyId) {
		_profesorLocalService.addProfesor(email, pwd, nombre, apellidos, area,
			despacho, companyId);
	}

	@Override
	public void clearOfertaProfesors(long ofertaId) {
		_profesorLocalService.clearOfertaProfesors(ofertaId);
	}

	@Override
	public void deleteOfertaProfesor(long ofertaId, long profesorId) {
		_profesorLocalService.deleteOfertaProfesor(ofertaId, profesorId);
	}

	@Override
	public void deleteOfertaProfesor(long ofertaId,
		service.tfg.model.Profesor profesor) {
		_profesorLocalService.deleteOfertaProfesor(ofertaId, profesor);
	}

	@Override
	public void deleteOfertaProfesors(long ofertaId,
		java.util.List<service.tfg.model.Profesor> profesors) {
		_profesorLocalService.deleteOfertaProfesors(ofertaId, profesors);
	}

	@Override
	public void deleteOfertaProfesors(long ofertaId, long[] profesorIds) {
		_profesorLocalService.deleteOfertaProfesors(ofertaId, profesorIds);
	}

	@Override
	public void setOfertaProfesors(long ofertaId, long[] profesorIds) {
		_profesorLocalService.setOfertaProfesors(ofertaId, profesorIds);
	}

	@Override
	public void updateProfesor(long profesorId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String area,
		java.lang.String despacho)
		throws com.liferay.portal.kernel.exception.PortalException {
		_profesorLocalService.updateProfesor(profesorId, nombre, apellidos,
			area, despacho);
	}

	@Override
	public ProfesorLocalService getWrappedService() {
		return _profesorLocalService;
	}

	@Override
	public void setWrappedService(ProfesorLocalService profesorLocalService) {
		_profesorLocalService = profesorLocalService;
	}

	private ProfesorLocalService _profesorLocalService;
}