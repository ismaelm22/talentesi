/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Empresa. This utility wraps
 * {@link service.tfg.service.impl.EmpresaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Ismael Medina
 * @see EmpresaLocalService
 * @see service.tfg.service.base.EmpresaLocalServiceBaseImpl
 * @see service.tfg.service.impl.EmpresaLocalServiceImpl
 * @generated
 */
@ProviderType
public class EmpresaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link service.tfg.service.impl.EmpresaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean hasOfertaEmpresa(long ofertaId, long empresaId) {
		return getService().hasOfertaEmpresa(ofertaId, empresaId);
	}

	public static boolean hasOfertaEmpresas(long ofertaId) {
		return getService().hasOfertaEmpresas(ofertaId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of empresas.
	*
	* @return the number of empresas
	*/
	public static int getEmpresasCount() {
		return getService().getEmpresasCount();
	}

	public static int getOfertaEmpresasCount(long ofertaId) {
		return getService().getOfertaEmpresasCount(ofertaId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Empresa> findByEmailId(
		java.lang.String email) {
		return getService().findByEmailId(email);
	}

	/**
	* Returns a range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of empresas
	*/
	public static java.util.List<service.tfg.model.Empresa> getEmpresas(
		int start, int end) {
		return getService().getEmpresas(start, end);
	}

	public static java.util.List<service.tfg.model.Empresa> getOfertaEmpresas(
		long ofertaId) {
		return getService().getOfertaEmpresas(ofertaId);
	}

	public static java.util.List<service.tfg.model.Empresa> getOfertaEmpresas(
		long ofertaId, int start, int end) {
		return getService().getOfertaEmpresas(ofertaId, start, end);
	}

	public static java.util.List<service.tfg.model.Empresa> getOfertaEmpresas(
		long ofertaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Empresa> orderByComparator) {
		return getService()
				   .getOfertaEmpresas(ofertaId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the ofertaIds of the ofertas associated with the empresa.
	*
	* @param empresaId the empresaId of the empresa
	* @return long[] the ofertaIds of ofertas associated with the empresa
	*/
	public static long[] getOfertaPrimaryKeys(long empresaId) {
		return getService().getOfertaPrimaryKeys(empresaId);
	}

	/**
	* Adds the empresa to the database. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was added
	*/
	public static service.tfg.model.Empresa addEmpresa(
		service.tfg.model.Empresa empresa) {
		return getService().addEmpresa(empresa);
	}

	/**
	* Creates a new empresa with the primary key. Does not add the empresa to the database.
	*
	* @param empresaId the primary key for the new empresa
	* @return the new empresa
	*/
	public static service.tfg.model.Empresa createEmpresa(long empresaId) {
		return getService().createEmpresa(empresaId);
	}

	/**
	* Deletes the empresa with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa that was removed
	* @throws PortalException if a empresa with the primary key could not be found
	*/
	public static service.tfg.model.Empresa deleteEmpresa(long empresaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteEmpresa(empresaId);
	}

	/**
	* Deletes the empresa from the database. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was removed
	*/
	public static service.tfg.model.Empresa deleteEmpresa(
		service.tfg.model.Empresa empresa) {
		return getService().deleteEmpresa(empresa);
	}

	public static service.tfg.model.Empresa fetchEmpresa(long empresaId) {
		return getService().fetchEmpresa(empresaId);
	}

	/**
	* Returns the empresa with the matching UUID and company.
	*
	* @param uuid the empresa's UUID
	* @param companyId the primary key of the company
	* @return the matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	public static service.tfg.model.Empresa fetchEmpresaByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchEmpresaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the empresa with the primary key.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa
	* @throws PortalException if a empresa with the primary key could not be found
	*/
	public static service.tfg.model.Empresa getEmpresa(long empresaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEmpresa(empresaId);
	}

	/**
	* Returns the empresa with the matching UUID and company.
	*
	* @param uuid the empresa's UUID
	* @param companyId the primary key of the company
	* @return the matching empresa
	* @throws PortalException if a matching empresa could not be found
	*/
	public static service.tfg.model.Empresa getEmpresaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEmpresaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the empresa in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was updated
	*/
	public static service.tfg.model.Empresa updateEmpresa(
		service.tfg.model.Empresa empresa) {
		return getService().updateEmpresa(empresa);
	}

	public static void addEmpresa(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String cif,
		java.lang.String direccion, java.lang.String telefono, long companyId) {
		getService()
			.addEmpresa(email, pwd, nombre, cif, direccion, telefono, companyId);
	}

	public static void addOfertaEmpresa(long ofertaId, long empresaId) {
		getService().addOfertaEmpresa(ofertaId, empresaId);
	}

	public static void addOfertaEmpresa(long ofertaId,
		service.tfg.model.Empresa empresa) {
		getService().addOfertaEmpresa(ofertaId, empresa);
	}

	public static void addOfertaEmpresas(long ofertaId,
		java.util.List<service.tfg.model.Empresa> empresas) {
		getService().addOfertaEmpresas(ofertaId, empresas);
	}

	public static void addOfertaEmpresas(long ofertaId, long[] empresaIds) {
		getService().addOfertaEmpresas(ofertaId, empresaIds);
	}

	public static void clearOfertaEmpresas(long ofertaId) {
		getService().clearOfertaEmpresas(ofertaId);
	}

	public static void deleteOfertaEmpresa(long ofertaId, long empresaId) {
		getService().deleteOfertaEmpresa(ofertaId, empresaId);
	}

	public static void deleteOfertaEmpresa(long ofertaId,
		service.tfg.model.Empresa empresa) {
		getService().deleteOfertaEmpresa(ofertaId, empresa);
	}

	public static void deleteOfertaEmpresas(long ofertaId,
		java.util.List<service.tfg.model.Empresa> empresas) {
		getService().deleteOfertaEmpresas(ofertaId, empresas);
	}

	public static void deleteOfertaEmpresas(long ofertaId, long[] empresaIds) {
		getService().deleteOfertaEmpresas(ofertaId, empresaIds);
	}

	public static void setOfertaEmpresas(long ofertaId, long[] empresaIds) {
		getService().setOfertaEmpresas(ofertaId, empresaIds);
	}

	public static void updateEmpresa(long empresaId, java.lang.String nombre,
		java.lang.String cif, java.lang.String direccion,
		java.lang.String telefono)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateEmpresa(empresaId, nombre, cif, direccion, telefono);
	}

	public static EmpresaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EmpresaLocalService, EmpresaLocalService> _serviceTracker =
		ServiceTrackerFactory.open(EmpresaLocalService.class);
}