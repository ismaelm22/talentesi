/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Alumno. This utility wraps
 * {@link service.tfg.service.impl.AlumnoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Ismael Medina
 * @see AlumnoLocalService
 * @see service.tfg.service.base.AlumnoLocalServiceBaseImpl
 * @see service.tfg.service.impl.AlumnoLocalServiceImpl
 * @generated
 */
@ProviderType
public class AlumnoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link service.tfg.service.impl.AlumnoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean hasOfertaAlumno(long ofertaId, long alumnoId) {
		return getService().hasOfertaAlumno(ofertaId, alumnoId);
	}

	public static boolean hasOfertaAlumnos(long ofertaId) {
		return getService().hasOfertaAlumnos(ofertaId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of alumnos.
	*
	* @return the number of alumnos
	*/
	public static int getAlumnosCount() {
		return getService().getAlumnosCount();
	}

	public static int getOfertaAlumnosCount(long ofertaId) {
		return getService().getOfertaAlumnosCount(ofertaId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Alumno> findByEmailId(
		java.lang.String email) {
		return getService().findByEmailId(email);
	}

	/**
	* Returns a range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of alumnos
	*/
	public static java.util.List<service.tfg.model.Alumno> getAlumnos(
		int start, int end) {
		return getService().getAlumnos(start, end);
	}

	public static java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId) {
		return getService().getOfertaAlumnos(ofertaId);
	}

	public static java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId, int start, int end) {
		return getService().getOfertaAlumnos(ofertaId, start, end);
	}

	public static java.util.List<service.tfg.model.Alumno> getOfertaAlumnos(
		long ofertaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Alumno> orderByComparator) {
		return getService()
				   .getOfertaAlumnos(ofertaId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the ofertaIds of the ofertas associated with the alumno.
	*
	* @param alumnoId the alumnoId of the alumno
	* @return long[] the ofertaIds of ofertas associated with the alumno
	*/
	public static long[] getOfertaPrimaryKeys(long alumnoId) {
		return getService().getOfertaPrimaryKeys(alumnoId);
	}

	/**
	* Adds the alumno to the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was added
	*/
	public static service.tfg.model.Alumno addAlumno(
		service.tfg.model.Alumno alumno) {
		return getService().addAlumno(alumno);
	}

	/**
	* Creates a new alumno with the primary key. Does not add the alumno to the database.
	*
	* @param alumnoId the primary key for the new alumno
	* @return the new alumno
	*/
	public static service.tfg.model.Alumno createAlumno(long alumnoId) {
		return getService().createAlumno(alumnoId);
	}

	/**
	* Deletes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno that was removed
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	public static service.tfg.model.Alumno deleteAlumno(long alumnoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteAlumno(alumnoId);
	}

	/**
	* Deletes the alumno from the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was removed
	*/
	public static service.tfg.model.Alumno deleteAlumno(
		service.tfg.model.Alumno alumno) {
		return getService().deleteAlumno(alumno);
	}

	public static service.tfg.model.Alumno fetchAlumno(long alumnoId) {
		return getService().fetchAlumno(alumnoId);
	}

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	public static service.tfg.model.Alumno fetchAlumnoByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchAlumnoByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the alumno with the primary key.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	public static service.tfg.model.Alumno getAlumno(long alumnoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getAlumno(alumnoId);
	}

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno
	* @throws PortalException if a matching alumno could not be found
	*/
	public static service.tfg.model.Alumno getAlumnoByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getAlumnoByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the alumno in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was updated
	*/
	public static service.tfg.model.Alumno updateAlumno(
		service.tfg.model.Alumno alumno) {
		return getService().updateAlumno(alumno);
	}

	public static service.tfg.model.AlumnoCurriculumBlobModel getCurriculumBlobModel(
		java.io.Serializable primaryKey) {
		return getService().getCurriculumBlobModel(primaryKey);
	}

	public static void addAlumno(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String apellidos,
		java.lang.String competencias, java.sql.Blob curriculum, long ofertaId,
		long companyId) {
		getService()
			.addAlumno(email, pwd, nombre, apellidos, competencias, curriculum,
			ofertaId, companyId);
	}

	public static void addOfertaAlumno(long ofertaId, long alumnoId) {
		getService().addOfertaAlumno(ofertaId, alumnoId);
	}

	public static void addOfertaAlumno(long ofertaId,
		service.tfg.model.Alumno alumno) {
		getService().addOfertaAlumno(ofertaId, alumno);
	}

	public static void addOfertaAlumnos(long ofertaId,
		java.util.List<service.tfg.model.Alumno> alumnos) {
		getService().addOfertaAlumnos(ofertaId, alumnos);
	}

	public static void addOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		getService().addOfertaAlumnos(ofertaId, alumnoIds);
	}

	public static void clearOfertaAlumnos(long ofertaId) {
		getService().clearOfertaAlumnos(ofertaId);
	}

	public static void deleteOfertaAlumno(long ofertaId, long alumnoId) {
		getService().deleteOfertaAlumno(ofertaId, alumnoId);
	}

	public static void deleteOfertaAlumno(long ofertaId,
		service.tfg.model.Alumno alumno) {
		getService().deleteOfertaAlumno(ofertaId, alumno);
	}

	public static void deleteOfertaAlumnos(long ofertaId,
		java.util.List<service.tfg.model.Alumno> alumnos) {
		getService().deleteOfertaAlumnos(ofertaId, alumnos);
	}

	public static void deleteOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		getService().deleteOfertaAlumnos(ofertaId, alumnoIds);
	}

	public static void setOfertaAlumnos(long ofertaId, long[] alumnoIds) {
		getService().setOfertaAlumnos(ofertaId, alumnoIds);
	}

	public static void updateAlumno(long alumnoId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String competencias,
		java.sql.Blob curriculum)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService()
			.updateAlumno(alumnoId, nombre, apellidos, competencias, curriculum);
	}

	public static void updateAlumnoOferta(long alumnoId, long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService().updateAlumnoOferta(alumnoId, ofertaId);
	}

	public static AlumnoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AlumnoLocalService, AlumnoLocalService> _serviceTracker =
		ServiceTrackerFactory.open(AlumnoLocalService.class);
}