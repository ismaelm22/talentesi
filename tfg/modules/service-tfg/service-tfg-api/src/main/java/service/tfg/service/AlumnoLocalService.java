/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import service.tfg.model.Alumno;
import service.tfg.model.AlumnoCurriculumBlobModel;

import java.io.Serializable;

import java.sql.Blob;

import java.util.List;

/**
 * Provides the local service interface for Alumno. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Ismael Medina
 * @see AlumnoLocalServiceUtil
 * @see service.tfg.service.base.AlumnoLocalServiceBaseImpl
 * @see service.tfg.service.impl.AlumnoLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface AlumnoLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AlumnoLocalServiceUtil} to access the alumno local service. Add custom service methods to {@link service.tfg.service.impl.AlumnoLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaAlumno(long ofertaId, long alumnoId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaAlumnos(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Returns the number of alumnos.
	*
	* @return the number of alumnos
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAlumnosCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getOfertaAlumnosCount(long ofertaId);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	public List<Alumno> findByEmailId(java.lang.String email);

	/**
	* Returns a range of all the alumnos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of alumnos
	* @param end the upper bound of the range of alumnos (not inclusive)
	* @return the range of alumnos
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Alumno> getAlumnos(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Alumno> getOfertaAlumnos(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Alumno> getOfertaAlumnos(long ofertaId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Alumno> getOfertaAlumnos(long ofertaId, int start, int end,
		OrderByComparator<Alumno> orderByComparator);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Returns the ofertaIds of the ofertas associated with the alumno.
	*
	* @param alumnoId the alumnoId of the alumno
	* @return long[] the ofertaIds of ofertas associated with the alumno
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getOfertaPrimaryKeys(long alumnoId);

	/**
	* Adds the alumno to the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Alumno addAlumno(Alumno alumno);

	/**
	* Creates a new alumno with the primary key. Does not add the alumno to the database.
	*
	* @param alumnoId the primary key for the new alumno
	* @return the new alumno
	*/
	public Alumno createAlumno(long alumnoId);

	/**
	* Deletes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno that was removed
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Alumno deleteAlumno(long alumnoId) throws PortalException;

	/**
	* Deletes the alumno from the database. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Alumno deleteAlumno(Alumno alumno);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Alumno fetchAlumno(long alumnoId);

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno, or <code>null</code> if a matching alumno could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Alumno fetchAlumnoByUuidAndCompanyId(java.lang.String uuid,
		long companyId);

	/**
	* Returns the alumno with the primary key.
	*
	* @param alumnoId the primary key of the alumno
	* @return the alumno
	* @throws PortalException if a alumno with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Alumno getAlumno(long alumnoId) throws PortalException;

	/**
	* Returns the alumno with the matching UUID and company.
	*
	* @param uuid the alumno's UUID
	* @param companyId the primary key of the company
	* @return the matching alumno
	* @throws PortalException if a matching alumno could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Alumno getAlumnoByUuidAndCompanyId(java.lang.String uuid,
		long companyId) throws PortalException;

	/**
	* Updates the alumno in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param alumno the alumno
	* @return the alumno that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Alumno updateAlumno(Alumno alumno);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AlumnoCurriculumBlobModel getCurriculumBlobModel(
		Serializable primaryKey);

	public void addAlumno(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String apellidos,
		java.lang.String competencias, Blob curriculum, long ofertaId,
		long companyId);

	public void addOfertaAlumno(long ofertaId, long alumnoId);

	public void addOfertaAlumno(long ofertaId, Alumno alumno);

	public void addOfertaAlumnos(long ofertaId, List<Alumno> alumnos);

	public void addOfertaAlumnos(long ofertaId, long[] alumnoIds);

	public void clearOfertaAlumnos(long ofertaId);

	public void deleteOfertaAlumno(long ofertaId, long alumnoId);

	public void deleteOfertaAlumno(long ofertaId, Alumno alumno);

	public void deleteOfertaAlumnos(long ofertaId, List<Alumno> alumnos);

	public void deleteOfertaAlumnos(long ofertaId, long[] alumnoIds);

	public void setOfertaAlumnos(long ofertaId, long[] alumnoIds);

	public void updateAlumno(long alumnoId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String competencias,
		Blob curriculum) throws PortalException;

	public void updateAlumnoOferta(long alumnoId, long ofertaId)
		throws PortalException;
}