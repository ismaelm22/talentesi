/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the Empresa service. Represents a row in the &quot;BBDD_Empresa&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link service.tfg.model.impl.EmpresaModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link service.tfg.model.impl.EmpresaImpl}.
 * </p>
 *
 * @author Ismael Medina
 * @see Empresa
 * @see service.tfg.model.impl.EmpresaImpl
 * @see service.tfg.model.impl.EmpresaModelImpl
 * @generated
 */
@ProviderType
public interface EmpresaModel extends BaseModel<Empresa>, ShardedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a empresa model instance should use the {@link Empresa} interface instead.
	 */

	/**
	 * Returns the primary key of this empresa.
	 *
	 * @return the primary key of this empresa
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this empresa.
	 *
	 * @param primaryKey the primary key of this empresa
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this empresa.
	 *
	 * @return the uuid of this empresa
	 */
	@AutoEscape
	public String getUuid();

	/**
	 * Sets the uuid of this empresa.
	 *
	 * @param uuid the uuid of this empresa
	 */
	public void setUuid(String uuid);

	/**
	 * Returns the empresa ID of this empresa.
	 *
	 * @return the empresa ID of this empresa
	 */
	public long getEmpresaId();

	/**
	 * Sets the empresa ID of this empresa.
	 *
	 * @param empresaId the empresa ID of this empresa
	 */
	public void setEmpresaId(long empresaId);

	/**
	 * Returns the company ID of this empresa.
	 *
	 * @return the company ID of this empresa
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this empresa.
	 *
	 * @param companyId the company ID of this empresa
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the email of this empresa.
	 *
	 * @return the email of this empresa
	 */
	@AutoEscape
	public String getEmail();

	/**
	 * Sets the email of this empresa.
	 *
	 * @param email the email of this empresa
	 */
	public void setEmail(String email);

	/**
	 * Returns the pwd of this empresa.
	 *
	 * @return the pwd of this empresa
	 */
	@AutoEscape
	public String getPwd();

	/**
	 * Sets the pwd of this empresa.
	 *
	 * @param pwd the pwd of this empresa
	 */
	public void setPwd(String pwd);

	/**
	 * Returns the nombre of this empresa.
	 *
	 * @return the nombre of this empresa
	 */
	@AutoEscape
	public String getNombre();

	/**
	 * Sets the nombre of this empresa.
	 *
	 * @param nombre the nombre of this empresa
	 */
	public void setNombre(String nombre);

	/**
	 * Returns the cif of this empresa.
	 *
	 * @return the cif of this empresa
	 */
	@AutoEscape
	public String getCif();

	/**
	 * Sets the cif of this empresa.
	 *
	 * @param cif the cif of this empresa
	 */
	public void setCif(String cif);

	/**
	 * Returns the direccion of this empresa.
	 *
	 * @return the direccion of this empresa
	 */
	@AutoEscape
	public String getDireccion();

	/**
	 * Sets the direccion of this empresa.
	 *
	 * @param direccion the direccion of this empresa
	 */
	public void setDireccion(String direccion);

	/**
	 * Returns the telefono of this empresa.
	 *
	 * @return the telefono of this empresa
	 */
	@AutoEscape
	public String getTelefono();

	/**
	 * Sets the telefono of this empresa.
	 *
	 * @param telefono the telefono of this empresa
	 */
	public void setTelefono(String telefono);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(service.tfg.model.Empresa empresa);

	@Override
	public int hashCode();

	@Override
	public CacheModel<service.tfg.model.Empresa> toCacheModel();

	@Override
	public service.tfg.model.Empresa toEscapedModel();

	@Override
	public service.tfg.model.Empresa toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}