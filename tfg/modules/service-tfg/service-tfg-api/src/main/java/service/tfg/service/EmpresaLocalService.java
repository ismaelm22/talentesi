/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import service.tfg.model.Empresa;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Empresa. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Ismael Medina
 * @see EmpresaLocalServiceUtil
 * @see service.tfg.service.base.EmpresaLocalServiceBaseImpl
 * @see service.tfg.service.impl.EmpresaLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface EmpresaLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmpresaLocalServiceUtil} to access the empresa local service. Add custom service methods to {@link service.tfg.service.impl.EmpresaLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaEmpresa(long ofertaId, long empresaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasOfertaEmpresas(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Returns the number of empresas.
	*
	* @return the number of empresas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEmpresasCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getOfertaEmpresasCount(long ofertaId);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	public List<Empresa> findByEmailId(java.lang.String email);

	/**
	* Returns a range of all the empresas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.EmpresaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of empresas
	* @param end the upper bound of the range of empresas (not inclusive)
	* @return the range of empresas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Empresa> getEmpresas(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Empresa> getOfertaEmpresas(long ofertaId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Empresa> getOfertaEmpresas(long ofertaId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Empresa> getOfertaEmpresas(long ofertaId, int start, int end,
		OrderByComparator<Empresa> orderByComparator);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Returns the ofertaIds of the ofertas associated with the empresa.
	*
	* @param empresaId the empresaId of the empresa
	* @return long[] the ofertaIds of ofertas associated with the empresa
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getOfertaPrimaryKeys(long empresaId);

	/**
	* Adds the empresa to the database. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Empresa addEmpresa(Empresa empresa);

	/**
	* Creates a new empresa with the primary key. Does not add the empresa to the database.
	*
	* @param empresaId the primary key for the new empresa
	* @return the new empresa
	*/
	public Empresa createEmpresa(long empresaId);

	/**
	* Deletes the empresa with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa that was removed
	* @throws PortalException if a empresa with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Empresa deleteEmpresa(long empresaId) throws PortalException;

	/**
	* Deletes the empresa from the database. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Empresa deleteEmpresa(Empresa empresa);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Empresa fetchEmpresa(long empresaId);

	/**
	* Returns the empresa with the matching UUID and company.
	*
	* @param uuid the empresa's UUID
	* @param companyId the primary key of the company
	* @return the matching empresa, or <code>null</code> if a matching empresa could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Empresa fetchEmpresaByUuidAndCompanyId(java.lang.String uuid,
		long companyId);

	/**
	* Returns the empresa with the primary key.
	*
	* @param empresaId the primary key of the empresa
	* @return the empresa
	* @throws PortalException if a empresa with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Empresa getEmpresa(long empresaId) throws PortalException;

	/**
	* Returns the empresa with the matching UUID and company.
	*
	* @param uuid the empresa's UUID
	* @param companyId the primary key of the company
	* @return the matching empresa
	* @throws PortalException if a matching empresa could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Empresa getEmpresaByUuidAndCompanyId(java.lang.String uuid,
		long companyId) throws PortalException;

	/**
	* Updates the empresa in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param empresa the empresa
	* @return the empresa that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Empresa updateEmpresa(Empresa empresa);

	public void addEmpresa(java.lang.String email, java.lang.String pwd,
		java.lang.String nombre, java.lang.String cif,
		java.lang.String direccion, java.lang.String telefono, long companyId);

	public void addOfertaEmpresa(long ofertaId, long empresaId);

	public void addOfertaEmpresa(long ofertaId, Empresa empresa);

	public void addOfertaEmpresas(long ofertaId, List<Empresa> empresas);

	public void addOfertaEmpresas(long ofertaId, long[] empresaIds);

	public void clearOfertaEmpresas(long ofertaId);

	public void deleteOfertaEmpresa(long ofertaId, long empresaId);

	public void deleteOfertaEmpresa(long ofertaId, Empresa empresa);

	public void deleteOfertaEmpresas(long ofertaId, List<Empresa> empresas);

	public void deleteOfertaEmpresas(long ofertaId, long[] empresaIds);

	public void setOfertaEmpresas(long ofertaId, long[] empresaIds);

	public void updateEmpresa(long empresaId, java.lang.String nombre,
		java.lang.String cif, java.lang.String direccion,
		java.lang.String telefono) throws PortalException;
}