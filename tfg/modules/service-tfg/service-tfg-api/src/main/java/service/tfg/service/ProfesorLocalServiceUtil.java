/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Profesor. This utility wraps
 * {@link service.tfg.service.impl.ProfesorLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Ismael Medina
 * @see ProfesorLocalService
 * @see service.tfg.service.base.ProfesorLocalServiceBaseImpl
 * @see service.tfg.service.impl.ProfesorLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProfesorLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link service.tfg.service.impl.ProfesorLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static boolean hasOfertaProfesor(long ofertaId, long profesorId) {
		return getService().hasOfertaProfesor(ofertaId, profesorId);
	}

	public static boolean hasOfertaProfesors(long ofertaId) {
		return getService().hasOfertaProfesors(ofertaId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int getOfertaProfesorsCount(long ofertaId) {
		return getService().getOfertaProfesorsCount(ofertaId);
	}

	/**
	* Returns the number of profesors.
	*
	* @return the number of profesors
	*/
	public static int getProfesorsCount() {
		return getService().getProfesorsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<service.tfg.model.Profesor> findByEmailId(
		java.lang.String email) {
		return getService().findByEmailId(email);
	}

	public static java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId) {
		return getService().getOfertaProfesors(ofertaId);
	}

	public static java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId, int start, int end) {
		return getService().getOfertaProfesors(ofertaId, start, end);
	}

	public static java.util.List<service.tfg.model.Profesor> getOfertaProfesors(
		long ofertaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Profesor> orderByComparator) {
		return getService()
				   .getOfertaProfesors(ofertaId, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of profesors
	*/
	public static java.util.List<service.tfg.model.Profesor> getProfesors(
		int start, int end) {
		return getService().getProfesors(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the ofertaIds of the ofertas associated with the profesor.
	*
	* @param profesorId the profesorId of the profesor
	* @return long[] the ofertaIds of ofertas associated with the profesor
	*/
	public static long[] getOfertaPrimaryKeys(long profesorId) {
		return getService().getOfertaPrimaryKeys(profesorId);
	}

	/**
	* Adds the profesor to the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was added
	*/
	public static service.tfg.model.Profesor addProfesor(
		service.tfg.model.Profesor profesor) {
		return getService().addProfesor(profesor);
	}

	/**
	* Creates a new profesor with the primary key. Does not add the profesor to the database.
	*
	* @param profesorId the primary key for the new profesor
	* @return the new profesor
	*/
	public static service.tfg.model.Profesor createProfesor(long profesorId) {
		return getService().createProfesor(profesorId);
	}

	/**
	* Deletes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor that was removed
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	public static service.tfg.model.Profesor deleteProfesor(long profesorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProfesor(profesorId);
	}

	/**
	* Deletes the profesor from the database. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was removed
	*/
	public static service.tfg.model.Profesor deleteProfesor(
		service.tfg.model.Profesor profesor) {
		return getService().deleteProfesor(profesor);
	}

	public static service.tfg.model.Profesor fetchProfesor(long profesorId) {
		return getService().fetchProfesor(profesorId);
	}

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public static service.tfg.model.Profesor fetchProfesorByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchProfesorByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the profesor with the primary key.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor
	* @throws PortalException if a profesor with the primary key could not be found
	*/
	public static service.tfg.model.Profesor getProfesor(long profesorId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProfesor(profesorId);
	}

	/**
	* Returns the profesor with the matching UUID and company.
	*
	* @param uuid the profesor's UUID
	* @param companyId the primary key of the company
	* @return the matching profesor
	* @throws PortalException if a matching profesor could not be found
	*/
	public static service.tfg.model.Profesor getProfesorByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProfesorByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the profesor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param profesor the profesor
	* @return the profesor that was updated
	*/
	public static service.tfg.model.Profesor updateProfesor(
		service.tfg.model.Profesor profesor) {
		return getService().updateProfesor(profesor);
	}

	public static void addOfertaProfesor(long ofertaId, long profesorId) {
		getService().addOfertaProfesor(ofertaId, profesorId);
	}

	public static void addOfertaProfesor(long ofertaId,
		service.tfg.model.Profesor profesor) {
		getService().addOfertaProfesor(ofertaId, profesor);
	}

	public static void addOfertaProfesors(long ofertaId,
		java.util.List<service.tfg.model.Profesor> profesors) {
		getService().addOfertaProfesors(ofertaId, profesors);
	}

	public static void addOfertaProfesors(long ofertaId, long[] profesorIds) {
		getService().addOfertaProfesors(ofertaId, profesorIds);
	}

	public static void addProfesor(java.lang.String email,
		java.lang.String pwd, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String area,
		java.lang.String despacho, long companyId) {
		getService()
			.addProfesor(email, pwd, nombre, apellidos, area, despacho,
			companyId);
	}

	public static void clearOfertaProfesors(long ofertaId) {
		getService().clearOfertaProfesors(ofertaId);
	}

	public static void deleteOfertaProfesor(long ofertaId, long profesorId) {
		getService().deleteOfertaProfesor(ofertaId, profesorId);
	}

	public static void deleteOfertaProfesor(long ofertaId,
		service.tfg.model.Profesor profesor) {
		getService().deleteOfertaProfesor(ofertaId, profesor);
	}

	public static void deleteOfertaProfesors(long ofertaId,
		java.util.List<service.tfg.model.Profesor> profesors) {
		getService().deleteOfertaProfesors(ofertaId, profesors);
	}

	public static void deleteOfertaProfesors(long ofertaId, long[] profesorIds) {
		getService().deleteOfertaProfesors(ofertaId, profesorIds);
	}

	public static void setOfertaProfesors(long ofertaId, long[] profesorIds) {
		getService().setOfertaProfesors(ofertaId, profesorIds);
	}

	public static void updateProfesor(long profesorId, java.lang.String nombre,
		java.lang.String apellidos, java.lang.String area,
		java.lang.String despacho)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService()
			.updateProfesor(profesorId, nombre, apellidos, area, despacho);
	}

	public static ProfesorLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProfesorLocalService, ProfesorLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ProfesorLocalService.class);
}