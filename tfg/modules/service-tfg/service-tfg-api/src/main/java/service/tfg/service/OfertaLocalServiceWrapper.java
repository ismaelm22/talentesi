/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OfertaLocalService}.
 *
 * @author Ismael Medina
 * @see OfertaLocalService
 * @generated
 */
@ProviderType
public class OfertaLocalServiceWrapper implements OfertaLocalService,
	ServiceWrapper<OfertaLocalService> {
	public OfertaLocalServiceWrapper(OfertaLocalService ofertaLocalService) {
		_ofertaLocalService = ofertaLocalService;
	}

	@Override
	public boolean hasAlumnoOferta(long alumnoId, long ofertaId) {
		return _ofertaLocalService.hasAlumnoOferta(alumnoId, ofertaId);
	}

	@Override
	public boolean hasAlumnoOfertas(long alumnoId) {
		return _ofertaLocalService.hasAlumnoOfertas(alumnoId);
	}

	@Override
	public boolean hasEmpresaOferta(long empresaId, long ofertaId) {
		return _ofertaLocalService.hasEmpresaOferta(empresaId, ofertaId);
	}

	@Override
	public boolean hasEmpresaOfertas(long empresaId) {
		return _ofertaLocalService.hasEmpresaOfertas(empresaId);
	}

	@Override
	public boolean hasProfesorOferta(long profesorId, long ofertaId) {
		return _ofertaLocalService.hasProfesorOferta(profesorId, ofertaId);
	}

	@Override
	public boolean hasProfesorOfertas(long profesorId) {
		return _ofertaLocalService.hasProfesorOfertas(profesorId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ofertaLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ofertaLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ofertaLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ofertaLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ofertaLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int getAlumnoOfertasCount(long alumnoId) {
		return _ofertaLocalService.getAlumnoOfertasCount(alumnoId);
	}

	@Override
	public int getEmpresaOfertasCount(long empresaId) {
		return _ofertaLocalService.getEmpresaOfertasCount(empresaId);
	}

	/**
	* Returns the number of ofertas.
	*
	* @return the number of ofertas
	*/
	@Override
	public int getOfertasCount() {
		return _ofertaLocalService.getOfertasCount();
	}

	@Override
	public int getProfesorOfertasCount(long profesorId) {
		return _ofertaLocalService.getProfesorOfertasCount(profesorId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ofertaLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ofertaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ofertaLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ofertaLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> findByOfertaId(
		long ofertaId) {
		return _ofertaLocalService.findByOfertaId(ofertaId);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> findByTitulo(
		java.lang.String titulo) {
		return _ofertaLocalService.findByTitulo(titulo);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> findByareaType(
		java.lang.String area) {
		return _ofertaLocalService.findByareaType(area);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId) {
		return _ofertaLocalService.getAlumnoOfertas(alumnoId);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId, int start, int end) {
		return _ofertaLocalService.getAlumnoOfertas(alumnoId, start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getAlumnoOfertas(
		long alumnoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return _ofertaLocalService.getAlumnoOfertas(alumnoId, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId) {
		return _ofertaLocalService.getEmpresaOfertas(empresaId);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId, int start, int end) {
		return _ofertaLocalService.getEmpresaOfertas(empresaId, start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getEmpresaOfertas(
		long empresaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return _ofertaLocalService.getEmpresaOfertas(empresaId, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the ofertas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link service.tfg.model.impl.OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ofertas
	* @param end the upper bound of the range of ofertas (not inclusive)
	* @return the range of ofertas
	*/
	@Override
	public java.util.List<service.tfg.model.Oferta> getOfertas(int start,
		int end) {
		return _ofertaLocalService.getOfertas(start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId) {
		return _ofertaLocalService.getProfesorOfertas(profesorId);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId, int start, int end) {
		return _ofertaLocalService.getProfesorOfertas(profesorId, start, end);
	}

	@Override
	public java.util.List<service.tfg.model.Oferta> getProfesorOfertas(
		long profesorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return _ofertaLocalService.getProfesorOfertas(profesorId, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ofertaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ofertaLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Returns the alumnoIds of the alumnos associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the alumnoIds of alumnos associated with the oferta
	*/
	@Override
	public long[] getAlumnoPrimaryKeys(long ofertaId) {
		return _ofertaLocalService.getAlumnoPrimaryKeys(ofertaId);
	}

	/**
	* Returns the empresaIds of the empresas associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the empresaIds of empresas associated with the oferta
	*/
	@Override
	public long[] getEmpresaPrimaryKeys(long ofertaId) {
		return _ofertaLocalService.getEmpresaPrimaryKeys(ofertaId);
	}

	/**
	* Returns the profesorIds of the profesors associated with the oferta.
	*
	* @param ofertaId the ofertaId of the oferta
	* @return long[] the profesorIds of profesors associated with the oferta
	*/
	@Override
	public long[] getProfesorPrimaryKeys(long ofertaId) {
		return _ofertaLocalService.getProfesorPrimaryKeys(ofertaId);
	}

	/**
	* Adds the oferta to the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was added
	*/
	@Override
	public service.tfg.model.Oferta addOferta(service.tfg.model.Oferta oferta) {
		return _ofertaLocalService.addOferta(oferta);
	}

	/**
	* Creates a new oferta with the primary key. Does not add the oferta to the database.
	*
	* @param ofertaId the primary key for the new oferta
	* @return the new oferta
	*/
	@Override
	public service.tfg.model.Oferta createOferta(long ofertaId) {
		return _ofertaLocalService.createOferta(ofertaId);
	}

	/**
	* Deletes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta that was removed
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Oferta deleteOferta(long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ofertaLocalService.deleteOferta(ofertaId);
	}

	/**
	* Deletes the oferta from the database. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was removed
	*/
	@Override
	public service.tfg.model.Oferta deleteOferta(
		service.tfg.model.Oferta oferta) {
		return _ofertaLocalService.deleteOferta(oferta);
	}

	@Override
	public service.tfg.model.Oferta fetchOferta(long ofertaId) {
		return _ofertaLocalService.fetchOferta(ofertaId);
	}

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta, or <code>null</code> if a matching oferta could not be found
	*/
	@Override
	public service.tfg.model.Oferta fetchOfertaByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _ofertaLocalService.fetchOfertaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the oferta with the primary key.
	*
	* @param ofertaId the primary key of the oferta
	* @return the oferta
	* @throws PortalException if a oferta with the primary key could not be found
	*/
	@Override
	public service.tfg.model.Oferta getOferta(long ofertaId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ofertaLocalService.getOferta(ofertaId);
	}

	/**
	* Returns the oferta with the matching UUID and company.
	*
	* @param uuid the oferta's UUID
	* @param companyId the primary key of the company
	* @return the matching oferta
	* @throws PortalException if a matching oferta could not be found
	*/
	@Override
	public service.tfg.model.Oferta getOfertaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ofertaLocalService.getOfertaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the oferta in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param oferta the oferta
	* @return the oferta that was updated
	*/
	@Override
	public service.tfg.model.Oferta updateOferta(
		service.tfg.model.Oferta oferta) {
		return _ofertaLocalService.updateOferta(oferta);
	}

	@Override
	public void addAlumnoOferta(long alumnoId, long ofertaId) {
		_ofertaLocalService.addAlumnoOferta(alumnoId, ofertaId);
	}

	@Override
	public void addAlumnoOferta(long alumnoId, service.tfg.model.Oferta oferta) {
		_ofertaLocalService.addAlumnoOferta(alumnoId, oferta);
	}

	@Override
	public void addAlumnoOfertas(long alumnoId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.addAlumnoOfertas(alumnoId, ofertas);
	}

	@Override
	public void addAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		_ofertaLocalService.addAlumnoOfertas(alumnoId, ofertaIds);
	}

	@Override
	public void addEmpresaOferta(long empresaId, long ofertaId) {
		_ofertaLocalService.addEmpresaOferta(empresaId, ofertaId);
	}

	@Override
	public void addEmpresaOferta(long empresaId, service.tfg.model.Oferta oferta) {
		_ofertaLocalService.addEmpresaOferta(empresaId, oferta);
	}

	@Override
	public void addEmpresaOfertas(long empresaId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.addEmpresaOfertas(empresaId, ofertas);
	}

	@Override
	public void addEmpresaOfertas(long empresaId, long[] ofertaIds) {
		_ofertaLocalService.addEmpresaOfertas(empresaId, ofertaIds);
	}

	@Override
	public void addOferta(long companyId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion,
		java.util.Collection<service.tfg.model.Profesor> profesores,
		java.util.Collection<service.tfg.model.Empresa> empresas) {
		_ofertaLocalService.addOferta(companyId, titulo, descripcion, area,
			competencias, duracion, profesores, empresas);
	}

	@Override
	public void addProfesorOferta(long profesorId, long ofertaId) {
		_ofertaLocalService.addProfesorOferta(profesorId, ofertaId);
	}

	@Override
	public void addProfesorOferta(long profesorId,
		service.tfg.model.Oferta oferta) {
		_ofertaLocalService.addProfesorOferta(profesorId, oferta);
	}

	@Override
	public void addProfesorOfertas(long profesorId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.addProfesorOfertas(profesorId, ofertas);
	}

	@Override
	public void addProfesorOfertas(long profesorId, long[] ofertaIds) {
		_ofertaLocalService.addProfesorOfertas(profesorId, ofertaIds);
	}

	@Override
	public void clearAlumnoOfertas(long alumnoId) {
		_ofertaLocalService.clearAlumnoOfertas(alumnoId);
	}

	@Override
	public void clearEmpresaOfertas(long empresaId) {
		_ofertaLocalService.clearEmpresaOfertas(empresaId);
	}

	@Override
	public void clearProfesorOfertas(long profesorId) {
		_ofertaLocalService.clearProfesorOfertas(profesorId);
	}

	@Override
	public void deleteAlumnoOferta(long alumnoId, long ofertaId) {
		_ofertaLocalService.deleteAlumnoOferta(alumnoId, ofertaId);
	}

	@Override
	public void deleteAlumnoOferta(long alumnoId,
		service.tfg.model.Oferta oferta) {
		_ofertaLocalService.deleteAlumnoOferta(alumnoId, oferta);
	}

	@Override
	public void deleteAlumnoOfertas(long alumnoId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.deleteAlumnoOfertas(alumnoId, ofertas);
	}

	@Override
	public void deleteAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		_ofertaLocalService.deleteAlumnoOfertas(alumnoId, ofertaIds);
	}

	@Override
	public void deleteEmpresaOferta(long empresaId, long ofertaId) {
		_ofertaLocalService.deleteEmpresaOferta(empresaId, ofertaId);
	}

	@Override
	public void deleteEmpresaOferta(long empresaId,
		service.tfg.model.Oferta oferta) {
		_ofertaLocalService.deleteEmpresaOferta(empresaId, oferta);
	}

	@Override
	public void deleteEmpresaOfertas(long empresaId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.deleteEmpresaOfertas(empresaId, ofertas);
	}

	@Override
	public void deleteEmpresaOfertas(long empresaId, long[] ofertaIds) {
		_ofertaLocalService.deleteEmpresaOfertas(empresaId, ofertaIds);
	}

	@Override
	public void deleteProfesorOferta(long profesorId, long ofertaId) {
		_ofertaLocalService.deleteProfesorOferta(profesorId, ofertaId);
	}

	@Override
	public void deleteProfesorOferta(long profesorId,
		service.tfg.model.Oferta oferta) {
		_ofertaLocalService.deleteProfesorOferta(profesorId, oferta);
	}

	@Override
	public void deleteProfesorOfertas(long profesorId,
		java.util.List<service.tfg.model.Oferta> ofertas) {
		_ofertaLocalService.deleteProfesorOfertas(profesorId, ofertas);
	}

	@Override
	public void deleteProfesorOfertas(long profesorId, long[] ofertaIds) {
		_ofertaLocalService.deleteProfesorOfertas(profesorId, ofertaIds);
	}

	@Override
	public void setAlumnoOfertas(long alumnoId, long[] ofertaIds) {
		_ofertaLocalService.setAlumnoOfertas(alumnoId, ofertaIds);
	}

	@Override
	public void setEmpresaOfertas(long empresaId, long[] ofertaIds) {
		_ofertaLocalService.setEmpresaOfertas(empresaId, ofertaIds);
	}

	@Override
	public void setProfesorOfertas(long profesorId, long[] ofertaIds) {
		_ofertaLocalService.setProfesorOfertas(profesorId, ofertaIds);
	}

	@Override
	public void updateAlumnosolcitudes(long ofertaId,
		java.util.Collection<service.tfg.model.Alumno> alumnos)
		throws com.liferay.portal.kernel.exception.PortalException {
		_ofertaLocalService.updateAlumnosolcitudes(ofertaId, alumnos);
	}

	@Override
	public void updateEmresasOferta(long ofertaId,
		java.util.Collection<service.tfg.model.Empresa> empresas)
		throws com.liferay.portal.kernel.exception.PortalException {
		_ofertaLocalService.updateEmresasOferta(ofertaId, empresas);
	}

	@Override
	public void updateOferta(long ofertaId, java.lang.String titulo,
		java.lang.String descripcion, java.lang.String area,
		java.lang.String competencias, java.lang.String duracion)
		throws com.liferay.portal.kernel.exception.PortalException {
		_ofertaLocalService.updateOferta(ofertaId, titulo, descripcion, area,
			competencias, duracion);
	}

	@Override
	public void updateProfesoresOferta(long ofertaId,
		java.util.Collection<service.tfg.model.Profesor> profesor)
		throws com.liferay.portal.kernel.exception.PortalException {
		_ofertaLocalService.updateProfesoresOferta(ofertaId, profesor);
	}

	@Override
	public OfertaLocalService getWrappedService() {
		return _ofertaLocalService;
	}

	@Override
	public void setWrappedService(OfertaLocalService ofertaLocalService) {
		_ofertaLocalService = ofertaLocalService;
	}

	private OfertaLocalService _ofertaLocalService;
}