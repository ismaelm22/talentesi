/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import service.tfg.model.Mensajes;

import java.util.List;

/**
 * The persistence utility for the mensajes service. This utility wraps {@link service.tfg.service.persistence.impl.MensajesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see MensajesPersistence
 * @see service.tfg.service.persistence.impl.MensajesPersistenceImpl
 * @generated
 */
@ProviderType
public class MensajesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Mensajes mensajes) {
		getPersistence().clearCache(mensajes);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Mensajes> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Mensajes> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Mensajes> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Mensajes update(Mensajes mensajes) {
		return getPersistence().update(mensajes);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Mensajes update(Mensajes mensajes,
		ServiceContext serviceContext) {
		return getPersistence().update(mensajes, serviceContext);
	}

	/**
	* Returns all the mensajeses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching mensajeses
	*/
	public static List<Mensajes> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByUuid_First(java.lang.String uuid,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes[] findByUuid_PrevAndNext(long mensajeId,
		java.lang.String uuid, OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByUuid_PrevAndNext(mensajeId, uuid, orderByComparator);
	}

	/**
	* Removes all the mensajeses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of mensajeses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching mensajeses
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching mensajeses
	*/
	public static List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Mensajes> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes[] findByUuid_C_PrevAndNext(long mensajeId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(mensajeId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the mensajeses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of mensajeses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching mensajeses
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the mensajeses where remitente = &#63;.
	*
	* @param remitente the remitente
	* @return the matching mensajeses
	*/
	public static List<Mensajes> findByremitenteId(java.lang.String remitente) {
		return getPersistence().findByremitenteId(remitente);
	}

	/**
	* Returns a range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public static List<Mensajes> findByremitenteId(java.lang.String remitente,
		int start, int end) {
		return getPersistence().findByremitenteId(remitente, start, end);
	}

	/**
	* Returns an ordered range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByremitenteId(java.lang.String remitente,
		int start, int end, OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .findByremitenteId(remitente, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the mensajeses where remitente = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param remitente the remitente
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findByremitenteId(java.lang.String remitente,
		int start, int end, OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByremitenteId(remitente, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByremitenteId_First(java.lang.String remitente,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByremitenteId_First(remitente, orderByComparator);
	}

	/**
	* Returns the first mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByremitenteId_First(
		java.lang.String remitente,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchByremitenteId_First(remitente, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findByremitenteId_Last(java.lang.String remitente,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByremitenteId_Last(remitente, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where remitente = &#63;.
	*
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchByremitenteId_Last(java.lang.String remitente,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchByremitenteId_Last(remitente, orderByComparator);
	}

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where remitente = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param remitente the remitente
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes[] findByremitenteId_PrevAndNext(long mensajeId,
		java.lang.String remitente,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findByremitenteId_PrevAndNext(mensajeId, remitente,
			orderByComparator);
	}

	/**
	* Removes all the mensajeses where remitente = &#63; from the database.
	*
	* @param remitente the remitente
	*/
	public static void removeByremitenteId(java.lang.String remitente) {
		getPersistence().removeByremitenteId(remitente);
	}

	/**
	* Returns the number of mensajeses where remitente = &#63;.
	*
	* @param remitente the remitente
	* @return the number of matching mensajeses
	*/
	public static int countByremitenteId(java.lang.String remitente) {
		return getPersistence().countByremitenteId(remitente);
	}

	/**
	* Returns all the mensajeses where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @return the matching mensajeses
	*/
	public static List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario) {
		return getPersistence().findBydestinatarioId(destinatario);
	}

	/**
	* Returns a range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of matching mensajeses
	*/
	public static List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end) {
		return getPersistence().findBydestinatarioId(destinatario, start, end);
	}

	/**
	* Returns an ordered range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .findBydestinatarioId(destinatario, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the mensajeses where destinatario = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param destinatario the destinatario
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching mensajeses
	*/
	public static List<Mensajes> findBydestinatarioId(
		java.lang.String destinatario, int start, int end,
		OrderByComparator<Mensajes> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findBydestinatarioId(destinatario, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findBydestinatarioId_First(
		java.lang.String destinatario,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findBydestinatarioId_First(destinatario, orderByComparator);
	}

	/**
	* Returns the first mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchBydestinatarioId_First(
		java.lang.String destinatario,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchBydestinatarioId_First(destinatario, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes
	* @throws NoSuchMensajesException if a matching mensajes could not be found
	*/
	public static Mensajes findBydestinatarioId_Last(
		java.lang.String destinatario,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findBydestinatarioId_Last(destinatario, orderByComparator);
	}

	/**
	* Returns the last mensajes in the ordered set where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	*/
	public static Mensajes fetchBydestinatarioId_Last(
		java.lang.String destinatario,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence()
				   .fetchBydestinatarioId_Last(destinatario, orderByComparator);
	}

	/**
	* Returns the mensajeses before and after the current mensajes in the ordered set where destinatario = &#63;.
	*
	* @param mensajeId the primary key of the current mensajes
	* @param destinatario the destinatario
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes[] findBydestinatarioId_PrevAndNext(long mensajeId,
		java.lang.String destinatario,
		OrderByComparator<Mensajes> orderByComparator)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence()
				   .findBydestinatarioId_PrevAndNext(mensajeId, destinatario,
			orderByComparator);
	}

	/**
	* Removes all the mensajeses where destinatario = &#63; from the database.
	*
	* @param destinatario the destinatario
	*/
	public static void removeBydestinatarioId(java.lang.String destinatario) {
		getPersistence().removeBydestinatarioId(destinatario);
	}

	/**
	* Returns the number of mensajeses where destinatario = &#63;.
	*
	* @param destinatario the destinatario
	* @return the number of matching mensajeses
	*/
	public static int countBydestinatarioId(java.lang.String destinatario) {
		return getPersistence().countBydestinatarioId(destinatario);
	}

	/**
	* Caches the mensajes in the entity cache if it is enabled.
	*
	* @param mensajes the mensajes
	*/
	public static void cacheResult(Mensajes mensajes) {
		getPersistence().cacheResult(mensajes);
	}

	/**
	* Caches the mensajeses in the entity cache if it is enabled.
	*
	* @param mensajeses the mensajeses
	*/
	public static void cacheResult(List<Mensajes> mensajeses) {
		getPersistence().cacheResult(mensajeses);
	}

	/**
	* Creates a new mensajes with the primary key. Does not add the mensajes to the database.
	*
	* @param mensajeId the primary key for the new mensajes
	* @return the new mensajes
	*/
	public static Mensajes create(long mensajeId) {
		return getPersistence().create(mensajeId);
	}

	/**
	* Removes the mensajes with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes that was removed
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes remove(long mensajeId)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence().remove(mensajeId);
	}

	public static Mensajes updateImpl(Mensajes mensajes) {
		return getPersistence().updateImpl(mensajes);
	}

	/**
	* Returns the mensajes with the primary key or throws a {@link NoSuchMensajesException} if it could not be found.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes
	* @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	*/
	public static Mensajes findByPrimaryKey(long mensajeId)
		throws service.tfg.exception.NoSuchMensajesException {
		return getPersistence().findByPrimaryKey(mensajeId);
	}

	/**
	* Returns the mensajes with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mensajeId the primary key of the mensajes
	* @return the mensajes, or <code>null</code> if a mensajes with the primary key could not be found
	*/
	public static Mensajes fetchByPrimaryKey(long mensajeId) {
		return getPersistence().fetchByPrimaryKey(mensajeId);
	}

	public static java.util.Map<java.io.Serializable, Mensajes> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the mensajeses.
	*
	* @return the mensajeses
	*/
	public static List<Mensajes> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @return the range of mensajeses
	*/
	public static List<Mensajes> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of mensajeses
	*/
	public static List<Mensajes> findAll(int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the mensajeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of mensajeses
	* @param end the upper bound of the range of mensajeses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of mensajeses
	*/
	public static List<Mensajes> findAll(int start, int end,
		OrderByComparator<Mensajes> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the mensajeses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of mensajeses.
	*
	* @return the number of mensajeses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static MensajesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MensajesPersistence, MensajesPersistence> _serviceTracker =
		ServiceTrackerFactory.open(MensajesPersistence.class);
}