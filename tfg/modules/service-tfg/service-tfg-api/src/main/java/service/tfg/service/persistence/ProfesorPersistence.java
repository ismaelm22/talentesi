/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import service.tfg.exception.NoSuchProfesorException;

import service.tfg.model.Profesor;

/**
 * The persistence interface for the profesor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see service.tfg.service.persistence.impl.ProfesorPersistenceImpl
 * @see ProfesorUtil
 * @generated
 */
@ProviderType
public interface ProfesorPersistence extends BasePersistence<Profesor> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProfesorUtil} to access the profesor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the profesors where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching profesors
	*/
	public java.util.List<Profesor> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the profesors where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the profesors where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns an ordered range of all the profesors where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first profesor in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the first profesor in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the last profesor in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the last profesor in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the profesors before and after the current profesor in the ordered set where uuid = &#63;.
	*
	* @param profesorId the primary key of the current profesor
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next profesor
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor[] findByUuid_PrevAndNext(long profesorId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Removes all the profesors where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of profesors where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching profesors
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the profesors where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching profesors
	*/
	public java.util.List<Profesor> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the profesors where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the profesors where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns an ordered range of all the profesors where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the first profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the last profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the last profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the profesors before and after the current profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param profesorId the primary key of the current profesor
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next profesor
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor[] findByUuid_C_PrevAndNext(long profesorId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Removes all the profesors where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of profesors where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching profesors
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the profesors where email = &#63;.
	*
	* @param email the email
	* @return the matching profesors
	*/
	public java.util.List<Profesor> findByEmailId(java.lang.String email);

	/**
	* Returns a range of all the profesors where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of matching profesors
	*/
	public java.util.List<Profesor> findByEmailId(java.lang.String email,
		int start, int end);

	/**
	* Returns an ordered range of all the profesors where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns an ordered range of all the profesors where email = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param email the email
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findByEmailId(java.lang.String email,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first profesor in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the first profesor in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByEmailId_First(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the last profesor in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the last profesor in the ordered set where email = &#63;.
	*
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchByEmailId_Last(java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the profesors before and after the current profesor in the ordered set where email = &#63;.
	*
	* @param profesorId the primary key of the current profesor
	* @param email the email
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next profesor
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor[] findByEmailId_PrevAndNext(long profesorId,
		java.lang.String email,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Removes all the profesors where email = &#63; from the database.
	*
	* @param email the email
	*/
	public void removeByEmailId(java.lang.String email);

	/**
	* Returns the number of profesors where email = &#63;.
	*
	* @param email the email
	* @return the number of matching profesors
	*/
	public int countByEmailId(java.lang.String email);

	/**
	* Returns all the profesors where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching profesors
	*/
	public java.util.List<Profesor> findBynombreProfesor(
		java.lang.String nombre);

	/**
	* Returns a range of all the profesors where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of matching profesors
	*/
	public java.util.List<Profesor> findBynombreProfesor(
		java.lang.String nombre, int start, int end);

	/**
	* Returns an ordered range of all the profesors where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findBynombreProfesor(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns an ordered range of all the profesors where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching profesors
	*/
	public java.util.List<Profesor> findBynombreProfesor(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first profesor in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findBynombreProfesor_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the first profesor in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchBynombreProfesor_First(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the last profesor in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor
	* @throws NoSuchProfesorException if a matching profesor could not be found
	*/
	public Profesor findBynombreProfesor_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Returns the last profesor in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	*/
	public Profesor fetchBynombreProfesor_Last(java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns the profesors before and after the current profesor in the ordered set where nombre = &#63;.
	*
	* @param profesorId the primary key of the current profesor
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next profesor
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor[] findBynombreProfesor_PrevAndNext(long profesorId,
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException;

	/**
	* Removes all the profesors where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	*/
	public void removeBynombreProfesor(java.lang.String nombre);

	/**
	* Returns the number of profesors where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching profesors
	*/
	public int countBynombreProfesor(java.lang.String nombre);

	/**
	* Caches the profesor in the entity cache if it is enabled.
	*
	* @param profesor the profesor
	*/
	public void cacheResult(Profesor profesor);

	/**
	* Caches the profesors in the entity cache if it is enabled.
	*
	* @param profesors the profesors
	*/
	public void cacheResult(java.util.List<Profesor> profesors);

	/**
	* Creates a new profesor with the primary key. Does not add the profesor to the database.
	*
	* @param profesorId the primary key for the new profesor
	* @return the new profesor
	*/
	public Profesor create(long profesorId);

	/**
	* Removes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor that was removed
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor remove(long profesorId) throws NoSuchProfesorException;

	public Profesor updateImpl(Profesor profesor);

	/**
	* Returns the profesor with the primary key or throws a {@link NoSuchProfesorException} if it could not be found.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor
	* @throws NoSuchProfesorException if a profesor with the primary key could not be found
	*/
	public Profesor findByPrimaryKey(long profesorId)
		throws NoSuchProfesorException;

	/**
	* Returns the profesor with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param profesorId the primary key of the profesor
	* @return the profesor, or <code>null</code> if a profesor with the primary key could not be found
	*/
	public Profesor fetchByPrimaryKey(long profesorId);

	@Override
	public java.util.Map<java.io.Serializable, Profesor> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the profesors.
	*
	* @return the profesors
	*/
	public java.util.List<Profesor> findAll();

	/**
	* Returns a range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of profesors
	*/
	public java.util.List<Profesor> findAll(int start, int end);

	/**
	* Returns an ordered range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of profesors
	*/
	public java.util.List<Profesor> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator);

	/**
	* Returns an ordered range of all the profesors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of profesors
	*/
	public java.util.List<Profesor> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the profesors from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of profesors.
	*
	* @return the number of profesors
	*/
	public int countAll();

	/**
	* Returns the primaryKeys of ofertas associated with the profesor.
	*
	* @param pk the primary key of the profesor
	* @return long[] of the primaryKeys of ofertas associated with the profesor
	*/
	public long[] getOfertaPrimaryKeys(long pk);

	/**
	* Returns all the ofertas associated with the profesor.
	*
	* @param pk the primary key of the profesor
	* @return the ofertas associated with the profesor
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk);

	/**
	* Returns a range of all the ofertas associated with the profesor.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the profesor
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @return the range of ofertas associated with the profesor
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end);

	/**
	* Returns an ordered range of all the ofertas associated with the profesor.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param pk the primary key of the profesor
	* @param start the lower bound of the range of profesors
	* @param end the upper bound of the range of profesors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ofertas associated with the profesor
	*/
	public java.util.List<service.tfg.model.Oferta> getOfertas(long pk,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<service.tfg.model.Oferta> orderByComparator);

	/**
	* Returns the number of ofertas associated with the profesor.
	*
	* @param pk the primary key of the profesor
	* @return the number of ofertas associated with the profesor
	*/
	public int getOfertasSize(long pk);

	/**
	* Returns <code>true</code> if the oferta is associated with the profesor.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPK the primary key of the oferta
	* @return <code>true</code> if the oferta is associated with the profesor; <code>false</code> otherwise
	*/
	public boolean containsOferta(long pk, long ofertaPK);

	/**
	* Returns <code>true</code> if the profesor has any ofertas associated with it.
	*
	* @param pk the primary key of the profesor to check for associations with ofertas
	* @return <code>true</code> if the profesor has any ofertas associated with it; <code>false</code> otherwise
	*/
	public boolean containsOfertas(long pk);

	/**
	* Adds an association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPK the primary key of the oferta
	*/
	public void addOferta(long pk, long ofertaPK);

	/**
	* Adds an association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param oferta the oferta
	*/
	public void addOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Adds an association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void addOfertas(long pk, long[] ofertaPKs);

	/**
	* Adds an association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertas the ofertas
	*/
	public void addOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Clears all associations between the profesor and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor to clear the associated ofertas from
	*/
	public void clearOfertas(long pk);

	/**
	* Removes the association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPK the primary key of the oferta
	*/
	public void removeOferta(long pk, long ofertaPK);

	/**
	* Removes the association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param oferta the oferta
	*/
	public void removeOferta(long pk, service.tfg.model.Oferta oferta);

	/**
	* Removes the association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPKs the primary keys of the ofertas
	*/
	public void removeOfertas(long pk, long[] ofertaPKs);

	/**
	* Removes the association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertas the ofertas
	*/
	public void removeOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	/**
	* Sets the ofertas associated with the profesor, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertaPKs the primary keys of the ofertas to be associated with the profesor
	*/
	public void setOfertas(long pk, long[] ofertaPKs);

	/**
	* Sets the ofertas associated with the profesor, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	*
	* @param pk the primary key of the profesor
	* @param ofertas the ofertas to be associated with the profesor
	*/
	public void setOfertas(long pk,
		java.util.List<service.tfg.model.Oferta> ofertas);

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}