/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import service.tfg.model.Empresa;
import service.tfg.model.impl.EmpresaImpl;
import service.tfg.service.base.EmpresaLocalServiceBaseImpl;

/**
 * The implementation of the empresa local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link service.tfg.service.EmpresaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Ismael Medina
 * @see EmpresaLocalServiceBaseImpl
 * @see service.tfg.service.EmpresaLocalServiceUtil
 */
public class EmpresaLocalServiceImpl extends EmpresaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * service.tfg.service.EmpresaLocalServiceUtil} to access the empresa local
	 * service.
	 */

	public void addEmpresa(String email, String pwd, String nombre, String cif, String direccion, String telefono,
			long companyId) {

		final Empresa empresa = new EmpresaImpl();

		empresa.setEmpresaId(counterLocalService.increment());
		empresa.setCompanyId(companyId);
		empresa.setEmail(email);
		empresa.setPwd(pwd);
		empresa.setNombre(nombre);
		empresa.setCif(cif);
		empresa.setDireccion(direccion);
		empresa.setTelefono(telefono);

		addEmpresa(empresa);

	}

	public void updateEmpresa(long empresaId, String nombre, String cif, String direccion, String telefono)
			throws PortalException {

		final Empresa empresa = getEmpresa(empresaId);

		empresa.setNombre(nombre);
		empresa.setCif(cif);
		empresa.setDireccion(direccion);
		empresa.setTelefono(telefono);

		updateEmpresa(empresa);
	}

	public List<Empresa> findByEmailId(java.lang.String email) {
		return getEmpresaPersistence().findByEmailId(email);
	}

	public List<Empresa> findByNombreEmpresa(java.lang.String nombre) {
		return getEmpresaPersistence().findBynombreEmpresa(nombre);
	}
}