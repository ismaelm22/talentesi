/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.impl;

import java.sql.Blob;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import service.tfg.model.Alumno;
import service.tfg.model.impl.AlumnoImpl;
import service.tfg.service.base.AlumnoLocalServiceBaseImpl;

/**
 * The implementation of the alumno local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link service.tfg.service.AlumnoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Ismael Medina
 * @see AlumnoLocalServiceBaseImpl
 * @see service.tfg.service.AlumnoLocalServiceUtil
 */
public class AlumnoLocalServiceImpl extends AlumnoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * service.tfg.service.AlumnoLocalServiceUtil} to access the alumno local
	 * service.
	 */
	public void addAlumno(String email, String pwd, String nombre, String apellidos, String competencias,
			Blob curriculum, long ofertaId, long companyId) {
		final Alumno alumno = new AlumnoImpl();
		alumno.setAlumnoId(counterLocalService.increment());
		alumno.setCompanyId(companyId);
		alumno.setEmail(email);
		alumno.setPwd(pwd);
		alumno.setNombre(nombre);
		alumno.setApellidos(apellidos);
		alumno.setCompetencias(competencias);
		alumno.setCurriculum(curriculum);
		alumno.setOfertaId(ofertaId);
		addAlumno(alumno);

	}

	public void updateAlumno(long alumnoId, String nombre, String apellidos, String competencias, Blob curriculum)
			throws PortalException {
		final Alumno alumno = getAlumno(alumnoId);
		alumno.setNombre(nombre);
		alumno.setApellidos(apellidos);
		alumno.setCompetencias(competencias);
		alumno.setCurriculum(curriculum);

		updateAlumno(alumno);
	}

	public void updateAlumnoOferta(long alumnoId, long ofertaId) throws PortalException {

		final Alumno alumno = getAlumno(alumnoId);
		alumno.setOfertaId(ofertaId);

		updateAlumno(alumno);
	}

	public List<Alumno> findByEmailId(java.lang.String email) {
		return getAlumnoPersistence().findByEmailId(email);
	}

	public List<Alumno> findByNombreAlumno(java.lang.String nombre) {
		return getAlumnoPersistence().findBynombreAlumno(nombre);
	}
}