/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import service.tfg.model.Oferta;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Oferta in entity cache.
 *
 * @author Ismael Medina
 * @see Oferta
 * @generated
 */
@ProviderType
public class OfertaCacheModel implements CacheModel<Oferta>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OfertaCacheModel)) {
			return false;
		}

		OfertaCacheModel ofertaCacheModel = (OfertaCacheModel)obj;

		if (ofertaId == ofertaCacheModel.ofertaId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ofertaId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", ofertaId=");
		sb.append(ofertaId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", titulo=");
		sb.append(titulo);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append(", area=");
		sb.append(area);
		sb.append(", competencias=");
		sb.append(competencias);
		sb.append(", duracion=");
		sb.append(duracion);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Oferta toEntityModel() {
		OfertaImpl ofertaImpl = new OfertaImpl();

		if (uuid == null) {
			ofertaImpl.setUuid(StringPool.BLANK);
		}
		else {
			ofertaImpl.setUuid(uuid);
		}

		ofertaImpl.setOfertaId(ofertaId);
		ofertaImpl.setCompanyId(companyId);

		if (titulo == null) {
			ofertaImpl.setTitulo(StringPool.BLANK);
		}
		else {
			ofertaImpl.setTitulo(titulo);
		}

		if (descripcion == null) {
			ofertaImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			ofertaImpl.setDescripcion(descripcion);
		}

		if (area == null) {
			ofertaImpl.setArea(StringPool.BLANK);
		}
		else {
			ofertaImpl.setArea(area);
		}

		if (competencias == null) {
			ofertaImpl.setCompetencias(StringPool.BLANK);
		}
		else {
			ofertaImpl.setCompetencias(competencias);
		}

		if (duracion == null) {
			ofertaImpl.setDuracion(StringPool.BLANK);
		}
		else {
			ofertaImpl.setDuracion(duracion);
		}

		ofertaImpl.resetOriginalValues();

		return ofertaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		ofertaId = objectInput.readLong();

		companyId = objectInput.readLong();
		titulo = objectInput.readUTF();
		descripcion = objectInput.readUTF();
		area = objectInput.readUTF();
		competencias = objectInput.readUTF();
		duracion = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(ofertaId);

		objectOutput.writeLong(companyId);

		if (titulo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(titulo);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}

		if (area == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(area);
		}

		if (competencias == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(competencias);
		}

		if (duracion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(duracion);
		}
	}

	public String uuid;
	public long ofertaId;
	public long companyId;
	public String titulo;
	public String descripcion;
	public String area;
	public String competencias;
	public String duracion;
}