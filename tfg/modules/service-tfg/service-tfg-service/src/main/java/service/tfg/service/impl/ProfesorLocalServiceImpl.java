/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import service.tfg.model.Profesor;
import service.tfg.model.impl.ProfesorImpl;
import service.tfg.service.base.ProfesorLocalServiceBaseImpl;

/**
 * The implementation of the profesor local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link service.tfg.service.ProfesorLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Ismael Medina
 * @see ProfesorLocalServiceBaseImpl
 * @see service.tfg.service.ProfesorLocalServiceUtil
 */
public class ProfesorLocalServiceImpl extends ProfesorLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * service.tfg.service.ProfesorLocalServiceUtil} to access the profesor
	 * local service.
	 */

	public void addProfesor(String email, String pwd, String nombre, String apellidos, String area, String despacho,
			long companyId) {

		final Profesor profesor = new ProfesorImpl();

		profesor.setProfesorId(counterLocalService.increment());
		profesor.setCompanyId(companyId);
		profesor.setEmail(email);
		profesor.setPwd(pwd);
		profesor.setNombre(nombre);
		profesor.setApellidos(apellidos);
		profesor.setArea(area);
		profesor.setDespacho(despacho);

		addProfesor(profesor);

	}

	public void updateProfesor(long profesorId, String nombre, String apellidos, String area, String despacho)
			throws PortalException {

		final Profesor profesor = getProfesor(profesorId);

		profesor.setNombre(nombre);
		profesor.setApellidos(apellidos);
		profesor.setArea(area);
		profesor.setDespacho(despacho);

		updateProfesor(profesor);

	}

	public List<Profesor> findByEmailId(java.lang.String email) {
		return getProfesorPersistence().findByEmailId(email);
	}

	public List<Profesor> findByNombreProfeosr(java.lang.String nombre) {
		return getProfesorPersistence().findBynombreProfesor(nombre);
	}

}