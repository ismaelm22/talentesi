/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReflectionUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import service.tfg.exception.NoSuchAlumnoException;

import service.tfg.model.Alumno;
import service.tfg.model.impl.AlumnoImpl;
import service.tfg.model.impl.AlumnoModelImpl;

import service.tfg.service.persistence.AlumnoPersistence;
import service.tfg.service.persistence.OfertaPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the alumno service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see AlumnoPersistence
 * @see service.tfg.service.persistence.AlumnoUtil
 * @generated
 */
@ProviderType
public class AlumnoPersistenceImpl extends BasePersistenceImpl<Alumno>
	implements AlumnoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AlumnoUtil} to access the alumno persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AlumnoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			AlumnoModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the alumnos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alumnos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid(String uuid, int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alumnos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid(String uuid, int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Alumno alumno : list) {
					if (!Objects.equals(uuid, alumno.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ALUMNO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AlumnoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first alumno in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByUuid_First(String uuid,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByUuid_First(uuid, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the first alumno in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByUuid_First(String uuid,
		OrderByComparator<Alumno> orderByComparator) {
		List<Alumno> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last alumno in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByUuid_Last(String uuid,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByUuid_Last(uuid, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the last alumno in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByUuid_Last(String uuid,
		OrderByComparator<Alumno> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Alumno> list = findByUuid(uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63;.
	 *
	 * @param alumnoId the primary key of the current alumno
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno[] findByUuid_PrevAndNext(long alumnoId, String uuid,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = findByPrimaryKey(alumnoId);

		Session session = null;

		try {
			session = openSession();

			Alumno[] array = new AlumnoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, alumno, uuid,
					orderByComparator, true);

			array[1] = alumno;

			array[2] = getByUuid_PrevAndNext(session, alumno, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Alumno getByUuid_PrevAndNext(Session session, Alumno alumno,
		String uuid, OrderByComparator<Alumno> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ALUMNO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AlumnoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(alumno);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Alumno> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the alumnos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Alumno alumno : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching alumnos
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ALUMNO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "alumno.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "alumno.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(alumno.uuid IS NULL OR alumno.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			AlumnoModelImpl.UUID_COLUMN_BITMASK |
			AlumnoModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the alumnos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alumnos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid_C(String uuid, long companyId, int start,
		int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Alumno> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alumnos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Alumno> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Alumno alumno : list) {
					if (!Objects.equals(uuid, alumno.getUuid()) ||
							(companyId != alumno.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ALUMNO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AlumnoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the first alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator) {
		List<Alumno> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the last alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Alumno> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the alumnos before and after the current alumno in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param alumnoId the primary key of the current alumno
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno[] findByUuid_C_PrevAndNext(long alumnoId, String uuid,
		long companyId, OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = findByPrimaryKey(alumnoId);

		Session session = null;

		try {
			session = openSession();

			Alumno[] array = new AlumnoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, alumno, uuid,
					companyId, orderByComparator, true);

			array[1] = alumno;

			array[2] = getByUuid_C_PrevAndNext(session, alumno, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Alumno getByUuid_C_PrevAndNext(Session session, Alumno alumno,
		String uuid, long companyId,
		OrderByComparator<Alumno> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_ALUMNO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AlumnoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(alumno);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Alumno> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the alumnos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Alumno alumno : findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching alumnos
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ALUMNO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "alumno.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "alumno.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(alumno.uuid IS NULL OR alumno.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "alumno.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAILID = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmailId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmailId",
			new String[] { String.class.getName() },
			AlumnoModelImpl.EMAIL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMAILID = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmailId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the alumnos where email = &#63;.
	 *
	 * @param email the email
	 * @return the matching alumnos
	 */
	@Override
	public List<Alumno> findByEmailId(String email) {
		return findByEmailId(email, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alumnos where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of matching alumnos
	 */
	@Override
	public List<Alumno> findByEmailId(String email, int start, int end) {
		return findByEmailId(email, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByEmailId(String email, int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return findByEmailId(email, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alumnos where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByEmailId(String email, int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID;
			finderArgs = new Object[] { email };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAILID;
			finderArgs = new Object[] { email, start, end, orderByComparator };
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Alumno alumno : list) {
					if (!Objects.equals(email, alumno.getEmail())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ALUMNO_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AlumnoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first alumno in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByEmailId_First(String email,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByEmailId_First(email, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the first alumno in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByEmailId_First(String email,
		OrderByComparator<Alumno> orderByComparator) {
		List<Alumno> list = findByEmailId(email, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last alumno in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByEmailId_Last(String email,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByEmailId_Last(email, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the last alumno in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByEmailId_Last(String email,
		OrderByComparator<Alumno> orderByComparator) {
		int count = countByEmailId(email);

		if (count == 0) {
			return null;
		}

		List<Alumno> list = findByEmailId(email, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the alumnos before and after the current alumno in the ordered set where email = &#63;.
	 *
	 * @param alumnoId the primary key of the current alumno
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno[] findByEmailId_PrevAndNext(long alumnoId, String email,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = findByPrimaryKey(alumnoId);

		Session session = null;

		try {
			session = openSession();

			Alumno[] array = new AlumnoImpl[3];

			array[0] = getByEmailId_PrevAndNext(session, alumno, email,
					orderByComparator, true);

			array[1] = alumno;

			array[2] = getByEmailId_PrevAndNext(session, alumno, email,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Alumno getByEmailId_PrevAndNext(Session session, Alumno alumno,
		String email, OrderByComparator<Alumno> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ALUMNO_WHERE);

		boolean bindEmail = false;

		if (email == null) {
			query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
		}
		else if (email.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
		}
		else {
			bindEmail = true;

			query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AlumnoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindEmail) {
			qPos.add(email);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(alumno);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Alumno> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the alumnos where email = &#63; from the database.
	 *
	 * @param email the email
	 */
	@Override
	public void removeByEmailId(String email) {
		for (Alumno alumno : findByEmailId(email, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos where email = &#63;.
	 *
	 * @param email the email
	 * @return the number of matching alumnos
	 */
	@Override
	public int countByEmailId(String email) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EMAILID;

		Object[] finderArgs = new Object[] { email };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ALUMNO_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EMAILID_EMAIL_1 = "alumno.email IS NULL";
	private static final String _FINDER_COLUMN_EMAILID_EMAIL_2 = "alumno.email = ?";
	private static final String _FINDER_COLUMN_EMAILID_EMAIL_3 = "(alumno.email IS NULL OR alumno.email = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OFERTAASIGNADA =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByofertaAsignada",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByofertaAsignada",
			new String[] { Long.class.getName() },
			AlumnoModelImpl.OFERTAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_OFERTAASIGNADA = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByofertaAsignada",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the alumnos where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @return the matching alumnos
	 */
	@Override
	public List<Alumno> findByofertaAsignada(long ofertaId) {
		return findByofertaAsignada(ofertaId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alumnos where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of matching alumnos
	 */
	@Override
	public List<Alumno> findByofertaAsignada(long ofertaId, int start, int end) {
		return findByofertaAsignada(ofertaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByofertaAsignada(long ofertaId, int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return findByofertaAsignada(ofertaId, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the alumnos where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findByofertaAsignada(long ofertaId, int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA;
			finderArgs = new Object[] { ofertaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OFERTAASIGNADA;
			finderArgs = new Object[] { ofertaId, start, end, orderByComparator };
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Alumno alumno : list) {
					if ((ofertaId != alumno.getOfertaId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ALUMNO_WHERE);

			query.append(_FINDER_COLUMN_OFERTAASIGNADA_OFERTAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AlumnoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ofertaId);

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first alumno in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByofertaAsignada_First(long ofertaId,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByofertaAsignada_First(ofertaId, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ofertaId=");
		msg.append(ofertaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the first alumno in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByofertaAsignada_First(long ofertaId,
		OrderByComparator<Alumno> orderByComparator) {
		List<Alumno> list = findByofertaAsignada(ofertaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last alumno in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findByofertaAsignada_Last(long ofertaId,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByofertaAsignada_Last(ofertaId, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ofertaId=");
		msg.append(ofertaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the last alumno in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchByofertaAsignada_Last(long ofertaId,
		OrderByComparator<Alumno> orderByComparator) {
		int count = countByofertaAsignada(ofertaId);

		if (count == 0) {
			return null;
		}

		List<Alumno> list = findByofertaAsignada(ofertaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the alumnos before and after the current alumno in the ordered set where ofertaId = &#63;.
	 *
	 * @param alumnoId the primary key of the current alumno
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno[] findByofertaAsignada_PrevAndNext(long alumnoId,
		long ofertaId, OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = findByPrimaryKey(alumnoId);

		Session session = null;

		try {
			session = openSession();

			Alumno[] array = new AlumnoImpl[3];

			array[0] = getByofertaAsignada_PrevAndNext(session, alumno,
					ofertaId, orderByComparator, true);

			array[1] = alumno;

			array[2] = getByofertaAsignada_PrevAndNext(session, alumno,
					ofertaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Alumno getByofertaAsignada_PrevAndNext(Session session,
		Alumno alumno, long ofertaId,
		OrderByComparator<Alumno> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ALUMNO_WHERE);

		query.append(_FINDER_COLUMN_OFERTAASIGNADA_OFERTAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AlumnoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ofertaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(alumno);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Alumno> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the alumnos where ofertaId = &#63; from the database.
	 *
	 * @param ofertaId the oferta ID
	 */
	@Override
	public void removeByofertaAsignada(long ofertaId) {
		for (Alumno alumno : findByofertaAsignada(ofertaId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @return the number of matching alumnos
	 */
	@Override
	public int countByofertaAsignada(long ofertaId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_OFERTAASIGNADA;

		Object[] finderArgs = new Object[] { ofertaId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ALUMNO_WHERE);

			query.append(_FINDER_COLUMN_OFERTAASIGNADA_OFERTAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ofertaId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_OFERTAASIGNADA_OFERTAID_2 = "alumno.ofertaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBREALUMNO =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBynombreAlumno",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO =
		new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, AlumnoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBynombreAlumno",
			new String[] { String.class.getName() },
			AlumnoModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NOMBREALUMNO = new FinderPath(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBynombreAlumno",
			new String[] { String.class.getName() });

	/**
	 * Returns all the alumnos where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching alumnos
	 */
	@Override
	public List<Alumno> findBynombreAlumno(String nombre) {
		return findBynombreAlumno(nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the alumnos where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of matching alumnos
	 */
	@Override
	public List<Alumno> findBynombreAlumno(String nombre, int start, int end) {
		return findBynombreAlumno(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findBynombreAlumno(String nombre, int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return findBynombreAlumno(nombre, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alumnos where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching alumnos
	 */
	@Override
	public List<Alumno> findBynombreAlumno(String nombre, int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO;
			finderArgs = new Object[] { nombre };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBREALUMNO;
			finderArgs = new Object[] { nombre, start, end, orderByComparator };
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Alumno alumno : list) {
					if (!Objects.equals(nombre, alumno.getNombre())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ALUMNO_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AlumnoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first alumno in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findBynombreAlumno_First(String nombre,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchBynombreAlumno_First(nombre, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the first alumno in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchBynombreAlumno_First(String nombre,
		OrderByComparator<Alumno> orderByComparator) {
		List<Alumno> list = findBynombreAlumno(nombre, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last alumno in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno
	 * @throws NoSuchAlumnoException if a matching alumno could not be found
	 */
	@Override
	public Alumno findBynombreAlumno_Last(String nombre,
		OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchBynombreAlumno_Last(nombre, orderByComparator);

		if (alumno != null) {
			return alumno;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAlumnoException(msg.toString());
	}

	/**
	 * Returns the last alumno in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching alumno, or <code>null</code> if a matching alumno could not be found
	 */
	@Override
	public Alumno fetchBynombreAlumno_Last(String nombre,
		OrderByComparator<Alumno> orderByComparator) {
		int count = countBynombreAlumno(nombre);

		if (count == 0) {
			return null;
		}

		List<Alumno> list = findBynombreAlumno(nombre, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the alumnos before and after the current alumno in the ordered set where nombre = &#63;.
	 *
	 * @param alumnoId the primary key of the current alumno
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno[] findBynombreAlumno_PrevAndNext(long alumnoId,
		String nombre, OrderByComparator<Alumno> orderByComparator)
		throws NoSuchAlumnoException {
		Alumno alumno = findByPrimaryKey(alumnoId);

		Session session = null;

		try {
			session = openSession();

			Alumno[] array = new AlumnoImpl[3];

			array[0] = getBynombreAlumno_PrevAndNext(session, alumno, nombre,
					orderByComparator, true);

			array[1] = alumno;

			array[2] = getBynombreAlumno_PrevAndNext(session, alumno, nombre,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Alumno getBynombreAlumno_PrevAndNext(Session session,
		Alumno alumno, String nombre,
		OrderByComparator<Alumno> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ALUMNO_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AlumnoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(alumno);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Alumno> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the alumnos where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	@Override
	public void removeBynombreAlumno(String nombre) {
		for (Alumno alumno : findBynombreAlumno(nombre, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching alumnos
	 */
	@Override
	public int countBynombreAlumno(String nombre) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NOMBREALUMNO;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ALUMNO_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBREALUMNO_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBREALUMNO_NOMBRE_1 = "alumno.nombre IS NULL";
	private static final String _FINDER_COLUMN_NOMBREALUMNO_NOMBRE_2 = "alumno.nombre = ?";
	private static final String _FINDER_COLUMN_NOMBREALUMNO_NOMBRE_3 = "(alumno.nombre IS NULL OR alumno.nombre = '')";

	public AlumnoPersistenceImpl() {
		setModelClass(Alumno.class);

		try {
			Field field = ReflectionUtil.getDeclaredField(BasePersistenceImpl.class,
					"_dbColumnNames");

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the alumno in the entity cache if it is enabled.
	 *
	 * @param alumno the alumno
	 */
	@Override
	public void cacheResult(Alumno alumno) {
		entityCache.putResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoImpl.class, alumno.getPrimaryKey(), alumno);

		alumno.resetOriginalValues();
	}

	/**
	 * Caches the alumnos in the entity cache if it is enabled.
	 *
	 * @param alumnos the alumnos
	 */
	@Override
	public void cacheResult(List<Alumno> alumnos) {
		for (Alumno alumno : alumnos) {
			if (entityCache.getResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
						AlumnoImpl.class, alumno.getPrimaryKey()) == null) {
				cacheResult(alumno);
			}
			else {
				alumno.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all alumnos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AlumnoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the alumno.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Alumno alumno) {
		entityCache.removeResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoImpl.class, alumno.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Alumno> alumnos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Alumno alumno : alumnos) {
			entityCache.removeResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
				AlumnoImpl.class, alumno.getPrimaryKey());
		}
	}

	/**
	 * Creates a new alumno with the primary key. Does not add the alumno to the database.
	 *
	 * @param alumnoId the primary key for the new alumno
	 * @return the new alumno
	 */
	@Override
	public Alumno create(long alumnoId) {
		Alumno alumno = new AlumnoImpl();

		alumno.setNew(true);
		alumno.setPrimaryKey(alumnoId);

		String uuid = PortalUUIDUtil.generate();

		alumno.setUuid(uuid);

		alumno.setCompanyId(companyProvider.getCompanyId());

		return alumno;
	}

	/**
	 * Removes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param alumnoId the primary key of the alumno
	 * @return the alumno that was removed
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno remove(long alumnoId) throws NoSuchAlumnoException {
		return remove((Serializable)alumnoId);
	}

	/**
	 * Removes the alumno with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the alumno
	 * @return the alumno that was removed
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno remove(Serializable primaryKey) throws NoSuchAlumnoException {
		Session session = null;

		try {
			session = openSession();

			Alumno alumno = (Alumno)session.get(AlumnoImpl.class, primaryKey);

			if (alumno == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAlumnoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(alumno);
		}
		catch (NoSuchAlumnoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Alumno removeImpl(Alumno alumno) {
		alumno = toUnwrappedModel(alumno);

		alumnoToOfertaTableMapper.deleteLeftPrimaryKeyTableMappings(alumno.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(alumno)) {
				alumno = (Alumno)session.get(AlumnoImpl.class,
						alumno.getPrimaryKeyObj());
			}

			if (alumno != null) {
				session.delete(alumno);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (alumno != null) {
			clearCache(alumno);
		}

		return alumno;
	}

	@Override
	public Alumno updateImpl(Alumno alumno) {
		alumno = toUnwrappedModel(alumno);

		boolean isNew = alumno.isNew();

		AlumnoModelImpl alumnoModelImpl = (AlumnoModelImpl)alumno;

		if (Validator.isNull(alumno.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			alumno.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (alumno.isNew()) {
				session.save(alumno);

				alumno.setNew(false);
			}
			else {
				session.evict(alumno);
				session.saveOrUpdate(alumno);
			}

			session.flush();
			session.clear();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!AlumnoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { alumnoModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					alumnoModelImpl.getUuid(), alumnoModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { alumnoModelImpl.getEmail() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
				args);

			args = new Object[] { alumnoModelImpl.getOfertaId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAASIGNADA, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA,
				args);

			args = new Object[] { alumnoModelImpl.getNombre() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREALUMNO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((alumnoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { alumnoModelImpl.getOriginalUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { alumnoModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((alumnoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						alumnoModelImpl.getOriginalUuid(),
						alumnoModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						alumnoModelImpl.getUuid(),
						alumnoModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((alumnoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { alumnoModelImpl.getOriginalEmail() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
					args);

				args = new Object[] { alumnoModelImpl.getEmail() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
					args);
			}

			if ((alumnoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						alumnoModelImpl.getOriginalOfertaId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAASIGNADA,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA,
					args);

				args = new Object[] { alumnoModelImpl.getOfertaId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAASIGNADA,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAASIGNADA,
					args);
			}

			if ((alumnoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { alumnoModelImpl.getOriginalNombre() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREALUMNO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO,
					args);

				args = new Object[] { alumnoModelImpl.getNombre() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREALUMNO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREALUMNO,
					args);
			}
		}

		entityCache.putResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
			AlumnoImpl.class, alumno.getPrimaryKey(), alumno, false);

		alumno.resetOriginalValues();

		return alumno;
	}

	protected Alumno toUnwrappedModel(Alumno alumno) {
		if (alumno instanceof AlumnoImpl) {
			return alumno;
		}

		AlumnoImpl alumnoImpl = new AlumnoImpl();

		alumnoImpl.setNew(alumno.isNew());
		alumnoImpl.setPrimaryKey(alumno.getPrimaryKey());

		alumnoImpl.setUuid(alumno.getUuid());
		alumnoImpl.setAlumnoId(alumno.getAlumnoId());
		alumnoImpl.setCompanyId(alumno.getCompanyId());
		alumnoImpl.setEmail(alumno.getEmail());
		alumnoImpl.setPwd(alumno.getPwd());
		alumnoImpl.setNombre(alumno.getNombre());
		alumnoImpl.setApellidos(alumno.getApellidos());
		alumnoImpl.setCompetencias(alumno.getCompetencias());
		alumnoImpl.setOfertaId(alumno.getOfertaId());
		alumnoImpl.setCurriculum(alumno.getCurriculum());

		return alumnoImpl;
	}

	/**
	 * Returns the alumno with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the alumno
	 * @return the alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAlumnoException {
		Alumno alumno = fetchByPrimaryKey(primaryKey);

		if (alumno == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAlumnoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return alumno;
	}

	/**
	 * Returns the alumno with the primary key or throws a {@link NoSuchAlumnoException} if it could not be found.
	 *
	 * @param alumnoId the primary key of the alumno
	 * @return the alumno
	 * @throws NoSuchAlumnoException if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno findByPrimaryKey(long alumnoId) throws NoSuchAlumnoException {
		return findByPrimaryKey((Serializable)alumnoId);
	}

	/**
	 * Returns the alumno with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the alumno
	 * @return the alumno, or <code>null</code> if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
				AlumnoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Alumno alumno = (Alumno)serializable;

		if (alumno == null) {
			Session session = null;

			try {
				session = openSession();

				alumno = (Alumno)session.get(AlumnoImpl.class, primaryKey);

				if (alumno != null) {
					cacheResult(alumno);
				}
				else {
					entityCache.putResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
						AlumnoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
					AlumnoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return alumno;
	}

	/**
	 * Returns the alumno with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param alumnoId the primary key of the alumno
	 * @return the alumno, or <code>null</code> if a alumno with the primary key could not be found
	 */
	@Override
	public Alumno fetchByPrimaryKey(long alumnoId) {
		return fetchByPrimaryKey((Serializable)alumnoId);
	}

	@Override
	public Map<Serializable, Alumno> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Alumno> map = new HashMap<Serializable, Alumno>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Alumno alumno = fetchByPrimaryKey(primaryKey);

			if (alumno != null) {
				map.put(primaryKey, alumno);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
					AlumnoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Alumno)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_ALUMNO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Alumno alumno : (List<Alumno>)q.list()) {
				map.put(alumno.getPrimaryKeyObj(), alumno);

				cacheResult(alumno);

				uncachedPrimaryKeys.remove(alumno.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(AlumnoModelImpl.ENTITY_CACHE_ENABLED,
					AlumnoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the alumnos.
	 *
	 * @return the alumnos
	 */
	@Override
	public List<Alumno> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alumnos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of alumnos
	 */
	@Override
	public List<Alumno> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of alumnos
	 */
	@Override
	public List<Alumno> findAll(int start, int end,
		OrderByComparator<Alumno> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alumnos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of alumnos
	 */
	@Override
	public List<Alumno> findAll(int start, int end,
		OrderByComparator<Alumno> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Alumno> list = null;

		if (retrieveFromCache) {
			list = (List<Alumno>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ALUMNO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ALUMNO;

				if (pagination) {
					sql = sql.concat(AlumnoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Alumno>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the alumnos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Alumno alumno : findAll()) {
			remove(alumno);
		}
	}

	/**
	 * Returns the number of alumnos.
	 *
	 * @return the number of alumnos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ALUMNO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of ofertas associated with the alumno.
	 *
	 * @param pk the primary key of the alumno
	 * @return long[] of the primaryKeys of ofertas associated with the alumno
	 */
	@Override
	public long[] getOfertaPrimaryKeys(long pk) {
		long[] pks = alumnoToOfertaTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the ofertas associated with the alumno.
	 *
	 * @param pk the primary key of the alumno
	 * @return the ofertas associated with the alumno
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk) {
		return getOfertas(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the ofertas associated with the alumno.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the alumno
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @return the range of ofertas associated with the alumno
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk, int start, int end) {
		return getOfertas(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas associated with the alumno.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AlumnoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the alumno
	 * @param start the lower bound of the range of alumnos
	 * @param end the upper bound of the range of alumnos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ofertas associated with the alumno
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return alumnoToOfertaTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of ofertas associated with the alumno.
	 *
	 * @param pk the primary key of the alumno
	 * @return the number of ofertas associated with the alumno
	 */
	@Override
	public int getOfertasSize(long pk) {
		long[] pks = alumnoToOfertaTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the oferta is associated with the alumno.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPK the primary key of the oferta
	 * @return <code>true</code> if the oferta is associated with the alumno; <code>false</code> otherwise
	 */
	@Override
	public boolean containsOferta(long pk, long ofertaPK) {
		return alumnoToOfertaTableMapper.containsTableMapping(pk, ofertaPK);
	}

	/**
	 * Returns <code>true</code> if the alumno has any ofertas associated with it.
	 *
	 * @param pk the primary key of the alumno to check for associations with ofertas
	 * @return <code>true</code> if the alumno has any ofertas associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsOfertas(long pk) {
		if (getOfertasSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPK the primary key of the oferta
	 */
	@Override
	public void addOferta(long pk, long ofertaPK) {
		Alumno alumno = fetchByPrimaryKey(pk);

		if (alumno == null) {
			alumnoToOfertaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, ofertaPK);
		}
		else {
			alumnoToOfertaTableMapper.addTableMapping(alumno.getCompanyId(),
				pk, ofertaPK);
		}
	}

	/**
	 * Adds an association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param oferta the oferta
	 */
	@Override
	public void addOferta(long pk, service.tfg.model.Oferta oferta) {
		Alumno alumno = fetchByPrimaryKey(pk);

		if (alumno == null) {
			alumnoToOfertaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, oferta.getPrimaryKey());
		}
		else {
			alumnoToOfertaTableMapper.addTableMapping(alumno.getCompanyId(),
				pk, oferta.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPKs the primary keys of the ofertas
	 */
	@Override
	public void addOfertas(long pk, long[] ofertaPKs) {
		long companyId = 0;

		Alumno alumno = fetchByPrimaryKey(pk);

		if (alumno == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = alumno.getCompanyId();
		}

		alumnoToOfertaTableMapper.addTableMappings(companyId, pk, ofertaPKs);
	}

	/**
	 * Adds an association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertas the ofertas
	 */
	@Override
	public void addOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		addOfertas(pk,
			ListUtil.toLongArray(ofertas,
				service.tfg.model.Oferta.OFERTA_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the alumno and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno to clear the associated ofertas from
	 */
	@Override
	public void clearOfertas(long pk) {
		alumnoToOfertaTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPK the primary key of the oferta
	 */
	@Override
	public void removeOferta(long pk, long ofertaPK) {
		alumnoToOfertaTableMapper.deleteTableMapping(pk, ofertaPK);
	}

	/**
	 * Removes the association between the alumno and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param oferta the oferta
	 */
	@Override
	public void removeOferta(long pk, service.tfg.model.Oferta oferta) {
		alumnoToOfertaTableMapper.deleteTableMapping(pk, oferta.getPrimaryKey());
	}

	/**
	 * Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPKs the primary keys of the ofertas
	 */
	@Override
	public void removeOfertas(long pk, long[] ofertaPKs) {
		alumnoToOfertaTableMapper.deleteTableMappings(pk, ofertaPKs);
	}

	/**
	 * Removes the association between the alumno and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertas the ofertas
	 */
	@Override
	public void removeOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		removeOfertas(pk,
			ListUtil.toLongArray(ofertas,
				service.tfg.model.Oferta.OFERTA_ID_ACCESSOR));
	}

	/**
	 * Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertaPKs the primary keys of the ofertas to be associated with the alumno
	 */
	@Override
	public void setOfertas(long pk, long[] ofertaPKs) {
		Set<Long> newOfertaPKsSet = SetUtil.fromArray(ofertaPKs);
		Set<Long> oldOfertaPKsSet = SetUtil.fromArray(alumnoToOfertaTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeOfertaPKsSet = new HashSet<Long>(oldOfertaPKsSet);

		removeOfertaPKsSet.removeAll(newOfertaPKsSet);

		alumnoToOfertaTableMapper.deleteTableMappings(pk,
			ArrayUtil.toLongArray(removeOfertaPKsSet));

		newOfertaPKsSet.removeAll(oldOfertaPKsSet);

		long companyId = 0;

		Alumno alumno = fetchByPrimaryKey(pk);

		if (alumno == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = alumno.getCompanyId();
		}

		alumnoToOfertaTableMapper.addTableMappings(companyId, pk,
			ArrayUtil.toLongArray(newOfertaPKsSet));
	}

	/**
	 * Sets the ofertas associated with the alumno, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the alumno
	 * @param ofertas the ofertas to be associated with the alumno
	 */
	@Override
	public void setOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		try {
			long[] ofertaPKs = new long[ofertas.size()];

			for (int i = 0; i < ofertas.size(); i++) {
				service.tfg.model.Oferta oferta = ofertas.get(i);

				ofertaPKs[i] = oferta.getPrimaryKey();
			}

			setOfertas(pk, ofertaPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AlumnoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the alumno persistence.
	 */
	public void afterPropertiesSet() {
		alumnoToOfertaTableMapper = TableMapperFactory.getTableMapper("BBDD_Alumno_Solicitudes",
				"companyId", "alumnoId", "ofertaId", this, ofertaPersistence);
	}

	public void destroy() {
		entityCache.removeCache(AlumnoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("BBDD_Alumno_Solicitudes");
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	@BeanReference(type = OfertaPersistence.class)
	protected OfertaPersistence ofertaPersistence;
	protected TableMapper<Alumno, service.tfg.model.Oferta> alumnoToOfertaTableMapper;
	private static final String _SQL_SELECT_ALUMNO = "SELECT alumno FROM Alumno alumno";
	private static final String _SQL_SELECT_ALUMNO_WHERE_PKS_IN = "SELECT alumno FROM Alumno alumno WHERE alumnoId IN (";
	private static final String _SQL_SELECT_ALUMNO_WHERE = "SELECT alumno FROM Alumno alumno WHERE ";
	private static final String _SQL_COUNT_ALUMNO = "SELECT COUNT(alumno) FROM Alumno alumno";
	private static final String _SQL_COUNT_ALUMNO_WHERE = "SELECT COUNT(alumno) FROM Alumno alumno WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "alumno.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Alumno exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Alumno exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(AlumnoPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}