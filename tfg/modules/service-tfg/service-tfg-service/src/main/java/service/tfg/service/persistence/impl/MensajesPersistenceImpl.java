/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReflectionUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import service.tfg.exception.NoSuchMensajesException;

import service.tfg.model.Mensajes;
import service.tfg.model.impl.MensajesImpl;
import service.tfg.model.impl.MensajesModelImpl;

import service.tfg.service.persistence.MensajesPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the mensajes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see MensajesPersistence
 * @see service.tfg.service.persistence.MensajesUtil
 * @generated
 */
@ProviderType
public class MensajesPersistenceImpl extends BasePersistenceImpl<Mensajes>
	implements MensajesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MensajesUtil} to access the mensajes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MensajesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			MensajesModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the mensajeses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mensajeses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @return the range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the mensajeses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid(String uuid, int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mensajeses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid(String uuid, int start, int end,
		OrderByComparator<Mensajes> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Mensajes> list = null;

		if (retrieveFromCache) {
			list = (List<Mensajes>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Mensajes mensajes : list) {
					if (!Objects.equals(uuid, mensajes.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MENSAJES_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MensajesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first mensajes in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByUuid_First(String uuid,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByUuid_First(uuid, orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the first mensajes in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByUuid_First(String uuid,
		OrderByComparator<Mensajes> orderByComparator) {
		List<Mensajes> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last mensajes in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByUuid_Last(String uuid,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByUuid_Last(uuid, orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the last mensajes in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByUuid_Last(String uuid,
		OrderByComparator<Mensajes> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Mensajes> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63;.
	 *
	 * @param mensajeId the primary key of the current mensajes
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes[] findByUuid_PrevAndNext(long mensajeId, String uuid,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = findByPrimaryKey(mensajeId);

		Session session = null;

		try {
			session = openSession();

			Mensajes[] array = new MensajesImpl[3];

			array[0] = getByUuid_PrevAndNext(session, mensajes, uuid,
					orderByComparator, true);

			array[1] = mensajes;

			array[2] = getByUuid_PrevAndNext(session, mensajes, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Mensajes getByUuid_PrevAndNext(Session session,
		Mensajes mensajes, String uuid,
		OrderByComparator<Mensajes> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MENSAJES_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MensajesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(mensajes);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Mensajes> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the mensajeses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Mensajes mensajes : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(mensajes);
		}
	}

	/**
	 * Returns the number of mensajeses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching mensajeses
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MENSAJES_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "mensajes.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "mensajes.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(mensajes.uuid IS NULL OR mensajes.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			MensajesModelImpl.UUID_COLUMN_BITMASK |
			MensajesModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the mensajeses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @return the range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid_C(String uuid, long companyId, int start,
		int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Mensajes> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mensajeses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Mensajes> list = null;

		if (retrieveFromCache) {
			list = (List<Mensajes>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Mensajes mensajes : list) {
					if (!Objects.equals(uuid, mensajes.getUuid()) ||
							(companyId != mensajes.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_MENSAJES_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MensajesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the first mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator) {
		List<Mensajes> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the last mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Mensajes> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the mensajeses before and after the current mensajes in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param mensajeId the primary key of the current mensajes
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes[] findByUuid_C_PrevAndNext(long mensajeId, String uuid,
		long companyId, OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = findByPrimaryKey(mensajeId);

		Session session = null;

		try {
			session = openSession();

			Mensajes[] array = new MensajesImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, mensajes, uuid,
					companyId, orderByComparator, true);

			array[1] = mensajes;

			array[2] = getByUuid_C_PrevAndNext(session, mensajes, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Mensajes getByUuid_C_PrevAndNext(Session session,
		Mensajes mensajes, String uuid, long companyId,
		OrderByComparator<Mensajes> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_MENSAJES_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MensajesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(mensajes);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Mensajes> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the mensajeses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Mensajes mensajes : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(mensajes);
		}
	}

	/**
	 * Returns the number of mensajeses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching mensajeses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_MENSAJES_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "mensajes.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "mensajes.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(mensajes.uuid IS NULL OR mensajes.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "mensajes.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REMITENTEID =
		new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByremitenteId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID =
		new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByremitenteId",
			new String[] { String.class.getName() },
			MensajesModelImpl.REMITENTE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REMITENTEID = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByremitenteId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the mensajeses where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @return the matching mensajeses
	 */
	@Override
	public List<Mensajes> findByremitenteId(String remitente) {
		return findByremitenteId(remitente, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mensajeses where remitente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param remitente the remitente
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @return the range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByremitenteId(String remitente, int start, int end) {
		return findByremitenteId(remitente, start, end, null);
	}

	/**
	 * Returns an ordered range of all the mensajeses where remitente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param remitente the remitente
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByremitenteId(String remitente, int start,
		int end, OrderByComparator<Mensajes> orderByComparator) {
		return findByremitenteId(remitente, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mensajeses where remitente = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param remitente the remitente
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findByremitenteId(String remitente, int start,
		int end, OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID;
			finderArgs = new Object[] { remitente };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REMITENTEID;
			finderArgs = new Object[] { remitente, start, end, orderByComparator };
		}

		List<Mensajes> list = null;

		if (retrieveFromCache) {
			list = (List<Mensajes>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Mensajes mensajes : list) {
					if (!Objects.equals(remitente, mensajes.getRemitente())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MENSAJES_WHERE);

			boolean bindRemitente = false;

			if (remitente == null) {
				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_1);
			}
			else if (remitente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_3);
			}
			else {
				bindRemitente = true;

				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MensajesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindRemitente) {
					qPos.add(remitente);
				}

				if (!pagination) {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first mensajes in the ordered set where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByremitenteId_First(String remitente,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByremitenteId_First(remitente,
				orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("remitente=");
		msg.append(remitente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the first mensajes in the ordered set where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByremitenteId_First(String remitente,
		OrderByComparator<Mensajes> orderByComparator) {
		List<Mensajes> list = findByremitenteId(remitente, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last mensajes in the ordered set where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findByremitenteId_Last(String remitente,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByremitenteId_Last(remitente, orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("remitente=");
		msg.append(remitente);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the last mensajes in the ordered set where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchByremitenteId_Last(String remitente,
		OrderByComparator<Mensajes> orderByComparator) {
		int count = countByremitenteId(remitente);

		if (count == 0) {
			return null;
		}

		List<Mensajes> list = findByremitenteId(remitente, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the mensajeses before and after the current mensajes in the ordered set where remitente = &#63;.
	 *
	 * @param mensajeId the primary key of the current mensajes
	 * @param remitente the remitente
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes[] findByremitenteId_PrevAndNext(long mensajeId,
		String remitente, OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = findByPrimaryKey(mensajeId);

		Session session = null;

		try {
			session = openSession();

			Mensajes[] array = new MensajesImpl[3];

			array[0] = getByremitenteId_PrevAndNext(session, mensajes,
					remitente, orderByComparator, true);

			array[1] = mensajes;

			array[2] = getByremitenteId_PrevAndNext(session, mensajes,
					remitente, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Mensajes getByremitenteId_PrevAndNext(Session session,
		Mensajes mensajes, String remitente,
		OrderByComparator<Mensajes> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MENSAJES_WHERE);

		boolean bindRemitente = false;

		if (remitente == null) {
			query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_1);
		}
		else if (remitente.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_3);
		}
		else {
			bindRemitente = true;

			query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MensajesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindRemitente) {
			qPos.add(remitente);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(mensajes);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Mensajes> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the mensajeses where remitente = &#63; from the database.
	 *
	 * @param remitente the remitente
	 */
	@Override
	public void removeByremitenteId(String remitente) {
		for (Mensajes mensajes : findByremitenteId(remitente,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(mensajes);
		}
	}

	/**
	 * Returns the number of mensajeses where remitente = &#63;.
	 *
	 * @param remitente the remitente
	 * @return the number of matching mensajeses
	 */
	@Override
	public int countByremitenteId(String remitente) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REMITENTEID;

		Object[] finderArgs = new Object[] { remitente };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MENSAJES_WHERE);

			boolean bindRemitente = false;

			if (remitente == null) {
				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_1);
			}
			else if (remitente.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_3);
			}
			else {
				bindRemitente = true;

				query.append(_FINDER_COLUMN_REMITENTEID_REMITENTE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindRemitente) {
					qPos.add(remitente);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REMITENTEID_REMITENTE_1 = "mensajes.remitente IS NULL";
	private static final String _FINDER_COLUMN_REMITENTEID_REMITENTE_2 = "mensajes.remitente = ?";
	private static final String _FINDER_COLUMN_REMITENTEID_REMITENTE_3 = "(mensajes.remitente IS NULL OR mensajes.remitente = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DESTINATARIOID =
		new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBydestinatarioId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID =
		new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, MensajesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBydestinatarioId",
			new String[] { String.class.getName() },
			MensajesModelImpl.DESTINATARIO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DESTINATARIOID = new FinderPath(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBydestinatarioId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the mensajeses where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @return the matching mensajeses
	 */
	@Override
	public List<Mensajes> findBydestinatarioId(String destinatario) {
		return findBydestinatarioId(destinatario, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mensajeses where destinatario = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param destinatario the destinatario
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @return the range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findBydestinatarioId(String destinatario, int start,
		int end) {
		return findBydestinatarioId(destinatario, start, end, null);
	}

	/**
	 * Returns an ordered range of all the mensajeses where destinatario = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param destinatario the destinatario
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findBydestinatarioId(String destinatario, int start,
		int end, OrderByComparator<Mensajes> orderByComparator) {
		return findBydestinatarioId(destinatario, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mensajeses where destinatario = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param destinatario the destinatario
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching mensajeses
	 */
	@Override
	public List<Mensajes> findBydestinatarioId(String destinatario, int start,
		int end, OrderByComparator<Mensajes> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID;
			finderArgs = new Object[] { destinatario };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DESTINATARIOID;
			finderArgs = new Object[] {
					destinatario,
					
					start, end, orderByComparator
				};
		}

		List<Mensajes> list = null;

		if (retrieveFromCache) {
			list = (List<Mensajes>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Mensajes mensajes : list) {
					if (!Objects.equals(destinatario, mensajes.getDestinatario())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MENSAJES_WHERE);

			boolean bindDestinatario = false;

			if (destinatario == null) {
				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_1);
			}
			else if (destinatario.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_3);
			}
			else {
				bindDestinatario = true;

				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MensajesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDestinatario) {
					qPos.add(destinatario);
				}

				if (!pagination) {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first mensajes in the ordered set where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findBydestinatarioId_First(String destinatario,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchBydestinatarioId_First(destinatario,
				orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("destinatario=");
		msg.append(destinatario);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the first mensajes in the ordered set where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchBydestinatarioId_First(String destinatario,
		OrderByComparator<Mensajes> orderByComparator) {
		List<Mensajes> list = findBydestinatarioId(destinatario, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last mensajes in the ordered set where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes
	 * @throws NoSuchMensajesException if a matching mensajes could not be found
	 */
	@Override
	public Mensajes findBydestinatarioId_Last(String destinatario,
		OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchBydestinatarioId_Last(destinatario,
				orderByComparator);

		if (mensajes != null) {
			return mensajes;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("destinatario=");
		msg.append(destinatario);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMensajesException(msg.toString());
	}

	/**
	 * Returns the last mensajes in the ordered set where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mensajes, or <code>null</code> if a matching mensajes could not be found
	 */
	@Override
	public Mensajes fetchBydestinatarioId_Last(String destinatario,
		OrderByComparator<Mensajes> orderByComparator) {
		int count = countBydestinatarioId(destinatario);

		if (count == 0) {
			return null;
		}

		List<Mensajes> list = findBydestinatarioId(destinatario, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the mensajeses before and after the current mensajes in the ordered set where destinatario = &#63;.
	 *
	 * @param mensajeId the primary key of the current mensajes
	 * @param destinatario the destinatario
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes[] findBydestinatarioId_PrevAndNext(long mensajeId,
		String destinatario, OrderByComparator<Mensajes> orderByComparator)
		throws NoSuchMensajesException {
		Mensajes mensajes = findByPrimaryKey(mensajeId);

		Session session = null;

		try {
			session = openSession();

			Mensajes[] array = new MensajesImpl[3];

			array[0] = getBydestinatarioId_PrevAndNext(session, mensajes,
					destinatario, orderByComparator, true);

			array[1] = mensajes;

			array[2] = getBydestinatarioId_PrevAndNext(session, mensajes,
					destinatario, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Mensajes getBydestinatarioId_PrevAndNext(Session session,
		Mensajes mensajes, String destinatario,
		OrderByComparator<Mensajes> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MENSAJES_WHERE);

		boolean bindDestinatario = false;

		if (destinatario == null) {
			query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_1);
		}
		else if (destinatario.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_3);
		}
		else {
			bindDestinatario = true;

			query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MensajesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDestinatario) {
			qPos.add(destinatario);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(mensajes);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Mensajes> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the mensajeses where destinatario = &#63; from the database.
	 *
	 * @param destinatario the destinatario
	 */
	@Override
	public void removeBydestinatarioId(String destinatario) {
		for (Mensajes mensajes : findBydestinatarioId(destinatario,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(mensajes);
		}
	}

	/**
	 * Returns the number of mensajeses where destinatario = &#63;.
	 *
	 * @param destinatario the destinatario
	 * @return the number of matching mensajeses
	 */
	@Override
	public int countBydestinatarioId(String destinatario) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DESTINATARIOID;

		Object[] finderArgs = new Object[] { destinatario };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MENSAJES_WHERE);

			boolean bindDestinatario = false;

			if (destinatario == null) {
				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_1);
			}
			else if (destinatario.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_3);
			}
			else {
				bindDestinatario = true;

				query.append(_FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDestinatario) {
					qPos.add(destinatario);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_1 = "mensajes.destinatario IS NULL";
	private static final String _FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_2 = "mensajes.destinatario = ?";
	private static final String _FINDER_COLUMN_DESTINATARIOID_DESTINATARIO_3 = "(mensajes.destinatario IS NULL OR mensajes.destinatario = '')";

	public MensajesPersistenceImpl() {
		setModelClass(Mensajes.class);

		try {
			Field field = ReflectionUtil.getDeclaredField(BasePersistenceImpl.class,
					"_dbColumnNames");

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the mensajes in the entity cache if it is enabled.
	 *
	 * @param mensajes the mensajes
	 */
	@Override
	public void cacheResult(Mensajes mensajes) {
		entityCache.putResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesImpl.class, mensajes.getPrimaryKey(), mensajes);

		mensajes.resetOriginalValues();
	}

	/**
	 * Caches the mensajeses in the entity cache if it is enabled.
	 *
	 * @param mensajeses the mensajeses
	 */
	@Override
	public void cacheResult(List<Mensajes> mensajeses) {
		for (Mensajes mensajes : mensajeses) {
			if (entityCache.getResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
						MensajesImpl.class, mensajes.getPrimaryKey()) == null) {
				cacheResult(mensajes);
			}
			else {
				mensajes.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all mensajeses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MensajesImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the mensajes.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Mensajes mensajes) {
		entityCache.removeResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesImpl.class, mensajes.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Mensajes> mensajeses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Mensajes mensajes : mensajeses) {
			entityCache.removeResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
				MensajesImpl.class, mensajes.getPrimaryKey());
		}
	}

	/**
	 * Creates a new mensajes with the primary key. Does not add the mensajes to the database.
	 *
	 * @param mensajeId the primary key for the new mensajes
	 * @return the new mensajes
	 */
	@Override
	public Mensajes create(long mensajeId) {
		Mensajes mensajes = new MensajesImpl();

		mensajes.setNew(true);
		mensajes.setPrimaryKey(mensajeId);

		String uuid = PortalUUIDUtil.generate();

		mensajes.setUuid(uuid);

		mensajes.setCompanyId(companyProvider.getCompanyId());

		return mensajes;
	}

	/**
	 * Removes the mensajes with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mensajeId the primary key of the mensajes
	 * @return the mensajes that was removed
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes remove(long mensajeId) throws NoSuchMensajesException {
		return remove((Serializable)mensajeId);
	}

	/**
	 * Removes the mensajes with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the mensajes
	 * @return the mensajes that was removed
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes remove(Serializable primaryKey)
		throws NoSuchMensajesException {
		Session session = null;

		try {
			session = openSession();

			Mensajes mensajes = (Mensajes)session.get(MensajesImpl.class,
					primaryKey);

			if (mensajes == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMensajesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(mensajes);
		}
		catch (NoSuchMensajesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Mensajes removeImpl(Mensajes mensajes) {
		mensajes = toUnwrappedModel(mensajes);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(mensajes)) {
				mensajes = (Mensajes)session.get(MensajesImpl.class,
						mensajes.getPrimaryKeyObj());
			}

			if (mensajes != null) {
				session.delete(mensajes);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (mensajes != null) {
			clearCache(mensajes);
		}

		return mensajes;
	}

	@Override
	public Mensajes updateImpl(Mensajes mensajes) {
		mensajes = toUnwrappedModel(mensajes);

		boolean isNew = mensajes.isNew();

		MensajesModelImpl mensajesModelImpl = (MensajesModelImpl)mensajes;

		if (Validator.isNull(mensajes.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			mensajes.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (mensajes.isNew()) {
				session.save(mensajes);

				mensajes.setNew(false);
			}
			else {
				mensajes = (Mensajes)session.merge(mensajes);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!MensajesModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { mensajesModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					mensajesModelImpl.getUuid(),
					mensajesModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { mensajesModelImpl.getRemitente() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_REMITENTEID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID,
				args);

			args = new Object[] { mensajesModelImpl.getDestinatario() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_DESTINATARIOID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((mensajesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { mensajesModelImpl.getOriginalUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { mensajesModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((mensajesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						mensajesModelImpl.getOriginalUuid(),
						mensajesModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						mensajesModelImpl.getUuid(),
						mensajesModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((mensajesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						mensajesModelImpl.getOriginalRemitente()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REMITENTEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID,
					args);

				args = new Object[] { mensajesModelImpl.getRemitente() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REMITENTEID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REMITENTEID,
					args);
			}

			if ((mensajesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						mensajesModelImpl.getOriginalDestinatario()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_DESTINATARIOID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID,
					args);

				args = new Object[] { mensajesModelImpl.getDestinatario() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_DESTINATARIOID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DESTINATARIOID,
					args);
			}
		}

		entityCache.putResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
			MensajesImpl.class, mensajes.getPrimaryKey(), mensajes, false);

		mensajes.resetOriginalValues();

		return mensajes;
	}

	protected Mensajes toUnwrappedModel(Mensajes mensajes) {
		if (mensajes instanceof MensajesImpl) {
			return mensajes;
		}

		MensajesImpl mensajesImpl = new MensajesImpl();

		mensajesImpl.setNew(mensajes.isNew());
		mensajesImpl.setPrimaryKey(mensajes.getPrimaryKey());

		mensajesImpl.setUuid(mensajes.getUuid());
		mensajesImpl.setMensajeId(mensajes.getMensajeId());
		mensajesImpl.setCompanyId(mensajes.getCompanyId());
		mensajesImpl.setRemitente(mensajes.getRemitente());
		mensajesImpl.setDestinatario(mensajes.getDestinatario());
		mensajesImpl.setMensaje(mensajes.getMensaje());
		mensajesImpl.setFecha(mensajes.getFecha());

		return mensajesImpl;
	}

	/**
	 * Returns the mensajes with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the mensajes
	 * @return the mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMensajesException {
		Mensajes mensajes = fetchByPrimaryKey(primaryKey);

		if (mensajes == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMensajesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return mensajes;
	}

	/**
	 * Returns the mensajes with the primary key or throws a {@link NoSuchMensajesException} if it could not be found.
	 *
	 * @param mensajeId the primary key of the mensajes
	 * @return the mensajes
	 * @throws NoSuchMensajesException if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes findByPrimaryKey(long mensajeId)
		throws NoSuchMensajesException {
		return findByPrimaryKey((Serializable)mensajeId);
	}

	/**
	 * Returns the mensajes with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the mensajes
	 * @return the mensajes, or <code>null</code> if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
				MensajesImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Mensajes mensajes = (Mensajes)serializable;

		if (mensajes == null) {
			Session session = null;

			try {
				session = openSession();

				mensajes = (Mensajes)session.get(MensajesImpl.class, primaryKey);

				if (mensajes != null) {
					cacheResult(mensajes);
				}
				else {
					entityCache.putResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
						MensajesImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
					MensajesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return mensajes;
	}

	/**
	 * Returns the mensajes with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mensajeId the primary key of the mensajes
	 * @return the mensajes, or <code>null</code> if a mensajes with the primary key could not be found
	 */
	@Override
	public Mensajes fetchByPrimaryKey(long mensajeId) {
		return fetchByPrimaryKey((Serializable)mensajeId);
	}

	@Override
	public Map<Serializable, Mensajes> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Mensajes> map = new HashMap<Serializable, Mensajes>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Mensajes mensajes = fetchByPrimaryKey(primaryKey);

			if (mensajes != null) {
				map.put(primaryKey, mensajes);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
					MensajesImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Mensajes)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_MENSAJES_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Mensajes mensajes : (List<Mensajes>)q.list()) {
				map.put(mensajes.getPrimaryKeyObj(), mensajes);

				cacheResult(mensajes);

				uncachedPrimaryKeys.remove(mensajes.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(MensajesModelImpl.ENTITY_CACHE_ENABLED,
					MensajesImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the mensajeses.
	 *
	 * @return the mensajeses
	 */
	@Override
	public List<Mensajes> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the mensajeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @return the range of mensajeses
	 */
	@Override
	public List<Mensajes> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the mensajeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mensajeses
	 */
	@Override
	public List<Mensajes> findAll(int start, int end,
		OrderByComparator<Mensajes> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the mensajeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MensajesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of mensajeses
	 * @param end the upper bound of the range of mensajeses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of mensajeses
	 */
	@Override
	public List<Mensajes> findAll(int start, int end,
		OrderByComparator<Mensajes> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Mensajes> list = null;

		if (retrieveFromCache) {
			list = (List<Mensajes>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_MENSAJES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MENSAJES;

				if (pagination) {
					sql = sql.concat(MensajesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Mensajes>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the mensajeses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Mensajes mensajes : findAll()) {
			remove(mensajes);
		}
	}

	/**
	 * Returns the number of mensajeses.
	 *
	 * @return the number of mensajeses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MENSAJES);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MensajesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the mensajes persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(MensajesImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_MENSAJES = "SELECT mensajes FROM Mensajes mensajes";
	private static final String _SQL_SELECT_MENSAJES_WHERE_PKS_IN = "SELECT mensajes FROM Mensajes mensajes WHERE mensajeId IN (";
	private static final String _SQL_SELECT_MENSAJES_WHERE = "SELECT mensajes FROM Mensajes mensajes WHERE ";
	private static final String _SQL_COUNT_MENSAJES = "SELECT COUNT(mensajes) FROM Mensajes mensajes";
	private static final String _SQL_COUNT_MENSAJES_WHERE = "SELECT COUNT(mensajes) FROM Mensajes mensajes WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "mensajes.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Mensajes exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Mensajes exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(MensajesPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}