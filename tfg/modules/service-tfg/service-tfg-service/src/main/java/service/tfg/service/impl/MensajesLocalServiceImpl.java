/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import service.tfg.model.Mensajes;
import service.tfg.model.impl.MensajesImpl;
import service.tfg.service.base.MensajesLocalServiceBaseImpl;

/**
 * The implementation of the mensajes local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link service.tfg.service.MensajesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Ismael Medina
 * @see MensajesLocalServiceBaseImpl
 * @see service.tfg.service.MensajesLocalServiceUtil
 */
public class MensajesLocalServiceImpl extends MensajesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * service.tfg.service.MensajesLocalServiceUtil} to access the mensajes
	 * local service.
	 */

	public void addMensae(String remitente, String destinatario, String mensaje, LocalDate fecha, long companyId) {

		final Mensajes mensajes = new MensajesImpl();

		mensajes.setMensajeId(counterLocalService.increment());
		mensajes.setCompanyId(companyId);
		mensajes.setDestinatario(destinatario);
		mensajes.setRemitente(remitente);
		mensajes.setMensaje(mensaje);
		mensajes.setFecha(localDateToDate(fecha));

		addMensajes(mensajes);

	}

	private Date localDateToDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public List<Mensajes> findBydestinatarioId(String destinatario) {
		return getMensajesPersistence().findBydestinatarioId(destinatario);
	}

	public List<Mensajes> findByremitenteId(String remitente) {
		return getMensajesPersistence().findByremitenteId(remitente);
	}

}