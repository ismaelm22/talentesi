/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import service.tfg.model.Alumno;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Alumno in entity cache.
 *
 * @author Ismael Medina
 * @see Alumno
 * @generated
 */
@ProviderType
public class AlumnoCacheModel implements CacheModel<Alumno>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AlumnoCacheModel)) {
			return false;
		}

		AlumnoCacheModel alumnoCacheModel = (AlumnoCacheModel)obj;

		if (alumnoId == alumnoCacheModel.alumnoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, alumnoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", alumnoId=");
		sb.append(alumnoId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", email=");
		sb.append(email);
		sb.append(", pwd=");
		sb.append(pwd);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidos=");
		sb.append(apellidos);
		sb.append(", competencias=");
		sb.append(competencias);
		sb.append(", ofertaId=");
		sb.append(ofertaId);

		return sb.toString();
	}

	@Override
	public Alumno toEntityModel() {
		AlumnoImpl alumnoImpl = new AlumnoImpl();

		if (uuid == null) {
			alumnoImpl.setUuid(StringPool.BLANK);
		}
		else {
			alumnoImpl.setUuid(uuid);
		}

		alumnoImpl.setAlumnoId(alumnoId);
		alumnoImpl.setCompanyId(companyId);

		if (email == null) {
			alumnoImpl.setEmail(StringPool.BLANK);
		}
		else {
			alumnoImpl.setEmail(email);
		}

		if (pwd == null) {
			alumnoImpl.setPwd(StringPool.BLANK);
		}
		else {
			alumnoImpl.setPwd(pwd);
		}

		if (nombre == null) {
			alumnoImpl.setNombre(StringPool.BLANK);
		}
		else {
			alumnoImpl.setNombre(nombre);
		}

		if (apellidos == null) {
			alumnoImpl.setApellidos(StringPool.BLANK);
		}
		else {
			alumnoImpl.setApellidos(apellidos);
		}

		if (competencias == null) {
			alumnoImpl.setCompetencias(StringPool.BLANK);
		}
		else {
			alumnoImpl.setCompetencias(competencias);
		}

		alumnoImpl.setOfertaId(ofertaId);

		alumnoImpl.resetOriginalValues();

		return alumnoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		alumnoId = objectInput.readLong();

		companyId = objectInput.readLong();
		email = objectInput.readUTF();
		pwd = objectInput.readUTF();
		nombre = objectInput.readUTF();
		apellidos = objectInput.readUTF();
		competencias = objectInput.readUTF();

		ofertaId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(alumnoId);

		objectOutput.writeLong(companyId);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (pwd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pwd);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidos == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidos);
		}

		if (competencias == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(competencias);
		}

		objectOutput.writeLong(ofertaId);
	}

	public String uuid;
	public long alumnoId;
	public long companyId;
	public String email;
	public String pwd;
	public String nombre;
	public String apellidos;
	public String competencias;
	public long ofertaId;
}