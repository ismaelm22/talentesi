/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReflectionUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import service.tfg.exception.NoSuchOfertaException;

import service.tfg.model.Oferta;
import service.tfg.model.impl.OfertaImpl;
import service.tfg.model.impl.OfertaModelImpl;

import service.tfg.service.persistence.AlumnoPersistence;
import service.tfg.service.persistence.EmpresaPersistence;
import service.tfg.service.persistence.OfertaPersistence;
import service.tfg.service.persistence.ProfesorPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the oferta service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see OfertaPersistence
 * @see service.tfg.service.persistence.OfertaUtil
 * @generated
 */
@ProviderType
public class OfertaPersistenceImpl extends BasePersistenceImpl<Oferta>
	implements OfertaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OfertaUtil} to access the oferta persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OfertaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			OfertaModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ofertas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ofertas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid(String uuid, int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid(String uuid, int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Oferta oferta : list) {
					if (!Objects.equals(uuid, oferta.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_OFERTA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OfertaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first oferta in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByUuid_First(String uuid,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByUuid_First(uuid, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the first oferta in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByUuid_First(String uuid,
		OrderByComparator<Oferta> orderByComparator) {
		List<Oferta> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last oferta in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByUuid_Last(String uuid,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByUuid_Last(uuid, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the last oferta in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByUuid_Last(String uuid,
		OrderByComparator<Oferta> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Oferta> list = findByUuid(uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63;.
	 *
	 * @param ofertaId the primary key of the current oferta
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta[] findByUuid_PrevAndNext(long ofertaId, String uuid,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = findByPrimaryKey(ofertaId);

		Session session = null;

		try {
			session = openSession();

			Oferta[] array = new OfertaImpl[3];

			array[0] = getByUuid_PrevAndNext(session, oferta, uuid,
					orderByComparator, true);

			array[1] = oferta;

			array[2] = getByUuid_PrevAndNext(session, oferta, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Oferta getByUuid_PrevAndNext(Session session, Oferta oferta,
		String uuid, OrderByComparator<Oferta> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_OFERTA_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OfertaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(oferta);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Oferta> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ofertas where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Oferta oferta : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching ofertas
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_OFERTA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "oferta.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "oferta.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(oferta.uuid IS NULL OR oferta.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			OfertaModelImpl.UUID_COLUMN_BITMASK |
			OfertaModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the ofertas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ofertas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid_C(String uuid, long companyId, int start,
		int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Oferta> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Oferta> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Oferta oferta : list) {
					if (!Objects.equals(uuid, oferta.getUuid()) ||
							(companyId != oferta.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_OFERTA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OfertaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the first oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator) {
		List<Oferta> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the last oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Oferta> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ofertas before and after the current oferta in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param ofertaId the primary key of the current oferta
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta[] findByUuid_C_PrevAndNext(long ofertaId, String uuid,
		long companyId, OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = findByPrimaryKey(ofertaId);

		Session session = null;

		try {
			session = openSession();

			Oferta[] array = new OfertaImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, oferta, uuid,
					companyId, orderByComparator, true);

			array[1] = oferta;

			array[2] = getByUuid_C_PrevAndNext(session, oferta, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Oferta getByUuid_C_PrevAndNext(Session session, Oferta oferta,
		String uuid, long companyId,
		OrderByComparator<Oferta> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_OFERTA_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OfertaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(oferta);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Oferta> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ofertas where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Oferta oferta : findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching ofertas
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_OFERTA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "oferta.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "oferta.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(oferta.uuid IS NULL OR oferta.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "oferta.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_AREATYPE = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByareaType",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE =
		new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByareaType",
			new String[] { String.class.getName() },
			OfertaModelImpl.AREA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_AREATYPE = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByareaType",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ofertas where area = &#63;.
	 *
	 * @param area the area
	 * @return the matching ofertas
	 */
	@Override
	public List<Oferta> findByareaType(String area) {
		return findByareaType(area, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ofertas where area = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param area the area
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of matching ofertas
	 */
	@Override
	public List<Oferta> findByareaType(String area, int start, int end) {
		return findByareaType(area, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas where area = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param area the area
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByareaType(String area, int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return findByareaType(area, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas where area = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param area the area
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByareaType(String area, int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE;
			finderArgs = new Object[] { area };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_AREATYPE;
			finderArgs = new Object[] { area, start, end, orderByComparator };
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Oferta oferta : list) {
					if (!Objects.equals(area, oferta.getArea())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_OFERTA_WHERE);

			boolean bindArea = false;

			if (area == null) {
				query.append(_FINDER_COLUMN_AREATYPE_AREA_1);
			}
			else if (area.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_AREATYPE_AREA_3);
			}
			else {
				bindArea = true;

				query.append(_FINDER_COLUMN_AREATYPE_AREA_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OfertaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindArea) {
					qPos.add(area);
				}

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first oferta in the ordered set where area = &#63;.
	 *
	 * @param area the area
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByareaType_First(String area,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByareaType_First(area, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("area=");
		msg.append(area);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the first oferta in the ordered set where area = &#63;.
	 *
	 * @param area the area
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByareaType_First(String area,
		OrderByComparator<Oferta> orderByComparator) {
		List<Oferta> list = findByareaType(area, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last oferta in the ordered set where area = &#63;.
	 *
	 * @param area the area
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByareaType_Last(String area,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByareaType_Last(area, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("area=");
		msg.append(area);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the last oferta in the ordered set where area = &#63;.
	 *
	 * @param area the area
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByareaType_Last(String area,
		OrderByComparator<Oferta> orderByComparator) {
		int count = countByareaType(area);

		if (count == 0) {
			return null;
		}

		List<Oferta> list = findByareaType(area, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ofertas before and after the current oferta in the ordered set where area = &#63;.
	 *
	 * @param ofertaId the primary key of the current oferta
	 * @param area the area
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta[] findByareaType_PrevAndNext(long ofertaId, String area,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = findByPrimaryKey(ofertaId);

		Session session = null;

		try {
			session = openSession();

			Oferta[] array = new OfertaImpl[3];

			array[0] = getByareaType_PrevAndNext(session, oferta, area,
					orderByComparator, true);

			array[1] = oferta;

			array[2] = getByareaType_PrevAndNext(session, oferta, area,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Oferta getByareaType_PrevAndNext(Session session, Oferta oferta,
		String area, OrderByComparator<Oferta> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_OFERTA_WHERE);

		boolean bindArea = false;

		if (area == null) {
			query.append(_FINDER_COLUMN_AREATYPE_AREA_1);
		}
		else if (area.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_AREATYPE_AREA_3);
		}
		else {
			bindArea = true;

			query.append(_FINDER_COLUMN_AREATYPE_AREA_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OfertaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindArea) {
			qPos.add(area);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(oferta);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Oferta> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ofertas where area = &#63; from the database.
	 *
	 * @param area the area
	 */
	@Override
	public void removeByareaType(String area) {
		for (Oferta oferta : findByareaType(area, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas where area = &#63;.
	 *
	 * @param area the area
	 * @return the number of matching ofertas
	 */
	@Override
	public int countByareaType(String area) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_AREATYPE;

		Object[] finderArgs = new Object[] { area };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_OFERTA_WHERE);

			boolean bindArea = false;

			if (area == null) {
				query.append(_FINDER_COLUMN_AREATYPE_AREA_1);
			}
			else if (area.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_AREATYPE_AREA_3);
			}
			else {
				bindArea = true;

				query.append(_FINDER_COLUMN_AREATYPE_AREA_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindArea) {
					qPos.add(area);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_AREATYPE_AREA_1 = "oferta.area IS NULL";
	private static final String _FINDER_COLUMN_AREATYPE_AREA_2 = "oferta.area = ?";
	private static final String _FINDER_COLUMN_AREATYPE_AREA_3 = "(oferta.area IS NULL OR oferta.area = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OFERTAID = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOfertaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID =
		new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOfertaId",
			new String[] { Long.class.getName() },
			OfertaModelImpl.OFERTAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_OFERTAID = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOfertaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the ofertas where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @return the matching ofertas
	 */
	@Override
	public List<Oferta> findByOfertaId(long ofertaId) {
		return findByOfertaId(ofertaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ofertas where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of matching ofertas
	 */
	@Override
	public List<Oferta> findByOfertaId(long ofertaId, int start, int end) {
		return findByOfertaId(ofertaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByOfertaId(long ofertaId, int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return findByOfertaId(ofertaId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas where ofertaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ofertaId the oferta ID
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByOfertaId(long ofertaId, int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID;
			finderArgs = new Object[] { ofertaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OFERTAID;
			finderArgs = new Object[] { ofertaId, start, end, orderByComparator };
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Oferta oferta : list) {
					if ((ofertaId != oferta.getOfertaId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_OFERTA_WHERE);

			query.append(_FINDER_COLUMN_OFERTAID_OFERTAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OfertaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ofertaId);

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first oferta in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByOfertaId_First(long ofertaId,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByOfertaId_First(ofertaId, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ofertaId=");
		msg.append(ofertaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the first oferta in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByOfertaId_First(long ofertaId,
		OrderByComparator<Oferta> orderByComparator) {
		List<Oferta> list = findByOfertaId(ofertaId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last oferta in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByOfertaId_Last(long ofertaId,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByOfertaId_Last(ofertaId, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ofertaId=");
		msg.append(ofertaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the last oferta in the ordered set where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByOfertaId_Last(long ofertaId,
		OrderByComparator<Oferta> orderByComparator) {
		int count = countByOfertaId(ofertaId);

		if (count == 0) {
			return null;
		}

		List<Oferta> list = findByOfertaId(ofertaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the ofertas where ofertaId = &#63; from the database.
	 *
	 * @param ofertaId the oferta ID
	 */
	@Override
	public void removeByOfertaId(long ofertaId) {
		for (Oferta oferta : findByOfertaId(ofertaId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas where ofertaId = &#63;.
	 *
	 * @param ofertaId the oferta ID
	 * @return the number of matching ofertas
	 */
	@Override
	public int countByOfertaId(long ofertaId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_OFERTAID;

		Object[] finderArgs = new Object[] { ofertaId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_OFERTA_WHERE);

			query.append(_FINDER_COLUMN_OFERTAID_OFERTAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ofertaId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_OFERTAID_OFERTAID_2 = "oferta.ofertaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TITULO = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTitulo",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO =
		new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, OfertaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTitulo",
			new String[] { String.class.getName() },
			OfertaModelImpl.TITULO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TITULO = new FinderPath(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTitulo",
			new String[] { String.class.getName() });

	/**
	 * Returns all the ofertas where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @return the matching ofertas
	 */
	@Override
	public List<Oferta> findByTitulo(String titulo) {
		return findByTitulo(titulo, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ofertas where titulo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param titulo the titulo
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of matching ofertas
	 */
	@Override
	public List<Oferta> findByTitulo(String titulo, int start, int end) {
		return findByTitulo(titulo, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas where titulo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param titulo the titulo
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByTitulo(String titulo, int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return findByTitulo(titulo, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas where titulo = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param titulo the titulo
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching ofertas
	 */
	@Override
	public List<Oferta> findByTitulo(String titulo, int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO;
			finderArgs = new Object[] { titulo };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TITULO;
			finderArgs = new Object[] { titulo, start, end, orderByComparator };
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Oferta oferta : list) {
					if (!Objects.equals(titulo, oferta.getTitulo())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_OFERTA_WHERE);

			boolean bindTitulo = false;

			if (titulo == null) {
				query.append(_FINDER_COLUMN_TITULO_TITULO_1);
			}
			else if (titulo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITULO_TITULO_3);
			}
			else {
				bindTitulo = true;

				query.append(_FINDER_COLUMN_TITULO_TITULO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OfertaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitulo) {
					qPos.add(titulo);
				}

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first oferta in the ordered set where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByTitulo_First(String titulo,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByTitulo_First(titulo, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("titulo=");
		msg.append(titulo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the first oferta in the ordered set where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByTitulo_First(String titulo,
		OrderByComparator<Oferta> orderByComparator) {
		List<Oferta> list = findByTitulo(titulo, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last oferta in the ordered set where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta
	 * @throws NoSuchOfertaException if a matching oferta could not be found
	 */
	@Override
	public Oferta findByTitulo_Last(String titulo,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByTitulo_Last(titulo, orderByComparator);

		if (oferta != null) {
			return oferta;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("titulo=");
		msg.append(titulo);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOfertaException(msg.toString());
	}

	/**
	 * Returns the last oferta in the ordered set where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching oferta, or <code>null</code> if a matching oferta could not be found
	 */
	@Override
	public Oferta fetchByTitulo_Last(String titulo,
		OrderByComparator<Oferta> orderByComparator) {
		int count = countByTitulo(titulo);

		if (count == 0) {
			return null;
		}

		List<Oferta> list = findByTitulo(titulo, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ofertas before and after the current oferta in the ordered set where titulo = &#63;.
	 *
	 * @param ofertaId the primary key of the current oferta
	 * @param titulo the titulo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta[] findByTitulo_PrevAndNext(long ofertaId, String titulo,
		OrderByComparator<Oferta> orderByComparator)
		throws NoSuchOfertaException {
		Oferta oferta = findByPrimaryKey(ofertaId);

		Session session = null;

		try {
			session = openSession();

			Oferta[] array = new OfertaImpl[3];

			array[0] = getByTitulo_PrevAndNext(session, oferta, titulo,
					orderByComparator, true);

			array[1] = oferta;

			array[2] = getByTitulo_PrevAndNext(session, oferta, titulo,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Oferta getByTitulo_PrevAndNext(Session session, Oferta oferta,
		String titulo, OrderByComparator<Oferta> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_OFERTA_WHERE);

		boolean bindTitulo = false;

		if (titulo == null) {
			query.append(_FINDER_COLUMN_TITULO_TITULO_1);
		}
		else if (titulo.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TITULO_TITULO_3);
		}
		else {
			bindTitulo = true;

			query.append(_FINDER_COLUMN_TITULO_TITULO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OfertaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTitulo) {
			qPos.add(titulo);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(oferta);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Oferta> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ofertas where titulo = &#63; from the database.
	 *
	 * @param titulo the titulo
	 */
	@Override
	public void removeByTitulo(String titulo) {
		for (Oferta oferta : findByTitulo(titulo, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas where titulo = &#63;.
	 *
	 * @param titulo the titulo
	 * @return the number of matching ofertas
	 */
	@Override
	public int countByTitulo(String titulo) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TITULO;

		Object[] finderArgs = new Object[] { titulo };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_OFERTA_WHERE);

			boolean bindTitulo = false;

			if (titulo == null) {
				query.append(_FINDER_COLUMN_TITULO_TITULO_1);
			}
			else if (titulo.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITULO_TITULO_3);
			}
			else {
				bindTitulo = true;

				query.append(_FINDER_COLUMN_TITULO_TITULO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTitulo) {
					qPos.add(titulo);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TITULO_TITULO_1 = "oferta.titulo IS NULL";
	private static final String _FINDER_COLUMN_TITULO_TITULO_2 = "oferta.titulo = ?";
	private static final String _FINDER_COLUMN_TITULO_TITULO_3 = "(oferta.titulo IS NULL OR oferta.titulo = '')";

	public OfertaPersistenceImpl() {
		setModelClass(Oferta.class);

		try {
			Field field = ReflectionUtil.getDeclaredField(BasePersistenceImpl.class,
					"_dbColumnNames");

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the oferta in the entity cache if it is enabled.
	 *
	 * @param oferta the oferta
	 */
	@Override
	public void cacheResult(Oferta oferta) {
		entityCache.putResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaImpl.class, oferta.getPrimaryKey(), oferta);

		oferta.resetOriginalValues();
	}

	/**
	 * Caches the ofertas in the entity cache if it is enabled.
	 *
	 * @param ofertas the ofertas
	 */
	@Override
	public void cacheResult(List<Oferta> ofertas) {
		for (Oferta oferta : ofertas) {
			if (entityCache.getResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
						OfertaImpl.class, oferta.getPrimaryKey()) == null) {
				cacheResult(oferta);
			}
			else {
				oferta.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ofertas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(OfertaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the oferta.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Oferta oferta) {
		entityCache.removeResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaImpl.class, oferta.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Oferta> ofertas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Oferta oferta : ofertas) {
			entityCache.removeResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
				OfertaImpl.class, oferta.getPrimaryKey());
		}
	}

	/**
	 * Creates a new oferta with the primary key. Does not add the oferta to the database.
	 *
	 * @param ofertaId the primary key for the new oferta
	 * @return the new oferta
	 */
	@Override
	public Oferta create(long ofertaId) {
		Oferta oferta = new OfertaImpl();

		oferta.setNew(true);
		oferta.setPrimaryKey(ofertaId);

		String uuid = PortalUUIDUtil.generate();

		oferta.setUuid(uuid);

		oferta.setCompanyId(companyProvider.getCompanyId());

		return oferta;
	}

	/**
	 * Removes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ofertaId the primary key of the oferta
	 * @return the oferta that was removed
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta remove(long ofertaId) throws NoSuchOfertaException {
		return remove((Serializable)ofertaId);
	}

	/**
	 * Removes the oferta with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the oferta
	 * @return the oferta that was removed
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta remove(Serializable primaryKey) throws NoSuchOfertaException {
		Session session = null;

		try {
			session = openSession();

			Oferta oferta = (Oferta)session.get(OfertaImpl.class, primaryKey);

			if (oferta == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOfertaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(oferta);
		}
		catch (NoSuchOfertaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Oferta removeImpl(Oferta oferta) {
		oferta = toUnwrappedModel(oferta);

		ofertaToAlumnoTableMapper.deleteLeftPrimaryKeyTableMappings(oferta.getPrimaryKey());

		ofertaToEmpresaTableMapper.deleteLeftPrimaryKeyTableMappings(oferta.getPrimaryKey());

		ofertaToProfesorTableMapper.deleteLeftPrimaryKeyTableMappings(oferta.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(oferta)) {
				oferta = (Oferta)session.get(OfertaImpl.class,
						oferta.getPrimaryKeyObj());
			}

			if (oferta != null) {
				session.delete(oferta);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (oferta != null) {
			clearCache(oferta);
		}

		return oferta;
	}

	@Override
	public Oferta updateImpl(Oferta oferta) {
		oferta = toUnwrappedModel(oferta);

		boolean isNew = oferta.isNew();

		OfertaModelImpl ofertaModelImpl = (OfertaModelImpl)oferta;

		if (Validator.isNull(oferta.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			oferta.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (oferta.isNew()) {
				session.save(oferta);

				oferta.setNew(false);
			}
			else {
				oferta = (Oferta)session.merge(oferta);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!OfertaModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { ofertaModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					ofertaModelImpl.getUuid(), ofertaModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { ofertaModelImpl.getArea() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_AREATYPE, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE,
				args);

			args = new Object[] { ofertaModelImpl.getOfertaId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID,
				args);

			args = new Object[] { ofertaModelImpl.getTitulo() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TITULO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((ofertaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { ofertaModelImpl.getOriginalUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { ofertaModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((ofertaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ofertaModelImpl.getOriginalUuid(),
						ofertaModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						ofertaModelImpl.getUuid(),
						ofertaModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((ofertaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { ofertaModelImpl.getOriginalArea() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_AREATYPE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE,
					args);

				args = new Object[] { ofertaModelImpl.getArea() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_AREATYPE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AREATYPE,
					args);
			}

			if ((ofertaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ofertaModelImpl.getOriginalOfertaId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID,
					args);

				args = new Object[] { ofertaModelImpl.getOfertaId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_OFERTAID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OFERTAID,
					args);
			}

			if ((ofertaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { ofertaModelImpl.getOriginalTitulo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_TITULO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO,
					args);

				args = new Object[] { ofertaModelImpl.getTitulo() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_TITULO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TITULO,
					args);
			}
		}

		entityCache.putResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
			OfertaImpl.class, oferta.getPrimaryKey(), oferta, false);

		oferta.resetOriginalValues();

		return oferta;
	}

	protected Oferta toUnwrappedModel(Oferta oferta) {
		if (oferta instanceof OfertaImpl) {
			return oferta;
		}

		OfertaImpl ofertaImpl = new OfertaImpl();

		ofertaImpl.setNew(oferta.isNew());
		ofertaImpl.setPrimaryKey(oferta.getPrimaryKey());

		ofertaImpl.setUuid(oferta.getUuid());
		ofertaImpl.setOfertaId(oferta.getOfertaId());
		ofertaImpl.setCompanyId(oferta.getCompanyId());
		ofertaImpl.setTitulo(oferta.getTitulo());
		ofertaImpl.setDescripcion(oferta.getDescripcion());
		ofertaImpl.setArea(oferta.getArea());
		ofertaImpl.setCompetencias(oferta.getCompetencias());
		ofertaImpl.setDuracion(oferta.getDuracion());

		return ofertaImpl;
	}

	/**
	 * Returns the oferta with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the oferta
	 * @return the oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOfertaException {
		Oferta oferta = fetchByPrimaryKey(primaryKey);

		if (oferta == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOfertaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return oferta;
	}

	/**
	 * Returns the oferta with the primary key or throws a {@link NoSuchOfertaException} if it could not be found.
	 *
	 * @param ofertaId the primary key of the oferta
	 * @return the oferta
	 * @throws NoSuchOfertaException if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta findByPrimaryKey(long ofertaId) throws NoSuchOfertaException {
		return findByPrimaryKey((Serializable)ofertaId);
	}

	/**
	 * Returns the oferta with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the oferta
	 * @return the oferta, or <code>null</code> if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
				OfertaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Oferta oferta = (Oferta)serializable;

		if (oferta == null) {
			Session session = null;

			try {
				session = openSession();

				oferta = (Oferta)session.get(OfertaImpl.class, primaryKey);

				if (oferta != null) {
					cacheResult(oferta);
				}
				else {
					entityCache.putResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
						OfertaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
					OfertaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return oferta;
	}

	/**
	 * Returns the oferta with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ofertaId the primary key of the oferta
	 * @return the oferta, or <code>null</code> if a oferta with the primary key could not be found
	 */
	@Override
	public Oferta fetchByPrimaryKey(long ofertaId) {
		return fetchByPrimaryKey((Serializable)ofertaId);
	}

	@Override
	public Map<Serializable, Oferta> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Oferta> map = new HashMap<Serializable, Oferta>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Oferta oferta = fetchByPrimaryKey(primaryKey);

			if (oferta != null) {
				map.put(primaryKey, oferta);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
					OfertaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Oferta)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_OFERTA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Oferta oferta : (List<Oferta>)q.list()) {
				map.put(oferta.getPrimaryKeyObj(), oferta);

				cacheResult(oferta);

				uncachedPrimaryKeys.remove(oferta.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(OfertaModelImpl.ENTITY_CACHE_ENABLED,
					OfertaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the ofertas.
	 *
	 * @return the ofertas
	 */
	@Override
	public List<Oferta> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ofertas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of ofertas
	 */
	@Override
	public List<Oferta> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ofertas
	 */
	@Override
	public List<Oferta> findAll(int start, int end,
		OrderByComparator<Oferta> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ofertas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of ofertas
	 */
	@Override
	public List<Oferta> findAll(int start, int end,
		OrderByComparator<Oferta> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Oferta> list = null;

		if (retrieveFromCache) {
			list = (List<Oferta>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_OFERTA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_OFERTA;

				if (pagination) {
					sql = sql.concat(OfertaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Oferta>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ofertas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Oferta oferta : findAll()) {
			remove(oferta);
		}
	}

	/**
	 * Returns the number of ofertas.
	 *
	 * @return the number of ofertas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_OFERTA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of alumnos associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return long[] of the primaryKeys of alumnos associated with the oferta
	 */
	@Override
	public long[] getAlumnoPrimaryKeys(long pk) {
		long[] pks = ofertaToAlumnoTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the alumnos associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the alumnos associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Alumno> getAlumnos(long pk) {
		return getAlumnos(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the alumnos associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of alumnos associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Alumno> getAlumnos(long pk, int start, int end) {
		return getAlumnos(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the alumnos associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of alumnos associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Alumno> getAlumnos(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Alumno> orderByComparator) {
		return ofertaToAlumnoTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of alumnos associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the number of alumnos associated with the oferta
	 */
	@Override
	public int getAlumnosSize(long pk) {
		long[] pks = ofertaToAlumnoTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the alumno is associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPK the primary key of the alumno
	 * @return <code>true</code> if the alumno is associated with the oferta; <code>false</code> otherwise
	 */
	@Override
	public boolean containsAlumno(long pk, long alumnoPK) {
		return ofertaToAlumnoTableMapper.containsTableMapping(pk, alumnoPK);
	}

	/**
	 * Returns <code>true</code> if the oferta has any alumnos associated with it.
	 *
	 * @param pk the primary key of the oferta to check for associations with alumnos
	 * @return <code>true</code> if the oferta has any alumnos associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsAlumnos(long pk) {
		if (getAlumnosSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPK the primary key of the alumno
	 */
	@Override
	public void addAlumno(long pk, long alumnoPK) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToAlumnoTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, alumnoPK);
		}
		else {
			ofertaToAlumnoTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, alumnoPK);
		}
	}

	/**
	 * Adds an association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumno the alumno
	 */
	@Override
	public void addAlumno(long pk, service.tfg.model.Alumno alumno) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToAlumnoTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, alumno.getPrimaryKey());
		}
		else {
			ofertaToAlumnoTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, alumno.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPKs the primary keys of the alumnos
	 */
	@Override
	public void addAlumnos(long pk, long[] alumnoPKs) {
		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToAlumnoTableMapper.addTableMappings(companyId, pk, alumnoPKs);
	}

	/**
	 * Adds an association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnos the alumnos
	 */
	@Override
	public void addAlumnos(long pk, List<service.tfg.model.Alumno> alumnos) {
		addAlumnos(pk,
			ListUtil.toLongArray(alumnos,
				service.tfg.model.Alumno.ALUMNO_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the oferta and its alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta to clear the associated alumnos from
	 */
	@Override
	public void clearAlumnos(long pk) {
		ofertaToAlumnoTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPK the primary key of the alumno
	 */
	@Override
	public void removeAlumno(long pk, long alumnoPK) {
		ofertaToAlumnoTableMapper.deleteTableMapping(pk, alumnoPK);
	}

	/**
	 * Removes the association between the oferta and the alumno. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumno the alumno
	 */
	@Override
	public void removeAlumno(long pk, service.tfg.model.Alumno alumno) {
		ofertaToAlumnoTableMapper.deleteTableMapping(pk, alumno.getPrimaryKey());
	}

	/**
	 * Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPKs the primary keys of the alumnos
	 */
	@Override
	public void removeAlumnos(long pk, long[] alumnoPKs) {
		ofertaToAlumnoTableMapper.deleteTableMappings(pk, alumnoPKs);
	}

	/**
	 * Removes the association between the oferta and the alumnos. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnos the alumnos
	 */
	@Override
	public void removeAlumnos(long pk, List<service.tfg.model.Alumno> alumnos) {
		removeAlumnos(pk,
			ListUtil.toLongArray(alumnos,
				service.tfg.model.Alumno.ALUMNO_ID_ACCESSOR));
	}

	/**
	 * Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnoPKs the primary keys of the alumnos to be associated with the oferta
	 */
	@Override
	public void setAlumnos(long pk, long[] alumnoPKs) {
		Set<Long> newAlumnoPKsSet = SetUtil.fromArray(alumnoPKs);
		Set<Long> oldAlumnoPKsSet = SetUtil.fromArray(ofertaToAlumnoTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeAlumnoPKsSet = new HashSet<Long>(oldAlumnoPKsSet);

		removeAlumnoPKsSet.removeAll(newAlumnoPKsSet);

		ofertaToAlumnoTableMapper.deleteTableMappings(pk,
			ArrayUtil.toLongArray(removeAlumnoPKsSet));

		newAlumnoPKsSet.removeAll(oldAlumnoPKsSet);

		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToAlumnoTableMapper.addTableMappings(companyId, pk,
			ArrayUtil.toLongArray(newAlumnoPKsSet));
	}

	/**
	 * Sets the alumnos associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param alumnos the alumnos to be associated with the oferta
	 */
	@Override
	public void setAlumnos(long pk, List<service.tfg.model.Alumno> alumnos) {
		try {
			long[] alumnoPKs = new long[alumnos.size()];

			for (int i = 0; i < alumnos.size(); i++) {
				service.tfg.model.Alumno alumno = alumnos.get(i);

				alumnoPKs[i] = alumno.getPrimaryKey();
			}

			setAlumnos(pk, alumnoPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	/**
	 * Returns the primaryKeys of empresas associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return long[] of the primaryKeys of empresas associated with the oferta
	 */
	@Override
	public long[] getEmpresaPrimaryKeys(long pk) {
		long[] pks = ofertaToEmpresaTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the empresas associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the empresas associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Empresa> getEmpresas(long pk) {
		return getEmpresas(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the empresas associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of empresas associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Empresa> getEmpresas(long pk, int start,
		int end) {
		return getEmpresas(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the empresas associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of empresas associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Empresa> getEmpresas(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Empresa> orderByComparator) {
		return ofertaToEmpresaTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of empresas associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the number of empresas associated with the oferta
	 */
	@Override
	public int getEmpresasSize(long pk) {
		long[] pks = ofertaToEmpresaTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the empresa is associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPK the primary key of the empresa
	 * @return <code>true</code> if the empresa is associated with the oferta; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEmpresa(long pk, long empresaPK) {
		return ofertaToEmpresaTableMapper.containsTableMapping(pk, empresaPK);
	}

	/**
	 * Returns <code>true</code> if the oferta has any empresas associated with it.
	 *
	 * @param pk the primary key of the oferta to check for associations with empresas
	 * @return <code>true</code> if the oferta has any empresas associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsEmpresas(long pk) {
		if (getEmpresasSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPK the primary key of the empresa
	 */
	@Override
	public void addEmpresa(long pk, long empresaPK) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToEmpresaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, empresaPK);
		}
		else {
			ofertaToEmpresaTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, empresaPK);
		}
	}

	/**
	 * Adds an association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresa the empresa
	 */
	@Override
	public void addEmpresa(long pk, service.tfg.model.Empresa empresa) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToEmpresaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, empresa.getPrimaryKey());
		}
		else {
			ofertaToEmpresaTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, empresa.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPKs the primary keys of the empresas
	 */
	@Override
	public void addEmpresas(long pk, long[] empresaPKs) {
		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToEmpresaTableMapper.addTableMappings(companyId, pk, empresaPKs);
	}

	/**
	 * Adds an association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresas the empresas
	 */
	@Override
	public void addEmpresas(long pk, List<service.tfg.model.Empresa> empresas) {
		addEmpresas(pk,
			ListUtil.toLongArray(empresas,
				service.tfg.model.Empresa.EMPRESA_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the oferta and its empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta to clear the associated empresas from
	 */
	@Override
	public void clearEmpresas(long pk) {
		ofertaToEmpresaTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPK the primary key of the empresa
	 */
	@Override
	public void removeEmpresa(long pk, long empresaPK) {
		ofertaToEmpresaTableMapper.deleteTableMapping(pk, empresaPK);
	}

	/**
	 * Removes the association between the oferta and the empresa. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresa the empresa
	 */
	@Override
	public void removeEmpresa(long pk, service.tfg.model.Empresa empresa) {
		ofertaToEmpresaTableMapper.deleteTableMapping(pk,
			empresa.getPrimaryKey());
	}

	/**
	 * Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPKs the primary keys of the empresas
	 */
	@Override
	public void removeEmpresas(long pk, long[] empresaPKs) {
		ofertaToEmpresaTableMapper.deleteTableMappings(pk, empresaPKs);
	}

	/**
	 * Removes the association between the oferta and the empresas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresas the empresas
	 */
	@Override
	public void removeEmpresas(long pk, List<service.tfg.model.Empresa> empresas) {
		removeEmpresas(pk,
			ListUtil.toLongArray(empresas,
				service.tfg.model.Empresa.EMPRESA_ID_ACCESSOR));
	}

	/**
	 * Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresaPKs the primary keys of the empresas to be associated with the oferta
	 */
	@Override
	public void setEmpresas(long pk, long[] empresaPKs) {
		Set<Long> newEmpresaPKsSet = SetUtil.fromArray(empresaPKs);
		Set<Long> oldEmpresaPKsSet = SetUtil.fromArray(ofertaToEmpresaTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeEmpresaPKsSet = new HashSet<Long>(oldEmpresaPKsSet);

		removeEmpresaPKsSet.removeAll(newEmpresaPKsSet);

		ofertaToEmpresaTableMapper.deleteTableMappings(pk,
			ArrayUtil.toLongArray(removeEmpresaPKsSet));

		newEmpresaPKsSet.removeAll(oldEmpresaPKsSet);

		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToEmpresaTableMapper.addTableMappings(companyId, pk,
			ArrayUtil.toLongArray(newEmpresaPKsSet));
	}

	/**
	 * Sets the empresas associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param empresas the empresas to be associated with the oferta
	 */
	@Override
	public void setEmpresas(long pk, List<service.tfg.model.Empresa> empresas) {
		try {
			long[] empresaPKs = new long[empresas.size()];

			for (int i = 0; i < empresas.size(); i++) {
				service.tfg.model.Empresa empresa = empresas.get(i);

				empresaPKs[i] = empresa.getPrimaryKey();
			}

			setEmpresas(pk, empresaPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	/**
	 * Returns the primaryKeys of profesors associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return long[] of the primaryKeys of profesors associated with the oferta
	 */
	@Override
	public long[] getProfesorPrimaryKeys(long pk) {
		long[] pks = ofertaToProfesorTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the profesors associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the profesors associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Profesor> getProfesors(long pk) {
		return getProfesors(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the profesors associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @return the range of profesors associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Profesor> getProfesors(long pk, int start,
		int end) {
		return getProfesors(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors associated with the oferta.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link OfertaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the oferta
	 * @param start the lower bound of the range of ofertas
	 * @param end the upper bound of the range of ofertas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profesors associated with the oferta
	 */
	@Override
	public List<service.tfg.model.Profesor> getProfesors(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Profesor> orderByComparator) {
		return ofertaToProfesorTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of profesors associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @return the number of profesors associated with the oferta
	 */
	@Override
	public int getProfesorsSize(long pk) {
		long[] pks = ofertaToProfesorTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the profesor is associated with the oferta.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPK the primary key of the profesor
	 * @return <code>true</code> if the profesor is associated with the oferta; <code>false</code> otherwise
	 */
	@Override
	public boolean containsProfesor(long pk, long profesorPK) {
		return ofertaToProfesorTableMapper.containsTableMapping(pk, profesorPK);
	}

	/**
	 * Returns <code>true</code> if the oferta has any profesors associated with it.
	 *
	 * @param pk the primary key of the oferta to check for associations with profesors
	 * @return <code>true</code> if the oferta has any profesors associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsProfesors(long pk) {
		if (getProfesorsSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPK the primary key of the profesor
	 */
	@Override
	public void addProfesor(long pk, long profesorPK) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToProfesorTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, profesorPK);
		}
		else {
			ofertaToProfesorTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, profesorPK);
		}
	}

	/**
	 * Adds an association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesor the profesor
	 */
	@Override
	public void addProfesor(long pk, service.tfg.model.Profesor profesor) {
		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			ofertaToProfesorTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, profesor.getPrimaryKey());
		}
		else {
			ofertaToProfesorTableMapper.addTableMapping(oferta.getCompanyId(),
				pk, profesor.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPKs the primary keys of the profesors
	 */
	@Override
	public void addProfesors(long pk, long[] profesorPKs) {
		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToProfesorTableMapper.addTableMappings(companyId, pk, profesorPKs);
	}

	/**
	 * Adds an association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesors the profesors
	 */
	@Override
	public void addProfesors(long pk, List<service.tfg.model.Profesor> profesors) {
		addProfesors(pk,
			ListUtil.toLongArray(profesors,
				service.tfg.model.Profesor.PROFESOR_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the oferta and its profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta to clear the associated profesors from
	 */
	@Override
	public void clearProfesors(long pk) {
		ofertaToProfesorTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPK the primary key of the profesor
	 */
	@Override
	public void removeProfesor(long pk, long profesorPK) {
		ofertaToProfesorTableMapper.deleteTableMapping(pk, profesorPK);
	}

	/**
	 * Removes the association between the oferta and the profesor. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesor the profesor
	 */
	@Override
	public void removeProfesor(long pk, service.tfg.model.Profesor profesor) {
		ofertaToProfesorTableMapper.deleteTableMapping(pk,
			profesor.getPrimaryKey());
	}

	/**
	 * Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPKs the primary keys of the profesors
	 */
	@Override
	public void removeProfesors(long pk, long[] profesorPKs) {
		ofertaToProfesorTableMapper.deleteTableMappings(pk, profesorPKs);
	}

	/**
	 * Removes the association between the oferta and the profesors. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesors the profesors
	 */
	@Override
	public void removeProfesors(long pk,
		List<service.tfg.model.Profesor> profesors) {
		removeProfesors(pk,
			ListUtil.toLongArray(profesors,
				service.tfg.model.Profesor.PROFESOR_ID_ACCESSOR));
	}

	/**
	 * Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesorPKs the primary keys of the profesors to be associated with the oferta
	 */
	@Override
	public void setProfesors(long pk, long[] profesorPKs) {
		Set<Long> newProfesorPKsSet = SetUtil.fromArray(profesorPKs);
		Set<Long> oldProfesorPKsSet = SetUtil.fromArray(ofertaToProfesorTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeProfesorPKsSet = new HashSet<Long>(oldProfesorPKsSet);

		removeProfesorPKsSet.removeAll(newProfesorPKsSet);

		ofertaToProfesorTableMapper.deleteTableMappings(pk,
			ArrayUtil.toLongArray(removeProfesorPKsSet));

		newProfesorPKsSet.removeAll(oldProfesorPKsSet);

		long companyId = 0;

		Oferta oferta = fetchByPrimaryKey(pk);

		if (oferta == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = oferta.getCompanyId();
		}

		ofertaToProfesorTableMapper.addTableMappings(companyId, pk,
			ArrayUtil.toLongArray(newProfesorPKsSet));
	}

	/**
	 * Sets the profesors associated with the oferta, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the oferta
	 * @param profesors the profesors to be associated with the oferta
	 */
	@Override
	public void setProfesors(long pk, List<service.tfg.model.Profesor> profesors) {
		try {
			long[] profesorPKs = new long[profesors.size()];

			for (int i = 0; i < profesors.size(); i++) {
				service.tfg.model.Profesor profesor = profesors.get(i);

				profesorPKs[i] = profesor.getPrimaryKey();
			}

			setProfesors(pk, profesorPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return OfertaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the oferta persistence.
	 */
	public void afterPropertiesSet() {
		ofertaToAlumnoTableMapper = TableMapperFactory.getTableMapper("BBDD_Alumno_Solicitudes",
				"companyId", "ofertaId", "alumnoId", this, alumnoPersistence);

		ofertaToEmpresaTableMapper = TableMapperFactory.getTableMapper("BBDD_Ofertas_Empresa",
				"companyId", "ofertaId", "empresaId", this, empresaPersistence);

		ofertaToProfesorTableMapper = TableMapperFactory.getTableMapper("BBDD_Ofertas_Profesor",
				"companyId", "ofertaId", "profesorId", this, profesorPersistence);
	}

	public void destroy() {
		entityCache.removeCache(OfertaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("BBDD_Alumno_Solicitudes");
		TableMapperFactory.removeTableMapper("BBDD_Ofertas_Empresa");
		TableMapperFactory.removeTableMapper("BBDD_Ofertas_Profesor");
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	@BeanReference(type = AlumnoPersistence.class)
	protected AlumnoPersistence alumnoPersistence;
	protected TableMapper<Oferta, service.tfg.model.Alumno> ofertaToAlumnoTableMapper;
	@BeanReference(type = EmpresaPersistence.class)
	protected EmpresaPersistence empresaPersistence;
	protected TableMapper<Oferta, service.tfg.model.Empresa> ofertaToEmpresaTableMapper;
	@BeanReference(type = ProfesorPersistence.class)
	protected ProfesorPersistence profesorPersistence;
	protected TableMapper<Oferta, service.tfg.model.Profesor> ofertaToProfesorTableMapper;
	private static final String _SQL_SELECT_OFERTA = "SELECT oferta FROM Oferta oferta";
	private static final String _SQL_SELECT_OFERTA_WHERE_PKS_IN = "SELECT oferta FROM Oferta oferta WHERE ofertaId IN (";
	private static final String _SQL_SELECT_OFERTA_WHERE = "SELECT oferta FROM Oferta oferta WHERE ";
	private static final String _SQL_COUNT_OFERTA = "SELECT COUNT(oferta) FROM Oferta oferta";
	private static final String _SQL_COUNT_OFERTA_WHERE = "SELECT COUNT(oferta) FROM Oferta oferta WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "oferta.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Oferta exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Oferta exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(OfertaPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}