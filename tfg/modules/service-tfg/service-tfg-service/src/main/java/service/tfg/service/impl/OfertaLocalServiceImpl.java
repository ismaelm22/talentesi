/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.impl;

import java.util.Collection;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Oferta;
import service.tfg.model.Profesor;
import service.tfg.model.impl.OfertaImpl;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;
import service.tfg.service.base.OfertaLocalServiceBaseImpl;

/**
 * The implementation of the oferta local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link service.tfg.service.OfertaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Ismael Medina
 * @see OfertaLocalServiceBaseImpl
 * @see service.tfg.service.OfertaLocalServiceUtil
 */
public class OfertaLocalServiceImpl extends OfertaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * service.tfg.service.OfertaLocalServiceUtil} to access the oferta local
	 * service.
	 */

	public void addOferta(long companyId, String titulo, String descripcion, String area, String competencias,
			String duracion, Collection<Profesor> profesores, Collection<Empresa> empresas) {

		final Oferta oferta = new OfertaImpl();

		oferta.setOfertaId(counterLocalService.increment());
		oferta.setCompanyId(companyId);
		oferta.setTitulo(titulo);
		oferta.setDescripcion(descripcion);
		oferta.setArea(area);
		oferta.setCompetencias(competencias);
		oferta.setDuracion(duracion);

		addOferta(oferta);

		if (null != profesores && !profesores.isEmpty()) {

			ProfesorLocalServiceUtil.setOfertaProfesors(oferta.getOfertaId(), getProfesoresIds(profesores));

		} else if (null != empresas && !empresas.isEmpty()) {

			EmpresaLocalServiceUtil.setOfertaEmpresas(oferta.getOfertaId(), getEmpresaIds(empresas));

		}

	}

	public void updateOferta(long ofertaId, String titulo, String descripcion, String area, String competencias,
			String duracion) throws PortalException {

		final Oferta oferta = getOferta(ofertaId);

		oferta.setOfertaId(ofertaId);
		oferta.setTitulo(titulo);
		oferta.setDescripcion(descripcion);
		oferta.setArea(area);
		oferta.setCompetencias(competencias);
		oferta.setDuracion(duracion);

		updateOferta(oferta);

	}

	public void updateAlumnosolcitudes(long ofertaId, Collection<Alumno> alumnos) throws PortalException {

		AlumnoLocalServiceUtil.setOfertaAlumnos(ofertaId, getAlumnosIds(alumnos));

	}

	public void updateProfesoresOferta(long ofertaId, Collection<Profesor> profesor) throws PortalException {

		ProfesorLocalServiceUtil.setOfertaProfesors(ofertaId, getProfesoresIds(profesor));

	}

	public void updateEmresasOferta(long ofertaId, Collection<Empresa> empresas) throws PortalException {

		EmpresaLocalServiceUtil.setOfertaEmpresas(ofertaId, getEmpresaIds(empresas));

	}

	private long[] getAlumnosIds(Collection<Alumno> alumnos) {
		return alumnos.stream().mapToLong(Alumno::getAlumnoId).toArray();
	}

	private long[] getProfesoresIds(Collection<Profesor> profesores) {
		return profesores.stream().mapToLong(Profesor::getProfesorId).toArray();
	}

	private long[] getEmpresaIds(Collection<Empresa> empresas) {
		return empresas.stream().mapToLong(Empresa::getEmpresaId).toArray();
	}

	public List<Oferta> findByOfertaId(long ofertaId) {
		return getOfertaPersistence().findByOfertaId(ofertaId);
	}

	public List<Oferta> findByTitulo(String titulo) {
		return getOfertaPersistence().findByTitulo(titulo);
	}

	public List<Oferta> findByareaType(String area) {
		return getOfertaPersistence().findByareaType(area);
	}
}