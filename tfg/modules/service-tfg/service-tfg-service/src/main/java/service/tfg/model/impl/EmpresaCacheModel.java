/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import service.tfg.model.Empresa;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Empresa in entity cache.
 *
 * @author Ismael Medina
 * @see Empresa
 * @generated
 */
@ProviderType
public class EmpresaCacheModel implements CacheModel<Empresa>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmpresaCacheModel)) {
			return false;
		}

		EmpresaCacheModel empresaCacheModel = (EmpresaCacheModel)obj;

		if (empresaId == empresaCacheModel.empresaId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, empresaId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", empresaId=");
		sb.append(empresaId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", email=");
		sb.append(email);
		sb.append(", pwd=");
		sb.append(pwd);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", cif=");
		sb.append(cif);
		sb.append(", direccion=");
		sb.append(direccion);
		sb.append(", telefono=");
		sb.append(telefono);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Empresa toEntityModel() {
		EmpresaImpl empresaImpl = new EmpresaImpl();

		if (uuid == null) {
			empresaImpl.setUuid(StringPool.BLANK);
		}
		else {
			empresaImpl.setUuid(uuid);
		}

		empresaImpl.setEmpresaId(empresaId);
		empresaImpl.setCompanyId(companyId);

		if (email == null) {
			empresaImpl.setEmail(StringPool.BLANK);
		}
		else {
			empresaImpl.setEmail(email);
		}

		if (pwd == null) {
			empresaImpl.setPwd(StringPool.BLANK);
		}
		else {
			empresaImpl.setPwd(pwd);
		}

		if (nombre == null) {
			empresaImpl.setNombre(StringPool.BLANK);
		}
		else {
			empresaImpl.setNombre(nombre);
		}

		if (cif == null) {
			empresaImpl.setCif(StringPool.BLANK);
		}
		else {
			empresaImpl.setCif(cif);
		}

		if (direccion == null) {
			empresaImpl.setDireccion(StringPool.BLANK);
		}
		else {
			empresaImpl.setDireccion(direccion);
		}

		if (telefono == null) {
			empresaImpl.setTelefono(StringPool.BLANK);
		}
		else {
			empresaImpl.setTelefono(telefono);
		}

		empresaImpl.resetOriginalValues();

		return empresaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		empresaId = objectInput.readLong();

		companyId = objectInput.readLong();
		email = objectInput.readUTF();
		pwd = objectInput.readUTF();
		nombre = objectInput.readUTF();
		cif = objectInput.readUTF();
		direccion = objectInput.readUTF();
		telefono = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(empresaId);

		objectOutput.writeLong(companyId);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (pwd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pwd);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (cif == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cif);
		}

		if (direccion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(direccion);
		}

		if (telefono == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefono);
		}
	}

	public String uuid;
	public long empresaId;
	public long companyId;
	public String email;
	public String pwd;
	public String nombre;
	public String cif;
	public String direccion;
	public String telefono;
}