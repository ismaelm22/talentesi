/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReflectionUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import service.tfg.exception.NoSuchProfesorException;

import service.tfg.model.Profesor;
import service.tfg.model.impl.ProfesorImpl;
import service.tfg.model.impl.ProfesorModelImpl;

import service.tfg.service.persistence.OfertaPersistence;
import service.tfg.service.persistence.ProfesorPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the profesor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Ismael Medina
 * @see ProfesorPersistence
 * @see service.tfg.service.persistence.ProfesorUtil
 * @generated
 */
@ProviderType
public class ProfesorPersistenceImpl extends BasePersistenceImpl<Profesor>
	implements ProfesorPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProfesorUtil} to access the profesor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProfesorImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProfesorModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the profesors where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching profesors
	 */
	@Override
	public List<Profesor> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profesors where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid(String uuid, int start, int end,
		OrderByComparator<Profesor> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the profesors where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid(String uuid, int start, int end,
		OrderByComparator<Profesor> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Profesor> list = null;

		if (retrieveFromCache) {
			list = (List<Profesor>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Profesor profesor : list) {
					if (!Objects.equals(uuid, profesor.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROFESOR_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProfesorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first profesor in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByUuid_First(String uuid,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByUuid_First(uuid, orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the first profesor in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByUuid_First(String uuid,
		OrderByComparator<Profesor> orderByComparator) {
		List<Profesor> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last profesor in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByUuid_Last(String uuid,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByUuid_Last(uuid, orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the last profesor in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByUuid_Last(String uuid,
		OrderByComparator<Profesor> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Profesor> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the profesors before and after the current profesor in the ordered set where uuid = &#63;.
	 *
	 * @param profesorId the primary key of the current profesor
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor[] findByUuid_PrevAndNext(long profesorId, String uuid,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = findByPrimaryKey(profesorId);

		Session session = null;

		try {
			session = openSession();

			Profesor[] array = new ProfesorImpl[3];

			array[0] = getByUuid_PrevAndNext(session, profesor, uuid,
					orderByComparator, true);

			array[1] = profesor;

			array[2] = getByUuid_PrevAndNext(session, profesor, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Profesor getByUuid_PrevAndNext(Session session,
		Profesor profesor, String uuid,
		OrderByComparator<Profesor> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROFESOR_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProfesorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(profesor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Profesor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the profesors where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Profesor profesor : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(profesor);
		}
	}

	/**
	 * Returns the number of profesors where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching profesors
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFESOR_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "profesor.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "profesor.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(profesor.uuid IS NULL OR profesor.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProfesorModelImpl.UUID_COLUMN_BITMASK |
			ProfesorModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the profesors where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching profesors
	 */
	@Override
	public List<Profesor> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profesors where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid_C(String uuid, long companyId, int start,
		int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Profesor> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the profesors where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Profesor> list = null;

		if (retrieveFromCache) {
			list = (List<Profesor>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Profesor profesor : list) {
					if (!Objects.equals(uuid, profesor.getUuid()) ||
							(companyId != profesor.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROFESOR_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProfesorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the first profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Profesor> orderByComparator) {
		List<Profesor> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the last profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Profesor> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Profesor> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the profesors before and after the current profesor in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param profesorId the primary key of the current profesor
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor[] findByUuid_C_PrevAndNext(long profesorId, String uuid,
		long companyId, OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = findByPrimaryKey(profesorId);

		Session session = null;

		try {
			session = openSession();

			Profesor[] array = new ProfesorImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, profesor, uuid,
					companyId, orderByComparator, true);

			array[1] = profesor;

			array[2] = getByUuid_C_PrevAndNext(session, profesor, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Profesor getByUuid_C_PrevAndNext(Session session,
		Profesor profesor, String uuid, long companyId,
		OrderByComparator<Profesor> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROFESOR_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProfesorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(profesor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Profesor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the profesors where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Profesor profesor : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(profesor);
		}
	}

	/**
	 * Returns the number of profesors where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching profesors
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROFESOR_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "profesor.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "profesor.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(profesor.uuid IS NULL OR profesor.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "profesor.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAILID = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEmailId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID =
		new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEmailId",
			new String[] { String.class.getName() },
			ProfesorModelImpl.EMAIL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMAILID = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmailId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the profesors where email = &#63;.
	 *
	 * @param email the email
	 * @return the matching profesors
	 */
	@Override
	public List<Profesor> findByEmailId(String email) {
		return findByEmailId(email, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profesors where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of matching profesors
	 */
	@Override
	public List<Profesor> findByEmailId(String email, int start, int end) {
		return findByEmailId(email, start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByEmailId(String email, int start, int end,
		OrderByComparator<Profesor> orderByComparator) {
		return findByEmailId(email, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the profesors where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findByEmailId(String email, int start, int end,
		OrderByComparator<Profesor> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID;
			finderArgs = new Object[] { email };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAILID;
			finderArgs = new Object[] { email, start, end, orderByComparator };
		}

		List<Profesor> list = null;

		if (retrieveFromCache) {
			list = (List<Profesor>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Profesor profesor : list) {
					if (!Objects.equals(email, profesor.getEmail())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROFESOR_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProfesorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				if (!pagination) {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first profesor in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByEmailId_First(String email,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByEmailId_First(email, orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the first profesor in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByEmailId_First(String email,
		OrderByComparator<Profesor> orderByComparator) {
		List<Profesor> list = findByEmailId(email, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last profesor in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findByEmailId_Last(String email,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByEmailId_Last(email, orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the last profesor in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchByEmailId_Last(String email,
		OrderByComparator<Profesor> orderByComparator) {
		int count = countByEmailId(email);

		if (count == 0) {
			return null;
		}

		List<Profesor> list = findByEmailId(email, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the profesors before and after the current profesor in the ordered set where email = &#63;.
	 *
	 * @param profesorId the primary key of the current profesor
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor[] findByEmailId_PrevAndNext(long profesorId, String email,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = findByPrimaryKey(profesorId);

		Session session = null;

		try {
			session = openSession();

			Profesor[] array = new ProfesorImpl[3];

			array[0] = getByEmailId_PrevAndNext(session, profesor, email,
					orderByComparator, true);

			array[1] = profesor;

			array[2] = getByEmailId_PrevAndNext(session, profesor, email,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Profesor getByEmailId_PrevAndNext(Session session,
		Profesor profesor, String email,
		OrderByComparator<Profesor> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROFESOR_WHERE);

		boolean bindEmail = false;

		if (email == null) {
			query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
		}
		else if (email.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
		}
		else {
			bindEmail = true;

			query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProfesorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindEmail) {
			qPos.add(email);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(profesor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Profesor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the profesors where email = &#63; from the database.
	 *
	 * @param email the email
	 */
	@Override
	public void removeByEmailId(String email) {
		for (Profesor profesor : findByEmailId(email, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(profesor);
		}
	}

	/**
	 * Returns the number of profesors where email = &#63;.
	 *
	 * @param email the email
	 * @return the number of matching profesors
	 */
	@Override
	public int countByEmailId(String email) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EMAILID;

		Object[] finderArgs = new Object[] { email };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFESOR_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAILID_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAILID_EMAIL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EMAILID_EMAIL_1 = "profesor.email IS NULL";
	private static final String _FINDER_COLUMN_EMAILID_EMAIL_2 = "profesor.email = ?";
	private static final String _FINDER_COLUMN_EMAILID_EMAIL_3 = "(profesor.email IS NULL OR profesor.email = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBREPROFESOR =
		new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBynombreProfesor",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR =
		new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, ProfesorImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBynombreProfesor",
			new String[] { String.class.getName() },
			ProfesorModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NOMBREPROFESOR = new FinderPath(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBynombreProfesor",
			new String[] { String.class.getName() });

	/**
	 * Returns all the profesors where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching profesors
	 */
	@Override
	public List<Profesor> findBynombreProfesor(String nombre) {
		return findBynombreProfesor(nombre, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profesors where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of matching profesors
	 */
	@Override
	public List<Profesor> findBynombreProfesor(String nombre, int start, int end) {
		return findBynombreProfesor(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findBynombreProfesor(String nombre, int start,
		int end, OrderByComparator<Profesor> orderByComparator) {
		return findBynombreProfesor(nombre, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the profesors where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching profesors
	 */
	@Override
	public List<Profesor> findBynombreProfesor(String nombre, int start,
		int end, OrderByComparator<Profesor> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR;
			finderArgs = new Object[] { nombre };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NOMBREPROFESOR;
			finderArgs = new Object[] { nombre, start, end, orderByComparator };
		}

		List<Profesor> list = null;

		if (retrieveFromCache) {
			list = (List<Profesor>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Profesor profesor : list) {
					if (!Objects.equals(nombre, profesor.getNombre())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROFESOR_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProfesorModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first profesor in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findBynombreProfesor_First(String nombre,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchBynombreProfesor_First(nombre,
				orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the first profesor in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchBynombreProfesor_First(String nombre,
		OrderByComparator<Profesor> orderByComparator) {
		List<Profesor> list = findBynombreProfesor(nombre, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last profesor in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor
	 * @throws NoSuchProfesorException if a matching profesor could not be found
	 */
	@Override
	public Profesor findBynombreProfesor_Last(String nombre,
		OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = fetchBynombreProfesor_Last(nombre, orderByComparator);

		if (profesor != null) {
			return profesor;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProfesorException(msg.toString());
	}

	/**
	 * Returns the last profesor in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching profesor, or <code>null</code> if a matching profesor could not be found
	 */
	@Override
	public Profesor fetchBynombreProfesor_Last(String nombre,
		OrderByComparator<Profesor> orderByComparator) {
		int count = countBynombreProfesor(nombre);

		if (count == 0) {
			return null;
		}

		List<Profesor> list = findBynombreProfesor(nombre, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the profesors before and after the current profesor in the ordered set where nombre = &#63;.
	 *
	 * @param profesorId the primary key of the current profesor
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor[] findBynombreProfesor_PrevAndNext(long profesorId,
		String nombre, OrderByComparator<Profesor> orderByComparator)
		throws NoSuchProfesorException {
		Profesor profesor = findByPrimaryKey(profesorId);

		Session session = null;

		try {
			session = openSession();

			Profesor[] array = new ProfesorImpl[3];

			array[0] = getBynombreProfesor_PrevAndNext(session, profesor,
					nombre, orderByComparator, true);

			array[1] = profesor;

			array[2] = getBynombreProfesor_PrevAndNext(session, profesor,
					nombre, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Profesor getBynombreProfesor_PrevAndNext(Session session,
		Profesor profesor, String nombre,
		OrderByComparator<Profesor> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROFESOR_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProfesorModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(profesor);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Profesor> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the profesors where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	@Override
	public void removeBynombreProfesor(String nombre) {
		for (Profesor profesor : findBynombreProfesor(nombre,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(profesor);
		}
	}

	/**
	 * Returns the number of profesors where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching profesors
	 */
	@Override
	public int countBynombreProfesor(String nombre) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NOMBREPROFESOR;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROFESOR_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_1 = "profesor.nombre IS NULL";
	private static final String _FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_2 = "profesor.nombre = ?";
	private static final String _FINDER_COLUMN_NOMBREPROFESOR_NOMBRE_3 = "(profesor.nombre IS NULL OR profesor.nombre = '')";

	public ProfesorPersistenceImpl() {
		setModelClass(Profesor.class);

		try {
			Field field = ReflectionUtil.getDeclaredField(BasePersistenceImpl.class,
					"_dbColumnNames");

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the profesor in the entity cache if it is enabled.
	 *
	 * @param profesor the profesor
	 */
	@Override
	public void cacheResult(Profesor profesor) {
		entityCache.putResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorImpl.class, profesor.getPrimaryKey(), profesor);

		profesor.resetOriginalValues();
	}

	/**
	 * Caches the profesors in the entity cache if it is enabled.
	 *
	 * @param profesors the profesors
	 */
	@Override
	public void cacheResult(List<Profesor> profesors) {
		for (Profesor profesor : profesors) {
			if (entityCache.getResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
						ProfesorImpl.class, profesor.getPrimaryKey()) == null) {
				cacheResult(profesor);
			}
			else {
				profesor.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all profesors.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProfesorImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the profesor.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Profesor profesor) {
		entityCache.removeResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorImpl.class, profesor.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Profesor> profesors) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Profesor profesor : profesors) {
			entityCache.removeResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
				ProfesorImpl.class, profesor.getPrimaryKey());
		}
	}

	/**
	 * Creates a new profesor with the primary key. Does not add the profesor to the database.
	 *
	 * @param profesorId the primary key for the new profesor
	 * @return the new profesor
	 */
	@Override
	public Profesor create(long profesorId) {
		Profesor profesor = new ProfesorImpl();

		profesor.setNew(true);
		profesor.setPrimaryKey(profesorId);

		String uuid = PortalUUIDUtil.generate();

		profesor.setUuid(uuid);

		profesor.setCompanyId(companyProvider.getCompanyId());

		return profesor;
	}

	/**
	 * Removes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param profesorId the primary key of the profesor
	 * @return the profesor that was removed
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor remove(long profesorId) throws NoSuchProfesorException {
		return remove((Serializable)profesorId);
	}

	/**
	 * Removes the profesor with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the profesor
	 * @return the profesor that was removed
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor remove(Serializable primaryKey)
		throws NoSuchProfesorException {
		Session session = null;

		try {
			session = openSession();

			Profesor profesor = (Profesor)session.get(ProfesorImpl.class,
					primaryKey);

			if (profesor == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProfesorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(profesor);
		}
		catch (NoSuchProfesorException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Profesor removeImpl(Profesor profesor) {
		profesor = toUnwrappedModel(profesor);

		profesorToOfertaTableMapper.deleteLeftPrimaryKeyTableMappings(profesor.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(profesor)) {
				profesor = (Profesor)session.get(ProfesorImpl.class,
						profesor.getPrimaryKeyObj());
			}

			if (profesor != null) {
				session.delete(profesor);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (profesor != null) {
			clearCache(profesor);
		}

		return profesor;
	}

	@Override
	public Profesor updateImpl(Profesor profesor) {
		profesor = toUnwrappedModel(profesor);

		boolean isNew = profesor.isNew();

		ProfesorModelImpl profesorModelImpl = (ProfesorModelImpl)profesor;

		if (Validator.isNull(profesor.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			profesor.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (profesor.isNew()) {
				session.save(profesor);

				profesor.setNew(false);
			}
			else {
				profesor = (Profesor)session.merge(profesor);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ProfesorModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { profesorModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					profesorModelImpl.getUuid(),
					profesorModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { profesorModelImpl.getEmail() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
				args);

			args = new Object[] { profesorModelImpl.getNombre() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREPROFESOR, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((profesorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { profesorModelImpl.getOriginalUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { profesorModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((profesorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						profesorModelImpl.getOriginalUuid(),
						profesorModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						profesorModelImpl.getUuid(),
						profesorModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((profesorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						profesorModelImpl.getOriginalEmail()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
					args);

				args = new Object[] { profesorModelImpl.getEmail() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_EMAILID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAILID,
					args);
			}

			if ((profesorModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						profesorModelImpl.getOriginalNombre()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREPROFESOR,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR,
					args);

				args = new Object[] { profesorModelImpl.getNombre() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_NOMBREPROFESOR,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NOMBREPROFESOR,
					args);
			}
		}

		entityCache.putResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
			ProfesorImpl.class, profesor.getPrimaryKey(), profesor, false);

		profesor.resetOriginalValues();

		return profesor;
	}

	protected Profesor toUnwrappedModel(Profesor profesor) {
		if (profesor instanceof ProfesorImpl) {
			return profesor;
		}

		ProfesorImpl profesorImpl = new ProfesorImpl();

		profesorImpl.setNew(profesor.isNew());
		profesorImpl.setPrimaryKey(profesor.getPrimaryKey());

		profesorImpl.setUuid(profesor.getUuid());
		profesorImpl.setProfesorId(profesor.getProfesorId());
		profesorImpl.setCompanyId(profesor.getCompanyId());
		profesorImpl.setEmail(profesor.getEmail());
		profesorImpl.setPwd(profesor.getPwd());
		profesorImpl.setNombre(profesor.getNombre());
		profesorImpl.setApellidos(profesor.getApellidos());
		profesorImpl.setArea(profesor.getArea());
		profesorImpl.setDespacho(profesor.getDespacho());

		return profesorImpl;
	}

	/**
	 * Returns the profesor with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the profesor
	 * @return the profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProfesorException {
		Profesor profesor = fetchByPrimaryKey(primaryKey);

		if (profesor == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProfesorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return profesor;
	}

	/**
	 * Returns the profesor with the primary key or throws a {@link NoSuchProfesorException} if it could not be found.
	 *
	 * @param profesorId the primary key of the profesor
	 * @return the profesor
	 * @throws NoSuchProfesorException if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor findByPrimaryKey(long profesorId)
		throws NoSuchProfesorException {
		return findByPrimaryKey((Serializable)profesorId);
	}

	/**
	 * Returns the profesor with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the profesor
	 * @return the profesor, or <code>null</code> if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
				ProfesorImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Profesor profesor = (Profesor)serializable;

		if (profesor == null) {
			Session session = null;

			try {
				session = openSession();

				profesor = (Profesor)session.get(ProfesorImpl.class, primaryKey);

				if (profesor != null) {
					cacheResult(profesor);
				}
				else {
					entityCache.putResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
						ProfesorImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
					ProfesorImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return profesor;
	}

	/**
	 * Returns the profesor with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param profesorId the primary key of the profesor
	 * @return the profesor, or <code>null</code> if a profesor with the primary key could not be found
	 */
	@Override
	public Profesor fetchByPrimaryKey(long profesorId) {
		return fetchByPrimaryKey((Serializable)profesorId);
	}

	@Override
	public Map<Serializable, Profesor> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Profesor> map = new HashMap<Serializable, Profesor>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Profesor profesor = fetchByPrimaryKey(primaryKey);

			if (profesor != null) {
				map.put(primaryKey, profesor);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
					ProfesorImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Profesor)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROFESOR_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Profesor profesor : (List<Profesor>)q.list()) {
				map.put(profesor.getPrimaryKeyObj(), profesor);

				cacheResult(profesor);

				uncachedPrimaryKeys.remove(profesor.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProfesorModelImpl.ENTITY_CACHE_ENABLED,
					ProfesorImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the profesors.
	 *
	 * @return the profesors
	 */
	@Override
	public List<Profesor> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the profesors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of profesors
	 */
	@Override
	public List<Profesor> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the profesors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of profesors
	 */
	@Override
	public List<Profesor> findAll(int start, int end,
		OrderByComparator<Profesor> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the profesors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of profesors
	 */
	@Override
	public List<Profesor> findAll(int start, int end,
		OrderByComparator<Profesor> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Profesor> list = null;

		if (retrieveFromCache) {
			list = (List<Profesor>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROFESOR);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROFESOR;

				if (pagination) {
					sql = sql.concat(ProfesorModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Profesor>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the profesors from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Profesor profesor : findAll()) {
			remove(profesor);
		}
	}

	/**
	 * Returns the number of profesors.
	 *
	 * @return the number of profesors
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROFESOR);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of ofertas associated with the profesor.
	 *
	 * @param pk the primary key of the profesor
	 * @return long[] of the primaryKeys of ofertas associated with the profesor
	 */
	@Override
	public long[] getOfertaPrimaryKeys(long pk) {
		long[] pks = profesorToOfertaTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the ofertas associated with the profesor.
	 *
	 * @param pk the primary key of the profesor
	 * @return the ofertas associated with the profesor
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk) {
		return getOfertas(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns a range of all the ofertas associated with the profesor.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the profesor
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @return the range of ofertas associated with the profesor
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk, int start, int end) {
		return getOfertas(pk, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ofertas associated with the profesor.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProfesorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the profesor
	 * @param start the lower bound of the range of profesors
	 * @param end the upper bound of the range of profesors (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ofertas associated with the profesor
	 */
	@Override
	public List<service.tfg.model.Oferta> getOfertas(long pk, int start,
		int end, OrderByComparator<service.tfg.model.Oferta> orderByComparator) {
		return profesorToOfertaTableMapper.getRightBaseModels(pk, start, end,
			orderByComparator);
	}

	/**
	 * Returns the number of ofertas associated with the profesor.
	 *
	 * @param pk the primary key of the profesor
	 * @return the number of ofertas associated with the profesor
	 */
	@Override
	public int getOfertasSize(long pk) {
		long[] pks = profesorToOfertaTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the oferta is associated with the profesor.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPK the primary key of the oferta
	 * @return <code>true</code> if the oferta is associated with the profesor; <code>false</code> otherwise
	 */
	@Override
	public boolean containsOferta(long pk, long ofertaPK) {
		return profesorToOfertaTableMapper.containsTableMapping(pk, ofertaPK);
	}

	/**
	 * Returns <code>true</code> if the profesor has any ofertas associated with it.
	 *
	 * @param pk the primary key of the profesor to check for associations with ofertas
	 * @return <code>true</code> if the profesor has any ofertas associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsOfertas(long pk) {
		if (getOfertasSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPK the primary key of the oferta
	 */
	@Override
	public void addOferta(long pk, long ofertaPK) {
		Profesor profesor = fetchByPrimaryKey(pk);

		if (profesor == null) {
			profesorToOfertaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, ofertaPK);
		}
		else {
			profesorToOfertaTableMapper.addTableMapping(profesor.getCompanyId(),
				pk, ofertaPK);
		}
	}

	/**
	 * Adds an association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param oferta the oferta
	 */
	@Override
	public void addOferta(long pk, service.tfg.model.Oferta oferta) {
		Profesor profesor = fetchByPrimaryKey(pk);

		if (profesor == null) {
			profesorToOfertaTableMapper.addTableMapping(companyProvider.getCompanyId(),
				pk, oferta.getPrimaryKey());
		}
		else {
			profesorToOfertaTableMapper.addTableMapping(profesor.getCompanyId(),
				pk, oferta.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPKs the primary keys of the ofertas
	 */
	@Override
	public void addOfertas(long pk, long[] ofertaPKs) {
		long companyId = 0;

		Profesor profesor = fetchByPrimaryKey(pk);

		if (profesor == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = profesor.getCompanyId();
		}

		profesorToOfertaTableMapper.addTableMappings(companyId, pk, ofertaPKs);
	}

	/**
	 * Adds an association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertas the ofertas
	 */
	@Override
	public void addOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		addOfertas(pk,
			ListUtil.toLongArray(ofertas,
				service.tfg.model.Oferta.OFERTA_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the profesor and its ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor to clear the associated ofertas from
	 */
	@Override
	public void clearOfertas(long pk) {
		profesorToOfertaTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPK the primary key of the oferta
	 */
	@Override
	public void removeOferta(long pk, long ofertaPK) {
		profesorToOfertaTableMapper.deleteTableMapping(pk, ofertaPK);
	}

	/**
	 * Removes the association between the profesor and the oferta. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param oferta the oferta
	 */
	@Override
	public void removeOferta(long pk, service.tfg.model.Oferta oferta) {
		profesorToOfertaTableMapper.deleteTableMapping(pk,
			oferta.getPrimaryKey());
	}

	/**
	 * Removes the association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPKs the primary keys of the ofertas
	 */
	@Override
	public void removeOfertas(long pk, long[] ofertaPKs) {
		profesorToOfertaTableMapper.deleteTableMappings(pk, ofertaPKs);
	}

	/**
	 * Removes the association between the profesor and the ofertas. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertas the ofertas
	 */
	@Override
	public void removeOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		removeOfertas(pk,
			ListUtil.toLongArray(ofertas,
				service.tfg.model.Oferta.OFERTA_ID_ACCESSOR));
	}

	/**
	 * Sets the ofertas associated with the profesor, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertaPKs the primary keys of the ofertas to be associated with the profesor
	 */
	@Override
	public void setOfertas(long pk, long[] ofertaPKs) {
		Set<Long> newOfertaPKsSet = SetUtil.fromArray(ofertaPKs);
		Set<Long> oldOfertaPKsSet = SetUtil.fromArray(profesorToOfertaTableMapper.getRightPrimaryKeys(
					pk));

		Set<Long> removeOfertaPKsSet = new HashSet<Long>(oldOfertaPKsSet);

		removeOfertaPKsSet.removeAll(newOfertaPKsSet);

		profesorToOfertaTableMapper.deleteTableMappings(pk,
			ArrayUtil.toLongArray(removeOfertaPKsSet));

		newOfertaPKsSet.removeAll(oldOfertaPKsSet);

		long companyId = 0;

		Profesor profesor = fetchByPrimaryKey(pk);

		if (profesor == null) {
			companyId = companyProvider.getCompanyId();
		}
		else {
			companyId = profesor.getCompanyId();
		}

		profesorToOfertaTableMapper.addTableMappings(companyId, pk,
			ArrayUtil.toLongArray(newOfertaPKsSet));
	}

	/**
	 * Sets the ofertas associated with the profesor, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the profesor
	 * @param ofertas the ofertas to be associated with the profesor
	 */
	@Override
	public void setOfertas(long pk, List<service.tfg.model.Oferta> ofertas) {
		try {
			long[] ofertaPKs = new long[ofertas.size()];

			for (int i = 0; i < ofertas.size(); i++) {
				service.tfg.model.Oferta oferta = ofertas.get(i);

				ofertaPKs[i] = oferta.getPrimaryKey();
			}

			setOfertas(pk, ofertaPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProfesorModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the profesor persistence.
	 */
	public void afterPropertiesSet() {
		profesorToOfertaTableMapper = TableMapperFactory.getTableMapper("BBDD_Ofertas_Profesor",
				"companyId", "profesorId", "ofertaId", this, ofertaPersistence);
	}

	public void destroy() {
		entityCache.removeCache(ProfesorImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper("BBDD_Ofertas_Profesor");
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	@BeanReference(type = OfertaPersistence.class)
	protected OfertaPersistence ofertaPersistence;
	protected TableMapper<Profesor, service.tfg.model.Oferta> profesorToOfertaTableMapper;
	private static final String _SQL_SELECT_PROFESOR = "SELECT profesor FROM Profesor profesor";
	private static final String _SQL_SELECT_PROFESOR_WHERE_PKS_IN = "SELECT profesor FROM Profesor profesor WHERE profesorId IN (";
	private static final String _SQL_SELECT_PROFESOR_WHERE = "SELECT profesor FROM Profesor profesor WHERE ";
	private static final String _SQL_COUNT_PROFESOR = "SELECT COUNT(profesor) FROM Profesor profesor";
	private static final String _SQL_COUNT_PROFESOR_WHERE = "SELECT COUNT(profesor) FROM Profesor profesor WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "profesor.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Profesor exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Profesor exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProfesorPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}