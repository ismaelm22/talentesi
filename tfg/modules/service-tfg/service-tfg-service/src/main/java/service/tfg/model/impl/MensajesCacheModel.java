/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import service.tfg.model.Mensajes;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Mensajes in entity cache.
 *
 * @author Ismael Medina
 * @see Mensajes
 * @generated
 */
@ProviderType
public class MensajesCacheModel implements CacheModel<Mensajes>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MensajesCacheModel)) {
			return false;
		}

		MensajesCacheModel mensajesCacheModel = (MensajesCacheModel)obj;

		if (mensajeId == mensajesCacheModel.mensajeId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, mensajeId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", mensajeId=");
		sb.append(mensajeId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", remitente=");
		sb.append(remitente);
		sb.append(", destinatario=");
		sb.append(destinatario);
		sb.append(", mensaje=");
		sb.append(mensaje);
		sb.append(", fecha=");
		sb.append(fecha);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Mensajes toEntityModel() {
		MensajesImpl mensajesImpl = new MensajesImpl();

		if (uuid == null) {
			mensajesImpl.setUuid(StringPool.BLANK);
		}
		else {
			mensajesImpl.setUuid(uuid);
		}

		mensajesImpl.setMensajeId(mensajeId);
		mensajesImpl.setCompanyId(companyId);

		if (remitente == null) {
			mensajesImpl.setRemitente(StringPool.BLANK);
		}
		else {
			mensajesImpl.setRemitente(remitente);
		}

		if (destinatario == null) {
			mensajesImpl.setDestinatario(StringPool.BLANK);
		}
		else {
			mensajesImpl.setDestinatario(destinatario);
		}

		if (mensaje == null) {
			mensajesImpl.setMensaje(StringPool.BLANK);
		}
		else {
			mensajesImpl.setMensaje(mensaje);
		}

		if (fecha == Long.MIN_VALUE) {
			mensajesImpl.setFecha(null);
		}
		else {
			mensajesImpl.setFecha(new Date(fecha));
		}

		mensajesImpl.resetOriginalValues();

		return mensajesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		mensajeId = objectInput.readLong();

		companyId = objectInput.readLong();
		remitente = objectInput.readUTF();
		destinatario = objectInput.readUTF();
		mensaje = objectInput.readUTF();
		fecha = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(mensajeId);

		objectOutput.writeLong(companyId);

		if (remitente == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(remitente);
		}

		if (destinatario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(destinatario);
		}

		if (mensaje == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mensaje);
		}

		objectOutput.writeLong(fecha);
	}

	public String uuid;
	public long mensajeId;
	public long companyId;
	public String remitente;
	public String destinatario;
	public String mensaje;
	public long fecha;
}