/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package service.tfg.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import service.tfg.model.Profesor;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Profesor in entity cache.
 *
 * @author Ismael Medina
 * @see Profesor
 * @generated
 */
@ProviderType
public class ProfesorCacheModel implements CacheModel<Profesor>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProfesorCacheModel)) {
			return false;
		}

		ProfesorCacheModel profesorCacheModel = (ProfesorCacheModel)obj;

		if (profesorId == profesorCacheModel.profesorId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, profesorId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", profesorId=");
		sb.append(profesorId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", email=");
		sb.append(email);
		sb.append(", pwd=");
		sb.append(pwd);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", apellidos=");
		sb.append(apellidos);
		sb.append(", area=");
		sb.append(area);
		sb.append(", despacho=");
		sb.append(despacho);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Profesor toEntityModel() {
		ProfesorImpl profesorImpl = new ProfesorImpl();

		if (uuid == null) {
			profesorImpl.setUuid(StringPool.BLANK);
		}
		else {
			profesorImpl.setUuid(uuid);
		}

		profesorImpl.setProfesorId(profesorId);
		profesorImpl.setCompanyId(companyId);

		if (email == null) {
			profesorImpl.setEmail(StringPool.BLANK);
		}
		else {
			profesorImpl.setEmail(email);
		}

		if (pwd == null) {
			profesorImpl.setPwd(StringPool.BLANK);
		}
		else {
			profesorImpl.setPwd(pwd);
		}

		if (nombre == null) {
			profesorImpl.setNombre(StringPool.BLANK);
		}
		else {
			profesorImpl.setNombre(nombre);
		}

		if (apellidos == null) {
			profesorImpl.setApellidos(StringPool.BLANK);
		}
		else {
			profesorImpl.setApellidos(apellidos);
		}

		if (area == null) {
			profesorImpl.setArea(StringPool.BLANK);
		}
		else {
			profesorImpl.setArea(area);
		}

		if (despacho == null) {
			profesorImpl.setDespacho(StringPool.BLANK);
		}
		else {
			profesorImpl.setDespacho(despacho);
		}

		profesorImpl.resetOriginalValues();

		return profesorImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		profesorId = objectInput.readLong();

		companyId = objectInput.readLong();
		email = objectInput.readUTF();
		pwd = objectInput.readUTF();
		nombre = objectInput.readUTF();
		apellidos = objectInput.readUTF();
		area = objectInput.readUTF();
		despacho = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(profesorId);

		objectOutput.writeLong(companyId);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (pwd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pwd);
		}

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (apellidos == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(apellidos);
		}

		if (area == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(area);
		}

		if (despacho == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(despacho);
		}
	}

	public String uuid;
	public long profesorId;
	public long companyId;
	public String email;
	public String pwd;
	public String nombre;
	public String apellidos;
	public String area;
	public String despacho;
}