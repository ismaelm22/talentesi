create index IX_C1AADAA3 on BBDD_Alumno (email[$COLUMN_LENGTH:75$]);
create index IX_9882A086 on BBDD_Alumno (nombre[$COLUMN_LENGTH:75$]);
create index IX_52D8EC69 on BBDD_Alumno (ofertaId);
create index IX_1BEEE63D on BBDD_Alumno (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_4902692F on BBDD_Alumno_Solicitudes (alumnoId);
create index IX_C4A3A6EC on BBDD_Alumno_Solicitudes (companyId);
create index IX_BF5023DC on BBDD_Alumno_Solicitudes (ofertaId);

create index IX_B213441C on BBDD_Empresa (email[$COLUMN_LENGTH:75$]);
create index IX_B527662D on BBDD_Empresa (nombre[$COLUMN_LENGTH:75$]);
create index IX_DA520724 on BBDD_Empresa (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_260E48FE on BBDD_Mensajes (destinatario[$COLUMN_LENGTH:75$]);
create index IX_F4A4DEB6 on BBDD_Mensajes (remitente[$COLUMN_LENGTH:75$]);
create index IX_D95786ED on BBDD_Mensajes (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_41B79337 on BBDD_Oferta (area[$COLUMN_LENGTH:75$]);
create index IX_170BAA16 on BBDD_Oferta (ofertaId);
create index IX_A68FA563 on BBDD_Oferta (titulo[$COLUMN_LENGTH:75$]);
create index IX_4653A6EA on BBDD_Oferta (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_88642DD5 on BBDD_Ofertas_Empresa (companyId);
create index IX_30F94BE1 on BBDD_Ofertas_Empresa (empresaId);
create index IX_BD5E9BD3 on BBDD_Ofertas_Empresa (ofertaId);

create index IX_20BEE35C on BBDD_Ofertas_Profesor (companyId);
create index IX_2DA3996C on BBDD_Ofertas_Profesor (ofertaId);
create index IX_D0E15125 on BBDD_Ofertas_Profesor (profesorId);

create index IX_ED4E483D on BBDD_Profesor (email[$COLUMN_LENGTH:75$]);
create index IX_E14CE62C on BBDD_Profesor (nombre[$COLUMN_LENGTH:75$]);
create index IX_A0B2D463 on BBDD_Profesor (uuid_[$COLUMN_LENGTH:75$], companyId);