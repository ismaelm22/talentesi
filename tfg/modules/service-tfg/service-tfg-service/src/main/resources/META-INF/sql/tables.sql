create table BBDD_Alumno (
	uuid_ VARCHAR(75) null,
	alumnoId LONG not null primary key,
	companyId LONG,
	email VARCHAR(75) null,
	pwd VARCHAR(75) null,
	nombre VARCHAR(75) null,
	apellidos VARCHAR(75) null,
	competencias VARCHAR(75) null,
	ofertaId LONG,
	curriculum BLOB
);

create table BBDD_Alumno_Solicitudes (
	companyId LONG not null,
	alumnoId LONG not null,
	ofertaId LONG not null,
	primary key (alumnoId, ofertaId)
);

create table BBDD_Empresa (
	uuid_ VARCHAR(75) null,
	empresaId LONG not null primary key,
	companyId LONG,
	email VARCHAR(75) null,
	pwd VARCHAR(75) null,
	nombre VARCHAR(75) null,
	cif VARCHAR(75) null,
	direccion VARCHAR(75) null,
	telefono VARCHAR(75) null
);

create table BBDD_Mensajes (
	uuid_ VARCHAR(75) null,
	mensajeId LONG not null primary key,
	companyId LONG,
	remitente VARCHAR(75) null,
	destinatario VARCHAR(75) null,
	mensaje VARCHAR(75) null,
	fecha DATE null
);

create table BBDD_Oferta (
	uuid_ VARCHAR(75) null,
	ofertaId LONG not null primary key,
	companyId LONG,
	titulo VARCHAR(75) null,
	descripcion VARCHAR(75) null,
	area VARCHAR(75) null,
	competencias VARCHAR(75) null,
	duracion VARCHAR(75) null
);

create table BBDD_Ofertas_Empresa (
	companyId LONG not null,
	empresaId LONG not null,
	ofertaId LONG not null,
	primary key (empresaId, ofertaId)
);

create table BBDD_Ofertas_Profesor (
	companyId LONG not null,
	ofertaId LONG not null,
	profesorId LONG not null,
	primary key (ofertaId, profesorId)
);

create table BBDD_Profesor (
	uuid_ VARCHAR(75) null,
	profesorId LONG not null primary key,
	companyId LONG,
	email VARCHAR(75) null,
	pwd VARCHAR(75) null,
	nombre VARCHAR(75) null,
	apellidos VARCHAR(75) null,
	area VARCHAR(75) null,
	despacho VARCHAR(75) null
);