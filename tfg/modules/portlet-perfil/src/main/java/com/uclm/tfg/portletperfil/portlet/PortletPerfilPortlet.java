package com.uclm.tfg.portletperfil.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.uclm.tfg.portletperfil.constants.PortletPerfilPortletKeys;

import portlet.commons.constants.SessionVariablesPortletKeys;
import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Oferta;
import service.tfg.model.Profesor;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.OfertaLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=portlet-perfil Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.name=" + PortletPerfilPortletKeys.PortletPerfil,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PortletPerfilPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		String rol = SessionVariablesPortletKeys.getRol(renderRequest);

		String email = SessionVariablesPortletKeys.getEmail(renderRequest);

		renderRequest.setAttribute("rol", rol);

		String url = PortalUtil.getCurrentURL(renderRequest);
		if (url.equals("/web/talent-esi/home")) {
			if (rol.equals("alumno")) {
				List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);
				renderRequest.setAttribute("nombre", alumno.get(0).getNombre());
				renderRequest.setAttribute("apellidos", alumno.get(0).getApellidos());
			} else if (rol.equals("profesor")) {
				List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
				renderRequest.setAttribute("nombre", profesor.get(0).getNombre());
				renderRequest.setAttribute("apellidos", profesor.get(0).getApellidos());
			} else if (rol.equals("empresa")) {
				List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);
				renderRequest.setAttribute("nombre", empresa.get(0).getNombre());
			}
			include("/META-INF/resources/view-menu.jsp", renderRequest, renderResponse);
		} else if (url.equals("/web/talent-esi/profile")) {
			if (SessionVariablesPortletKeys.isEditarPerfil(renderRequest)) {

				if (rol.equals("alumno")) {
					List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("nombre", alumno.get(0).getNombre());
					renderRequest.setAttribute("apellidos", alumno.get(0).getApellidos());
					String competencias = alumno.get(0).getCompetencias();
					renderRequest.setAttribute("competencias", competencias);
					renderRequest.setAttribute("ofertaasignada", alumno.get(0).getOfertaId());

				} else if (rol.equals("profesor")) {
					List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("nombre", profesor.get(0).getNombre());
					renderRequest.setAttribute("apellidos", profesor.get(0).getApellidos());
					renderRequest.setAttribute("area", profesor.get(0).getArea());
					renderRequest.setAttribute("despacho", profesor.get(0).getDespacho());

				} else if (rol.equals("empresa")) {
					List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("nombre", empresa.get(0).getNombre());
					renderRequest.setAttribute("cif", empresa.get(0).getCif());
					renderRequest.setAttribute("direccion", empresa.get(0).getDireccion());
					renderRequest.setAttribute("telefono", empresa.get(0).getTelefono());
				}

				include("/META-INF/resources/view-editperfil.jsp", renderRequest, renderResponse);

			} else if (SessionVariablesPortletKeys.isVerPerfil(renderRequest)) {

				if (rol.equals("alumno")) {
					List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("email", alumno.get(0).getEmail());
					renderRequest.setAttribute("nombre", alumno.get(0).getNombre());
					renderRequest.setAttribute("apellidos", alumno.get(0).getApellidos());
					String competencias = alumno.get(0).getCompetencias();
					String[] splitcompetencias = competencias.split(";");
					renderRequest.setAttribute("competencias", splitcompetencias);
					renderRequest.setAttribute("curriculum", alumno.get(0).getCurriculum());
					final List<Oferta> ofertas = OfertaLocalServiceUtil.getAlumnoOfertas(alumno.get(0).getAlumnoId());
					renderRequest.setAttribute("ofertas", ofertas);

				} else if (rol.equals("profesor")) {
					List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("email", profesor.get(0).getEmail());
					renderRequest.setAttribute("nombre", profesor.get(0).getNombre());
					renderRequest.setAttribute("apellidos", profesor.get(0).getApellidos());
					renderRequest.setAttribute("area", profesor.get(0).getArea());
					renderRequest.setAttribute("despacho", profesor.get(0).getDespacho());

					final List<Oferta> ofertas = OfertaLocalServiceUtil
							.getProfesorOfertas(profesor.get(0).getProfesorId());
					if (!ofertas.isEmpty()) {
						List<Alumno> solicitud = AlumnoLocalServiceUtil.getOfertaAlumnos(ofertas.get(0).getOfertaId());

						renderRequest.setAttribute("ofertas", ofertas);
						if (!solicitud.isEmpty()) {
							renderRequest.setAttribute("solicitudes", solicitud.get(0));
						}
					}

				} else if (rol.equals("empresa")) {
					List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);
					renderRequest.setAttribute("email", empresa.get(0).getEmail());
					renderRequest.setAttribute("nombre", empresa.get(0).getNombre());
					renderRequest.setAttribute("cif", empresa.get(0).getCif());
					renderRequest.setAttribute("direccion", empresa.get(0).getDireccion());
					renderRequest.setAttribute("telefono", empresa.get(0).getTelefono());

					final List<Oferta> ofertas = OfertaLocalServiceUtil
							.getEmpresaOfertas(empresa.get(0).getEmpresaId());
					if (!ofertas.isEmpty()) {
						List<Alumno> solicitud = AlumnoLocalServiceUtil.getOfertaAlumnos(ofertas.get(0).getOfertaId());
						renderRequest.setAttribute("solicitudes", solicitud.get(0));
						renderRequest.setAttribute("ofertas", ofertas);
					}

				}
				include("/META-INF/resources/view-principal.jsp", renderRequest, renderResponse);
			}
		}
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "verPerfil")
	public void verPerfil(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {

		SessionVariablesPortletKeys.setVerPerfil(actionRequest, true);
		SessionVariablesPortletKeys.setEditarPerfil(actionRequest, false);
		String urlprofile = "http://localhost:8080/web/talent-esi/profile";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);

	}

	@ProcessAction(name = "editarPerfil")
	public void editarPerfil(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {

		SessionVariablesPortletKeys.setVerPerfil(actionRequest, false);
		SessionVariablesPortletKeys.setEditarPerfil(actionRequest, true);
		String urlprofile = "http://localhost:8080/web/talent-esi/profile";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);

	}

	@ProcessAction(name = "registrar")
	public void registrar(ActionRequest actionRequest, ActionResponse actionResponse)
			throws PortalException, IOException {
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		String email = SessionVariablesPortletKeys.getEmail(actionRequest);

		if (SessionVariablesPortletKeys.getRol(actionRequest).equals("alumno")) {
			List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);

			String nombre = ParamUtil.getString(uploadPortletRequest, "nombre");
			String apellidos = ParamUtil.getString(uploadPortletRequest, "apellidos");
			String competencias = ParamUtil.getString(uploadPortletRequest, "competencias");

			File file = uploadPortletRequest.getFile("curriculum");
			OutputBlob blobData = null;
			try {
				InputStream inputStream = new FileInputStream(file);
				/**
				 * Below is the actual blob data
				 */
				blobData = new OutputBlob(inputStream, file.length());
			} catch (Exception e) {

			}
			AlumnoLocalServiceUtil.updateAlumno(alumno.get(0).getAlumnoId(), nombre, apellidos, competencias, blobData);

		} else if (SessionVariablesPortletKeys.getRol(actionRequest).equals("profesor")) {

			List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
			String nombre = ParamUtil.getString(uploadPortletRequest, "nombre");
			String apellidos = ParamUtil.getString(uploadPortletRequest, "apellidos");
			String area = ParamUtil.getString(uploadPortletRequest, "area");
			String despacho = ParamUtil.getString(uploadPortletRequest, "despacho");

			ProfesorLocalServiceUtil.updateProfesor(profesor.get(0).getProfesorId(), nombre, apellidos, area, despacho);

		} else if (SessionVariablesPortletKeys.getRol(actionRequest).equals("empresa")) {

			List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);
			String nombre = ParamUtil.getString(uploadPortletRequest, "nombre");
			String cif = ParamUtil.getString(uploadPortletRequest, "cif");
			String direccion = ParamUtil.getString(uploadPortletRequest, "direccion");
			String telefono = ParamUtil.getString(uploadPortletRequest, "telefono");

			EmpresaLocalServiceUtil.updateEmpresa(empresa.get(0).getEmpresaId(), nombre, cif, direccion, telefono);
		}

		String urlprofile = "http://localhost:8080/web/talent-esi/home";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);

	}

	@ProcessAction(name = "mensajes")
	public void mensajes(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {
		String urlprofile = "http://localhost:8080/web/talent-esi/messages";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);
	}

	@ProcessAction(name = "eliminar")
	public void eliminar(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {
		String email = SessionVariablesPortletKeys.getEmail(actionRequest);
		String rol = SessionVariablesPortletKeys.getRol(actionRequest);

		if (rol.equals("alumno")) {
			List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);
			try {
				AlumnoLocalServiceUtil.deleteAlumno(alumno.get(0).getAlumnoId());
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (rol.equals("profesor")) {
			List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(email);
			try {
				ProfesorLocalServiceUtil.deleteProfesor(profesor.get(0).getProfesorId());
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (rol.equals("empresa")) {
			List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(email);
			try {
				EmpresaLocalServiceUtil.deleteEmpresa(empresa.get(0).getEmpresaId());
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		SessionVariablesPortletKeys.limpiarSessionVariables(actionRequest);
		String urlprofile = "http://localhost:8080/web/talent-esi/login";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);

	}

}