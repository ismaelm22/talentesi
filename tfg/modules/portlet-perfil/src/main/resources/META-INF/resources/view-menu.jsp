<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>


<portlet:actionURL name="verPerfil" var="verPerfilURL"></portlet:actionURL>
<portlet:actionURL name="editarPerfil" var="editarPerfilURL"></portlet:actionURL>
<portlet:actionURL name="mensajes" var="mensajesURL"></portlet:actionURL>

<div class="ads__ad"> 
<h1>Mi Perfil</h1>


<div >${nombre} ${apellidos}</div>
<div class="row" style="
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 20px;
">
	<div class="col-4">
	<a class="buttonTalent buttonTalent--type1"  style="padding: 5px; display:inline-block" href="${verPerfilURL}">Ver perfil</a>
	</div>
	<div class="col-4">
	<a class="buttonTalent buttonTalent--type1" style="padding: 5px; display:inline-block"  href="${editarPerfilURL}">Editar perfil</a> 
	</div>
	<div class="col-4">
	<a class="buttonTalent buttonTalent--type1"  style="padding: 5px; display:inline-block" href="${mensajesURL}">Mensajes</a>
	</div>
</div>
</div>
	
	
	
