<%@ include file="/init.jsp" %>
	
<portlet:actionURL name="eliminar" var="eliminarURL"></portlet:actionURL>	
	


<c:choose>
	<c:when test="${rol=='alumno'}">	
		<div class="row">
		<div class="col-6 offset-3">
			<h2>Mi Perfil: Alumno</h2>
			<div class="list__element list__element-profile">
				<a class="btn btn-danger deleteuser" href="#"  style="position: absolute;right: 10px;">Eliminar usuario</a>
				<div style="text-align: center">
					<img src="<%= themeDisplay.getPathThemeImages() %>/alumn.png">
				</div>
				<span class="titleprofile">Email: </span> 
				<span class="subtitleprofile"> ${email} </span>
				<span class="titleprofile">Nombre </span> 
				<span class="subtitleprofile">${nombre} </span>
				<span class="titleprofile">Apellidos </span> 
				<span class="subtitleprofile">${apellidos} </span> 
				<span class="titleprofile">Competencias: </span> 
				<c:forEach items="${competencias}" var="competencia">
				<p class="subtitleprofile">-${competencia} </p>   
				</c:forEach>
				<!-- 
				<span class="titleprofile">Curriculum: </span> 
				<a href="#" class="buttonTalent buttonTalent--type2" style="padding:5px"> Descargar Curriculum  </a>
				 -->	
				<span class="titleprofile">Ofertas solicitadas: </span> 	
				<c:forEach items="${ofertas}" var="oferta">
					<p class="subtitleprofile">${oferta.ofertaId} ${oferta.titulo}<a href="#" title="preview" style="text-align:right;"> <img src="<%= themeDisplay.getPathThemeImages() %>/preview.png" style="width: 25px;height: 25px;"></a> 
					 </p>  							
				<span class="titleprofile">Ofertas asignada: </span> 	 
				<span class="subtitleprofile">${oferta.ofertaId} ${oferta.titulo}</span>
				</c:forEach>
				
			</div>
		</div>
	</div>			
	</c:when>
	<c:when test="${rol=='profesor'}">
		<div class="row">
		<div class="col-6 offset-3">
			<h2>Mi Perfil: Profesor</h2>
			<div class="list__element list__element-profile">
				<a class="btn btn-danger deleteuser" href="#"  style="position: absolute;right: 10px;">Eliminar usuario</a>
				<div style="text-align: center">
					<img src="<%= themeDisplay.getPathThemeImages() %>/user.png">
				</div>
				<span class="titleprofile">Email: </span> 
				<span class="subtitleprofile"> ${email} </span>
				<span class="titleprofile">Nombre </span> 
				<span class="subtitleprofile">${nombre} </span>
				<span class="titleprofile">Apellidos </span> 
				<span class="subtitleprofile">${apellidos} </span> 
				<span class="titleprofile">Area: </span> 
				<span class="subtitleprofile">${area} </span> 
				<span class="titleprofile">Despacho: </span> 
				<span class="subtitleprofile">${despacho} </span> 
				
				<span class="titleprofile">Ofertas:  </span> 
				
				
				
				
				<div class="row" style="border:1px solid grey; margin:10px;">
					<div class="col-12">
						<c:forEach items="${ofertas}" var="oferta">
							<p class="subtitleprofile" style="display:inline-block">${oferta.ofertaId} ${oferta.titulo} </p>  
							<a href="#" title="preview" style="text-align:right;"> <img src="<%= themeDisplay.getPathThemeImages() %>/preview.png" style="width: 25px;height: 25px;"></a> 
							<div class="col-12">
						<label style="padding-left:20px;">Asignar
							</label>
								<select  class="aui-field-select" name="area">
									<option class="" value="">Seleccione un candidato</option>
									<option class="" value="TI">Ismael Medina</option>
									<option class="" value="SI">Pilar Montero</option>
									<option class="" value="CO">David Nieto</option>
								</select>
							<a href="#" title="assign" style="text-align:right;"> <img src="<%= themeDisplay.getPathThemeImages() %>/assign.png" style="width: 25px;height: 25px;"></a> 
								
					</div>
						</c:forEach>
					</div>
					
				</div>
				<!--  <a class="buttonTalent buttonTalent--type1" href="#" ">Asignar</a>
				--> 
			</div>
		</div>
	</div>			
	</c:when>
	<c:when test="${rol=='empresa'}">
		<div class="row">
		<div class="col-6 offset-3">
			<h2>Mi Perfil: Empresa</h2>
			<div class="list__element list__element-profile">
				<a class="btn btn-danger deleteuser" href="#"  style="position: absolute;right: 10px;">Eliminar usuario</a>
				<div style="text-align: center">
					<img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg">
				</div>
				<span class="titleprofile">Email: </span> 
				<span class="subtitleprofile"> ${email} </span>
				<span class="titleprofile">Nombre: </span> 
				<span class="subtitleprofile">${nombre} </span>
				<span class="titleprofile">Cif: </span> 
				<span class="subtitleprofile">${cif} </span> 
				<span class="titleprofile">Direccion: </span> 
				<span class="subtitleprofile">${direccion} </span> 
				<span class="titleprofile">Telefono: </span> 
				<span class="subtitleprofile">${telefono} </span> 
				<span class="titleprofile">Ofertas:  </span> 
				<c:forEach items="${ofertas}" var="oferta">
				<p class="subtitleprofile">${oferta.ofertaId} ${oferta.titulo} </p>   
				</c:forEach>
				
				<!--  <a class="buttonTalent buttonTalent--type1" href="#" ">Asignar</a>
				--> 		
			</div>
		</div>
	</div>			
	</c:when>
</c:choose>

<aui:form id="fmDeleteUser" name="fmDeleteUser" action="#"> 
</aui:form>

<div class="yui3-skin-sam">
	<div id="modal" class="defaultmodal"></div>
</div>


<script type="text/javascript">
$(document).ready( function() {
	var $clickelement = $('.deleteuser')
	console.log($clickelement);
	$clickelement.click(function(e) {
		e.preventDefault();
			YUI().use('aui-modal', function(Y) {
				var modal = new Y.Modal({
					bodyContent : '�Desea eliminar el usuario?',
					centered : true,
					destroyOnHide : false,
					headerContent : '<h3>Eliminar usuario</h3>',
					modal : true,
					render : '#modal',
					resizable : {
						handles : 'b, r'
					},
					visible : true,
					width : 450
				}).render();
		
				modal.addToolbar([ {
					label : 'Cancel',
					on : {
						click : function() {
							modal.hide();
						}
					}
				}, {
					label : 'Okay',
					on : {
						click : function() {
							$("#<portlet:namespace/>fmDeleteUser").attr("action", "${eliminarURL}").submit();
						}
					}
				} ]);
					modal.show();	
			});
	});
})
</script>




