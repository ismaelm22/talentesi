<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%><%@
taglib
	uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%><%@
taglib
	uri="http://liferay.com/tld/theme" prefix="liferay-theme"%><%@
taglib
	uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<portlet:actionURL name="registrar" var="registrarURL"></portlet:actionURL>

<div>
	<c:choose>
		<c:when test="${rol=='alumno'}">
			<aui:form action="<%=registrarURL%>" method="post">
				<div class="row">
					<div class="col-6 offset-3">
						<h1>Editar mi perfil</h1>
						<h2>Alumno</h2>
						<div class="form-wrapper">
							<label class="control-label">Nombre </label> 
							<input type="text" name="nombre" required value="${nombre}"> 
							<label class="control-label">Apellidos</label>
							<input type="text" name="apellidos" required value="${apellidos}"> 
							<label class="control-label">Competencias (Separelas por ";" Ej: C++;Java;Liferay)</label>
							<input type="text" name="competencias" required value="${competencias}"> 
							<!--  
							<label class="control-label">Curriculum</label>
							<input type="file" name="curriculum">
							-->
							<button type="submit" class="buttonTalent buttonTalent--type2">Actualizar información</button>
						</div>
					</div>
				</div>
			</aui:form>
		</c:when>
		<c:when test="${rol=='profesor'}">
			<aui:form action="<%=registrarURL%>" method="post">
				<div class="row">
					<div class="col-6 offset-3">
						<h1>Editar mi perfil</h1>
						<h2>Profesor</h2>
						<div class="form-wrapper">
							<label>Nombre </label>
							<input type="text" name="nombre" required value="${nombre}">
							<label>Apellidos</label>
							<input type="text" name="apellidos" required value="${apellidos}">
							<label>Area</label>
							<select class="" id="" name="area" required>
								<option value="IS">IS (Ingeniría del Software)</option>
								<option value="TI">TI (Tecnologías de la Informacion)</option>
								<option value="IC">IC (Ingeniría de Computadores)</option>
								<option value="CO">CO (Computación)</option>
							</select>
	
							<label>despacho</label>
							<input type="text" name="despacho" required value="${despacho}">
	
							<button type="submit" class="buttonTalent buttonTalent--type2">Actualizar información</button>
						</div>
					</div>
				</div>
			</aui:form>
		</c:when>
		<c:when test="${rol=='empresa'}">
			<aui:form action="<%=registrarURL%>" method="post">
				<div class="row">
					<div class="col-6 offset-3">
						<h1>Editar mi perfil</h1>
						<h2>Empresa</h2>
						<div class="form-wrapper">
							<label>Nombre </label>
							<input type="text" name="nombre" required value="${nombre}">
			
							<label>CIF</label>
							<input type="text" name="cif" required value="${cif}">
			
							<label>Direccion</label>
							<input type="text" name="direccion" required value="${direccion}">
			
							<label>Telefono</label>
							<input type="text" name="telefono" required value="${telefono}">
			
							<button type="submit" class="buttonTalent buttonTalent--type2">Actualizar información</button>
						</div>
					</div>
				</div>	
			</aui:form>
		</c:when>
	</c:choose>

</div>