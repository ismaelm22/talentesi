package com.uclm.tfg.portletmensajes.portlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.uclm.tfg.portletmensajes.constants.PortletMesanjesPortletKeys;

import portlet.commons.constants.SessionVariablesPortletKeys;
import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Mensajes;
import service.tfg.model.Profesor;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.MensajesLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=portlet-mesajes Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PortletMesanjesPortletKeys.PortletMesanjes,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PortletMesanjesPortlet extends MVCPortlet {

	@SuppressWarnings({ "null", "unused" })
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		// if
		// (SessionVariablesPortletKeys.getRol(renderRequest).equals("alumno"))
		// {
		// List<Alumno> alumno = AlumnoLocalServiceUtil
		// .findByEmailId(SessionVariablesPortletKeys.getEmail(renderRequest));
		// renderRequest.setAttribute("nombre", alumno.get(0).getNombre());
		// renderRequest.setAttribute("apellidos",
		// alumno.get(0).getApellidos());
		// } else if
		// (SessionVariablesPortletKeys.getRol(renderRequest).equals("profesor"))
		// {
		// List<Profesor> profesor = ProfesorLocalServiceUtil
		// .findByEmailId(SessionVariablesPortletKeys.getEmail(renderRequest));
		// renderRequest.setAttribute("nombre", profesor.get(0).getNombre());
		// renderRequest.setAttribute("apellidos",
		// profesor.get(0).getApellidos());
		// } else if
		// (SessionVariablesPortletKeys.getRol(renderRequest).equals("empresa"))
		// {
		// List<Empresa> empresa = EmpresaLocalServiceUtil
		// .findByEmailId(SessionVariablesPortletKeys.getEmail(renderRequest));
		// renderRequest.setAttribute("nombre", empresa.get(0).getNombre());
		// }
		//
		// List<Mensajes> listmensajesRemitente = MensajesLocalServiceUtil
		// .findByremitenteId(SessionVariablesPortletKeys.getEmail(renderRequest));
		// ArrayList<Object> contactos = new ArrayList<>();
		//
		// for (Mensajes mensaje : listmensajesRemitente) {
		// if (!contactos.contains(mensaje.getDestinatario())) {
		// contactos.add(mensaje.getDestinatario());
		// }
		// }
		// renderRequest.setAttribute("contactos", contactos);
		//
		// List<Mensajes> mensajesDestinatario =
		// MensajesLocalServiceUtil.findByremitenteId(contactos.get(1).toString());
		// List<Mensajes> mensajesRemitente = MensajesLocalServiceUtil
		// .findByremitenteId(SessionVariablesPortletKeys.getEmail(renderRequest));
		//
		// ArrayList<Mensajes> chat = new ArrayList<Mensajes>();
		// for (Mensajes mensaje : mensajesRemitente) {
		// if (mensaje.getDestinatario().equals(contactos.get(1).toString())) {
		// chat.add(mensaje);
		// }
		// }
		// for (Mensajes mensaje : mensajesDestinatario) {
		// if
		// (mensaje.getDestinatario().equals(SessionVariablesPortletKeys.getEmail(renderRequest)))
		// {
		// chat.add(mensaje);
		// }
		// }
		//
		// Collections.sort(chat);
		// if (chat != null) {
		// renderRequest.setAttribute("destinatario",
		// contactos.get(1).toString());
		// renderRequest.setAttribute("remitente",
		// SessionVariablesPortletKeys.getEmail(renderRequest));
		// renderRequest.setAttribute("chat", chat);
		// }

		// TODO Auto-generated method stub
		String destinatario = SessionVariablesPortletKeys.getDestinatario(renderRequest);
		String remitente = SessionVariablesPortletKeys.getEmail(renderRequest);

		List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(remitente);
		List<Profesor> profesor = ProfesorLocalServiceUtil.findByEmailId(remitente);
		List<Empresa> empresa = EmpresaLocalServiceUtil.findByEmailId(remitente);

		if (!alumno.isEmpty()) {
			renderRequest.setAttribute("nombre", alumno.get(0).getNombre());
			renderRequest.setAttribute("apellidos", alumno.get(0).getApellidos());

		}
		if (!profesor.isEmpty()) {
			renderRequest.setAttribute("nombre", profesor.get(0).getNombre());
			renderRequest.setAttribute("apellidos", profesor.get(0).getApellidos());
		}
		if (!empresa.isEmpty()) {
			renderRequest.setAttribute("nombre", empresa.get(0).getNombre());
		}

		renderRequest.setAttribute("destinatario", destinatario);

		List<Mensajes> mensajesDestinatario = MensajesLocalServiceUtil.findByremitenteId(destinatario);
		List<Mensajes> mensajesRemitente = MensajesLocalServiceUtil.findByremitenteId(remitente);

		ArrayList<Mensajes> chat = new ArrayList<Mensajes>();
		for (Mensajes mensaje : mensajesRemitente) {
			if (mensaje.getDestinatario().equals(destinatario)) {
				chat.add(mensaje);
			}
		}
		for (Mensajes mensaje : mensajesDestinatario) {
			if (mensaje.getDestinatario().equals(remitente)) {
				chat.add(mensaje);
			}
		}

		Collections.sort(chat);
		if (chat != null) {
			renderRequest.setAttribute("destinatario", destinatario);
			renderRequest.setAttribute("remitente", remitente);
			renderRequest.setAttribute("chat", chat);
		}

		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "enviarMensaje")
	public void goEnviarMensaje(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
		final ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		String text = ParamUtil.getString(uploadPortletRequest, "textmessage");
		String remitente = SessionVariablesPortletKeys.getEmail(actionRequest);
		String destinatario = SessionVariablesPortletKeys.getDestinatario(actionRequest);
		LocalDate fecha = LocalDate.now();

		MensajesLocalServiceUtil.addMensae(remitente, destinatario, text, fecha, td.getCompanyId());

	}
}