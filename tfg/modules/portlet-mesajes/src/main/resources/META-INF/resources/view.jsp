<%@ include file="/init.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<portlet:actionURL name="enviarMensaje" var="enviarMensajeURL"></portlet:actionURL>

<div class="container">
	<h2>Perfil: ${nombre} ${apellidos}</h2>
	<div class="row message">
		<div class="col-12">
			<div class="tab">
			  <button class="tablinks" onclick="talentEsi.general.openCity(event, '${destinatario}')">${destinatario}</button>
			</div>
	
			<div id="${destinatario}" class="tabcontent">
				<div class="tabcontent-messages">
					<c:forEach var="mensaje" items="${chat}">
							<c:if test="${mensaje.remitente==remitente}">
								<p class="message__received">${mensaje.mensaje} <span style=" display: block; text-align: right; font-size: 0.8em; color: gray;">Enviado <fmt:formatDate value="${mensaje.fecha}" pattern="dd-MM-yyyy"/></span></p>
								<hr>
							</c:if>
							<c:if test="${mensaje.destinatario==remitente }">
								<p class="message__sent">${mensaje.mensaje} <span style=" display: block; text-align: right; font-size: 0.8em; color: gray;">Enviado <fmt:formatDate value="${mensaje.fecha}" pattern="dd-MM-yyyy"/></span></p>
								<hr>
							</c:if>

					</c:forEach>
				</div>
				<aui:form action="<%=enviarMensajeURL%>" method="POST">
					<div class="tabcontent-input" style="bottom: 10px">
					 	<div class="row">
					 		<div class="col-9">
					 			<input class="field form-control" name="textmessage" placeholder="Introduce mensaje" type="text" value="" maxlength="90">
					 		</div>
					 		<div class="col-2">
					 			<button type="submit" class="buttonTalent buttonTalent--type1" style="margin-top: 3px">Enviar</button>
					 		</div>
					 	</div>
					 	
					 </div>
				</aui:form>
			</div>
			
	
			<div id="Paris" class="tabcontent" style="display: none;">
			  <h3>Ismael</h3>
			  <p>Chat con ismael.</p> 
			</div>
	
			<div id="Tokyo" class="tabcontent" style="display: none;">
			  <h3>Tokyo</h3>
			  <p>Tokyo is the capital of Japan.</p>
			</div>
		</div>
	</div>
</div>
