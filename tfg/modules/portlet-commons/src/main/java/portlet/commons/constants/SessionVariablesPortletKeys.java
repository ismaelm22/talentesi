package portlet.commons.constants;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import com.liferay.portal.kernel.util.GetterUtil;

/**
 * @author ismaelmedinarodriguez
 */
public class SessionVariablesPortletKeys {

	private static final String _LOGEADO = "LIFERAY_SHARED_LTFG_LOGEADO";
	private static final String _ROL = "LIFERAY_SHARED_TFG_ROL";
	private static final String _EMAIL = "LIFERAY_SHARED_TFG_EMAIL";
	private static final String _VERPERFIL = "LIFERAY_SHARED_TFG_VERPERFIL";
	private static final String _EDITARPERFIL = "LIFERAY_SHARED_TFG_EDITARPERFIL";
	// private static final String _USUARIO = "LIFERAY_SHARED_TFG_USUARIO";
	private static final String _VEROFERTA = "LIFERAY_SHARED_TFG_VEROFERTA";
	private static final String _DESTINATARIO = "LIFERAY_SHARED_TFG_DESTINATARIO";
	private static final String _OFERTA = "LIFERAY_SHARED_TFG_OFERTA";

	public static void limpiarSessionVariables(PortletRequest portletRequest) {
		SessionVariablesPortletKeys.setLogeado(portletRequest, false);
		SessionVariablesPortletKeys.setRol(portletRequest, null);
		SessionVariablesPortletKeys.setEmail(portletRequest, null);
		SessionVariablesPortletKeys.setVerPerfil(portletRequest, false);
		SessionVariablesPortletKeys.setEditarPerfil(portletRequest, false);

	}

	public static void setOferta(PortletRequest portletRequest, String oferta) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_OFERTA, oferta, PortletSession.APPLICATION_SCOPE);
	}

	public static String getOferta(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getString(psession.getAttribute(_OFERTA, PortletSession.APPLICATION_SCOPE), "");
	}

	public static void setRol(PortletRequest portletRequest, String rol) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_ROL, rol, PortletSession.APPLICATION_SCOPE);
	}

	public static String getRol(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getString(psession.getAttribute(_ROL, PortletSession.APPLICATION_SCOPE), "");
	}

	public static void setEmail(PortletRequest portletRequest, String email) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_EMAIL, email, PortletSession.APPLICATION_SCOPE);
	}

	public static String getEmail(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getString(psession.getAttribute(_EMAIL, PortletSession.APPLICATION_SCOPE), "");
	}

	public static void setLogeado(PortletRequest portletRequest, boolean logeado) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_LOGEADO, logeado, PortletSession.APPLICATION_SCOPE);
	}

	public static boolean isLogeado(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getBoolean(psession.getAttribute(_LOGEADO, PortletSession.APPLICATION_SCOPE), false);
	}

	public static void setEditarPerfil(PortletRequest portletRequest, boolean editarperfil) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_EDITARPERFIL, editarperfil, PortletSession.APPLICATION_SCOPE);
	}

	public static boolean isEditarPerfil(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getBoolean(psession.getAttribute(_EDITARPERFIL, PortletSession.APPLICATION_SCOPE), false);
	}

	public static void setVerPerfil(PortletRequest portletRequest, boolean verperfil) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_VERPERFIL, verperfil, PortletSession.APPLICATION_SCOPE);
	}

	public static boolean isVerPerfil(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getBoolean(psession.getAttribute(_VERPERFIL, PortletSession.APPLICATION_SCOPE), false);
	}

	public static void setVerOferta(PortletRequest portletRequest, boolean verOferta) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_VEROFERTA, verOferta, PortletSession.APPLICATION_SCOPE);
	}

	public static boolean isVerOferta(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getBoolean(psession.getAttribute(_VEROFERTA, PortletSession.APPLICATION_SCOPE), false);
	}

	public static void setDestinatario(PortletRequest portletRequest, String destinatario) {
		PortletSession psession = portletRequest.getPortletSession();
		psession.setAttribute(_DESTINATARIO, destinatario, PortletSession.APPLICATION_SCOPE);
	}

	public static String getDestinatario(PortletRequest portletRequest) {
		PortletSession psession = portletRequest.getPortletSession();
		return GetterUtil.getString(psession.getAttribute(_DESTINATARIO, PortletSession.APPLICATION_SCOPE), "");
	}

}