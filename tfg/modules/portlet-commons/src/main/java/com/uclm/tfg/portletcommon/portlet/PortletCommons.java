package com.uclm.tfg.portletcommon.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=portlet-common Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.name=" + "Portlet comun",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PortletCommons extends MVCPortlet {
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		String url = PortalUtil.getCurrentURL(renderRequest);

		if (url.equals("/web/talent-esi/contact")) {
			include("/META-INF/resources/view-contact.jsp", renderRequest, renderResponse);
		} else if (url.equals("/web/talent-esi/politics")) {
			include("/META-INF/resources/view-politics.jsp", renderRequest, renderResponse);

		}
		super.doView(renderRequest, renderResponse);
	}

}