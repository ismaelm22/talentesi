package com.uclm.tfg.portletbuscaroferta.constants;

/**
 * @author ismaelmedinarodriguez
 */
public class PortletBuscarOfertaPortletKeys {

	public static final String PortletBuscarOferta = "PortletBuscarOferta";

}