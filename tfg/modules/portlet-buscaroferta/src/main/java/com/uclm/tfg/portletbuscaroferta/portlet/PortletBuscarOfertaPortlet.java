package com.uclm.tfg.portletbuscaroferta.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.uclm.tfg.portletbuscaroferta.constants.PortletBuscarOfertaPortletKeys;

import portlet.commons.constants.SessionVariablesPortletKeys;
import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Oferta;
import service.tfg.model.Profesor;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.OfertaLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=portlet-buscaroferta Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.name=" + PortletBuscarOfertaPortletKeys.PortletBuscarOferta,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PortletBuscarOfertaPortlet extends MVCPortlet {

	final List<Oferta> ofertasfiltradas = new ArrayList<Oferta>();

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		String url = PortalUtil.getCurrentURL(renderRequest);

		String rol = SessionVariablesPortletKeys.getRol(renderRequest);

		renderRequest.setAttribute("rol", rol);

		if (url.equals("/web/talent-esi/home")) {
			SessionVariablesPortletKeys.setVerOferta(renderRequest, false);
			include("/META-INF/resources/view-menu.jsp", renderRequest, renderResponse);

		} else if (url.equals("/web/talent-esi/search-job") || url.contains("?")) {

			if (SessionVariablesPortletKeys.isVerOferta(renderRequest)) {
				long ofertaId = Long.parseLong(SessionVariablesPortletKeys.getOferta(renderRequest));
				List<Oferta> oferta = OfertaLocalServiceUtil.findByOfertaId(ofertaId);
				List<Empresa> empresa = EmpresaLocalServiceUtil.getOfertaEmpresas(ofertaId);
				List<Profesor> profesor = ProfesorLocalServiceUtil.getOfertaProfesors(ofertaId);

				if (!empresa.isEmpty()) {
					renderRequest.setAttribute("empresa", empresa.get(0));
				}
				if (!profesor.isEmpty()) {
					renderRequest.setAttribute("profesor", profesor.get(0));
				}

				renderRequest.setAttribute("oferta", oferta.get(0));
				include("/META-INF/resources/view-offer.jsp", renderRequest, renderResponse);
			} else {

				final List<Oferta> ofertas = OfertaLocalServiceUtil.getOfertas(0, Integer.MAX_VALUE);

				List<Oferta> ofertasEmpresa = new ArrayList<Oferta>();
				List<Oferta> ofertasProfesor = new ArrayList<Oferta>();
				List<Oferta> ofertasTutorizadas = new ArrayList<Oferta>();

				for (Oferta oferta : ofertas) {
					if ((ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()) != null
							&& !ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()).isEmpty())
							&& (EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()) != null
									&& !EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()).isEmpty())) {
						ofertasTutorizadas.add(oferta);
					} else if (ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()) != null
							&& !ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()).isEmpty()) {
						ofertasProfesor.add(oferta);
					} else if (EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()) != null
							&& !EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()).isEmpty()) {
						ofertasEmpresa.add(oferta);
					}

				}
				renderRequest.setAttribute("ofertas", ofertas);

				renderRequest.setAttribute("ofertasFiltradas", ofertasfiltradas);
				renderRequest.setAttribute("ofertasTutorizadas", ofertasTutorizadas);
				renderRequest.setAttribute("ofertasEmpresa", ofertasEmpresa);
				renderRequest.setAttribute("ofertasProfesor", ofertasProfesor);

				include("/META-INF/resources/view-principal.jsp", renderRequest, renderResponse);
			}

		}
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "buscaroferta")
	public void goBuscarOferta(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {

		String urlhome = "http://localhost:8080/web/talent-esi/search-job";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	@ProcessAction(name = "addOffer")
	public void goAddOffer(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		final ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String titulo = ParamUtil.getString(uploadPortletRequest, "titulo");
		String descripcion = ParamUtil.getString(uploadPortletRequest, "descripcion");
		String competencias = ParamUtil.getString(uploadPortletRequest, "competencias");
		String area = ParamUtil.getString(uploadPortletRequest, "area");
		String duracion = ParamUtil.getString(uploadPortletRequest, "duracion");

		if (SessionVariablesPortletKeys.getRol(actionRequest).equals("profesor")) {
			List<Profesor> profesores = ProfesorLocalServiceUtil
					.findByEmailId(SessionVariablesPortletKeys.getEmail(actionRequest));
			OfertaLocalServiceUtil.addOferta(td.getCompanyId(), titulo, descripcion, area, competencias, duracion,
					profesores, null);

		} else {
			List<Empresa> empresas = EmpresaLocalServiceUtil
					.findByEmailId(SessionVariablesPortletKeys.getEmail(actionRequest));
			OfertaLocalServiceUtil.addOferta(td.getCompanyId(), titulo, descripcion, area, competencias, duracion, null,
					empresas);
		}

		String urlhome = "http://localhost:8080/web/talent-esi/search-job";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	@ProcessAction(name = "verOferta")
	public void goVerOferta(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String oferta = ParamUtil.getString(uploadPortletRequest, "ofertaId");

		SessionVariablesPortletKeys.setVerOferta(actionRequest, true);
		SessionVariablesPortletKeys.setOferta(actionRequest, oferta);

		String urlhome = "http://localhost:8080/web/talent-esi/search-job";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	@ProcessAction(name = "solicitarOferta")
	public void solicitarOferta(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		String oferta = SessionVariablesPortletKeys.getOferta(actionRequest);
		String email = SessionVariablesPortletKeys.getEmail(actionRequest);

		List<Alumno> alumno = AlumnoLocalServiceUtil.findByEmailId(email);

		List<Oferta> ofertas = OfertaLocalServiceUtil.findByOfertaId(Long.parseLong(oferta));

		OfertaLocalServiceUtil.setAlumnoOfertas(alumno.get(0).getAlumnoId(), getOfertasIds(ofertas));

		String urlhome = "http://localhost:8080/web/talent-esi/home";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	private long[] getOfertasIds(Collection<Oferta> ofertas) {
		return ofertas.stream().mapToLong(Oferta::getOfertaId).toArray();
	}

	@ProcessAction(name = "filtrarOferta")
	public void filtraroferta(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		String area = ParamUtil.getString(uploadPortletRequest, "area");
		String propietario = ParamUtil.getString(uploadPortletRequest, "propietario");

		final List<Oferta> ofertas = OfertaLocalServiceUtil.getOfertas(0, Integer.MAX_VALUE);
		// List<Oferta> ofertasfiltradas = new ArrayList<Oferta>();
		for (Oferta oferta : ofertas) {

			if (!area.isEmpty() && !propietario.isEmpty()) {

				if (oferta.getArea().equals(area) && propietario.equals("PR")) {
					if (ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()) != null
							&& !ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()).isEmpty()) {
						ofertasfiltradas.add(oferta);
					}
				} else if (oferta.getArea().equals(area) && propietario.equals("EM")) {

					if (EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()) != null
							&& !EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()).isEmpty()) {
						ofertasfiltradas.add(oferta);
					}
				}
			} else if (area.isEmpty() && !propietario.isEmpty()) {
				if (propietario.equals("PR")) {
					if (ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()) != null
							&& !ProfesorLocalServiceUtil.getOfertaProfesors(oferta.getOfertaId()).isEmpty()) {
						ofertasfiltradas.add(oferta);
					}
				} else if (propietario.equals("EM")) {

					if (EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()) != null
							&& !EmpresaLocalServiceUtil.getOfertaEmpresas(oferta.getOfertaId()).isEmpty()) {
						ofertasfiltradas.add(oferta);
					}
				}

			} else if (!area.isEmpty() && propietario.isEmpty()) {
				if (oferta.getArea().equals(area)) {
					ofertasfiltradas.add(oferta);
				}
			}
		}
	}

}