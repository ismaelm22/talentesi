<%@ include file="/init.jsp"%>

<portlet:actionURL name="addOffer" var="addOfferURL"></portlet:actionURL>

<aui:form action="<%=addOfferURL%>" method="post">
	<div class="row">
		<div class="col-6 offset-3">
			<h1>A�adir Oferta</h1>
			<div class="form-wrapper">
				<label class="control-label">Titulo </label> 
				<input type="text" name="titulo"> 
				<label class="control-label">Descripcion</label>
				<textarea rows="4" cols="50" name="descripcion"> </textarea>
				<label class="control-label">Competencias (Separelas por ";" Ej: C++;Java;Liferay)</label> 
				<input type="text" name="competencias"> 
				<label>Area</label>
					<select class="" id="" name="area">
						<option value="IS">IS (Ingenir�a del Software)</option>
						<option value="TI">TI (Tecnolog�as de la Informacion)</option>
						<option value="IC">IC (Ingenir�a de Computadores)</option>
						<option value="CO">CO (Computaci�n)</option>
					</select>
				<label class="control-label">Duraci�n</label> 
				<input type="text" name="duracion"> 
				<button type="submit" class="buttonTalent buttonTalent--type2">A�adir Oferta</button>
			</div>
		</div>
	</div>
</aui:form>

