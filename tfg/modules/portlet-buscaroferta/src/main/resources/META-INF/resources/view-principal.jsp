<%@ include file="/init.jsp" %>



<portlet:renderURL var='addOffer'>
	<portlet:param name='jspPage' value="/addOffer.jsp" />
</portlet:renderURL>

<portlet:actionURL name="verOferta" var="verOfertaURL">
</portlet:actionURL>

<portlet:actionURL name="filtrarOferta" var="filtrarOfertaURL">
</portlet:actionURL>

<div class="container">
	<div class="row">
		<div class="col-4">
			<aui:form action="<%=filtrarOfertaURL%>" class="form form-search" method="POST">
				<section class="">
					<div class="justify-content-center">
						<div class="form-wrapper">
							<span class="ads__icon icon-search"></span>
							<div class="control-group">
								<label class="control-label"
									for="">
									Titulo </label> 
									<input class="field form-control yui3-aclist-input"
									id=""
									name=""
									placeholder="Titulo" type="text" value="" maxlength="90"
									aria-invalid="false"  aria-required="true">
								
							</div>
							<label>Area
							</label>
								<select  class="aui-field-select" name="area">
									<option class="" value="">Seleccione una opcion</option>
									<option class="" value="IS">ISO</option>
									<option class="" value="TI">TI</option>
									<option class="" value="SI">SI</option>
									<option class="" value="CO">CO</option>
								</select>
								
							<c:if test="${rol=='profesor' || rol=='empresa'}"> 
							<label>Propietario
							</label>
								<select  class="aui-field-select" name="propietario">
									<option class="" value="">Seleccione una opcion</option>
									<option class="" value="TU">Tutorizadas</option>
									<option class="" value="PR">Creadas por profesores</option>
									<option class="" value="EM">Creadas por empresas</option>
								</select>
							</c:if>		

							<button type="submit" class="buttonTalent buttonTalent--type1">Buscar</button>
						</div>
						
					</div>
				</section>
			</aui:form>
			
			<c:if test="${rol=='profesor' || rol=='empresa'}"> 
			
			<a  class="buttonTalent buttonTalent--type2"  href="${addOffer}" style="display: block;margin-top: 20px;line-height: 2.23077em;" >A�adir oferta</a>			
			</c:if> 
			
			
			<ul style="margin-top: 30px; padding-left: 10px;list-style: none;">
				<li><img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg" style="width: 25px;height: 25px;"><span style="font-size: 0.8em;"> Creado por empresa</span></li>
				<li><img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"><span style="font-size: 0.8em;"> Creado por profesor</span></li>
				<li><img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg" style="width: 25px;height: 25px;"><img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"><span style="font-size: 0.8em;"> Creado por empresa y validada por profesor</span></li>
			
			</ul>
			
		</div>
		<div class="col-8">
			<div class="list">
				<div class="col-xs-12">
					<div class="row">
						<c:choose>
						<c:when test="${rol=='alumno'}">
						
						 <c:forEach var="ofertaprofesor" items="${ofertasProfesor}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertaprofesor.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${oferta.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertaprofesor.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertaprofesor.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertaprofesor.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertaprofesor.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertaprofesor.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"> 
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						 <c:forEach var="ofertatutorizadas" items="${ofertasTutorizadas}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertatutorizadas.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${oferta.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertatutorizadas.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertatutorizadas.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertatutorizadas.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertatutorizadas.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertatutorizadas.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg" style="width: 25px;height: 25px;">
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"> 
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						</c:when>
						<c:when test="${rol=='profesor'}">
						
						
						
						<c:if test="${empty ofertasFiltradas}">
						<c:forEach var="ofertaprofesor" items="${ofertasProfesor}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertaprofesor.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${oferta.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertaprofesor.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertaprofesor.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertaprofesor.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertaprofesor.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertaprofesor.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"> 
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						 <c:forEach var="ofertatutorizadas" items="${ofertasTutorizadas}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertatutorizadas.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${oferta.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertatutorizadas.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertatutorizadas.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertatutorizadas.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertatutorizadas.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertatutorizadas.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg" style="width: 25px;height: 25px;">
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"> 
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						 <c:forEach var="ofertaempresa" items="${ofertasEmpresa}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertaempresa.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${ofertaempresa.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertaempresa.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertaempresa.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertaempresa.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertaempresa.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertaempresa.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/company.jpg" style="width: 25px;height: 25px;">
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						</c:if>
						
						<c:if test="${not empty ofertasFiltradas}">
						<c:forEach var="ofertaempresa" items="${ofertasFiltradas}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertaempresa.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${ofertaempresa.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertaempresa.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertaempresa.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertaempresa.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertaempresa.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertaempresa.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;">
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						</c:if>
						</c:when>
						
						
						
						
						<c:when test="${rol=='empresa'}">
						
						 <c:forEach var="ofertaprofesor" items="${ofertasEmpresa}">
							<a href="${verOfertaURL}&amp;ofertaId=${ofertaprofesor.ofertaId}" class="col-12">
								<input type="hidden" id="inputH" name="inputH" value="${oferta.ofertaId}">
								<div class="list__element">
									<div class="list__element__hover">
										<h2 class="list__element__title">Titulo: ${ofertaprofesor.titulo}</h2>
										<p class="list__element__subtitle" style=" white-space: nowrap;  overflow: hidden; text-overflow: ellipsis; padding-left: 30px; padding-right: 30px;">Descripcion: ${ofertaprofesor.descripcion}</p>
										<p class="list__element__subtitle">Area: ${ofertaprofesor.area} </p>
										<p class="list__element__subtitle">Competencias: ${ofertaprofesor.competencias} </p>
										<p class="list__element__subtitle">Duracion: ${ofertaprofesor.duracion}</p>
										<div>
										<img src="<%= themeDisplay.getPathThemeImages() %>/user.png" style="width: 25px;height: 25px;"> 
										</div>
									</div>
								</div>
							</a> 
						</c:forEach>
						</c:when>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

