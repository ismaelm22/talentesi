<%@ include file="/init.jsp" %>


<portlet:actionURL name="solicitarOferta" var="solicitarOfertaURL">
</portlet:actionURL>

<portlet:actionURL name="tutorizarOferta" var="tutorizarOfertaURL">
</portlet:actionURL>

<div class="row">
		<div class="col-6 offset-3">
			<h2>Oferta:</h2>
			<div class="list__element list__element-profile">
				<div style="text-align: center">
					<img  style="width: 200px;" src="<%= themeDisplay.getPathThemeImages() %>/offerjob.png">
				</div>
				<span class="titleprofile" style="padding-top:20px;">Titulo: </span> 
				<span class="subtitleprofile"> ${oferta.titulo} </span>
				<span class="titleprofile">Descripcion: </span> 
				<span class="subtitleprofile">${oferta.descripcion} </span>
				<span class="titleprofile">Area: </span> 
				<span class="subtitleprofile">${oferta.area} </span> 
				<span class="titleprofile">Competencias: </span> 
				<span class="subtitleprofile">${oferta.competencias} </span> 
				<span class="titleprofile">Duraci�n: </span> 
				<span class="subtitleprofile">${oferta.duracion} </span> 
				<span class="titleprofile">Oferta creada por: </span> 
				<c:if test="${profesor!=null}">
				<span class="subtitleprofile">${profesor.nombre} ${profesor.apellidos} </span> 
				</c:if>
				<c:if test="${empresa!=null}">
				<span class="subtitleprofile">${empresa.nombre} </span> 
				</c:if>
				
				<c:if test="${rol=='alumno'}">
					<a class="buttonTalent buttonTalent--type1"  style="padding: 5px; display:inline-block" href="${solicitarOfertaURL}">Solicitar</a>
				</c:if>
				
			
			</div>
		</div>
	</div>		