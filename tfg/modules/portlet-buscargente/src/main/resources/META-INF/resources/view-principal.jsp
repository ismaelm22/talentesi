<%@ include file="/init.jsp"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>


<portlet:actionURL name="enviarMensaje" var="enviarMensajeURL"></portlet:actionURL>
<portlet:actionURL name="filtrarPersona" var="filtrarPersonaURL"></portlet:actionURL>
<div class="container">

	<aui:form action="<%=filtrarPersonaURL%>" class="form form-search" method="POST">
		<section class="">
			<div class="row justify-content-center">
				<div class="col-md-5 form-wrapper">
					<span class="ads__icon icon-search"></span>
					<div class="control-group">
						<label class="control-label" for=""> Buscador * </label> <input
							class="field form-control yui3-aclist-input" id="buscador"
							name="buscador" placeholder="Nombre" type="text" value=""
							maxlength="90" aria-invalid="false"
							aria-required="true" aria-autocomplete="list"
							aria-owns="yui_patched_v3_11_0_1_1530003359195_861"
							autocomplete="off">
						<div id="yui_patched_v3_11_0_1_1530003359195_850"
							class="yui3-widget yui3-aclist yui3-widget-positioned yui3-aclist-hidden"
							aria-hidden="true" style="width: 217px; left: 8px; top: 51px;">
							<div id="yui_patched_v3_11_0_1_1530003359195_852"
								class="yui3-aclist-content">
								<ul class="yui3-aclist-list"
									id="yui_patched_v3_11_0_1_1530003359195_861" role="listbox">
								</ul>
							</div>
						</div>
						<div class="yui3-aclist-aria" aria-live="polite" role="status">
						</div>
					</div>
					<button type="submit" class="buttonTalent buttonTalent--type1">Buscar</button>
				</div>
			</div>
		</section>
	</aui:form>


	<div class="list">
		<div class="col-xs-12">
			<div class="row">
				<c:choose>
					<c:when test="${rol=='alumno'}">
					
						<c:forEach var="profesor" items="${profesores}"
							varStatus="statusProfesor">
							<div class="col-xs-12 col-sm-4" data-id="${statusProfesor.count-1}" data-object="profesor-${profesor.email}-${profesor.nombre}-${profesor.apellidos}-${profesor.area}-${profesor.despacho}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/user.png">
											<h2 class="list__element__title">${profesor.nombre}
												${profesor.apellidos}</h2>
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
									<input type="hidden" id="inputH" name="inputH" value="${profesor.email}">
									<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						
						<c:forEach var="empresa" items="${empresas}"
							varStatus="statusEmpresa">
							<div class="col-xs-12 col-sm-4" data-id="${statusEmpresa.count-1}"
								data-object="empresa-${empresa.email}-${empresa.nombre}-${empresa.cif}-${empresa.direccion}-${empresa.telefono}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/company.jpg">
											<h2 class="list__element__title">${empresa.nombre}</h2>
											
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
										<input type="hidden" id="inputH" name="inputH" value="${empresa.email}">
										<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						
					</c:when>
					
					<c:when test="${rol=='profesor'}">
					
					
					<c:if test="${empty alumnosfiltradas}">
						<c:forEach var="alumno" items="${alumnos}"
							varStatus="statusAlumno">
							<div class="col-xs-12 col-sm-4" data-id="${statusAlumno.count-1}"
								data-object="alumno-${alumno.email}-${alumno.nombre}-${alumno.apellidos}-${alumno.competencias}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/alumn.png">
											<h2 class="list__element__title">${alumno.nombre}
												${alumno.apellidos}</h2>
											
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
									<input type="hidden" id="inputH" name="inputH" value="${alumno.email}">
									<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						
						<c:forEach var="profesor" items="${profesores}"
							varStatus="statusProfesor">
							<c:if test="${profesor.email!=email}">
							<div class="col-xs-12 col-sm-4" data-id="${statusProfesor.count-1}" data-object="profesor-${profesor.email}-${profesor.nombre}-${profesor.apellidos}-${profesor.area}-${profesor.despacho}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/user.png">
											<h2 class="list__element__title">${profesor.nombre}
												${profesor.apellidos}</h2>
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
									<input type="hidden" id="inputH" name="inputH" value="${profesor.email}">
									<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
							</c:if>
						</c:forEach>
						
						<c:forEach var="empresa" items="${empresas}"
							varStatus="statusEmpresa">
							<div class="col-xs-12 col-sm-4" data-id="${statusEmpresa.count-1}" data-object="empresa-${empresa.email}-${empresa.nombre}-${empresa.cif}-${empresa.direccion}-${empresa.telefono}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/company.jpg">
											<h2 class="list__element__title">${empresa.nombre}</h2>
											
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
										<input type="hidden" id="inputH" name="inputH" value="${empresa.email}">
										<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						</c:if>
						<c:if test="${not empty alumnosfiltradas}">
						<c:forEach var="alumno" items="${alumnosfiltradas}"
							varStatus="statusAlumno">
							<div class="col-xs-12 col-sm-4" data-id="${statusAlumno.count-1}"
								data-object="alumno-${alumno.email}-${alumno.nombre}-${alumno.apellidos}-${alumno.competencias}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/alumn.png">
											<h2 class="list__element__title">${alumno.nombre}
												${alumno.apellidos}</h2>
											
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
									<input type="hidden" id="inputH" name="inputH" value="${alumno.email}">
									<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						</c:if>
						
					</c:when>
					
					<c:when test="${rol=='empresa'}">
					
						<c:forEach var="alumno" items="${alumnos}">
							<div class="col-xs-12 col-sm-4" data-id="${statusAlumno.count-1}" data-object="alumno-${alumno.email}-${alumno.nombre}-${alumno.apellidos}-${alumno.competencias}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/alumn.png">
											<h2 class="list__element__title">${alumno.nombre}
												${alumno.apellidos}</h2>
										
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
									<input type="hidden" id="inputH" name="inputH" value="${alumno.email}">
									<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						
						<c:forEach var="profesor" items="${profesores}"
							varStatus="statusProfesor">
							<div class="col-xs-12 col-sm-4" data-id="${statusProfesor.count-1}" data-object="profesor-${profesor.email}-${profesor.nombre}-${profesor.apellidos}-${profesor.area}-${profesor.despacho}">
								<a class="linkview" href="#">
									<div class="list__element">
										<div class="list__element__hover">
											<img src="<%=themeDisplay.getPathThemeImages()%>/user.png">
											<h2 class="list__element__title">${profesor.nombre}
												${profesor.apellidos}</h2>
											
										</div>
									</div>
								</a>
								<aui:form action="<%=enviarMensajeURL%>" method="POST">
										<input type="hidden" id="inputH" name="inputH" value="${profesor.email}">
										<button type="submit" class="buttonTalent buttonTalent--type2" name="sendMessage" style="margin-top: 5px; margin-bottom: 10px">Enviar Mensaje</button>
								</aui:form>
							</div>
						</c:forEach>
						
					</c:when>
				</c:choose>
				
			</div>
		</div>
	</div>
</div>


<div class="yui3-skin-sam">
	<div id="modal" class="defaultmodal"></div>
</div>



<script type="text/javascript">
	$(document).ready(
			function() {
				var $clickelement = $('.linkview'), variableimprimir;
				$clickelement.click(function(e) {
					var $this = jQuery(this), 
						id = parseInt($this.parent().attr('data-id')), 
						object = $this.parent().attr('data-object'),
						res= object.split("-");							
					e.preventDefault();	
					if (res[0] == "alumno") {
						variableimprimir =  "<div class='list__element list__element-profile'><span class='titleprofile'>Email: </span> <span class='subtitleprofile'> "+res[1]+" </span><span class='titleprofile'>Nombre </span> <span class='subtitleprofile'> "+res[2]+" </span><span class='titleprofile'>Apellidos </span> <span class='subtitleprofile'> "+res[3]+" </span> <span class='titleprofile'>Competencias: </span><span class='subtitleprofile'> "+res[4]+" </span> </div>";						
					} else if (res[0] == "profesor") {
						variableimprimir =  "<div class='list__element list__element-profile'><span class='titleprofile'>Email: </span> <span class='subtitleprofile'> "+res[1]+" </span><span class='titleprofile'>Nombre </span> <span class='subtitleprofile'>"+res[2]+" </span><span class='titleprofile'>Apellidos </span> <span class='subtitleprofile'>"+res[3]+" </span> <span class='titleprofile'>Area: </span><span class='subtitleprofile'>"+res[4]+"</span>  <span class='titleprofile'>Despacho: </span> <span class='subtitleprofile'>"+res[5]+" </span>   </div>";						
					} else if (res[0] == "empresa") {
						variableimprimir =  "<div class='list__element list__element-profile'><span class='titleprofile'>Email: </span> <span class='subtitleprofile'> "+res[1]+" </span><span class='titleprofile'>Nombre </span> <span class='subtitleprofile'>"+res[2]+" </span><span class='titleprofile'>Cif </span> <span class='subtitleprofile'>"+res[3]+" </span> <span class='titleprofile'>Telefono: </span><span class='subtitleprofile'>"+res[4]+" </span>  <span class='titleprofile'>Direccion: </span> <span class='subtitleprofile'>"+res[5]+" </span>   </div>";						
					}
					
					YUI().use('aui-modal', function(Y) {
						var modal = new Y.Modal({
							bodyContent : variableimprimir,
							centered : true,
							destroyOnHide : false,
							headerContent : '<h3>Perfil</h3>',
							modal : true,
							render : '#modal',
							resizable : {
								handles : 'b, r'
							},
							
							visible : false,
							width : 450
						}).render();
						modal.addToolbar([ {
							cssClass : 'closeclass',
							label : 'Close',
							on : {
								click : function() {
									modal.hide();
								}
							}
						},  ]);
						modal.show();
					});
				});
			})
</script>

