<%@ include file="/init.jsp" %>


<div class="row">
		<div class="col-6 offset-3">
			<h2>Mi Perfil:</h2>
			<div class="list__element list__element-profile">
				<a class="btn btn-danger" href="${eliminarURL}"  style="position: absolute;right: 10px;">Eliminar usuario</a>
				<div style="text-align: center">
					<img src="<%= themeDisplay.getPathThemeImages() %>/user.png">
				</div>
				<span class="titleprofile">Email: </span> 
				<span class="subtitleprofile"> ${email} </span>
				<span class="titleprofile">Nombre </span> 
				<span class="subtitleprofile">${nombre} </span>
				<span class="titleprofile">Apellidos </span> 
				<span class="subtitleprofile">${apellidos} </span> 
				<span class="titleprofile">Competencias: </span> 
				<c:forEach items="${competencias}" var="competencia">
				<p class="subtitleprofile">-${competencia} </p>   
				</c:forEach>
				<span class="titleprofile">Curriculum: </span> 
				<a href="#" class="buttonTalent buttonTalent--type2" style="padding:5px"> Descargar Curriculum  </a>
				
				
			</div>
		</div>
	</div>	

Enviar mensaje