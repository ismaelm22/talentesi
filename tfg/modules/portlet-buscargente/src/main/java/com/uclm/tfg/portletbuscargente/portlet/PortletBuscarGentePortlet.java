package com.uclm.tfg.portletbuscargente.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.uclm.tfg.portletbuscargente.constants.PortletBuscarGentePortletKeys;

import portlet.commons.constants.SessionVariablesPortletKeys;
import service.tfg.model.Alumno;
import service.tfg.model.Empresa;
import service.tfg.model.Profesor;
import service.tfg.service.AlumnoLocalServiceUtil;
import service.tfg.service.EmpresaLocalServiceUtil;
import service.tfg.service.ProfesorLocalServiceUtil;

/**
 * @author ismaelmedinarodriguez
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=TFG",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=portlet-buscargente Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.name=" + PortletBuscarGentePortletKeys.PortletBuscarGente,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PortletBuscarGentePortlet extends MVCPortlet {

	final List<Alumno> alumnosfiltradas = new ArrayList<Alumno>();
	final List<Empresa> empresasfiltradas = new ArrayList<Empresa>();
	final List<Profesor> profesoresfiltradas = new ArrayList<Profesor>();

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		String url = PortalUtil.getCurrentURL(renderRequest);
		renderRequest.setAttribute("rol", SessionVariablesPortletKeys.getRol(renderRequest));
		renderRequest.setAttribute("email", SessionVariablesPortletKeys.getEmail(renderRequest));

		if (url.equals("/web/talent-esi/home")) {
			include("/META-INF/resources/view-menu.jsp", renderRequest, renderResponse);
		} else if (url.equals("/web/talent-esi/search-people") || url.contains("?")) {

			if (SessionVariablesPortletKeys.getRol(renderRequest).equals("alumno")) {

				final List<Empresa> empresas = EmpresaLocalServiceUtil.getEmpresas(0, Integer.MAX_VALUE);
				final List<Profesor> profesores = ProfesorLocalServiceUtil.getProfesors(0, Integer.MAX_VALUE);
				if (!empresas.isEmpty()) {
					renderRequest.setAttribute("empresas", empresas);
				}
				if (!profesores.isEmpty()) {
					renderRequest.setAttribute("profesores", profesores);
				}

				renderRequest.setAttribute("empresasfiltradas", empresasfiltradas);
				renderRequest.setAttribute("profesoresfiltradas", profesoresfiltradas);

			} else if (SessionVariablesPortletKeys.getRol(renderRequest).equals("profesor")) {

				final List<Alumno> alumnos = AlumnoLocalServiceUtil.getAlumnos(0, Integer.MAX_VALUE);
				final List<Empresa> empresas = EmpresaLocalServiceUtil.getEmpresas(0, Integer.MAX_VALUE);
				final List<Profesor> profesores = ProfesorLocalServiceUtil.getProfesors(0, Integer.MAX_VALUE);

				if (!empresas.isEmpty()) {
					renderRequest.setAttribute("empresas", empresas);
				}
				if (!profesores.isEmpty()) {
					renderRequest.setAttribute("profesores", profesores);
				}
				if (!alumnos.isEmpty()) {
					renderRequest.setAttribute("alumnos", alumnos);

				}
				renderRequest.setAttribute("alumnosfiltradas", alumnosfiltradas);
				renderRequest.setAttribute("empresasfiltradas", empresasfiltradas);
				renderRequest.setAttribute("profesoresfiltradas", profesoresfiltradas);

			} else if (SessionVariablesPortletKeys.getRol(renderRequest).equals("empresa")) {

				final List<Alumno> alumnos = AlumnoLocalServiceUtil.getAlumnos(0, Integer.MAX_VALUE);
				final List<Profesor> profesores = ProfesorLocalServiceUtil.getProfesors(0, Integer.MAX_VALUE);

				if (!profesores.isEmpty()) {
					renderRequest.setAttribute("profesores", profesores);
				}
				if (!alumnos.isEmpty()) {
					renderRequest.setAttribute("alumnos", alumnos);

				}
				renderRequest.setAttribute("alumnosfiltradas", alumnosfiltradas);
				renderRequest.setAttribute("profesoresfiltradas", profesoresfiltradas);
			}

			include("/META-INF/resources/view-principal.jsp", renderRequest, renderResponse);
		}
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "buscargente")
	public void goBuscarOferta(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {

		String urlhome = "http://localhost:8080/web/talent-esi/search-people";
		((ActionResponse) actionResponse).sendRedirect(urlhome);
	}

	@ProcessAction(name = "enviarMensaje")
	public void goEnviarMensaje(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		String destinatario = ParamUtil.getString(uploadPortletRequest, "inputH");
		SessionVariablesPortletKeys.setDestinatario(actionRequest, destinatario);

		String urlprofile = "http://localhost:8080/web/talent-esi/messages";
		((ActionResponse) actionResponse).sendRedirect(urlprofile);
	}

	@ProcessAction(name = "filtrarPersona")
	public void filtrarPersona(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {

		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		String nombre = ParamUtil.getString(uploadPortletRequest, "buscador");

		final List<Alumno> alumnos = AlumnoLocalServiceUtil.getAlumnos(0, Integer.MAX_VALUE);
		final List<Empresa> empresas = EmpresaLocalServiceUtil.getEmpresas(0, Integer.MAX_VALUE);
		final List<Profesor> profesores = ProfesorLocalServiceUtil.getProfesors(0, Integer.MAX_VALUE);

		for (Alumno alumno : alumnos) {
			if (alumno.getNombre().equals(nombre)) {
				alumnosfiltradas.add(alumno);
			}
		}
		for (Empresa empresa : empresas) {
			if (empresa.getNombre().equals(nombre)) {
				empresasfiltradas.add(empresa);
			}
		}
		for (Profesor profesor : profesores) {
			if (profesor.getNombre().equals(nombre)) {
				profesoresfiltradas.add(profesor);
			}
		}

	}

}