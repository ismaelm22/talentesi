<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
	
	
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<#if (actual_url="/home" || actual_url="/profile" || actual_url="/search-job" || actual_url="/search-people" || actual_url="/politics" || actual_url="/contact" || actual_url="/messages") >
	<header class="header" id="banner" role="banner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
	
					<a href="/web/talent-esi/home"><h1 class="header__title"> TALENT<span>ESI</span></h1></a>
				</div>
				<div class="col-xs-12 col-md-8 header__menu">				
					<button type="button" id="headerLangBtn" class="d-xl-inline-block header__lang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Perfil <span class="icon-arrow-down header__lang__icon" aria-hidden="true"></span></button>
					<div class="dropdown-menu header__lang__dropdown" aria-labelledby="headerLangBtn" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(674px, 18px, 0px);">
						<a class="dropdown-item header__lang__dropdown__item" href="login">
							<span class="header__lang__dropdown__text">Cerrar Sesión</span>
						</a>
					</div>
				</div>
				
			</div>
		</div>
	</header>
<#else>
	<header class="header" id="banner" role="banner">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
	
					<h1 class="header__title">TALENT<span>ESI</span></h1>
				</div>
			</div>
		</div>
	</header>
</#if>

	
	<div id="content" class="container">
		<h1 class="hide-accessible">${the_title}</h1>


		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</div>

<#if actual_url="/home">
	<div class="ads__ad ads__ad--type3 container" style="background-image:url('${images_folder}/dummy/home-ads-01.jpg')">
						
		<h2 class="ads__title">¡Talent Esi, la red social de la ESI!</h2>
						
	</div>
</#if>

<#if (actual_url="/home" || actual_url="/profile" || actual_url="/search-job" || actual_url="/search-people")>
	<footer class="footer-bottom footer" id="footer" role="contentinfo">
		<div class="container">
			<ul class="footer__corp-links-list">
				<li>
					<a href="/web/talent-esi/contact" class="footer__corp-links-list__link">Contacto</a>
				</li>
				<li>
					<span class="footer__corp-links-list__separator">|</span>
					<a href="/web/talent-esi/politics" class="footer__corp-links-list__link">Política de privacidad y condiciones de uso</a>
				</li>
			</ul>
		</div>
	</footer>
</#if>

</body>

<script src="${javascript_folder}/vendor.min.js" type='text/javascript'></script>
<script src="${javascript_folder}/talent-esi.min.js" type='text/javascript'></script>

</html>


